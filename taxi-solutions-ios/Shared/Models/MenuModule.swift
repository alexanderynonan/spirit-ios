//
//  MenuModule.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

struct MenuModule {
    var icon = UIImage()
    var title = ""

    static func getMenuOptions() -> [MenuModule] {
        var menuModules: [MenuModule] = []
        let module1 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_trips"), title: MenuDriverModuleNames.rides)
        let module2 = MenuModule(icon: #imageLiteral(resourceName: "ic_ganancias"), title: MenuDriverModuleNames.earnings)
        //let module4 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_charges"), title: MenuDriverModuleNames.charges)
        let module3 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_share"), title: MenuDriverModuleNames.shareAndWin)
        let module4 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_help"), title: MenuDriverModuleNames.help)
        menuModules = [ module1, module2, module3, module4]
        return menuModules
    }

    static func getClientMenuOptions() -> [MenuModule] {
        var menuModules: [MenuModule] = []
        let module1 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_trips"), title: MenuClientModuleNames.rides)
        let module2 = MenuModule(icon: #imageLiteral(resourceName: "ic_card_2"), title: MenuClientModuleNames.payment)
        //let module3 = MenuModule(icon: #imageLiteral(resourceName: "ic_ticket"), title: MenuClientModuleNames.promotions)
        let module4 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_share"), title: MenuClientModuleNames.shareAndWin)
       // let module5 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_contact"), title: MenuClientModuleNames.trustContacts)
        let module6 = MenuModule(icon: #imageLiteral(resourceName: "ic_menu_help"), title:
            MenuClientModuleNames.help)
        menuModules = [module1, module2, module4, module6]
        return menuModules
    }
}
