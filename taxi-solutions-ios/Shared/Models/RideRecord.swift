//
//  RideRecord.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 7/15/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class RideRecord: Object {
    @objc dynamic var id = 0
    @objc dynamic var rideAtString = ""
    @objc dynamic var totalAmount = ""
    @objc dynamic var status = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var shared = false
    @objc dynamic var originAddress = ""
    @objc dynamic var destinationAddress = ""
    @objc dynamic var userFullName = ""
    @objc dynamic var userPhotoUrl = ""
    @objc dynamic var userRating = ""
    #if USER
    @objc dynamic var carDetails = ""
    #endif
    #if DRIVER
    @objc dynamic var paymentStatus = ""
    #endif
    
    var rideAtDate: Date? {
        self.rideAtString.dateSecond()
    }
    var rideAtFormatted: String {
        self.rideAtDate?.toText() ?? ""
    }
    
    @objc dynamic var paymentDetail: PaymentDetail?
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON, detailed: Bool = false) {
        self.init()
        self.id = data["id"].intValue
        self.rideAtString = data["ride_at"].stringValue
        self.totalAmount = data["total_amount"].stringValue
        self.status = data["ride_status"].stringValue
        self.photoUrl = data["ride_photo"].stringValue
        self.shared = data["is_shared"].boolValue
        self.originAddress = data["origin_address"].stringValue
        self.destinationAddress = data["destination_address"].stringValue
        self.userRating = data["rating"].stringValue
        
        #if DRIVER
        self.userFullName = data["client_full_name"].stringValue
        self.userPhotoUrl = data["client_photo_url"].stringValue
        self.paymentStatus = data["client_payment_status"].stringValue
        #endif
        #if USER
        self.userFullName = data["driver_full_name"].stringValue
        self.userPhotoUrl = data["driver_photo_url"].stringValue
        self.carDetails = data["driver_car"].stringValue
        #endif
        
        if detailed {
            self.paymentDetail = PaymentDetail(data: data, rideId: self.id)
        }
    }
}
