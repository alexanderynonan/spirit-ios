//
//  CreditCard.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 7/29/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class TokenizedCard: Object {
    @objc dynamic var id = 0
    @objc dynamic var brand = ""
    @objc dynamic var isDefault = false
    @objc dynamic var number = ""
    @objc dynamic var expirationMonth = ""
    @objc dynamic var expirationYear = ""
    @objc dynamic var cardholderFirstName = ""
    @objc dynamic var cardholderLastName = ""
    @objc dynamic var token = ""
    @objc dynamic var formattedExpirationDate = ""
    @objc dynamic var cardHolder = ""
    
    var maskedCard: String {
        return number.hiddenCharacters(4).separate(every: 4, with: " ")
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: Any]) {
        self.init()
        if let tokenData = data["token"] as? [String: Any], let cardData = data["card"] as? [String: Any] {
            self.id = Int(truncatingIfNeeded: Date().currentTimeMillis())
            self.brand = cardData["brand"] as? String ?? ""
            self.number = cardData["cardNumber"] as? String ?? ""
            self.expirationMonth = cardData["expirationMonth"] as? String ?? ""
            self.expirationYear = cardData["expirationYear"] as? String ?? ""
            self.cardholderFirstName = cardData["firstName"] as? String ?? ""
            self.cardholderLastName = cardData["lastName"] as? String ?? ""
            self.token = tokenData["tokenId"] as? String ?? ""
        }
    }
    
    convenience init(data: [String: JSON]) {
        self.init()
        self.id = data["id"]?.intValue ?? 0
        self.brand = data["brand"]?.stringValue ?? ""
        self.isDefault = data["is_predetermined"]?.boolValue ?? false
        self.number = data["masked_number"]?.stringValue ?? ""
        self.expirationMonth = data["expiration_month"]?.stringValue ?? ""
        self.expirationYear = data["expiration_year"]?.stringValue ?? ""
        self.cardholderFirstName = data["first_name"]?.stringValue ?? ""
        self.cardholderLastName = data["last_name"]?.stringValue ?? ""
        self.token = data["card_id"]?.stringValue ?? ""
        self.formattedExpirationDate = data["expiration_date"]?.stringValue ?? ""
        self.cardHolder = data["card_holder"]?.stringValue ?? ""
    }
}
