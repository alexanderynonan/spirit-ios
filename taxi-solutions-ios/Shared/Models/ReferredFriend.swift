//
//  ReferredFriend.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/26/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ReferredFriend: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var amount = ""
    @objc dynamic var uses = 0
    @objc dynamic var totalUsage = 0
    @objc dynamic var referredAt = ""
    @objc dynamic var expiresAt = ""
    @objc dynamic var photoUrl = ""
    
    var usage: String {
        return "\(uses)/\(totalUsage) viajes"
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["full_name"].stringValue
        self.amount = data["amount"].stringValue
        self.uses = data["uses"].intValue
        self.totalUsage = data["total_uses"].intValue
        self.referredAt = data["referred_at"].stringValue
        self.expiresAt = data["expiration_at"].stringValue
        self.photoUrl = data["photo_url"].stringValue
    }
}
