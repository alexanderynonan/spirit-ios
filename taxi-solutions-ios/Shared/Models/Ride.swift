//
//  Ride.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 6/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation
import SwiftyJSON

class Ride: Object {
    @objc dynamic var id = ""
    @objc dynamic var rideId = 0
    @objc dynamic var chatId = ""
    @objc dynamic var clientSocketId = ""
    @objc dynamic var driverSocketId = ""
    @objc dynamic var serviceTypeId = ""
    @objc dynamic var estimatedTime = ""
    @objc dynamic var estimatedDistance = 0
    @objc dynamic var originAddress = ""
    @objc dynamic var originLat = 0.0
    @objc dynamic var originLong = 0.0
    @objc dynamic var destinationAddress = ""
    @objc dynamic var destinationLat = 0.0
    @objc dynamic var destinationLong = 0.0
    @objc dynamic var paymentType = ""
    @objc dynamic var estimatedAmount = ""
    @objc dynamic var maxTimeToAcceptTrip = 0.0
    @objc dynamic var maxTimeToWaitForClient = 0
    @objc dynamic var paymentStatus = ""
    @objc dynamic var requestType = ""
    
    @objc dynamic var requested = false
    @objc dynamic var nearToOrigin = false
    @objc dynamic var nearToDestination = false
    @objc dynamic var shown = false
    
    @objc dynamic var stateString = ""
    @objc dynamic var state = -1
    @objc dynamic var startedToWaitAt: Date = Date()
    
    #if DRIVER
    @objc dynamic var clientOnRide: ClientOnRide?
    @objc dynamic var requestedAt: Date = Date()
    var isValidRequest: Bool {
        let elapsedSeconds = Calendar.current.dateComponents([.second], from: requestedAt, to: Date()).second ?? 0
        print("⏱ ELAPSED TIME: \(elapsedSeconds)")
        return maxTimeToAcceptTrip >= Double(elapsedSeconds)
    }
    var timeWaiting: Int {
        return Calendar.current.dateComponents([.second], from: startedToWaitAt, to: Date()).second ?? 0
    }
    #endif
    
    #if USER
    @objc dynamic var driverOnRide: DriverOnRide?
    @objc dynamic var requestedAt: Date = Date()
    
    /// Valida que no hayan transcurrido más de 5 minutos desde que consulto el servicio de getTripRoute. Si pasaron más de 5 minutos, el valor sera false.
    var isValidRequest: Bool {
        let minutes = Calendar.current.dateComponents([.minute], from: requestedAt, to: Date()).minute ?? 0
        print("⏱ Minutos \(minutes)")
        return minutes < 5
    }
    #endif
    
    @objc dynamic var paymentDetail: PaymentDetail?
    
    var originCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: originLat, longitude: originLong)
    }
    
    var destinationCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
    }
    
    /// Estado del viaje cuando ya comenzó. Posibles valores: onWay, waiting, onRide, finished
    var rideState: RideState? {
        RideState.allCases.first(where: { $0.rawValue == state }) ?? .none
    }
    
    var paymentTypeTitle: String {
        self.paymentType == "cash" ? "Efectivo" : "Tarjeta"
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: Any]) {
        self.init()
        self.id = UUID().uuidString
        self.rideId = (data["ride_id"] as? Int ?? (data["ride_id"] as? String)?.int) ?? 0
        self.chatId = data["chat_id"] as? String ?? ""
        self.clientSocketId = data["client_socket_id"] as? String ?? ""
        self.driverSocketId = data["driver_socket_id"] as? String ?? ""
        self.serviceTypeId = data["service_type_id"] as? String ?? ""
        self.estimatedTime = data["estimated_time"] as? String ?? ""
        
        self.originAddress = data["origin_address"] as? String ?? ""
        self.originLat = (data["origin_lat"] as? String ?? "").double
        self.originLong = (data["origin_long"] as? String ?? "").double
        
        self.destinationAddress = data["destination_address"] as? String ?? ""
        self.destinationLat = data["destination_lat"] as? Double ?? ((data["destination_lat"] as? String)?.double ?? 0.0)
        self.destinationLong = data["destination_long"] as? Double ?? ((data["destination_long"] as? String)?.double ?? 0.0)
        
        self.paymentType = data["payment_type"] as? String ?? ""
        
        self.estimatedAmount = (data["total_amount"] as? String ?? "") != "" ? data["total_amount"] as? String ?? "" : "\(data["total_amount"] as? Int ?? 0)"
        self.maxTimeToAcceptTrip = (data["max_waiting_time_to_accept_ride"] as? String ?? "").double
        
        #if DRIVER
        self.requestedAt = (data["started_at"] as? String)?.date() ?? Date()
        #endif
        
        self.stateString = data["status"] as? String ?? ""
        self.state = RideState.getStatusFromString(stateString)?.rawValue ?? -1
        self.paymentStatus = data["payment_status"] as? String ?? ""
        self.requestType = data["type"] as? String ?? ""
        
        #if DRIVER
        self.clientOnRide = ClientOnRide(data: data)
        #endif
    }
    
    convenience init(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        self.init()
        self.originLat = origin.latitude
        self.originLong = origin.longitude
        self.destinationLat = destination.latitude
        self.destinationLong = destination.longitude
    }
    
    func setOriginCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let realm = try! Realm()
        try! realm.write {
            self.originLat = coordinate.latitude
            self.originLong = coordinate.longitude
        }
    }
    
    func setDestinationCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let realm = try! Realm()
        try! realm.write {
            self.destinationLat = coordinate.latitude
            self.destinationLong = coordinate.longitude
        }
    }
}
