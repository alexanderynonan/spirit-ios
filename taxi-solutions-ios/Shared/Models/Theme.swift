//
//  Theme.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Theme: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    
    var questions = List<Question>()
    
    var questionsArray: [Question] {
        return Array(questions)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: JSON]) {
        self.init()
        self.id = data["id"]?.intValue ?? 0
        self.name = data["name"]?.stringValue ?? ""
        
        guard let questionsData = data["faqs"]?.arrayValue else { return }
        questionsData.forEach {
            let question = Question(data: $0.dictionaryValue)
            questions.append(question)
        }
    }
}
