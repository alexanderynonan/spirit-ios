//
//  Message.swift
//  TaxiSvico-User
//
//  Created by Fernanda Alvarado Tarrillo on 6/30/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import FirebaseDatabase

class Message : Object {
    
    @objc dynamic var id = ""
    @objc dynamic var message = ""
    @objc dynamic var timestamp = Int64()
    @objc dynamic var type = 0
    
    convenience init(data: DataSnapshot) {
        self.init()
        self.id = data.key
        guard let dictionary = data.value as? [String: Any] else { return }
        self.message = dictionary["message"] as? String ?? ""
        self.timestamp = dictionary["timestamp"] as? Int64 ?? Int64()
        self.type = dictionary["type"] as? Int ?? 0
    }
}
