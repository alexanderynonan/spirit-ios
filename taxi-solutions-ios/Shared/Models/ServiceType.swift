//
//  ServiceType.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 6/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ServiceType: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var information = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var descriptionInfo = ""
    @objc dynamic var estimatedArrivalTimeInMinutes = 0
    @objc dynamic var estimatedArrivalTime = ""
    
    class override func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(_ data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["name"].stringValue
        self.information = data["extra_information"].stringValue
        self.photoUrl = data["photo_url"].stringValue
        self.amount = data["amount"].doubleValue
        self.descriptionInfo = data["description"].stringValue
        self.estimatedArrivalTimeInMinutes = data["estimated_driver_arrival_time"].intValue
        self.estimatedArrivalTime = data["estimated_arrival_time"].stringValue
    }
}
