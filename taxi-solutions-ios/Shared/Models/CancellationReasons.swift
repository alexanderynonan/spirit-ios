//
//  CancellationReasons.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class CancellationReason: Object {
    @objc dynamic var key = ""
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.key = data["key"].stringValue
        self.name = data["name"].stringValue
    }
}
