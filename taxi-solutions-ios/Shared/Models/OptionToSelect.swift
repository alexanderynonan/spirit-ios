//
//  Option.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class OptionToSelect: Object {
    @objc dynamic var key = ""
    @objc dynamic var name = ""
    @objc dynamic var type = ""
     
    convenience init(key: String, name: String, type: String) {
        self.init()
        self.key = key
        self.name = name
        self.type = type
    }
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(data: JSON, forType type: String) {
        self.init()
        self.key = data["key"].stringValue
        self.name = data["name"].stringValue
        self.type = type
    }
    
    class func rideRequestDenialReasons() -> [OptionToSelect] {
        let option1 = OptionToSelect()
        let option2 = OptionToSelect()
        let option3 = OptionToSelect()
        let option4 = OptionToSelect()
        option1.key = "Zona peligrosa"
        option1.name = option1.key
        option2.key = "Tráfico"
        option2.name = option2.key
        option3.key = "Problemas Técnicos"
        option3.name = option3.key
        option4.key = "Otros"
        option4.name = option4.key
        return [option1, option2, option3, option4]
    }
    
    class func timeFrames() -> [OptionToSelect] {
        var optionsToSelect: [OptionToSelect] = []
        RangeType.allCases.forEach {
            let optionToSelect = OptionToSelect(key: $0.description, name: $0.rawValue, type: "time_frames")
            optionsToSelect.append(optionToSelect)
        }
        return optionsToSelect
    }
    
    //FOR ON WAY OR WAITING STATE
    class func dynamicRideCancelReasons() -> [OptionToSelect] {
        let realm = try! Realm()
        var options = Array(realm.objects(OptionToSelect.self).filter("type == 'cancel_ride_reasons' && key != 'others'"))
        let others = OptionToSelect(key: "others", name: "Otros", type: "cancel_ride_reasons")
        try! realm.write {
            realm.add(others, update: .all)
        }
        options.append(others)
        return options
    }
    
    //FOR DRIVER ON RIDE STATE
    class func driverOnRideCancelReasons() -> [OptionToSelect] {
        let option1 = OptionToSelect()
        let option2 = OptionToSelect()
        let option3 = OptionToSelect()
        option1.key = "Fallas mecánicas o averías"
        option1.name = option1.key
        option2.key = "Accidente de tránsito"
        option2.name = option2.key
        option3.key = "Comentario"
        option3.name = option3.key
        return [option1, option2, option3]
    }
    
    //FOR CLIENT ON WAY OR WAITING STATE
    class func clientWaitingCancelReasons() -> [OptionToSelect] {
        let option1 = OptionToSelect()
        let option2 = OptionToSelect()
        let option3 = OptionToSelect()
        let option4 = OptionToSelect()
        option1.key = "Conductor no responde"
        option1.name = option1.key
        option2.key = "Conductor no se mueve"
        option2.name = option2.key
        option3.key = "Otros"
        option3.name = option3.key
        option4.key = "Comentario"
        option4.name = option4.key
        return [option1, option2, option3, option4]
    }
    
    //FOR DRIVER ON RIDE STATE
    class func clientOnRideCancelReasons() -> [OptionToSelect] {
        let option1 = OptionToSelect()
        let option2 = OptionToSelect()
        let option3 = OptionToSelect()
        option1.key = "Cambio de planes"
        option1.name = option1.key
        option2.key = "Otros"
        option2.name = option2.key
        option3.key = "Comentario"
        option3.name = option3.key
        return [option1, option2, option3]
    }
    
    //FOR DRIVER ON CHARGES
    
    class func driverRechargeType() -> [OptionToSelect] {
        let option1 = OptionToSelect()
        let option2 = OptionToSelect()
        option1.key = "Depósito"
        option1.name = option1.key
        option2.key = "Tarjeta"
        option2.name = option2.key
        return [option1, option2]
    }
    
}
