//
//  PaymentInfo.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class PaymentDetail: Object {
    @objc dynamic var id = 0
    @objc dynamic var totalAmount = ""
    @objc dynamic var baseAmount = ""
    @objc dynamic var discount = ""
    @objc dynamic var discountCode = ""
    @objc dynamic var commission = ""
    @objc dynamic var pendingAmount = ""
    @objc dynamic var exceedTimeAmount = ""
    @objc dynamic var paymentType = ""
    
    var paymentMethod: PaymentMethod? {
        switch paymentType {
        case "Tarjeta":
            return .creditCard
        case "Efectivo":
            return .cash
        default:
            return .none
        }
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON, rideId: Int) {
        self.init()
        self.id = rideId
        self.totalAmount = data["total"].stringValue
        self.baseAmount = data["base_amount"].stringValue
        self.commission = data["commission"].stringValue
        self.discount = data["discount"].stringValue
        self.discountCode = data["discount_code"].stringValue
        self.pendingAmount = data["pending_amount"].stringValue
        self.exceedTimeAmount = data["exceed_time_amount"].stringValue
        self.paymentType = data["payment_type"].stringValue
    }
    
    convenience init(dictionary: [String: Any], rideId: Int) {
        self.init()
        self.id = rideId
        self.totalAmount = dictionary["total"] as? String ?? ""
        self.baseAmount = dictionary["base_amount"] as? String ?? ""
        self.commission = dictionary["commission"] as? String ?? ""
        self.discount = dictionary["discount"] as? String ?? ""
        self.discountCode = dictionary["discount_code"] as? String ?? ""
        self.pendingAmount = dictionary["pending_amount"] as? String ?? ""
        self.exceedTimeAmount = dictionary["exceed_time_amount"] as? String ?? ""
        self.paymentType = dictionary["payment_type"] as? String ?? ""
    }
}
