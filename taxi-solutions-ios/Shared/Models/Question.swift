//
//  Question.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Question: Object {
    @objc dynamic var id = 0
    @objc dynamic var themeId = 0
    @objc dynamic var question = ""
    @objc dynamic var answer = ""
    
    convenience init(data: [String: JSON]) {
        self.init()
        self.id = data["id"]?.intValue ?? 0
        self.themeId = data["theme_id"]?.intValue ?? 0
        self.question = data["question"]?.stringValue ?? ""
        self.answer = data["answer"]?.stringValue ?? ""
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
