//
//  DriverOnRide.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/3/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class DriverOnRide: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var serviceType = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var phone = ""
    @objc dynamic var rating = ""
    @objc dynamic var carPlateNumber = ""
    @objc dynamic var carColor = ""
    @objc dynamic var carBrand = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: Any]) {
        self.init()
        self.id = data["driver_id"] as? Int ?? 0
        self.name = data["driver_full_name"] as? String ?? ""
        self.serviceType = data["driver_service_name"] as? String ?? ""
        self.photoUrl = data["driver_photo_url"] as? String ?? ""
        self.phone = data["driver_phone"] as? String ?? ""
        self.rating = data.double(key: "driver_rating")?.string ?? ""
        self.carPlateNumber = data["car_plate_number"] as? String ?? ""
        self.carColor = data["car_color"] as? String ?? ""
        self.carBrand = data["car_brand"] as? String ?? ""
    }
    
    //Parseo del servicio reconnection
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["full_name"].stringValue
        self.serviceType = data["service_type_name"].stringValue
        self.photoUrl = data["photo_url"].stringValue
        self.phone = data["phone"].stringValue
        self.rating = data["rating"].stringValue
        let cardPlateNumberAndColor = data["car_plate_number_color"].stringValue
        self.carPlateNumber = cardPlateNumberAndColor.components(separatedBy: ",").first ?? ""
        self.carColor = cardPlateNumberAndColor.components(separatedBy: ",").last ?? ""
        self.carBrand = data["car_brand"].stringValue
    }
}
