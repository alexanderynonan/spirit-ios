//
//  Client.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ClientOnRide: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var rating = ""
    @objc dynamic var phone = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: Any]) {
        self.init()
        self.id = data["client_id"] as? Int ?? 0
        self.name = data["client_name"] as? String ?? ""
        self.photoUrl = data["client_photo"] as? String ?? ""
        self.rating = data["client_rating"] as? String ?? ""
        self.phone = data["client_phone"] as? String ?? ""
    }
    
    //Parseo del servicio reconnection
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["full_name"].stringValue
        self.photoUrl = data["photo_url"].stringValue
        self.rating = data["rating"].stringValue
        self.phone = data["phone"].stringValue
    }
}
