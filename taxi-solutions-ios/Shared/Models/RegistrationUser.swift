//
//  User.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift

enum PersonalData: Int, CaseIterable {
    case phone = 0
    case email = 1
    case bank = 2
    case accountNumber = 3
    case password = 4
}

class RegistrationUser: Object {
    @objc dynamic var id = 0
    @objc dynamic var base64Photo = ""
    @objc dynamic var phoneNumber = ""
    @objc dynamic var documentType = ""
    @objc dynamic var documentNumber = ""
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var email = ""
    @objc dynamic var documentPhoto = ""
    @objc dynamic var bank = ""
    @objc dynamic var accountNumber = ""
    @objc dynamic var password = ""
    @objc dynamic var invitationNumber = ""
    @objc dynamic var licenseNumber = ""
    @objc dynamic var category = ""
    @objc dynamic var licensePlate = ""
    @objc dynamic var TUCExpiringDate = ""
    @objc dynamic var invitationCode = ""
    @objc dynamic var deviceToken = "38o146246124akjsl"
    @objc dynamic var birthDate = ""
    @objc dynamic var gender = ""
    @objc dynamic var platePhoto = ""
    @objc dynamic var soatPhoto = ""
    @objc dynamic var requiresTechnicalReview = false
    @objc dynamic var technicalReviewPhoto = ""
    
}
