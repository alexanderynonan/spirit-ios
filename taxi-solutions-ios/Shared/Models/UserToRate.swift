//
//  UserToRate.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/9/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserToRate {
    var rideId = 0
    var userId = 0
    var clientName = ""
    var rating = ""
    var photoUrl = ""
    
    #if USER
    convenience init(data: JSON) {
        self.init()
        self.rideId = data["ride_id"].intValue
        self.userId = data["driver_id"].intValue
        self.clientName = data["driver_name"].stringValue
        self.rating = data["rating"].stringValue
        self.photoUrl = data["photo_url"].stringValue
    }
    #endif
    
    #if DRIVER
    convenience init(data: JSON) {
        self.init()
        self.rideId = data["ride_id"].intValue
        self.userId = data["client_id"].intValue
        self.clientName = data["client_name"].stringValue
        self.rating = data["rating"].stringValue
        self.photoUrl = data["photo_url"].stringValue
    }
    #endif
}
