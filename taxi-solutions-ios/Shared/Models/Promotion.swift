//
//  Promotion.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 14/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Promotion: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var code = ""
    @objc dynamic var amount = ""
    @objc dynamic var uses = ""
    @objc dynamic var totalUsages = ""
    @objc dynamic var expiresAt = ""
    @objc dynamic var photoUrl = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["full_name"].stringValue
        self.code = data["code"].stringValue
        self.amount = data["amount"].stringValue
        self.uses = data["uses"].stringValue
        self.totalUsages = data["total_uses"].stringValue
        self.expiresAt = data["expiration_at"].stringValue
        self.photoUrl = data["photo_url"].stringValue
    }
}
