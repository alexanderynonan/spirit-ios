//
//  DocumentType.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class DocumentType: Object {
    @objc dynamic var key = ""
    @objc dynamic var name = ""
    @objc dynamic var maxValue = 0
    @objc dynamic var isNumeric = false
    @objc dynamic var requiresPhoto = false
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.key = data["key"].stringValue
        self.name = data["name"].stringValue
        self.maxValue = data["max"].intValue
        self.isNumeric = data["is_numeric"].boolValue
        self.requiresPhoto = data["requires_photo"].boolValue
    }
}
