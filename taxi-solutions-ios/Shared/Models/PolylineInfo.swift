//
//  MapInfo.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 22/09/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import CoreLocation

struct PolylineInfo {
    var polyline: String
    var origin: CLLocationCoordinate2D? = nil
    var destination: CLLocationCoordinate2D? = nil
    var duration: String? = nil
    var driverMoving: Bool = false
    var rotation: Double? = nil
    var animated: Bool = true
}
