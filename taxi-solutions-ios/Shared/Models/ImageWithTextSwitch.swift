//
//  FavoritePlace.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/14/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ImageWithTextSwitch {
    
    var name = ""
    var imageOn = UIImage()
    var imageOff = UIImage()
    var colorOn = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    var colorOff = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    var state = false
    
    init(name: String? = nil, imageOn : UIImage? = nil, imageOff : UIImage? = nil,colorOn : UIColor? = nil,colorOff : UIColor? = nil) {
        self.name = name ?? ""
        self.imageOn = imageOn ?? UIImage()
        self.imageOff = imageOff ?? UIImage()
        self.colorOn = colorOn ?? .white
        self.colorOff = colorOff ?? .white
    }
    
    static func getFavoritePlaces() -> [ImageWithTextSwitch] {
        let home = ImageWithTextSwitch(name: "Casa", imageOn: #imageLiteral(resourceName: "ic_favorite_house_on"), imageOff: #imageLiteral(resourceName: "ic_favorite_house_off"), colorOn: Colors.primary, colorOff: Colors.background)
        let office = ImageWithTextSwitch(name: "Trabajo", imageOn: #imageLiteral(resourceName: "ic_favorite_office_on"), imageOff: #imageLiteral(resourceName: "ic_favorite_office_off"), colorOn: Colors.primary, colorOff: Colors.background)
        return [home, office]
    }
}
