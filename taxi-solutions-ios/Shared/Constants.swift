//
//  Constants.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

enum App {
    #if DRIVER
    static let name = "Taxi Spirit"
    #endif
    #if USER
    static let name = "Taxi Spirit"
    #endif
    
    static let logoUrl = UserDefaults.standard.object(forKey: Constants.logoUrlKey) as? String ?? ""
    static let contactEmail = UserDefaults.standard.object(forKey: Constants.contactEmailKey) as? String ?? ""
    static let contactPhone = UserDefaults.standard.object(forKey: Constants.contactPhoneKey) as? String ?? ""
    static let contactWeb = UserDefaults.standard.object(forKey: Constants.contactWebKey) as? String ?? ""
    static let sosPhone = UserDefaults.standard.object(forKey: Constants.sosPhoneKey) as? String ?? ""
    static let terms = UserDefaults.standard.object(forKey: Constants.termsKey) as? String ?? ""
    static let fcmToken = UserDefaults.standard.object(forKey: Constants.fcmTokenKey) as? String ?? ""
    static let distanceFilter = UserDefaults.standard.object(forKey: Constants.distanceFilterKey) as? Double ?? 2.0
    static let maxFileSize = 15
    
    static let rideRequestTitle = "Nueva solicitud"
    static let aloSvicoRequestTitle = "Aló Svico"
}

enum Constants {
    static let logoUrlKey = "logoUrl"
    static let contactEmailKey = "contactEmail"
    static let contactPhoneKey = "contactPhone"
    static let contactWebKey = "contactWeb"
    static let sosPhoneKey = "sosPhone"
    static let termsKey = "terms"
    static let fcmTokenKey = "fcmToken"
    static let distanceFilterKey = "distanceFilter"
}

enum Api {
    //static let baseUrl = "https://taxi-svico.pappstest.com/api/v1/"
   // static let socketUrl = "https://pappstest.com:4060/"
   // static let baseUrl = "https://api.taxisvico.com/api/v1/"
   //static let socketUrl = "https://socket.taxisvico.com:4060/"
    static let baseUrl = "https://dev-sass-taxi-api.pappstest.com/api/v1/"
    static let socketUrl = "https://dev-sass-taxi-sockets.pappstest.com:4060/"


    static let resetPassword = "reset-password/"
}

enum UserRole {
    static let driver = "driver"
    static let client = "client"
}

enum ValidationMessages {
    static let email = "El correo electrónico es obligatorio"
    static let emailFormat = "Ingresa un correo válido"
    static let phone = "El número de celular es obligatorio"
    static let phoneLength = "El número de celular debe tener 9 dígitos"
    static let currentPassword = "La contraseña actual está vacía"
    static let password = "La contraseña está vacía"
    static let passwordLength = "La contraseña debe tener 6 caracteres como mínimo"
    static let confirmPassword = "La confirmación de la contraseña está vacía"
    static let passwordsDifferent = "Las contraseñas no coinciden"
    static let invitationCode = "Ingresa un código de invitación válido o deja el campo en blanco"
    static let documentNumberEmpty = "El número de documento es obligatorio"
    static let documentNumberLength = "El número de documento es inválido"
    static let namesEmpty = "Ingrese sus nombres y apellidos"
    static let documentPhotoEmpty = "Debe subir una foto de su documento de identidad para su respectiva validación"
    static let reniecNames = "Los datos ingresados no corresponden al número de documento, ingrese correctamente sus datos"
    static let agreeTerms = "Debes aceptar los términos y condiciones"
    static let bankName = "Ingrese un nombre de banco válido o deje el campo en blanco"
    static let accountNumberLength = "Ingrese un número de cuenta válido (con al menos 10 caracteres) o deje el campo en blanco"
    static let licenseNumber = "El número de licencia es obligatorio"
    static let licenseNumberLength = "El número de licencia debe tener 9 caracteres como mínimo"
    static let drivingLicenseCategory = "Selecciona una categoría"
    static let licensePlate = "La placa del vehículo es obligatoria"
    static let licensePlateLength = "La placa del vehículo debe tener 6 caracteres como mínimo"
    static let tucExpiringDate = "La fecha de vencimiento TUC es obligatoria"
    
    static let voucherImage = "Para poder validar el deposito debe subir la foto de su voucher"
    static let amountMoney = "Por favor ingrese un monto válido mayor a 0"
    
    static let comment = "Por favor ingrese un comentario válido"
    static let soatPhotoEmpty = "Debe subir una foto de su SOAT para su respectiva validación"
    static let platePhotoEmpty = "Debe subir una foto de su placa de vehículo para su respectiva validación"
    static let technicalReviewPhotoEmpty = "Debe subir una foto de la revisión técnica para su respectiva validación"
}

enum DefaultApiMessages {
    static let formatResponse = "Ocurrió un error al formatear la respuesta del servicio"
}

enum Default {
    static let selectedDocumentType = "dni"
    static let documentTypeToCheck = "dni"
}

enum MenuDriverModuleNames {
    static let myProfile = "Mi perfil"
    static let objectives = "Mis objetivos"
    static let rides = "Mis viajes"
    static let earnings = "Mis ganancias"
    static let charges = "Recargas"
    static let invitations = "Invita amigos"
    static let connections = "Mis conexiones"
    static let shareAndWin = "Comparte y gana"
    static let help = "Ayuda"
    static let logout = "CERRAR SESIÓN"
    static let terms = "Términos y condiciones"
}

enum MenuClientModuleNames {
    static let myProfile = "Mi perfil"
    static let rides = "Mis viajes"
    static let payment = "Métodos de pago"
    static let promotions = "Promociones"
    static let shareAndWin = "Comparte y gana"
    static let trustContacts = "Contactos de confianza"
    static let help = "Ayuda"
    static let logout = "CERRAR SESIÓN"
    static let terms = "Términos y condiciones"
}

enum MenuBackendModuleKeys {
    static let myProfile = "profile"
    static let objectives = "objectives"
    static let payment = "payment"
    static let promotions = "promotions"
    static let shareAndWin = "share_and_win"
    static let trustContacts = "trust_contacts"
    static let rides = "rides"
    static let earnings = "earnings"
    static let charges = "recharges"
    static let invitations = "referred_users"
    static let connections = "connections"
    static let help = "help"
}

enum GoogleApiKeys {
    static let maps = "AIzaSyDdSetmoDrwADfBKqQVQODpRebu9rW6hfs"
    static let places = GoogleApiKeys.maps
}

enum AppFontNames {
    static let base = "Poppins"
    static let thin = base + "-Thin"
    static let regular = base + "-Regular"
    static let medium = base + "-Medium"
    static let semibold = base + "-Semibold"
    static let bold = base + "-Bold"
    static let extrabold = base + "-ExtraBold"
    static let black = base + "-Black"
}

enum OptionType {
    case doctype
    case bank
    case country
}

enum StoryboardNames {
    static let shared = "Common"
}

enum FavoriteAddressNames {
    static let house = "Casa"
    static let work = "Trabajo"
}

enum InteractionVia {
    case call
    case message
    case sos
}

enum SocketEventsEmit {
    static let clientLocation = "clientLocation"
    static let driverLocation = "driverLocation"
    static let userConnected = "userConnected"
    static let arrivingToOriginPoint = "arrivingToOriginPoint"
    static let driverNearToClient = "notifyDriverNearToClient"
    static let driverOnTrip = "driverOnTrip"
    static let updateDriverSocketId = "updateDriverSocketIdInRide"
    static let updateClientSocketId = "updateClientSocketIdInRide"
}

enum SocketEventsOn {
    static let connect = "connect"
    static let userConnectResponse = "userConnectResponse"
    static let correctAuthentication = "correctAuthentication"
    static let failedAuth = "failedAuth"
    static let rideRequest = "newClientRequest"
    static let driverLocationResponse = "driverLocationResponse"
    static let cancelledClientRide = "notifyCancelledClientRide"
    static let notFoundClientRide = "notifyNotFoundClientRide"
    static let driverArrivingToOriginPoint = "driverArrivingToOriginPoint"
    static let nearToOriginPoint = "nearToOriginPoint"
    static let clientDriverAtOriginPoint = "notifyClientDriverAtOriginPoint"
    static let rideCancelledByDriverForClientReasons = "rideCancelledByDriverForClientReasons"
    static let clientOnTrip = "clientOnTrip"
    static let clientStartingRide = "notifyClientStartingRide"
    static let nearToDestinationPoint = "nearToDestinationPoint"
    static let clientEndRideDetail = "notifyClientEndRideDetail"
    static let clientEndRidePaymentDetail = "notifyPaymentGatewayResponse"
    static let driverCancelRideInProgress = "notifyDriverCancelRideInProgress"
    static let clientCancelRideInProgress = "notifyClientCancelRideInProgress"
    static let rideCancelledByClientBeforeStart = "rideCancelledByClientBeforeStart"
    static let routeChanged = "notifyClientChangeRoute"
    static let clientHasNewSocketId = "notifyClientHasNewSocketId"
    static let closeDrivers = "responseDriversClosestToClient"
}

enum NiubizInfo {
    static let commerceCode = "456879852"
    static let user = "integraciones@niubiz.com.pe"
    static let password = "_7z3@8fF"
}
