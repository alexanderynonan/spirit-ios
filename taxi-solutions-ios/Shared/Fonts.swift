//
//  AppFont.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit
    
class Fonts {
    
    static func thin(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.thin, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func regular(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.regular, size: size) ?? UIFont.systemFont(ofSize: size)
    }

    static func medium(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.medium, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func semibold(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.semibold, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func bold(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.bold, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func black(_ size: CGFloat = 17.0) -> UIFont {
        return UIFont(name: AppFontNames.black, size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
