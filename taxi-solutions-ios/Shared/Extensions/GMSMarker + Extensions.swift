//
//  GMSMarker + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/22/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}
