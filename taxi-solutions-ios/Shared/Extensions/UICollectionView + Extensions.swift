//
//  UITableView + Extensions.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
    }
    
    func animate(completion: (() -> Void)? = nil) {
        self.reloadData {
            let width = self.frame.width
            let cells = self.visibleCells
            
            cells.forEach {
                $0.transform = CGAffineTransform(translationX: width, y: 0)
                $0.alpha = 0
            }
            
            var delayCounter = 0
            cells.forEach { cell in
                UIView.animate(withDuration: 1.25, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    cell.transform = .identity
                    cell.alpha = 1
                }) { _ in
                    completion?()
                }
                delayCounter += 1
            }
        }
    }
}
