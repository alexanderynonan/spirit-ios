//
//  UIDevice + Helpers.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import AVFoundation

extension UIDevice {
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}
