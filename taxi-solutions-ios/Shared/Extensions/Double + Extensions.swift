//
//  Double + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/15/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    var string: String {
        return String(format: "%.2f", self)
    }
    var oneDecimalString: String {
        return String(format: "%.1f", self)
    }
}
