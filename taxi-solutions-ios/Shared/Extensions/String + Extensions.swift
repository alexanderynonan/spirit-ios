//
//  String + Extensions.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension StringProtocol {
    func nsRange(from range: Range<Index>) -> NSRange {
        return .init(range, in: self)
    }
}

extension String {
    var data: Data {
        return self.data(using: .utf8) ?? Data()
    }
    
    var int: Int? {
        return Int(self)
    }
    
    var url: URL? {
        let urlString = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        return URL(string: urlString ?? "")
    }

//    func url() -> URL? {
//        let urlString = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        return URL(string: urlString ?? "")
//    }
    
    var double: Double {
        return Double(self) ?? 0.0
    }
    
    var firstWordCapitalized: String {
        return prefix(1).capitalized + dropFirst()
    }
    
    var capitalizedName: String {
        return (self.components(separatedBy: " ").map { $0.lowercased().firstWordCapitalized }).joined(separator: " ")
    }
    
    var imageData: Data {
        return Data(base64Encoded: self) ?? Data()
    }
    
    var imageFromBase64: UIImage {
        return UIImage(data: imageData) ?? UIImage()
    }
    
    var dictionary: [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    var arrayDictionary: [[String: Any]]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    var timedOut: String {
        return self.contains("The request timed out") ? "Revise su conexión de red y vuelva a intentarlo" : self
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }

    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue, ], documentAttributes: nil)
        } catch {
            return nil
        }
    }

    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }


    var firstWord: String {
        return self.components(separatedBy: " ").first ?? ""
    }
    
    var isNumeric: Bool {
        return Double(self) != nil
    }
    
    func withDefault(_ string: String) -> String {
        return self == "" ? string : self
    }
    
    func hiddenCharacters(_ numberOfShownCharacters: Int) -> String {
        return String(repeating: "•", count: Swift.max(0, count-numberOfShownCharacters)) + suffix(numberOfShownCharacters)
    }
    
    func masked() -> String {
        return prefix(4) + String(repeating: "•", count: Swift.max(0, count-8)) + suffix(4)
    }
    
    func separate(every stride: Int = 4, with separator: Character = " ") -> String {
        return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
    }
    
    func isEmptyOrWhitespace() -> Bool {
        if self.isEmpty || self.trimmingCharacters(in: CharacterSet.whitespaces) == "" || self != self.trimmingCharacters(in: CharacterSet.whitespaces) {
            return true
        } else {
            return false
        }
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func containsWord(_ word: String) -> Bool {
        let components = self.components(separatedBy: " ")
        return components.contains(word) || self == word
    }
    
    func containsFirstWordOrAll(_ text: String) -> Bool {
        if text.components(separatedBy: " ").count == 1 {
            let components = self.components(separatedBy: " ")
            return components.first == text
        } else {
            return self == text
        }
    }
    
    func date(fromFormat format: String = "yyyy-MM-dd HH:mm:ss") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "es_PE")
        return dateFormatter.date(from: self)
    }

    func dateSecond(fromFormat format: String = "dd/MM/yy hh:mm a") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier:"GMT")
        return dateFormatter.date(from: self)
    }

    func toDate(fromFormat format: String = "dd/MM/yyyy hh:mm") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "es_PE")
        return dateFormatter.date(from: self)
    }

    func toDateSecond(fromFormat format: String = "dd/MM/yyyy hh:mm") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        return dateFormatter.date(from: self)
    }
    
    func getAttributedTextWithBoldWords() -> NSMutableAttributedString {
        let components = self.components(separatedBy: "*")
        let mutableText = NSMutableAttributedString(string: "")
        let boldFontAttribute: [NSAttributedString.Key: Any] = [.font: Fonts.semibold(12)]
        for (index, string) in components.enumerated() {
            if index%2 != 0 {
                let attributedString = NSAttributedString(string: string, attributes: boldFontAttribute)
                mutableText.append(attributedString)
            } else {
                let attributedString = NSAttributedString(string: string)
                mutableText.append(attributedString)
            }
        }
        return mutableText
    }
    
    func replaceSpecialCharacters() -> String {
        var text = self
        text = text.replacingOccurrences(of: "Ã¡", with: "á" )
        text = text.replacingOccurrences(of: "Ã©", with: "é" )
        text = text.replacingOccurrences(of: "Ã­", with: "í" )
        text = text.replacingOccurrences(of: "Ã³", with: "ó" )
        text = text.replacingOccurrences(of: "Ãº", with: "ú" )

        // caracteres raros: tildes mayusculas
        text = text.replacingOccurrences(of: "Ã", with: "Á" )
        text = text.replacingOccurrences(of: "Ã‰", with: "É" )
        text = text.replacingOccurrences(of: "Ã", with: "Í" )
        text = text.replacingOccurrences(of: "Ã“", with: "Ó" )
        text = text.replacingOccurrences(of: "Ãš", with: "Ú" )


        // caracteres raros: tildes inversas minusculas
        text = text.replacingOccurrences(of: "Ã ", with: "à" )
        text = text.replacingOccurrences(of: "Ã¨", with: "è" )
        text = text.replacingOccurrences(of: "Ã¬", with: "ì" )
        text = text.replacingOccurrences(of: "Ã²", with: "ò" )
        text = text.replacingOccurrences(of: "Ã¹", with: "ù" )

        // caracteres raros: tildes inversas mayusculas
        text = text.replacingOccurrences(of: "Ã€", with: "À" )
        text = text.replacingOccurrences(of: "Ãˆ", with: "È" )
        text = text.replacingOccurrences(of: "ÃŒ", with: "Ì" )
        text = text.replacingOccurrences(of: "Ã’", with: "Ò" )
        text = text.replacingOccurrences(of: "Ã™", with: "Ù" )

        // caracteres raros: ñ minuscula y mayuscula
        text = text.replacingOccurrences(of: "Ã‘", with: "ñ" )
        text = text.replacingOccurrences(of: "Ã±", with: "Ñ" )
        
        return text
    }
}

extension Dictionary where Key == String {
    func double(key: String) -> Double? {
        return self[key] as? Double ?? (self[key] as? String)?.double
    }
}
