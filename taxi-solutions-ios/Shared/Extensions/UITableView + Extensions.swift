//
//  UITableView + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func numberOfRows<T>(for items: [T], emptyMessage: String) -> Int {
        if items.count == 0 {
            setEmptyMessage(emptyMessage)
        } else {
            restore()
        }
        return items.count
    }
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 15, y: 0, width: self.bounds.size.width - 30, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = #colorLiteral(red: 0.2509803922, green: 0.2706818879, blue: 0.3060019016, alpha: 0.25)
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = Fonts.regular(13)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func getMinimumHeight<T>(forVC vc: UIViewController, items: [T], tableViewCellHeight: CGFloat) -> CGFloat {
        let minTableViewHeight = vc.view.frame.height - self.convert(self.bounds, to: vc.view).origin.y
        return (items.count * Int(tableViewCellHeight)).float < minTableViewHeight ? minTableViewHeight : CGFloat(items.count) * tableViewCellHeight
    }

    func restore() {
        self.backgroundView = nil
    }
    
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
    }
    
    func reloadData(with animation: UITableView.RowAnimation) {
        reloadSections(IndexSet(integersIn: 0..<numberOfSections), with: animation)
    }
    
    func animate(withDuration duration: Double = 1.25, completion: (() -> Void)? = nil) {
        self.reloadData()
        let height = self.frame.height
        let cells = self.visibleCells
        
        cells.forEach {
            $0.transform = CGAffineTransform(translationX: 0, y: height)
            $0.alpha = 0
        }
        
        var delayCounter = 0
        cells.forEach { cell in
            UIView.animate(withDuration: duration, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut, .transitionCrossDissolve], animations: {
                cell.transform = .identity
                cell.alpha = 1
            }) { _ in
                completion?()
            }
            delayCounter += 1
        }
    }
}
