//
//  ButtonBarPagerTabStripViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import XLPagerTabStrip

extension ButtonBarPagerTabStripViewController {
    func configureButtonBarPagerTabStrip() {
        settings.style.buttonBarBackgroundColor = Colors.background
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemFont = Fonts.regular(15)

        settings.style.selectedBarBackgroundColor = Colors.primary
        settings.style.selectedBarHeight = 2

        settings.style.buttonBarItemLeftRightMargin = 8
        settings.style.buttonBarItemTitleColor = Colors.text
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = Colors.text
            newCell?.label.textColor = Colors.secondary
            oldCell?.label.font = Fonts.regular(15)
            newCell?.label.font = Fonts.bold(15)
        }
    }
}
