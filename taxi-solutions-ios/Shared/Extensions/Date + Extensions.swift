//
//  Date + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

extension Date {
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    var dateOnly: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
    var timeOnly: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: self)
    }
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    static var isNight: Bool {
        var calendar = Calendar.current
        if calendar.timeZone.abbreviation() != "GMT-5", let timeZone = TimeZone(identifier: "GMT-5") ?? TimeZone(abbreviation: "GMT-5") {
            calendar.timeZone = timeZone
        }
        let now = calendar.component(.hour, from: Date())
        let morning = 6
        let night = 18
        print(now)
        print(morning)
        print(night)
        return now > morning && now < night ? false : true
    }
    
    func toText(withFormat format: String = "dd/MM/yyyy hh:mm a") -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    func convertToTimeZone(initTimeZone: TimeZone, timeZone: TimeZone) -> Date {
        let delta = TimeInterval(timeZone.secondsFromGMT(for: self) - initTimeZone.secondsFromGMT(for: self))
        return addingTimeInterval(delta)
    }
}
