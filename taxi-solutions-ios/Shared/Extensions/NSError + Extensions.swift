//
//  NSError + Extensions.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

extension NSError {
    convenience init(domain:String = App.name, code:Int = -999, message:String) {
        let userInfo = [NSLocalizedDescriptionKey:message.timedOut, NSLocalizedFailureReasonErrorKey:message, NSLocalizedRecoverySuggestionErrorKey:message]
        self.init(domain: domain, code: code, userInfo: userInfo)
    }
}
