//
//  GMSMapView + Helpers.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMapView {
    
    func resetLeavingMarker(_ marker: GMSMarker) {
        self.clear()
        marker.map = self
    }
    
    func drawPolyline(_ polylineInfo: PolylineInfo, nearToDestination: Bool = false) {
        self.clear()
        
        let path = GMSPath(fromEncodedPath: polylineInfo.polyline)
        let polyline = GMSPolyline(path: path)
        polyline.geodesic = true
        polyline.strokeColor = Colors.primary
        polyline.strokeWidth = 2.5
        polyline.map = self
        
        if let origin = polylineInfo.origin, let destination = polylineInfo.destination {
            let originMarker = GMSMarker(position: origin)
            polylineInfo.driverMoving ? originMarker.rotation = polylineInfo.rotation ?? 0.0 : ()
            originMarker.map = self
            originMarker.icon = polylineInfo.driverMoving ? #imageLiteral(resourceName: "ic_driver_car") : #imageLiteral(resourceName: "ic_map_spot_origin")
            originMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            let destinationMarker = GMSMarker(position: destination)
            destinationMarker.map = self
            destinationMarker.icon = #imageLiteral(resourceName: "ic_map_spot_destination")
            destinationMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            
            if polylineInfo.animated {
                let cameraUpdate = GMSCameraUpdate.fit(GMSCoordinateBounds(coordinate: origin, coordinate: destination))
                self.moveCamera(cameraUpdate)
                let currentZoom = self.camera.zoom
                self.animate(toZoom: currentZoom - 1.5)
            }
            
            if let time = polylineInfo.duration, let marker = UINib(nibName: "TimeMarkerView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? TimeMarkerView {
                #if DRIVER
                let timerCoordinate = destination
                destinationMarker.map = nil
                #endif
                #if USER
                let timerCoordinate = origin
                #endif
                
                marker.setTime(time)
                let timeMarker = GMSMarker(position: timerCoordinate)
                timeMarker.map = self
                #if USER
                timeMarker.groundAnchor = CGPoint(x: 0.5, y: 1.5)
                #endif
                timeMarker.icon = marker.asImage()
                
                if nearToDestination {
                    timeMarker.icon = #imageLiteral(resourceName: "ic_driver_arrived")
                }
            }
        }
    }
}
