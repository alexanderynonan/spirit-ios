//
//  UIViewController + Extensions.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit
import SocketIO
import RealmSwift
import NVActivityIndicatorView
import Toast_Swift
import CoreLocation

extension UIViewController: NVActivityIndicatorViewable {
    
    var realm: Realm {
        return try! Realm()
    }
    
    var socket: SocketIOClient {
        return SocketIOManager.sharedInstance.socket
    }
    
    var user: User {
        return UserManager.sharedManager.currentUser
    }
    
    /// Variable que indica si los permisos de localización han sido habilitados o no. En caso no hayan sido habilitados, muestra un popup requiriendo que se habiliten los permisos
    var locationPermissionsEnabled: Bool {
        let locationAuth = CLLocationManager.authorizationStatus()
        let enabled = locationAuth != .notDetermined && locationAuth != .denied && locationAuth != .restricted
        enabled ? () : requestLocationPermissions()
        return enabled
    }
    
    @IBAction func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissWithCompletion(completion: (() -> Void)? = nil) {
        self.dismiss(animated: true, completion: completion)
    }
    
    func hideKeyboardWhenTappedAround(_ view: UIView? = nil) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        if let view = view {
            view.addGestureRecognizer(tapGesture)
        } else {
            self.view.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func showAlert(withTitle title: String = App.name, message: String, withOkayButton: Bool = true, okayTitle: String = "Ok", okayHandler: ((_ alertAction: UIAlertAction) -> Void)? = nil, withCancelButton: Bool = false, cancelTitle: String = "Cancelar") {
        let alert = UIAlertController(title: title, message: message
            , preferredStyle: .alert)
        if withOkayButton {
            let okayAction = UIAlertAction(title: okayTitle, style: .default, handler: okayHandler)
            alert.addAction(okayAction)
        }
        if withCancelButton {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showToast(message: String, duration: Double = 2.0) {
        var style = ToastStyle()
        style.backgroundColor = Colors.secondary
        style.messageColor = Colors.background
        self.view.makeToast(message, duration: duration, position: .bottom, style: style)
    }
    
    func openUrl(url: String?) {
        guard let url = URL(string: url ?? ""), UIApplication.shared.canOpenURL(url) else {
            self.showAlert(message: "El enlace es inválido")
            return
        }
        UIApplication.shared.open(url, options: [:]) { success in
            print(success)
        }
    }
    
    func openMail(email: String?) {
        guard let url = URL(string: "mailto:\(email ?? "")"), UIApplication.shared.canOpenURL(url) else {
            self.showAlert(message: "El correo es inválido")
            return
        }
        UIApplication.shared.open(url, options: [:]) { success in
            print(success)
        }
    }
    
    func call(phoneNumber: String?) {
        guard let url = URL(string: "tel://\(phoneNumber?.replacingOccurrences(of: " ", with: "") ?? "")"), UIApplication.shared.canOpenURL(url) else {
            self.showAlert(message: "El número del contacto es inválido")
            return
        }
        UIApplication.shared.open(url, options: [:]) { success in
            print(success)
        }
    }
    
    /// This function shows an ActivityIndicator
    /// - Parameter object: You can use this parameter to check if there are objects of the same type as the objects you are waiting in the service. If there are objects in realm of the same type as the objects you are waiting from the service, the activity indicator will not show.
    func startLoader(withMessage message: String? = nil, waitingForObjectsOfType object: Object.Type? = nil) {
        if let object = object {
            if self.realm.objects(object.self).count != 0 {
                return
            }
        }
        self.startAnimating(CGSize(width: 50, height: 50), message: message, messageFont: Fonts.bold(22), type: .ballPulse, color: .white, minimumDisplayTime: 5, backgroundColor: nil, fadeInAnimation: .none)
    }
    
    func stopLoader() {
        stopAnimating()
    }
    
    private func requestLocationPermissions() {
        showAlert(withTitle: "Permisos de Ubicación", message: "Para habilitar los permisos de Ubicación debes ir a Configuracion de tu iPhone > Privacidad > Localización > \(App.name)", withOkayButton: true, okayTitle: "Ir a configuraciones", okayHandler: { (_) in
            Helpers.goToTaxiSvicoLocationSettings()
        }, withCancelButton: true)
    }
    
    func setRootViewController(_ viewControllerId: String? = nil, fromStoryboard storyboardName: String? = nil) -> UIViewController? {
        let storyboard = storyboardName != nil ? UIStoryboard(name: storyboardName!, bundle: nil) : self.storyboard
        let viewController = viewControllerId != nil ? storyboard?.instantiateViewController(withIdentifier: viewControllerId!) : storyboard?.instantiateInitialViewController()
        if let window = UIApplication.shared.keyWindow, let vc = viewController {
            window.rootViewController = vc
            UIView.transition(with: window, duration: 0.5, options: [.transitionCrossDissolve], animations: {}, completion: nil)
            return vc
        }
        
        return nil
    }
    
    static func getTopViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    func goDirection(latitude: Double, longitude: Double, address: String? = nil) {
        let optionsVC = UIAlertController(title: "Ir a destino", message: address ?? "", preferredStyle: .actionSheet)
        let googleMapsAction = UIAlertAction(title: "Google Maps", style: .default) { (_) in
            self.openGoogleMaps(latitude: latitude, longitude: longitude)
        }
        let wazeAction = UIAlertAction(title: "Waze", style: .default) { (_) in
            self.openWaze(latitude: latitude, longitude: longitude)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        optionsVC.addActions(actions: [googleMapsAction, wazeAction, cancelAction])
        self.present(optionsVC, animated: true, completion: nil)
    }
    
    func openGoogleMaps(latitude: Double, longitude: Double) {
        if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            self.showAlert(message: "No cuenta con Google Maps instalado en su dispositivo")
        }
    }
    
    func openWaze(latitude: Double, longitude: Double) {
        if let url = URL(string: "waze://?ll=\(latitude),\(longitude)&navigate=yes"), UIApplication.shared.canOpenURL(url) {
           UIApplication.shared.openURL(url)
       } else {
            self.showAlert(message: "No cuenta con Waze instalado en su dispositivo")
       }
    }
    
    func logout(expiredToken: Bool = false) -> UIViewController? {
        print("LOGOUT")
        if expiredToken {
            do {
                try self.realm.write {
                self.realm.deleteAll()
                }
            } catch {
                print("Error realm")
            }
            SocketIOManager.sharedInstance.closeConnection()
            return setRootViewController(fromStoryboard: "Login")
        }else {
            startLoader()
            MainManager.sharedManager.logOut(successResponse: {(response) in
                self.stopLoader()
                try! self.realm.write {
                    self.realm.deleteAll()
                }
                SocketIOManager.sharedInstance.closeConnection()
                _ = self.setRootViewController(fromStoryboard: "Login")
            }, failureResponse: {(error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            })
            return nil
        }
    }
}
