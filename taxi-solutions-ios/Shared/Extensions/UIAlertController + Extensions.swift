//
//  UIAlertController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    func addActions(actions: [UIAlertAction]) {
        actions.forEach { action in
            self.addAction(action)
        }
    }
}
