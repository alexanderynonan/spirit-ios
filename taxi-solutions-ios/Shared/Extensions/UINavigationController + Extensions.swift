//
//  UINavigationController + Extensions.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/09/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    static func setCustomAppearance(_ custom: Bool) {
        let barAppearance = UINavigationBar.appearance()
        barAppearance.tintColor = custom ? Colors.background : nil
        barAppearance.barTintColor = custom ? Colors.secondary : nil
        barAppearance.isTranslucent = custom ? false : true
        barAppearance.titleTextAttributes = custom ? [.foregroundColor: Colors.background] : nil
    }
}
