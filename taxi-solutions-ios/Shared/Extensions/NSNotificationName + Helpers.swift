//
//  NSNotificationName + Helpers.swift
//  TaxiSvico-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 6/24/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let goToProfileView = Notification.Name("goToProfileView")
}
