//
//  Int + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    var string: String {
        return "\(self)"
    }
    var float: CGFloat {
        return CGFloat(self)
    }
    
    func secondsToTime() -> String {
        let (m,s) = ((self % 3600) / 60, (self % 3600) % 60)

        let minutes =  m < 10 ? "0\(m)" : "\(m)"
        let seconds =  s < 10 ? "0\(s)" : "\(s)"

        return "\(minutes):\(seconds)"
    }
}
