//
//  Object + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/3/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift

extension Object {
    var realm: Realm {
        get {
            return try! Realm()
        }
    }
    
    func generateId() -> String {
        return UUID().uuidString
    }
    
    class func getAll<T: Object>() -> Results<T> {
        let realm = try! Realm()
        return realm.objects(T.self)
    }
    
    class func withId<T: Object>(id: Int) -> T? {
        let realm = try! Realm()
        return realm.objects(T.self).filter("id == \(id)").first
    }
}
