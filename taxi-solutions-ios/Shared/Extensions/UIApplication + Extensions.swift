//
//  UIApplication + Extensions.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var isKeyboardPresented: Bool {
        if let keyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow"), self.windows.contains(where: { $0.isKind(of: keyboardWindowClass) }) {
            return true
        } else {
            return false
        }
    }
}
