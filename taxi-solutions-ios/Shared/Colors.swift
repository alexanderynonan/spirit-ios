//
//  Color.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    
    static let background = UIColor(named: "background") ?? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    static let primary = UIColor(named: "primary") ?? #colorLiteral(red: 0.0431372549, green: 0.2980392157, blue: 0.8784313725, alpha: 1)
    
    static let secondary = UIColor(named: "secondary") ?? #colorLiteral(red: 0.03921568627, green: 0.07843137255, blue: 0.3137254902, alpha: 1)
    
    static let error = UIColor(named: "error") ?? #colorLiteral(red: 0.9450980392, green: 0.05098039216, blue: 0.1960784314, alpha: 1)
    
    static let terciary = UIColor(named: "terciary") ?? #colorLiteral(red: 0.7725490196, green: 0.9411764706, blue: 0.9843137255, alpha: 1)
    
    static let text = UIColor(named: "text") ?? #colorLiteral(red: 0.03921568627, green: 0.07843137255, blue: 0.3137254902, alpha: 1)
}
