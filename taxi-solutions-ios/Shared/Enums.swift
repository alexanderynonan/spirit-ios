//
//  Enums.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/29/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

enum RangeType: String, CaseIterable {
    case day = "día"
    case week = "semana"
    case month = "mes"

    var description : String {
      switch self {
      case .day: return "day"
      case .week: return "week"
      case .month: return "month"
      }
    }
}
 
enum RideState: Int, CaseIterable {
    case onWay = 0
    case waiting = 1
    case onRide = 2
    case finished = 3
    
    static func getStatusFromString(_ statusString: String?) -> RideState? {
        switch statusString {
        case "arriving_to_origin_point":
            return .onWay
        case "at_origin_point":
            return .waiting
        case "in_progress", "ending_trip":
            return .onRide
        case "cancelled", "cancelled_in_service", "paying", "finished":
            return .finished
        default:
            return .none
        }
    }
}

enum AddressType: String {
    case origin = "punto de partida"
    case destination = "destino"
}

enum PaymentMethod: String, CaseIterable, CustomStringConvertible {
    case cash = "Efectivo"
    case creditCard = "Tarjeta"
    
    var description : String {
      switch self {
      case .cash: return "cash"
      case .creditCard: return "creditCart"
      }
    }
}

enum ConsumptionType: String, CaseIterable {
    case service = "Servicio"
    case recharge = "Recarga"
}

enum InputType {
    case textField
    case textView
}

enum PaymentStatus: String, CaseIterable {
    case completed = "complete"
    case incompleted = "incomplete"
    case notPaid = "not_paid"
}

enum ConfirmationTitles: String {
    case defaultTitle = "Confirmar"
    case SOS = "Emergencia"
    case changeRoute = "Cambiar ruta"
    case debtorUser = "Saldo pendiente"
    case removeCreditCard = "Eliminar tarjeta"
    case logout = "Cerrar sesión"
    case helpPhone = "Llamar a Svico"
    case helpMail = "Enviar correo"
    case helpWeb = "Página web"
    case inactivity = "Inactividad"
    case qr = "Confirmar QR"
}
