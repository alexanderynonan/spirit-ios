//
//  BaseServiceProtocol.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import Photos
import NVActivityIndicatorView

typealias SuccessResponse = (_ response: JSON) -> Void
typealias FailureResponse = (_ error: NSError) -> Void

let baseURL = Api.baseUrl
var headers: [String: String] {
    //TODO:- SET HEADERS
    return ["Accept":"application/json", "Authorization": "Bearer \(UserManager.sharedManager.currentUser.accessToken)"]
}

class NetworkManager {

    var manager: SessionManager?

    init(timeout: TimeInterval = 60) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeout
        manager = Alamofire.SessionManager(configuration: configuration)
    }
}

protocol BaseServiceProtocol {
    
    // MARK: - Public
    
    func validateResponse(_ response: JSON, statusCode: Int, success: SuccessResponse, failure: FailureResponse) -> Void
    
    func queryString(fromArray array: [String]?) -> String
    
    func GET(toEndpoint endpoint: String!, params: [String: AnyObject]?,success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void
    
    func POST(toEndpoint endpoint: String!, params: [String: AnyObject]?,success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void
    
    func DELETE(toEndpoint endpoint: String!, params: [String: AnyObject]?,success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void
    
}

extension BaseServiceProtocol {
    
    var user: User {
        return UserManager.sharedManager.currentUser
    }
    
    // MARK: - Private
    
    func validateResponse(_ response: JSON, statusCode: Int, success: SuccessResponse, failure: FailureResponse) -> Void {
        
        let isSuccess = [200, 201, 202, 203, 204].contains(statusCode) ? true : false
        
        if isSuccess {
            success(response)
        } else {
            print("🚨 STATUS CODE -> \(statusCode) 🚨")
            print(response)
            if statusCode == 401 {
                let errorMessage = "Se inició sesión en otro dispositivo, se procederá a cerrar la sesión actual"
                let error = NSError(domain: App.name, code: 401, message: errorMessage)
                let loginVC = UIViewController().logout(expiredToken: true)
                loginVC?.showAlert(message: error.localizedDescription)
                failure(error)
            } else if statusCode == 403 {
                let errorMessage = response["message"].stringValue
                let error = NSError(domain: App.name, code: 403, message: errorMessage)
                let loginVC = UIViewController().logout(expiredToken: true)
                loginVC?.showAlert(message: errorMessage)
                failure(error)
            } else if response["payment_data"].dictionaryObject != nil {
                let error = NSError(domain: App.name, code: -999, userInfo: response["payment_data"].dictionaryObject)
                        failure(error)
            } else {
                let errorMessage = response["message"].stringValue
                let error = NSError(domain: App.name, code: -999, message: errorMessage)
                failure(error)
            }
        }
    }
    
    // MARK: - Public
    
    func queryString(fromArray array: [String]?) -> String {
        guard let v = array else {
            return ""
        }
        var s = ""
        for i in 0 ..< v.count {
            if i > 0 && i < v.count {
                s.append(",")
            }
            s.append(v[i].addingPercentEncoding(withAllowedCharacters: CharacterSet.init(charactersIn: "-_.!~*'()"))!)
        }
        return s
    }
    
    func GET(toEndpoint endpoint: String!, params: [String: AnyObject]?, success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void {
        
        let manager = NetworkManager().manager ?? Alamofire.SessionManager.default
        
        var url = ""
        
        if "\(endpoint)".contains(".full") {
            url = endpoint.replacingOccurrences(of: ".full", with: "")
        } else {
            url = "\(baseURL)\(endpoint!)"
        }
        
        manager.request(url, method: .get, parameters: params, headers: headers).responseJSON
            { (response) in
                
                print("*******************************************************************\n")
                print("PARAMS:\n\(params)")
                print("GET URL:\(url)")
                print("\(JSON(response.data ?? "Problemas con el servicio"))")
                print("*******************************************************************\n")
                
                switch response.result {
                case .success:
                    let json = try! JSON(data: response.data!)
                    self.validateResponse(json, statusCode: response.response?.statusCode ?? 0, success: success, failure: failure)
                    break
                case .failure(let error):
                    let nsError = NSError(domain: App.name, code: -999, message: error.localizedDescription.translatedMessage)
                    print(error.localizedDescription)
                    failure(nsError)
                    break
                }
                manager.session.invalidateAndCancel()
        }
    }
    
    func POST(toEndpoint endpoint: String!, params: [String: AnyObject]?, success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void {
        
        let manager = NetworkManager().manager ?? Alamofire.SessionManager.default
        
        let url = "\(baseURL)\(endpoint!)"
        
        manager.request(url, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            let data = response.data!
            
            print("*******************************************************************\n")
            print("PARAMS:\n\(params)")
            print("GET URL:\(url)")
            print("\(JSON(response.data ?? "Problemas con el servicio"))")
            print("*******************************************************************\n")
            
            switch response.result {
            case .success:
                let json = try! JSON(data: data)
                self.validateResponse(json, statusCode: response.response?.statusCode ?? 0, success: success, failure: failure)
                break
            case .failure(let error):
                let nsError = NSError(domain: App.name, code: -999, message: error.localizedDescription.translatedMessage)
                print(error.localizedDescription)
                failure(nsError)
                break
            }
            manager.session.invalidateAndCancel()
        }
    }
    
    func PATCH(toEndpoint endpoint: String!, params: [String: AnyObject]?, success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void {
        
        let manager = NetworkManager().manager ?? Alamofire.SessionManager.default
        
        let url = "\(baseURL)\(endpoint!)"
        
        manager.request(url, method: .patch, parameters: params, headers: headers).responseJSON { (response) in
            
            print("*******************************************************************\n")
            print("PARAMS:\n\(params)")
            print("GET URL:\(url)")
            print("\(JSON(response.data ?? "Problemas con el servicio"))")
            print("*******************************************************************\n")
            
            let data = response.data!
            
            switch response.result {
            case .success:
                let json = try! JSON(data: data)
                self.validateResponse(json, statusCode: response.response?.statusCode ?? 0, success: success, failure: failure)
                break
            case .failure(let error):
                let nsError = NSError(domain: App.name, code: -999, message: error.localizedDescription.translatedMessage)
                print(error.localizedDescription)
                failure(nsError)
                break
            }
            manager.session.invalidateAndCancel()
        }
    }
    
    func DELETE(toEndpoint endpoint: String!, params: [String: AnyObject]?,success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void {
        
        let manager = NetworkManager().manager ?? Alamofire.SessionManager.default
        
        let url = "\(baseURL)\(endpoint!)"
        
        manager.request(url, method: .delete, parameters: params, headers: headers).responseJSON
            { (response) in
                
                print("*******************************************************************\n")
                print("PARAMS:\n\(params)")
                print("GET URL:\(url)")
                print("\(JSON(response.data ?? "Problemas con el servicio"))")
                print("*******************************************************************\n")
                
                switch response.result {
                case .success:
                    let json = try! JSON(data: response.data!)
                    self.validateResponse(json, statusCode: response.response?.statusCode ?? 0, success: success, failure: failure)
                    break
                case .failure(let error):
                    let nsError = NSError(domain: App.name, code: -999, message: error.localizedDescription.translatedMessage)
                    print(error.localizedDescription)
                    failure(nsError)
                    break
                }
                manager.session.invalidateAndCancel()
        }
    }
    
    /// Método para llamar a servicios que requieren subida de imágenes
    /// - Parameters:
    ///   - endpoint: Endpoint del servicio a consultar
    ///   - key: Key de la imágen. En caso se requiera subir un arreglo de imágenes con diferentes keys (que incluyan el indice), poner el texto 'index' el cual será reemplazado por el indice del item dentro del arreglo.
    ///   - images: Arreglo de imágenes a subir
    ///   - params: Parámetros para la consulta al servicio
    ///   - success: Handler del flujo exitoso
    ///   - failure: Handler del flujo fallido
    /// - Returns: Void
    func POSTImage(toEndpoint endpoint: String!, key: String, images: [UIImage], params: [String: AnyObject]?, success: @escaping SuccessResponse, failure: @escaping FailureResponse) -> Void {
        
        let manager = NetworkManager().manager ?? Alamofire.SessionManager.default
        
        let url = "\(baseURL)\(endpoint!)"

        Alamofire.upload(multipartFormData: { (form) in
            for (index, image) in images.enumerated() {
                if let data = image.jpegData(compressionQuality: 0.5) {
                    form.append(data, withName: key.replacingOccurrences(of: "index", with: "\(index)"), fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
            }
            
            for (key, value) in params!{
                form.append("\(value)".data(using: String.Encoding.utf8)!, withName: "\(key)")
            }
            
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: headers) { (result) in
            
            switch result {
                
            case .failure(let error):
                print("UploadImageController.requestWith.Alamofire.usingThreshold:", error)
                failure(error as NSError)
                
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                
                upload.uploadProgress(closure: { (progressValue) in
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("\(Int(progressValue.fractionCompleted*100))%")
                    print("PROGRESS UPLOAD IMAGES: \(Float(progressValue.fractionCompleted))")
                })
                upload.responseJSON(completionHandler: { (response) in
                    
                    print("*******************************************************************\n")
                    print("PARAMS:\n\(params)")
                    print("GET URL:\(url)")
                    print("\(JSON(response.data ?? "Problemas con el servicio"))")
                    print("*******************************************************************\n\n\n")
                    
                    let data = response.data!
                    
                    switch response.result {
                    case .success:
                        let json = try! JSON(data: data)
                        self.validateResponse(json, statusCode: response.response?.statusCode ?? 0, success: success, failure: failure)
                        break
                    case .failure(let error):
                        let nsError = NSError(domain: App.name, code: -999, message: error.localizedDescription.translatedMessage)
                        print(error.localizedDescription)
                        failure(nsError)
                        break
                    }
                    manager.session.invalidateAndCancel()
                })
            }
        }
    }

}

extension String {
    var translatedMessage: String {
        if self.contains("The Internet connection appears to be offline.") {
            return "Por favor, revise su conexión a internet."
        }
        return self
    }
}
