//
//  UserService.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import NVActivityIndicatorView

class UserService: BaseServiceProtocol {
    
    //MARK:- Singleton
    static let sharedService = UserService()
    
    #if DRIVER
    let profile = "driver"
    let isDriver = 1
    #endif
    #if USER
    let profile = "client"
    let isDriver = 0
    #endif
    
    func login(email: String,
               password: String,
               successResponse success: @escaping(_ response: JSON) -> Void,
               failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "email": email,
            "password": password,
            "device_token": App.fcmToken,
            "type": profile
            ] as [String: AnyObject]
        POST(toEndpoint: "auth/login", params: params, success: success, failure: failure)
    }
    
    func getUser(user: User,
                 successResponse success: @escaping(_ response: JSON) -> Void,
                 failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "\(profile)s/\(user.id)", params: nil, success: success, failure: failure)
    }
    
    func sendPasswordRecoveryMail(email: String,
                                  successResponse success: @escaping(_ response: JSON) -> Void,
                                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var device = ""

        #if DRIVER
            device = "driver_device"
        #endif

        #if USER
            device = "client_device"
        #endif
        let params = [
            "email": email,
            "device": device
            ] as [String: AnyObject]
        
        POST(toEndpoint: "forgot-password?role=\(profile)", params: params, success: success, failure: failure)
    }
    
    func verifyPhone(phoneNumber: String,
                     verificationMode: PhoneVerificationMode,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "phone_number": phoneNumber,
            "is_driver": isDriver
            ] as [String: AnyObject]
        POST(toEndpoint: verificationMode == .sms ? "account/verification-code" : "account/verification-code-call", params: params, success: success, failure: failure)
    }
    
    func verifyDocumentNumber(documentNumber: String,
                              successResponse success: @escaping(_ response: JSON) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "users/document-number/\(documentNumber)", params: nil, success: success, failure: failure)
    }
    
    func validateDocumentNumber(documentNumber: String,
                                documentType: String,
                                successResponse success: @escaping(_ response: JSON) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "is_driver": isDriver,
            "document_type": documentType,
            "document_number": documentNumber
        ] as [String: AnyObject]
        POST(toEndpoint: "account/validate-document", params: params, success: success, failure: failure)
    }
    
    func registerUser(_ user: RegistrationUser,
                      successResponse success: @escaping(_ response: JSON) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var url = ""
        
        var params = [
            "device_token": App.fcmToken,
            "document_number": user.documentNumber,
            "document_type": user.documentType,
            "name": user.firstName,
            "last_name": user.lastName,
            "phone": user.phoneNumber,
            "email": user.email,
            "password": user.password,
            "password_confirmation": user.password,
            "external_invitation_code": user.invitationCode
        ]
        
        if user.base64Photo != "" {
            params.updateValue(user.base64Photo, forKey: "photo")
        }
        
        #if DRIVER
        url = "\(baseURL)drivers"
        params.updateValue(user.licenseNumber, forKey: "license_number")
        params.updateValue(user.category, forKey: "license_category")
        params.updateValue(user.licensePlate, forKey: "car_plate_number")
        params.updateValue(user.TUCExpiringDate, forKey: "expiration_date_TUC")
        params.updateValue(user.bank, forKey: "bank_name")
        params.updateValue(user.accountNumber, forKey: "bank_account_number")
        params.updateValue(user.birthDate, forKey: "birthdate")
        params.updateValue(user.gender, forKey: "gender")
        params.updateValue(user.platePhoto, forKey: "plate_img")
        params.updateValue(user.soatPhoto, forKey: "soat_img")
        params.updateValue(user.documentPhoto, forKey: "document_img")
        if !user.technicalReviewPhoto.isEmptyOrWhitespace() {
            params.updateValue(user.technicalReviewPhoto, forKey: "technical_review_img")
        }
        #endif
        
        #if USER
        url = "\(baseURL)clients"
        params.updateValue(user.documentPhoto, forKey: "document_photo")
        #endif
        
        print("URL \(url)")
//        print("PARAMS \n\n\(params)\n\nPARAMS")
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in params {
                switch key {
                case "photo":

                    if let imageData = value.imageFromBase64.pngData() {
                        multipartFormData.append(imageData, withName: key, fileName: "\(user.firstName)_\(user.lastName).png", mimeType: "image/png")
                    }
                case "document_photo", "document_img":
                    if let imageData = value.imageFromBase64.pngData() {
                        multipartFormData.append(imageData, withName: key, fileName: "\(user.firstName)_\(user.lastName)_doc_\(user.documentType).png", mimeType: "image/png")
                    }
                case "plate_img":
                    if let imageData = value.imageFromBase64.pngData() {
                        multipartFormData.append(imageData, withName: key, fileName: "\(user.firstName)_\(user.lastName)_plate.png", mimeType: "image/png")
                    }
                case "soat_img":
                    if let imageData = value.imageFromBase64.pngData() {
                        multipartFormData.append(imageData, withName: key, fileName: "\(user.firstName)_\(user.lastName)_soat.png", mimeType: "image/png")
                    }
                case "technical_review_img":
                    if let imageData = value.imageFromBase64.pngData() {
                        multipartFormData.append(imageData, withName: key, fileName: "\(user.firstName)_\(user.lastName)_techinal_review.png", mimeType: "image/png")
                    }
                default:
                    multipartFormData.append(value.data, withName: key)
                }
            }
            
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url , method: .post, headers: ["Accept": "application/json", "Content": "application/json"]){ (result : SessionManager.MultipartFormDataEncodingResult) in
            switch result {
            case .failure(let error):
                failure(NSError(message: error.localizedDescription))
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.uploadProgress(closure: { progressValue in
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("\(Int(progressValue.fractionCompleted*100))%")
                    print("PROGRESS UPLOAD IMAGES: \(Float(progressValue.fractionCompleted))")
                })
                    .responseJSON(completionHandler: { (response: DataResponse<Any>) in
                        print("Success: \(response.result.isSuccess)")
                        if let data = response.data, let json = try? JSON(data: data) {
                            success(json)
                        } else {
                            failure(NSError(message: "No se pudo parsear la respuesta"))
                        }
                    })
            }
        }
    }
    
    func resetPassword(password: String,
                       accessToken: String,
                       successResponse success: @escaping(_ response: JSON) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "password": password,
            "password_confirmation": password,
            "access_token": accessToken
            ] as [String: AnyObject]
        POST(toEndpoint: "reset-password", params: params, success: success, failure: failure)
    }
    
    func updatePhone(phone: String,
                     userId: Int,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        let params = [
            "_method": "PUT",
            "phone": phone
            ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(userId)", params: params, success: success, failure: failure)
    }
    
    func updateEmail(email: String,
                     userId: Int,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        let params = [
            "_method": "PUT",
            "email": email
            ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(userId)", params: params, success: success, failure: failure)
    }
    
    func updatePassword(oldPassword: String,
                        newPassword: String,
                        confirmPassword: String,
                        userId: Int,
                        successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        let params = [
            "_method": "PUT",
            "old_password": oldPassword,
            "password": newPassword,
            "password_confirmation": confirmPassword
            ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(userId)", params: params, success: success, failure: failure)
    }
    
    func setDriverNearToDestination(successResponse success: @escaping(_ response: JSON) -> Void,
                                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "_method": "PUT",
            "connection_status": "near_to_destination_point"
        ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(user.id)", params: params, success: success, failure: failure)
    }
    
    func updatePhoto(_ image: UIImage,
                     user: User,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let url = "\(baseURL)clients/\(user.id)"
        let headers = ["Accept":"application/json", "Authorization": "Bearer \(UserManager.sharedManager.currentUser.accessToken)"]
        let fileName = "\(user.firstName)_\(user.lastName).png"
        print("🔗 URL: \(url)")
        
        NetworkManager(timeout: 600).manager?.upload(multipartFormData: { (multipartFormData) in
            if let imageData = image.pngData() {
                multipartFormData.append("PUT".data, withName: "_method")
                multipartFormData.append(imageData, withName: "photo", fileName: fileName, mimeType: "image/png")
            }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: headers, encodingCompletion: { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
            case .failure(let error):
                failure(NSError(message: error.localizedDescription))
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.uploadProgress(closure: { progress in
                    print("PROGRESS UPLOAD IMAGE: \(Float(progress.fractionCompleted))")
                })
                    .responseJSON(completionHandler: { (response: DataResponse<Any>) in
                        print("Success: \(response.result.isSuccess)")
                        print(response.result)
                        if let data = response.data, let json = try? JSON(data: data) {
                            success(json)
                        } else {
                            failure(NSError(message: "No se pudo parsear la respuesta"))
                        }
                    })
            }
        })
    }
    
    func verifyEmail(email: String,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "email": email,
            "is_driver": isDriver
            ] as [String: AnyObject]
        POST(toEndpoint: "account/verification-code-mail", params: params, success: success, failure: failure)
    }
    
    func validateEmail(email: String,
                       successResponse success: @escaping(_ response: JSON) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "email": email
            ] as [String: AnyObject]
        POST(toEndpoint: "account/validate-driver-email", params: params, success: success, failure: failure)
    }
    
}
