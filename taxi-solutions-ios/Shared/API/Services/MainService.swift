//
//  MenuService.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation
import Alamofire

class MainService: BaseServiceProtocol {
    
    //MARK:- Singleton
    static let sharedService = MainService()
    
    #if DRIVER
    let profile = "driver"
    let opposite = "client"
    #endif
    
    #if USER
    let profile = "client"
    let opposite = "driver"
    #endif
    
    func getSecurityToken(successResponse success: @escaping(_ data: Data) -> Void,
                          failureResponse failure: @escaping(_ error: Error) -> Void) -> Void {
        
        guard let url = URL(string: "https://apitestenv.vnforapps.com/api.security/v1/security") else { return }
        
        let loginString = String(format: "%@:%@", NiubizInfo.user, NiubizInfo.password)
        let loginData = loginString.data(using: String.Encoding.utf8) ?? Data()
        let base64LoginString = loginData.base64EncodedString()
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                failure(error)
            } else if let data = data {
                success(data)
            } else {
                failure(NSError(message: "No se pudo obtener la información del servidor"))
            }
        }.resume() 
    }
    
    func getAddressName(coordinate: CLLocationCoordinate2D,
                        successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "lat": coordinate.latitude,
            "long": coordinate.longitude
        ] as [String: AnyObject]
        POST(toEndpoint: "get-address-name", params: params, success: success, failure: failure)
    }
    
    func getCoordinateFrom(address: String,
                                  successResponse success: @escaping(_ response: JSON) -> Void,
                                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "address": address
        ] as [String: AnyObject]
        POST(toEndpoint: "get-lat-long-coordinates", params: params, success: success, failure: failure)
    }
    
    func getLogoUrl(successResponse success: @escaping(_ response: JSON) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "application-logo", params: nil, success: success, failure: failure)
    }
    
    func getConfigurationParams(successResponse success: @escaping(_ response: JSON) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "configuration-params", params: nil, success: success, failure: failure)
    }
    
    func getDocumentTypes(successResponse success: @escaping(_ response: JSON) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "documents/\(profile)s", params: nil, success: success, failure: failure)
        
    }
    
    func getLicenseCategories(successResponse success: @escaping(_ response: JSON) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "licenses", params: nil, success: success, failure: failure)
    }
    
    func getReferredFriends(successResponse success: @escaping(_ response: JSON) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "\(profile)s/\(user.id)/referred-users", params: nil, success: success, failure: failure)
    }
    
    func getHelpQuestions(successResponse success: @escaping(_ response: JSON) -> Void,
                 failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "help/faqs?profile=\(profile)", params: nil, success: success, failure: failure)
    }
    
    func sendHelpQuestion(themeId: Int,
                          question: String,
                          successResponse success: @escaping(_ response: JSON) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var params = [
            "question": question
        ] as [String: AnyObject]
        if themeId != 0 {
            params.updateValue(themeId as AnyObject, forKey: "theme_id")
        }
        POST(toEndpoint: "help/faqs/send-question", params: params, success: success, failure: failure)
    }
    
    func getPolyline(originCoordinate: CLLocationCoordinate2D,
                     middleCoordinate: CLLocationCoordinate2D?,
                     destinationCoordinate: CLLocationCoordinate2D,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let origin = "\(originCoordinate.latitude),\(originCoordinate.longitude)"
        let destination = "\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)"
        let waypoints = middleCoordinate != nil ? "&waypoints=\(middleCoordinate?.latitude ?? 0.0),\(middleCoordinate?.longitude ?? 0.0)" : ""
        let url = ".fullhttps://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)\(waypoints)&mode=driving&key=\(GoogleApiKeys.maps)"
        
        GET(toEndpoint: url, params: nil, success: success, failure: failure)
    }
    
    func cancelRide(ignored: Bool,
                    reason: String,
                    rideId: Int,
                    comment: String?,
                    successResponse success: @escaping(_ response: JSON) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var params = [
            "\(profile)_id": user.id,
            "reason": reason,
            "is_ignored": ignored
        ] as [String: AnyObject]
        if let comment = comment {
            params.updateValue(comment as AnyObject, forKey: "comment")
        }
        POST(toEndpoint: "rides/\(rideId)/cancel", params: params, success: success, failure: failure)
    }
    
    func getLastUserToRate(successResponse success: @escaping(_ response: JSON) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) {
        GET(toEndpoint: "rides/\(profile)s/\(user.id)/last-\(opposite)-ride", params: nil, success: success, failure: failure)
    }
    
    func rateUser(userToRate: UserToRate,
                  rating: Double,
                  commentary: String?,
                  successResponse success: @escaping(_ response: JSON) -> Void,
                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var params = [
            "ride_id": userToRate.rideId,
            "\(opposite)_id": userToRate.userId,
            "rating": rating.string
        ] as [String: AnyObject]
        if let commentary = commentary {
            params.updateValue(commentary as AnyObject, forKey: "comment")
        }
        POST(toEndpoint: "rides/\(opposite)-rating", params: params, success: success, failure: failure)
    }
    
    func getRideRecords(successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "\(profile)s/\(user.id)/rides", params: nil, success: success, failure: failure)
    }
    
    func getRideRecordDetail(rideId: Int,
                            successResponse success: @escaping(_ response: JSON) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "\(profile)s/\(user.id)/rides/\(rideId)", params: nil, success: success, failure: failure)
    }
    
    func rideReconnection(successResponse success: @escaping(_ response: JSON) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "rides/reconnection", params: nil, success: success, failure: failure)
    }
    
    func getTokenizedCards(successResponse success: @escaping(_ response: JSON) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "\(profile)s/\(user.id)/cards", params: nil, success: success, failure: failure)
    }
    
    func registerTokenizedCard(_ card: TokenizedCard,
                               successResponse success: @escaping(_ response: JSON) -> Void,
                               failureRepsonse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "card_id": card.token,
            "brand": card.brand,
            "is_predetermined": false,
            "masked_number": card.number,
            "expiration_date": "\(card.expirationMonth)-\(card.expirationYear)",
            "card_holder": "\(card.cardholderFirstName) \(card.cardholderLastName)"
        ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(user.id)/cards", params: params, success: success, failure: failure)
    }
    
    func setPredeterminedCard(_ card: TokenizedCard,
                              predetermined: Bool,
                              successResponse success: @escaping(_ response: JSON) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "card_id": card.token,
            "is_predetermined": predetermined
        ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(user.id)/cards/\(card.id)", params: params, success: success, failure: failure)
    }
    
    func removeTokenizedCard(cardId: Int,
                             successResponse success: @escaping(_ response: JSON) -> Void,
                             failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DELETE(toEndpoint: "\(profile)s/\(user.id)/cards/\(cardId)", params: nil, success: success, failure: failure)
    }
    
    func sendSOS(currentCoordinate: CLLocationCoordinate2D,
                 ride: Ride,
                 successResponse success: @escaping(_ response: JSON) -> Void,
                 failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "lat": currentCoordinate.latitude,
            "long": currentCoordinate.longitude
        ] as [String: AnyObject]
        POST(toEndpoint: "\(profile)s/\(user.id)/rides/\(ride.rideId)/sos-alert", params: params, success: success, failure: failure)
    }
    
    func reportTrip(rideId: Int, claim: String, photos: [UIImage], successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError)-> Void) -> Void {
        let params = [
            "body_content": claim
            ] as [String: AnyObject]
        
        if photos.count != 0 {
            POSTImage(toEndpoint: "\(profile)s/\(user.id)/rides/\(rideId)/report", key: "images[index][file]", images: photos, params: params, success: success, failure: failure)
        } else {
            POST(toEndpoint: "\(profile)s/\(user.id)/rides/\(rideId)/report", params: params, success: success, failure: failure)
        }
    }
    
    func getLinkToShare(rideId: Int, successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) {
        POST(toEndpoint: "rides/\(rideId)/share", params: nil, success: success, failure: failure)
    }
    
    func logOut(successResponse successs: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        POST(toEndpoint: "auth/logout", params: nil, success: successs, failure: failure)
    }
}
