//
//  SocketIOManager.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SocketIO
import CoreLocation
import RealmSwift

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    
    let manager: SocketManager = {
        SocketManager(socketURL: Api.socketUrl.url!, config: [/*.log(true),*/ .compress, .connectParams(["token": UserManager.sharedManager.currentUser.socketToken])])
    }()
    var socket: SocketIOClient {
        get {
            return manager.defaultSocket
        }
    }
    
    override init() {
        super.init()
        socket.on(SocketEventsOn.connect) { dataArray, ack in
            print("🧵ON_CONNECT\n\("SOCKET CONNECTED")\n_🧵")
            self.authenticateUser()
        }
        
        socket.on(SocketEventsOn.userConnectResponse) { (dataArray, ack) in
            print("🧵ON_userConnectResponse\n\(dataArray)\n_🧵")
        }
        
        socket.on(SocketEventsOn.correctAuthentication) { (dataArray, ack) in
            print("🧵ON_correctAuthentication\n\(dataArray)\n_🧵")
        }
        
        socket.on(SocketEventsOn.failedAuth) { (dataArray, ack) in
            print("🧵ON_failedAuth\n\(dataArray)\n_🧵")
        }
    }

    func establishConnection() {
        socket.connect()
    }

    func closeConnection() {
        socket.disconnect()
    }
    
    func authenticateUser() {
        #if USER
        let params = [
            "client_id": UserManager.sharedManager.currentUser.id,
            "room": "clients"
        ] as Any
        #endif
        
        #if DRIVER
        let params = [
            "driver_id": UserManager.sharedManager.currentUser.id,
            "room": "drivers"
        ] as Any
        #endif
        print("SOCKET ID: \(self.socket.sid)")
        self.socket.emit(SocketEventsEmit.userConnected, with: [params])
        
        checkIfOnRide()
    }
    
    func checkIfOnRide() {
        let realm = try! Realm()
        guard let ride = realm.objects(Ride.self).first else { return }
        #if DRIVER
        let params = [
            "ride_id": ride.rideId,
            "driver_socket_id": self.socket.sid
        ] as Any
        self.socket.emit(SocketEventsEmit.updateDriverSocketId, with: [params])
        print("NEW SOCKET ID SENT: \(params as? [String: Any])")
        #endif
        
        #if USER
        let params = [
            "ride_id": ride.rideId,
            "client_socket_id": self.socket.sid
        ] as Any
        self.socket.emit(SocketEventsEmit.updateClientSocketId, with: [params])
        print("🧶🧶🧶 NEW SOCKET ID SENT: \(params as? [String: Any] ?? [:]) 🧶🧶🧶")
        #endif
    }
}
