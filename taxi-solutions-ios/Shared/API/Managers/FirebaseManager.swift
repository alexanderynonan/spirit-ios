//
//  FirebaseManager.swift
//  TaxiSvico-User
//
//  Created by Fernanda Alvarado Tarrillo on 6/30/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import FirebaseDatabase

class FirebaseManager: NSObject {

    static let shared = FirebaseManager()
    var ref = Database.database().reference()
    
    func sendMessage(message: Message, chatId: String) {
        self.ref.child("chats/\(chatId)").childByAutoId().updateChildValues([
            "type" : message.type,
            "message" : message.message,
            "timestamp" : message.timestamp
        ])
    }
    
}
