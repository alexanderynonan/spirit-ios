//
//  MenuManager.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import SwiftyJSON
import RealmSwift

class MainManager: NSObject {
    
    //MARK:- Singleton
    static let sharedManager = MainManager()
    
    let realm = try! Realm()
    
    func getSecurityToken(successResponse success: @escaping(_ response: String) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getSecurityToken(successResponse: { data in
            if let token = String(data: data, encoding: .utf8) {
                success(token)
            } else {
                failure(NSError(message: "No se pudo obtener token de seguridad para la pasarela de pagos"))
            }
        }) { (error) in
            failure(NSError(message: error.localizedDescription))
        }
    }
    
    func getAddressName(coordinate: CLLocationCoordinate2D,
                        successResponse success: @escaping(_ response: String) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getAddressName(coordinate: coordinate, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let address = data["address"]?.stringValue {
                success(address)
            } else {
                failure(NSError(message: "No se pudo obtener el nombre de la dirección"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getCoordinateFrom(address: String,
                           successResponse success: @escaping(_ response: CLLocationCoordinate2D) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getCoordinateFrom(address: address, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let lat = data["lat"]?.doubleValue, let long = data["long"]?.doubleValue {
                success(CLLocationCoordinate2D(latitude: lat, longitude: long))
            } else {
                failure(NSError(message: "No se pudieron obtener las coordenadas de dicha dirección"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getLogoUrl(successResponse success: @escaping(_ logoUrl: String) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getLogoUrl(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let url = data["application_logo"]?.stringValue {
                success(url)
            } else {
                print("NO SE PUDO OBTENER LA URL DE LA IMAGEN DEL LOGO")
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getConfigurationParams(successResponse success: @escaping(_ message: String) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getConfigurationParams(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue {
                let contactEmail = data["contact_email"]?.stringValue ?? ""
                let contactPhone = data["contact_number"]?.stringValue ?? ""
                let contactWeb = data["contact_web"]?.stringValue ?? ""
                let sosPhone = data["panic_button_number"]?.stringValue ?? ""
                let terms = data["terms"]?.stringValue ?? ""
                let distanceFilter = data["distance_filter"]?.double ?? 0.0
                UserDefaults.standard.set(contactEmail, forKey: Constants.contactEmailKey)
                UserDefaults.standard.set(contactPhone, forKey: Constants.contactPhoneKey)
                UserDefaults.standard.set(contactWeb, forKey: Constants.contactWebKey)
                UserDefaults.standard.set(sosPhone, forKey: Constants.sosPhoneKey)
                UserDefaults.standard.set(terms, forKey: Constants.termsKey)
                UserDefaults.standard.set(distanceFilter, forKey: Constants.distanceFilterKey)
                success("Los parametros de configuración se obtuvieron correctamente")
            } else {
                failure(NSError(message: "Error al obtener datos de configuración"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getDocumentTypes(successResponse success: @escaping(_ documentTypes: [DocumentType]) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getDocumentTypes(successResponse: { (response) in
            var documentTypes = [DocumentType]()
            if let dictionary = response.dictionary, let data = dictionary["data"]?.dictionary, let documentTypesData = data["documents"]?.arrayValue {
                for data in documentTypesData {
                    let documentType = DocumentType(data: data)
                    documentTypes.append(documentType)
                }
                success(documentTypes)
            } else {
                failure(NSError(message: "Error al obtener tipos de documentos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getLicenseCategories(successResponse success: @escaping(_ licenseCategories: [OptionToSelect]) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getLicenseCategories(successResponse: { (response) in
            var licenseCategories = [OptionToSelect]()
            if let dictionary = response.dictionary, let data = dictionary["data"]?.dictionary, let licenseCategoriesData = data["licenses"]?.arrayValue {
                for data in licenseCategoriesData {
                    let licenseCategory = OptionToSelect(data: data, forType: "license_categories")
                    licenseCategories.append(licenseCategory)
                }
                success(licenseCategories)
            } else {
                failure(NSError(message: "Error al obtener las categorías de licencia"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getReferredFriends(successResponse success: @escaping(_ referredFriends: [ReferredFriend], _ totalBonus: String) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getReferredFriends(successResponse: { (response) in
            var referredFriends: [ReferredFriend] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let referredUsersData = data["referred_users"]?.arrayValue {
                referredUsersData.forEach {
                    let referredFriend = ReferredFriend(data: $0)
                    try! self.realm.write {
                        self.realm.add(referredFriend, update: .all)
                    }
                    referredFriends.append(referredFriend)
                }
                success(referredFriends, data["total_bonus"]?.stringValue ?? "0.0")
            } else {
                failure(NSError(message: "Error al obtener el listado de referidos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getHelpQuestions(successResponse success: @escaping(_ themes: [Theme]) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getHelpQuestions(successResponse: { (response) in
            var themes = [Theme]()
            if let dictionary = response.dictionary, let data = dictionary["data"]?.dictionary, let themesData = data["themes"]?.arrayValue {
                themesData.forEach {
                    let theme = Theme(data: $0.dictionaryValue)
                    try! self.realm.write {
                        self.realm.add(theme, update: .all)
                    }
                    themes.append(theme)
                }
                success(themes)
            } else {
                failure(NSError(message: "Error al obtener las preguntas"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func sendHelpQuestion(themeId: Int,
                          question: String,
                          successResponse success: @escaping(_ message: String) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.sendHelpQuestion(themeId: themeId, question: question, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionary, let message = data["message"]?.stringValue {
                success(message)
            } else {
                success("Su pregunta fue enviada exitosamente")
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getPolyline(originCoordinate: CLLocationCoordinate2D,
                     middleCoordinate: CLLocationCoordinate2D? = nil,
                     destinationCoordinate: CLLocationCoordinate2D,
                     successResponse success: @escaping(_ polyline: String, _ distance: String, _ duration: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getPolyline(originCoordinate: originCoordinate, middleCoordinate: middleCoordinate, destinationCoordinate: destinationCoordinate, successResponse: { (response) in
            print("DIRECTIONS API RESPONSE ------> \n\(response)\n------------------------")
            let routes = response["routes"].arrayValue
            if let route = routes.first {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue ?? ""
                var distance = ""
                var duration = ""
                
                if let leg = route["legs"].arrayValue.first {
                    distance = leg["distance"]["text"].stringValue
                    duration = leg["duration"]["text"].stringValue
                }
                success(points, distance, duration)
            } else {
                failure(NSError(message: "Error al obtener ruta"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func cancelRide(ignored: Bool,
                    reason: String,
                    rideId: Int,
                    comment: String?,
                    successResponse success: @escaping(_ paymentDetail: PaymentDetail?) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.cancelRide(ignored: ignored, reason: reason, rideId: rideId, comment: comment, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"] {
                if data["total"] != .null {
                    let paymentDetail = PaymentDetail(data: data, rideId: rideId)
                    success(paymentDetail)
                } else {
                    success(nil)
                }
            } else {
                failure(NSError(message: "Error al rechazar la solicitud del viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getLastUserToRate(successResponse success: @escaping(_ userToRate: UserToRate) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) {
        MainService.sharedService.getLastUserToRate(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let userToRateData = data["data"] {
                let userToRate = UserToRate(data: userToRateData)
                success(userToRate)
            } else {
                #if USER
                let userToRate = "conductor"
                #endif
                #if DRIVER
                let userToRate = "cliente"
                #endif
                failure(NSError(message: "Error al obtener al último \(userToRate) para calificar"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func rateUser(userToRate: UserToRate,
                  rating: Double,
                  commentary: String?,
                  successResponse success: @escaping(_ response: String) -> Void,
                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.rateUser(userToRate: userToRate, rating: rating, commentary: commentary, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            #if DRIVER
            let ratingKey = "client_rating"
            #endif
            #if USER
            let ratingKey = "driver_rating"
            #endif
            if let data = dictionary["data"]?.dictionaryValue, let rating = data[ratingKey]?.stringValue {
                success("Calificación satisfactoria \(rating)")
            } else {
                failure(NSError(message: "Error al calificar al usuario"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getRideRecords(successResponse success: @escaping(_ rideRecords: [RideRecord]) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getRideRecords(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            var rideRecords = [RideRecord]()
            if let data = dictionary["data"]?.dictionaryValue, let rideRecordsData = data["rides"]?.arrayValue {
                rideRecordsData.forEach { (data) in
                    let rideRecord = RideRecord(data: data)
                    if self.realm.objects(RideRecord.self).filter("id == \(rideRecord.id)").first == nil {
                        try! self.realm.write {
                            self.realm.add(rideRecord, update: .all)
                        }
                    }
                    rideRecords.append(rideRecord)
                }
                success(rideRecords)
            } else {
                failure(NSError(message: "Error al obtener el historial de viajes"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getRideRecordDetail(rideId: Int,
                             successResponse success: @escaping(_ rideRecord: RideRecord) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getRideRecordDetail(rideId: rideId, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let rideData = data["ride"] {
                let rideRecord = RideRecord(data: rideData, detailed: true)
                try! self.realm.write {
                    self.realm.add(rideRecord, update: .all)
                }
                success(rideRecord)
            } else {
                failure(NSError(message: "Error al obtener detalle del viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func rideReconnection(successResponse success: @escaping(_ ride: [String: JSON], _ userOnRide: Any?, _ pendingRide: [String: JSON]?, _ userOnPendingRide: Any?) -> Void,
                          failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.rideReconnection(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let rideData = dictionary["data"]?.dictionaryValue {
                var userOnRide: Any?
                var pendingRideData: [String: JSON]?
                var userOnPendingRide: Any?
                
                #if DRIVER
                if let clientData = rideData["client"] {
                    userOnRide = ClientOnRide(data: clientData) as Any
                }
                //VALIDAMOS SI TIENE UN VIAJE PENDIENTE
                if let hasPendingRide = rideData["has_pending_ride"]?.boolValue {
                    if hasPendingRide {
                        pendingRideData = rideData["pending_ride"]?.dictionaryValue
                        //PARSEAMOS LA DATA DEL CLIENTE DEL VIAJE PENDIENTE
                        if let clientOnPendingRideData = pendingRideData?["client"] {
                            userOnPendingRide = ClientOnRide(data: clientOnPendingRideData) as Any
                        }
                    }
                }
                #endif
                
                #if USER
                if let driverData = rideData["driver"] {
                    userOnRide = DriverOnRide(data: driverData) as Any
                }
                #endif
                
                success(rideData, userOnRide, pendingRideData, userOnPendingRide)
            } else {
                failure(NSError(message: "Error al obtener el estado actual del viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getTokenizedCards(successResponse success: @escaping(_ cards: [TokenizedCard]) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.getTokenizedCards(successResponse: { (response) in
            var cards = [TokenizedCard]()
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let cardsData = data["cards"]?.arrayValue {
                cardsData.forEach { data in
                    let card = TokenizedCard(data: data.dictionaryValue)
                    try! self.realm.write {
                        self.realm.add(card, update: .all)
                    }
                    cards.append(card)
                }
                success(cards)
            } else {
                failure(NSError(message: "Error al obtener las tarjetas"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func registerTokenizedCard(_ card: TokenizedCard,
                               successResponse success: @escaping(_ card: TokenizedCard) -> Void,
                               failureResponse failure: @escaping(_ error: NSError) -> Void ) -> Void {
        MainService.sharedService.registerTokenizedCard(card, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue {
                let card = TokenizedCard(data: data)
                success(card)
            } else {
                failure(NSError(message: "No se pudo registrar la tarjeta"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func setPredeterminedCard(_ card: TokenizedCard,
                              predetermined: Bool = true,
                              successResponse success: @escaping(_ card: TokenizedCard) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.setPredeterminedCard(card, predetermined: predetermined, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let cardData = data["card"]?.dictionaryValue {
                let card = TokenizedCard(data: cardData)
                try! self.realm.write {
                    self.realm.objects(TokenizedCard.self).forEach { card in
                        card.isDefault = false
                    }
                    self.realm.add(card, update: .modified)
                }
                success(card)
            } else {
                failure(NSError(message: "No se pudo seleccionar la tarjeta como predeterminada"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func removeTokenizedCard(cardId: Int,
                             successResponse success: @escaping(_ message: String) -> Void,
                             failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.removeTokenizedCard(cardId: cardId, successResponse: { (response) in
            try! self.realm.write {
                let card = self.realm.objects(TokenizedCard.self).filter("id == \(cardId)")
                self.realm.delete(card)
            }
            success("Su tarjeta fue eliminada exitosamente")
        }) { (error) in
            failure(error)
        }
    }
    
    func sendSOS(currentCoordinate: CLLocationCoordinate2D,
                 ride: Ride,
                 successResponse success: @escaping(_ message: String) -> Void,
                 failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.sendSOS(currentCoordinate: currentCoordinate, ride: ride, successResponse: { (response) in
            success("Alerta enviada con éxito")
        }) { (error) in
            failure(error)
        }
    }
    
    func reportTrip(rideId: Int, claim: String, photos: [UIImage], successResponse success: @escaping(_ message: String) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.reportTrip(rideId: rideId, claim: claim, photos: photos, successResponse: {(response) in
            print(response)
            success("Reporte enviada con éxito")
        }, failureResponse: {(error) in
            failure(error)
        })
    }
    
    func getLinkToShare(rideId: Int, successResponse success: @escaping(_ urlToShare: String) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) {
        MainService.sharedService.getLinkToShare(rideId: rideId, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let url = data["url"]?.stringValue {
                success(url)
            } else {
                failure(NSError(message: "Error al obtener enlace para compartir"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func logOut(successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        MainService.sharedService.logOut(successResponse: {(response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue {
                success(response)
            } else {
                failure(NSError(message: "Error al cerrar sesión"))
            }
        }, failureResponse: {(error) in
            failure(error)
        })
    }
}
