//
//  UserManager.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class UserManager: NSObject {
    
    var currentUser: User {
        let realm = try! Realm()
        return realm.objects(User.self).first ?? User()
    }
    
    private let realm = try! Realm()
    
    //MARK:- Singleton
    static let sharedManager = UserManager()
    
    func login(email: String,
               password: String,
               successResponse success: @escaping(_ user: User) -> Void,
               failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.login(email: email, password: password, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let accessToken = data["access_token"]?.stringValue, let userData = data["user"] {
                print("RESPONSE-----------------------\n\(dictionary)\nRESPONSE-----------------------")
                let user = User(userData, accessToken: accessToken)
                #if USER
                let properRole = UserRole.client
                #endif
                #if DRIVER
                let properRole = UserRole.driver
                #endif
                if user.profile == properRole {
                    try! self.realm.write {
                        self.realm.add(user)
                    }
                    success(user)
                } else {
                    failure(NSError(message: "Credenciales inválidas"))
                }
            } else {
                failure(NSError(message: "Error al obtener iniciar sesión"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getUser(user: User,
                 successResponse success: @escaping(_ user: User) -> Void,
                 failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let accessToken = user.accessToken
        #if DRIVER
        let hasExpiredDocuments = user.hasExpiredDocuments
        let documentsCheckedDate = user.documentsCheckedDate
        #endif
        UserService.sharedService.getUser(user: user, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let userData = data["user"] {
                print("RESPONSE-----------------------\n\(dictionary)\nRESPONSE-----------------------")
                let user = User(userData, accessToken: accessToken)
                #if USER
                let properRole = UserRole.client
                #endif
                #if DRIVER
                let properRole = UserRole.driver
                #endif
                if user.profile == properRole {
                    try! self.realm.write {
                        #if DRIVER
                        user.hasExpiredDocuments = hasExpiredDocuments
                        user.documentsCheckedDate = documentsCheckedDate
                        #endif
                        self.realm.add(user, update: .all)
                    }
                    success(user)
                } else {
                    failure(NSError(message: "Credenciales inválidas"))
                }
            } else {
                failure(NSError(message: "Error al obtener iniciar sesión"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func sendPasswordRecoveryMail(email: String,
                                  successResponse success: @escaping(_ message: String) -> Void,
                                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.sendPasswordRecoveryMail(email: email, successResponse: { (response) in
            if let dictionary = response.dictionary, let message = dictionary["message"]?.stringValue {
                success(message)
            } else {
                failure(NSError(message: "Error al obtener el código de verificación"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func verifyPhone(phoneNumber: String,
                     verificationMode: PhoneVerificationMode,
                     successResponse success: @escaping(_ code: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.verifyPhone(phoneNumber: phoneNumber, verificationMode: verificationMode, successResponse: { (response) in
            let dictionary = response.dictionary
            let data = dictionary?["data"]?.dictionaryValue
            guard let code = data?["code"]?.stringValue else {
                failure(NSError(message: "Error al obtener el código de verificación"))
                return
            }
            print("\(verificationMode == .call ? "📞" : "📨") VERIFICATION CODE -> \(code)")
            success(code)
        }) { (error) in
            failure(error)
        }
    }
    
    func verifyDocumentNumber(documentNumber: String,
                              successResponse success: @escaping(_ names: [String]) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.verifyDocumentNumber(documentNumber: documentNumber, successResponse: { (response) in
            let dictionary = response.dictionary
            let data = dictionary?["data"]?.dictionaryValue
            if let clientData = data?["client"]?.dictionaryValue {
                let firstName = clientData["name"]?.stringValue ?? ""
                let lastName = clientData["last_name"]?.stringValue ?? ""
                success([firstName, lastName])
            } else {
                failure(NSError(message: "Error al verificar número de documento"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func validateDocumentNumber(documentNumber: String,
                                documentType: String,
                                successResponse success: @escaping(_ message: String, _ user: User?) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.validateDocumentNumber(documentNumber: documentNumber, documentType: documentType, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue {
                var user: User? = nil
                if let clientData = data["client"] {
                    user = User(clientData, accessToken: "")
                }
                success("Nº de documento válido", user)
            } else {
                failure(NSError(message: "Error al validar el número de documento"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func register(user: RegistrationUser,
                  successResponse success: @escaping(_ driver: User) -> Void,
                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.registerUser(user, successResponse: { (response) in
            if let dictionary = response.dictionary, let data = dictionary["data"] {
                let accessToken = data["access_token"].stringValue
                let userData = data["user"]
                let user = User(userData, accessToken: accessToken)
                try! self.realm.write {
                    self.realm.add(user)
                }
                success(user)
            } else {
                if let message = response.dictionary?["message"]?.stringValue {
                    failure(NSError(message: message))
                } else {
                    failure(NSError(message: "Error al parsear los datos"))
                }
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func resetPassword(password: String,
                       accessToken: String,
                       successResponse success: @escaping(_ message: String) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.resetPassword(password: password, accessToken: accessToken, successResponse: { (response) in
            if let dictionary = response.dictionary, let status = dictionary["status"]?.stringValue, status.lowercased() == "success" {
                success("Su contraseña se actualizó correctamente")
            } else {
                failure(NSError(message: "Error al actualizar contraseña"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func updatePhone(phone: String,
                     successResponse success: @escaping(_ message: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.updatePhone(phone: phone, userId: currentUser.id, successResponse: { (response) in
            success("Su número celular se actualizó correctamente")
        }) { (error) in
            failure(error)
        }
    }
    
    func updateEmail(email: String,
                     successResponse success: @escaping(_ message: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.updateEmail(email: email, userId: currentUser.id, successResponse: { (response) in
            success("Su correo se actualizó correctamente")
        }) { (error) in
            failure(error)
        }
    }
    
    func updatePassword(oldPassword: String,
                        newPassword: String,
                        confirmPassword: String,
                        successResponse success: @escaping(_ message: String) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.updatePassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword, userId: currentUser.id, successResponse: { (response) in
            success("Su contraseña se actualizó correctamente")
        }) { (error) in
            failure(error)
        }
    }
    
    func setDriverNearToDestination(successResponse success: @escaping(_ message: String) -> Void,
                                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.setDriverNearToDestination(successResponse: { (response) in
            if let dictionary = response.dictionary, let status = dictionary["status"]?.stringValue, status.lowercased() == "success" {
                success("Su connection status se actualizó correctamente")
            } else {
                failure(NSError(message: "Error al actualizar contraseña"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func updatePhoto(_ image: UIImage,
                     user: User,
                     successResponse success: @escaping(_ message: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.updatePhoto(image, user: user, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            print(dictionary)
            if let data = dictionary["data"]?.dictionaryValue, let userData = data["user"]?.dictionaryValue, let photoUrl = userData["photo_url"]?.stringValue {
                if let userToUpdate = self.realm.objects(User.self).first {
                    try! self.realm.write {
                        userToUpdate.photoUrl = photoUrl
                    }
                }
                success("Su foto de perfil se actualizó correctamente")
            } else {
                if let message = response.dictionary?["message"]?.stringValue {
                    failure(NSError(message: message))
                } else {
                    failure(NSError(message: "No se pudo actualizar su foto"))
                }
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func verifyEmail(email: String,
                     successResponse success: @escaping(_ code: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        UserService.sharedService.verifyEmail(email: email, successResponse: { (response) in
            let dictionary = response.dictionary
            let data = dictionary?["data"]?.dictionaryValue
            guard let code = data?["code"]?.stringValue else {
                failure(NSError(message: "Error al obtener el código de verificación"))
                return
            }
            print("\( "📨") VERIFICATION CODE -> \(code)")
            success(code)
        }) { (error) in
            failure(error)
        }
    }
    
    func validateEmail(email: String,
                       successResponse success: @escaping(_ message: String) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) {
        UserService.sharedService.validateEmail(email: email, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if dictionary["data"] != nil {
                success("Email validado correctamente")
            } else {
                failure(NSError(message: "Error al validar email"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
}

