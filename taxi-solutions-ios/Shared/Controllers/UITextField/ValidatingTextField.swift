//
//  ValidatingTextField.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 8/09/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ValidatingTextField: CustomTextField {

    lazy var validatingSwitchButton: SwitchButton = {
        let switchButton = SwitchButton()
        let buttonHeight = self.frame.height / 3
        switchButton.frame = CGRect(x: self.frame.width - buttonHeight - 15, y: (self.frame.height - buttonHeight) / 2, width: buttonHeight, height: buttonHeight)
        switchButton.isImageSwitch = true
        switchButton.isColorSwitch = false
        switchButton.imageOn = #imageLiteral(resourceName: "ic_accept")
        switchButton.imageOff = #imageLiteral(resourceName: "ic_reject")
        switchButton.isUserInteractionEnabled = false
        
        self.addSubview(switchButton)
        return switchButton
    }()
    
    var validated = false {
        didSet {
            validatingSwitchButton.isOn = validated
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.paddingRight = self.frame.height
    }

}
