//
//  CustomTextField.swift
//  Taxi
//
//  Created by PeruApps Dev on 9/4/19.
//  Copyright © 2019 PeruApps. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {

    var padLeft:CGFloat = 0.0
    var padRight:CGFloat = 0.0
    var padTop:CGFloat = 0.0
    var padBottom:CGFloat = 0.0
    
    @IBInspectable var shadow: Bool = false {
        didSet {
            setShadow()
        }
    }
    @IBInspectable var shadowOpacity: Double = 0.2
    @IBInspectable var shadowRadius: Double = 4
    
    @IBInspectable var noWhiteSpace: Bool = false
    @IBInspectable var iconImage: UIImage?
    @IBInspectable var iconImageHeight: Double = 0.0
    @IBInspectable var iconLeft: Bool = true
    
    @IBInspectable open var paddingLeft:CGFloat = 0.0 {
        didSet {
            self.padLeft = self.paddingLeft
        }
    }
    
    @IBInspectable open var paddingRight:CGFloat = 0.0 {
        didSet {
            self.padRight = self.paddingRight
        }
    }
    
    @IBInspectable open var paddingTop:CGFloat = 0.5 {
        didSet {
            self.padTop = self.paddingTop
        }
    }
    
    @IBInspectable open var paddingBottom:CGFloat = 0.0 {
        didSet {
            self.padBottom = self.paddingBottom
        }
    }
    
    @IBInspectable open var borderColor:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    
    @IBInspectable open var borderRadius:CGFloat = 15 {
        didSet {
            self.layer.cornerRadius = borderRadius == 0.1 ? self.frame.height/2 : borderRadius
        }
    }
    
    @IBInspectable open var borderWidth:CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }


    @IBInspectable var titleText: String = ""
    @IBInspectable var addLineView: Bool = false

    open var titleLabel: UILabel!


    lazy var icon: UIImageView = {
        let imageView = UIImageView()
        self.addSubview(imageView)
        return imageView
    }()
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: self.padTop, left: self.padLeft + CGFloat(iconLeft ? iconImageHeight : 0), bottom: self.padBottom, right: self.padRight))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: self.padTop, left: self.padLeft + CGFloat(iconLeft ? iconImageHeight : 0), bottom: self.padBottom, right: self.padRight))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: self.padTop, left: self.padLeft + CGFloat(iconLeft ? iconImageHeight : 0), bottom: self.padBottom, right: self.padRight))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createTitleLabel()
        if let image = iconImage {
            icon.image = image
            icon.contentMode = .scaleAspectFit
            let iconHeight: CGFloat = CGFloat(iconImageHeight)
            self.layoutIfNeeded()
            if iconLeft {
                icon.frame = CGRect(x: (paddingLeft + iconHeight) * 0.5, y: (self.frame.height - iconHeight) * 0.5, width: iconHeight, height: iconHeight)
            } else {
                icon.frame = CGRect(x: self.frame.width - paddingRight, y: ((self.frame.height - iconHeight) * 0.5), width: iconHeight, height: iconHeight)
            }
            self.layoutIfNeeded()
        }
        setShadow()
        self.addTarget(self, action: #selector(removeWhiteSpaces), for: .editingChanged)
//        self.backgroundColor = UIColor.white
        addLineView ? self.addLine() : nil
    }

    fileprivate func createTitleLabel() {
        let titleLabel = UILabel()
        titleLabel.autoresizingMask = [.flexibleHeight]
        titleLabel.font = Fonts.medium(12)
        titleLabel.alpha = 0.0
        titleLabel.textColor = Colors.primary
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 15.0, height: 2.0))
        self.leftView = leftView
        self.leftViewMode = .always
        addSubview(titleLabel)
        self.titleLabel = titleLabel
        let alpha: CGFloat = 1.0
        let frame: CGRect = titleLabelRectForBounds(bounds, editing: true)
        self.titleLabel.alpha = alpha
        self.titleLabel.frame = frame
        self.titleLabel.text = titleText
    }

    open func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {
            return CGRect(x: 0, y: self.frame.height/11, width: bounds.size.width, height: titleHeight())
        }
        return CGRect(x: 25, y: titleHeight(), width: bounds.size.width, height: titleHeight())
    }

    open func titleHeight() -> CGFloat {
        if let titleLabel = titleLabel,
            let font = titleLabel.font {
            return font.lineHeight
        }
        return 15.0
    }

    @objc func removeWhiteSpaces() {
        if noWhiteSpace {
            self.text = self.text?.replacingOccurrences(of: " ", with: "")
        }
    }
    
    private func setShadow() {
        if shadow {
            layer.shadowColor = Colors.secondary.cgColor
            layer.shadowOpacity = Float(shadowOpacity)
            layer.shadowOffset = .zero
            layer.shadowRadius = CGFloat(shadowRadius)
        }
    }
    
}
