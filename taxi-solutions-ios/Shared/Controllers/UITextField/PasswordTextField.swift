//
//  PasswordTextField.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 8/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class PasswordTextField: CustomTextField {
    
    private let feebackGenerator = UIImpactFeedbackGenerator()
    
    lazy var showButton: SwitchButton = {
        let switchButton = SwitchButton()
        let buttonHeight = self.frame.height
        switchButton.frame = CGRect(x: self.frame.width - (buttonHeight * 0.5), y: 0, width: buttonHeight, height: buttonHeight)
        switchButton.isImageSwitch = true
        switchButton.isColorSwitch = false
        switchButton.coloredImage = true
        switchButton.imageOn = UIImage(named:"ic_visible") ?? UIImage()
        switchButton.imageOff = UIImage(named:"ic_visible_not") ?? UIImage()
        switchButton.tintColor = Colors.secondary
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showPassword(_:)))
        switchButton.addGestureRecognizer(tapGesture)
        
        self.addSubview(switchButton)
        return switchButton
    }()
    
    var maskedPassword = false {
        didSet {
            feebackGenerator.impactOccurred()
            showButton.isOn = maskedPassword
            self.isSecureTextEntry = maskedPassword
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        maskedPassword = true
        self.paddingRight = self.frame.height
    }
    
    @objc func showPassword(_ button: UIButton) {
        maskedPassword = !maskedPassword
    }

}
