//
//  ImageCollectionViewCell.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol ImageCollectionViewCellDelegate: class {
    func imageCollectionViewCell(deleteImageFrom cell: ImageCollectionViewCell)
}

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    var delegate: ImageCollectionViewCellDelegate?
    var image = UIImage()
    
    override func awakeFromNib() {
        imageView.image = image
    }
    
    @IBAction func removeImage(_ sender: UIButton) {
        delegate?.imageCollectionViewCell(deleteImageFrom: self)
    }
}
