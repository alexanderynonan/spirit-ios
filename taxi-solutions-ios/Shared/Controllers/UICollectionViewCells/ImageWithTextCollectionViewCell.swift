//
//  ImageWithTextCollectionViewCell.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/14/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ImageWithTextCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageWithTextSwitchButton: ImageSwitchButton!
    
    var imageWithTextSwitch = ImageWithTextSwitch()
    
    override func awakeFromNib() {
        imageWithTextSwitchButton.awakeFromNib()
        imageWithTextSwitchButton.isOn = false
        imageWithTextSwitchButton.isUserInteractionEnabled = false
        imageWithTextSwitchButton.isColorSwitch = true
        imageWithTextSwitchButton.isImageSwitch = true
        imageWithTextSwitchButton.setTitle(imageWithTextSwitch.name, for: .normal)
        imageWithTextSwitchButton.textOn = imageWithTextSwitch.name
        imageWithTextSwitchButton.textOff = imageWithTextSwitch.name
        imageWithTextSwitchButton.imageOn = imageWithTextSwitch.imageOn
        imageWithTextSwitchButton.imageOff = imageWithTextSwitch.imageOff
        imageWithTextSwitchButton.colorOn = imageWithTextSwitch.colorOn
        imageWithTextSwitchButton.colorOff = imageWithTextSwitch.colorOff
        imageWithTextSwitchButton.borderColorOn = imageWithTextSwitch.colorOn
        imageWithTextSwitchButton.borderColorOff = imageWithTextSwitch.colorOn
        imageWithTextSwitchButton.borderWidth = 2
        imageWithTextSwitchButton.setImage(imageWithTextSwitch.imageOn, for: .normal)
    }
}

//by Alexander Ynoñan
class ImageFavoriteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSelect : UIImageView!
    @IBOutlet weak var lblTitle  : UILabel!
    
    var imageWithTextSwitch : ImageWithTextSwitch!{
        didSet{
            self.loadData()
        }
    }
    
    private func loadData(){
        self.imgSelect.image = self.imageWithTextSwitch.state ? self.imageWithTextSwitch.imageOn : self.imageWithTextSwitch.imageOff
        self.backgroundColor = self.imageWithTextSwitch.state ? imageWithTextSwitch.colorOn : imageWithTextSwitch.colorOff
        self.lblTitle.text = self.imageWithTextSwitch.name
        self.lblTitle.textColor = self.imageWithTextSwitch.state ? imageWithTextSwitch.colorOff : imageWithTextSwitch.colorOn
    }
    
    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.cornerRadius = 10
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = CGSize(width: 0, height: 0.1)
        self.layer.masksToBounds = false
    }
}
