//
//  DocumentTypeCollectionViewCell.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 2/16/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class DocumentTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var documentTypeSwitchButton: SwitchButton!
    
    var documentType = DocumentType()
    
    override func awakeFromNib() {
        documentTypeSwitchButton.isUserInteractionEnabled = false
        documentTypeSwitchButton.setTitle(documentType.name, for: .normal)
        documentTypeSwitchButton.textOn = documentType.name
        documentTypeSwitchButton.textOff = documentType.name
    }
}
