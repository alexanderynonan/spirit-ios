//
//  TripTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class RideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var mapImageView: UIImageView!
    
    private var cancelledBeforeService: Bool {
        guard let rideRecord = rideRecord else { return false }
        return rideRecord.status.lowercased() == "Cancelado".lowercased()
    }
    
    private var rideCancelled: Bool {
        guard let rideRecord = rideRecord else { return false }
        return rideRecord.status.lowercased().contains("Cancelado".lowercased())
    }
    
    var rideRecord: RideRecord?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        guard let rideRecord = self.rideRecord else { return }
        
        DispatchQueue.main.async {
            self.dateLabel.text = rideRecord.rideAtString
            self.amountLabel.text = self.cancelledBeforeService ? rideRecord.status : String(format: "S/ %.2f", rideRecord.totalAmount.double)
            self.amountLabel.textColor = self.cancelledBeforeService ? Colors.error : Colors.primary
            self.stateLabel.text = self.cancelledBeforeService ? "" : rideRecord.status
            self.stateLabel.textColor = self.cancelledBeforeService ? Colors.error : Colors.primary.withAlphaComponent(0.5)
            self.rateView.isHidden = self.cancelledBeforeService ? true : false
            self.rateView.rating = rideRecord.userRating.double
            self.mapImageView.sd_setImage(with: rideRecord.photoUrl.url, placeholderImage: UIImage(named: "img_map"), completed: nil)
        }
    }
}
