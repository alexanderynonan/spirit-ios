//
//  InvitationTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

class ReferralTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var stateIconImageView: UIImageView!
    
    var referredFriend: ReferredFriend?
    var promotion: Promotion?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        loadData()
    }
    
    func loadData() {
        #if DRIVER
        amountLabel.textColor = Colors.primary
        #else
        amountLabel.textColor = Colors.secondary
        #endif
        
        if let referredFriend = referredFriend {
            photoImageView.sd_setImage(with: referredFriend.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached, .allowInvalidSSLCertificates, .retryFailed, .progressiveLoad, .continueInBackground], completed: nil)
            nameLabel.text = referredFriend.name
            #if DRIVER
            dateLabel.text = "Se registró: \(referredFriend.referredAt)"
            counterLabel.isHidden = true
            #else
            dateLabel.text = "Vence: \(referredFriend.expiresAt)"
            counterLabel.isHidden = false
            counterLabel.text = referredFriend.usage
            #endif
            amountLabel.text = "PEN \(referredFriend.amount)"
        }
        if let bonus = promotion {
            photoImageView.sd_setImage(with: bonus.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_promo"), options: [.refreshCached, .allowInvalidSSLCertificates, .retryFailed, .progressiveLoad, .continueInBackground], completed: nil)
            nameLabel.text = bonus.name
            dateLabel.text = "Vence: \(bonus.expiresAt)"
            amountLabel.text = "PEN \(bonus.amount)"
            counterLabel.isHidden = false
            counterLabel.text = "\(bonus.uses)/\(bonus.totalUsages) viajes"
        }
    }

}
