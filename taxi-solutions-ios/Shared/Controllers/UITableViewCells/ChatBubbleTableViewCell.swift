//
//  ChatUserBubbleTableViewCell.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/8/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import Foundation

enum BubbleUserType {
    case user
    case otherUser
}

class ChatBubbleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bubbleView: SpecificRoundedCornersView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var bubbleUserType: BubbleUserType = .user
    var message = Message()
    var ownBubble = false

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure() {
        messageLabel.text = message.message
        
        let hasHorizontalConstraints = bubbleView.constraintsAffectingLayout(for: .horizontal).count != 0
        bubbleView.translatesAutoresizingMaskIntoConstraints = false
        
        if !hasHorizontalConstraints {
            if ownBubble {
                bubbleView.leftAnchor.constraint(greaterThanOrEqualTo: contentView.leftAnchor, constant: 60).isActive = true
                bubbleView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
            } else {
                bubbleView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
                bubbleView.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor, constant: -60).isActive = true
            }
        }
        
        messageLabel.textColor = ownBubble ? Colors.background : Colors.secondary
        bubbleView.backgroundColor = ownBubble ? Colors.primary : Colors.background
        bubbleView.shadow = ownBubble ? true : false
        bubbleView.clipsToBounds = ownBubble ? true : false
        bubbleView.topLeftCorner = ownBubble ? true : false
        bubbleView.topRightCorner = ownBubble ? false : true
        bubbleView.awakeFromNib()
    }
}
