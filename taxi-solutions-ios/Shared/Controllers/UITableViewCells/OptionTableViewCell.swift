//
//  DrivingLicenseCategoryTableViewCell.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 2/15/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol OptionTableViewCellDelegate: class {
    func didSelectOption(tableViewCell: OptionTableViewCell)
}

class OptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var optionNameLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    var option = OptionToSelect()
    var fromDidSelect = false
    var isSelectedOption = false {
        didSet {
            self.configureCellForSelection()
        }
    }
    
    var delegate: OptionTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        optionNameLabel.text = option.name
    }
    
    func configureCellForSelection() {
        DispatchQueue.main.async {
            UIView.transition(with: self.containerView, duration: 0.35, options: [.transitionCrossDissolve], animations: {
                self.containerView.shadow = self.isSelectedOption ? true : false
                self.containerView.backgroundColor = UIColor(named: self.isSelectedOption ? "background" : "cell_background")
                self.containerView.borderWidth = self.isSelectedOption ? 2.0 : 0.0
                self.optionNameLabel.textColor = UIColor(named: self.isSelectedOption ? "primary" : "text")
                self.checkImageView.isHidden = self.isSelectedOption ? false : true
                self.containerView.clipsToBounds = self.isSelectedOption ? false : true
                let zoomScale: CGFloat = self.isSelectedOption ? 1.05 : 1.0
                self.containerView.transform = CGAffineTransform(scaleX: zoomScale, y: zoomScale)
            }, completion: { _ in
                if self.fromDidSelect {
                    self.delegate?.didSelectOption(tableViewCell: self)
                }
                self.fromDidSelect = false
            })
        }
    }

}
