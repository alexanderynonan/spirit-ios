//
//  TripPointTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

enum PointType {
    case initial
    case middle
    case final
}

class TripPointTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pointIconImageView: UIImageView!
    @IBOutlet weak var fromPointView: UIView?
    @IBOutlet weak var toPointView: UIView?
    @IBOutlet weak var pointNameLabel: UILabel?
    @IBOutlet weak var pointAddressLabel: UILabel!
    @IBOutlet weak var bottomSeparator: UIView?
    var pointType: PointType = .middle
    var pointAddress = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        pointAddressLabel.text = pointAddress
        configurePointType()
    }
    
    func configurePointType() {
        switch pointType {
        case .initial:
            pointNameLabel?.text = "Recojo"
            pointIconImageView.image = #imageLiteral(resourceName: "ic_spot_origin")
            pointIconImageView.contentMode = .scaleAspectFit
            fromPointView?.isHidden = true
            toPointView?.isHidden = false
            bottomSeparator?.isHidden = false
        case .middle:
            pointNameLabel?.text = "Recojo"
            pointIconImageView.image = #imageLiteral(resourceName: "ic_spot_origin")
            pointIconImageView.contentMode = .scaleAspectFit
            fromPointView?.isHidden = false
            toPointView?.isHidden = false
            bottomSeparator?.isHidden = false
        case .final:
            pointNameLabel?.text = "Destino"
            pointIconImageView.image = UIImage(named: "ic_mini_spot")
            pointIconImageView.contentMode = .scaleAspectFit
            fromPointView?.isHidden = false
            toPointView?.isHidden = true
            bottomSeparator?.isHidden = true
        }
    }

}
