//
//  PaymentMethodTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol PaymentMethodTableViewCellDelegate {
    func paymentMethodTableViewCellDelegate(showDetailOfCell cell: PaymentMethodTableViewCell)
}

class PaymentMethodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var actionIconImageView: UIImageView!
    var paymentMethod: PaymentMethod? = .none
    var card: TokenizedCard?
    var delegate: PaymentMethodTableViewCellDelegate?
    
    override var isSelected: Bool {
        didSet {
            DispatchQueue.main.async {
                self.containerView.shadow = self.isSelected ? true : false
                self.containerView.borderWidth = 0.0
                self.containerView.borderColor = .clear
                self.containerView.clipsToBounds = self.isSelected ? false : true
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        selectionStyle = .none
        self.containerView.clipsToBounds = true
        
        DispatchQueue.main.async {
            
            switch self.paymentMethod {
            case .cash:
                self.containerView.shadow = false
                self.numberLabel.text = "Efectivo"
                self.numberLabel.textColor = Colors.primary
                self.actionIconImageView.isHidden = true
                self.iconImageView.image = #imageLiteral(resourceName: "ic_cash")
            case .creditCard:
                #if DRIVER
                self.containerView.shadow = true
                #endif
                #if USER
                self.containerView.shadow = false
                #endif
                self.numberLabel.text = "•••• •••• •••• 8996"
                self.numberLabel.textColor = Colors.primary
                self.actionIconImageView.isHidden = false
                self.iconImageView.image = #imageLiteral(resourceName: "ic_credit_card")
                self.actionIconImageView.image = #imageLiteral(resourceName: "ic_forward")
            case .none:
                self.containerView.shadow = false
                #if DRIVER
                self.numberLabel.text = "Recargar con otra tarjeta"
                #endif
                #if USER
                self.numberLabel.text = "Añadir otra tarjeta"
                #endif
                self.numberLabel.textColor = Colors.primary
                self.actionIconImageView.isHidden = false
                self.actionIconImageView.image = #imageLiteral(resourceName: "ic_add")
            }
        }
    }
    
    func loadContent() {
        guard let card = card else { return }
        DispatchQueue.main.async {
            self.numberLabel.text = card.maskedCard
        }
    }
    
    @IBAction func showDetail(sender: UIControl) {
        delegate?.paymentMethodTableViewCellDelegate(showDetailOfCell: self)
    }
    
}
