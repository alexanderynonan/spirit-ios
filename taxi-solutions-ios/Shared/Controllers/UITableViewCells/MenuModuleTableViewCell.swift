//
//  MenuModuleTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class MenuModuleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!


    var module = MenuModule()

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        iconImageView.image = module.icon
        titleLabel.text = module.title
    }

}
