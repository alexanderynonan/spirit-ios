//
//  FAQTableViewCell.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {
    
    @IBOutlet weak var questionLabel: UILabel!
    var name = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        questionLabel.text = name
    }

}
