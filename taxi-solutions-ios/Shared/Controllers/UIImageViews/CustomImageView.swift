//
//  CustomImageView.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

@IBDesignable
class CustomImageView: UIImageView {

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            self.layoutIfNeeded()
            self.layer.cornerRadius = cornerRadius == 0.1 ? (self.frame.height / 2) : cornerRadius
        }
    }
    @IBInspectable var coloredImage: Bool = false
    
    override func awakeFromNib() {
        self.layoutIfNeeded()
        self.layer.cornerRadius = cornerRadius == 0.1 ? (self.frame.height / 2) : cornerRadius
        self.image = image?.withRenderingMode(coloredImage ? .alwaysTemplate : .alwaysOriginal)
    }
}
