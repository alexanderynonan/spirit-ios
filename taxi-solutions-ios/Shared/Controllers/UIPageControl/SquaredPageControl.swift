//
//  SquaredPageControl.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/7/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class SquaredPageControl: UIPageControl {
    
    @IBInspectable var dotHeight: CGFloat = 6.0
    @IBInspectable var dotWidth: CGFloat = 20.0
    @IBInspectable var spacing: CGFloat = 3.0
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard !subviews.isEmpty else { return }
        
        var total: CGFloat = 0
        
        subviews.forEach { view in
            view.layer.cornerRadius = dotHeight / 2
            view.frame = CGRect(x: total, y: frame.size.height / 2 - dotHeight / 2, width: dotWidth, height: dotHeight)
            total += dotWidth + spacing
        }
        
        total -= spacing
        
        frame.origin.x = frame.origin.x + frame.size.width / 2 - total / 2
        frame.size.width = total
    }
    
}
