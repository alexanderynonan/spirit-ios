//
//  TopRoundedCornerView.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class TopRoundedCornerView: CustomView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorners([.topLeft, .topRight], radius: cornerRadius == 0.1 ? self.frame.size.height/2 : cornerRadius)
    }
    
    override func setShadow() {
        let contactRect = CGRect(x: -(self.frame.width * 0.25), y: -1, width: self.frame.width * 1.5, height: 5)
        layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
        layer.shadowColor = Colors.secondary.cgColor
        layer.shadowOpacity = Float(shadowOpacity)
        layer.shadowOffset = .zero
        layer.shadowRadius = CGFloat(shadowRadius)
    }
}
