//
//  GradientView.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2019 Macbook Pro Papps. All rights reserved.
//

import UIKit

class GradientView: UIView {

    override func awakeFromNib() {
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.6)
        let color = self.backgroundColor ?? .clear
        gradient.colors = [color.withAlphaComponent(1.0).cgColor, color.withAlphaComponent(0.6).cgColor, color.withAlphaComponent(0).cgColor]
        gradient.locations = [NSNumber(value: 0.0),NSNumber(value: 0.5),NSNumber(value: 1.0)]
        gradient.frame = self.bounds
        self.layer.mask = gradient
    }

}
