//
//  BlurredView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 6/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

@IBDesignable
class BlurredView: UIView {

    override func awakeFromNib() {
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibility.isReduceTransparencyEnabled {
            self.backgroundColor = .clear

            let blurEffect = UIBlurEffect(style: .regular)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            self.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            self.backgroundColor = .black
        }
    }

}
