//
//  CustomView.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2019 PeruApps. All rights reserved.
//

import UIKit

@IBDesignable
class CustomView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 15 {
        didSet {
            self.layer.cornerRadius = cornerRadius == 0.1 ? self.frame.height/2 : cornerRadius
            self.layoutIfNeeded()
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var shadow: Bool = false {
        didSet {
            setShadow()
        }
    }
    @IBInspectable var shadowOpacity: Double = 0.35
    @IBInspectable var shadowRadius: Double = 5
    
    
    override func awakeFromNib() {
        self.layoutIfNeeded()
        layer.cornerRadius = cornerRadius == 0.1 ? frame.height/2 : cornerRadius
        layer.borderWidth = borderWidth
        configureColor()
        setShadow()
    }
    
    func configureColor() {
        layer.borderColor = borderColor.cgColor
    }
    
    func setShadow() {
        if shadow {
            layer.shadowColor = Colors.primary.cgColor
            layer.shadowOpacity = Float(shadowOpacity)
            layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            layer.shadowRadius = CGFloat(shadowRadius)
        }
    }

}
