//
//  SpecificRoundedCornersView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/8/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

@IBDesignable
class SpecificRoundedCornersView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 15
    
    //MARK: - IBOutlet
    @IBInspectable var topLeftCorner: Bool = false
    @IBInspectable var topRightCorner: Bool = false
    @IBInspectable var bottomLeftCorner: Bool = false
    @IBInspectable var bottomRightCorner: Bool = false
    
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var shadow: Bool = false {
        didSet {
            setShadow()
        }
    }
    @IBInspectable var shadowOpacity: Double = 0.2
    @IBInspectable var shadowRadius: Double = 4
    
    override func awakeFromNib() {
        //CORNERS
        var cornersToRound: UIRectCorner = UIRectCorner()
        if topLeftCorner {
            cornersToRound.insert(.topLeft)
        }
        if topRightCorner {
            cornersToRound.insert(.topRight)
        }
        if bottomLeftCorner {
            cornersToRound.insert(.bottomLeft)
        }
        if bottomRightCorner {
            cornersToRound.insert(.bottomRight)
        }
        roundCorners(cornersToRound, radius: cornerRadius == 0.1 ? self.frame.size.height/2 : cornerRadius)
        
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        setShadow()
    }
    
    func setShadow() {
        if shadow {
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOpacity = Float(shadowOpacity)
            layer.shadowOffset = .zero
            layer.shadowRadius = CGFloat(shadowRadius)
        }
    }
    
}
