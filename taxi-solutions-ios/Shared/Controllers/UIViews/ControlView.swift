//
//  ControlView.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 20/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

///Vista que pasa el evento de touch a la vista del controlador especificado en el touchDelegate
class ControlView: UIView {

    weak var touchDelegate: UIView? = nil

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let view = super.hitTest(point, with: event) else {
            return nil
        }

        guard view === self, let point = touchDelegate?.convert(point, from: self) else {
            return view
        }

        return touchDelegate?.hitTest(point, with: event)
    }
}
