//
//  CodeView.swift
//  TaxiSvico
//
//  Created by Yonny Rivera on 8/12/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class CodeView: UIView {
    
    @IBInspectable var code: Int = 0
    
    var verificationCodeTextFields = [UITextField]()
    var valueCode = ""
    var verificationCode: String  {
        get {
            return self.valueCode
        }
        set {
            valueCode = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.layoutIfNeeded()
            let spaces = self.code - 1
            let spaceCodeView: CGFloat = CGFloat(spaces * 6)
            let codeWith = (self.frame.width  - spaceCodeView) / CGFloat(self.code)
            let margin: CGFloat = codeWith / 2
            let lineWidth = self.frame.width - margin * 2.0

            let interval = lineWidth / CGFloat(spaces)
            for i in 0..<self.code {
                let verificationCodeTextField = UITextField()
                verificationCodeTextField.backgroundColor = #colorLiteral(red: 0.3286978602, green: 0.5079771876, blue: 0.9390231371, alpha: 0.08)
                verificationCodeTextField.layer.borderWidth = 0
                verificationCodeTextField.layer.cornerRadius = 10
                verificationCodeTextField.tag = i
                verificationCodeTextField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
                verificationCodeTextField.textAlignment = .center
                
                verificationCodeTextField.frame = CGRect(x: (interval * CGFloat(i)) + (margin - codeWith/2.0), y: self.frame.height / 2 - codeWith/2.0, width: codeWith, height: codeWith)
                self.verificationCodeTextFields.append(verificationCodeTextField)
                verificationCodeTextField.delegate = self
                verificationCodeTextField.awakeFromNib()
                self.addSubview(verificationCodeTextField)
            }
        }
    }
}

extension CodeView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            textField.text = ""
            
            self.verificationCodeTextFields.forEach {
                if $0.tag == textField.tag - 1 {
                    $0.becomeFirstResponder()
                }
            }
            return false
        }
        if string.count == 1 {
            if textField.text?.count == 1 {
                if verificationCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                    let nextTextField = verificationCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                    nextTextField?.text = string
                    nextTextField?.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                    verificationCode = ""
                    verificationCodeTextFields.forEach { textField in
                        verificationCode += textField.text ?? ""
                    }
                }
            } else {
                textField.text = string
            }
        } else {
            if let character = string.first {
                textField.text = String(character)
            }
        }
        return false
    }
}
