//
//  PaymentDetailView.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class PaymentDetailView: UIView {
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var baseAmountLabel: UILabel!
    @IBOutlet weak var exceedTimeAmountLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var pendingAmountLabel: UILabel!
    @IBOutlet weak var discountCodeLabel: UILabel!
    @IBOutlet weak var discountsLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    var paymentDetail: PaymentDetail? {
        didSet {
            loadContent()
        }
    }
    var preferredHeight: CGFloat {
        print("💰 ALTO DE VISTA DE DETALLE PAGO -> \((CGFloat(visibleLabels - 1) * 28.0) + 80.0)")
        return (CGFloat(visibleLabels) * 25.0) + 60.0
    }
    var showPaymentMethod = true {
        didSet {
            paymentDetail != nil ? loadContent() : ()
        }
    }
    var visibleLabels = 0
    
    func loadContent() {
        guard let paymentDetail = self.paymentDetail else { return }
        totalAmountLabel.text = "\(String(format: "PEN %.2f", paymentDetail.totalAmount.double))"
        baseAmountLabel.text = "\(String(format: "%.2f", paymentDetail.baseAmount.double))"
        discountsLabel.text = "\(String(format: "%.2f", paymentDetail.discount.double))"
        discountCodeLabel.text = paymentDetail.discountCode.hiddenCharacters(1)
        #if DRIVER
        commissionLabel.text = "\(String(format: "%.2f", paymentDetail.commission.double))"
        #else
        commissionLabel.text = ""
        #endif
        pendingAmountLabel.text = "\(String(format: "%.2f", paymentDetail.pendingAmount.double))"
        exceedTimeAmountLabel.text = paymentDetail.exceedTimeAmount.double == 0.0 ? "" : "\(String(format: "%.2f", paymentDetail.exceedTimeAmount.double))"
        paymentMethodLabel.text = showPaymentMethod ? paymentDetail.paymentType : ""
        
        hideViewIfEmpty()
    }
    
    func hideViewIfEmpty() {
        visibleLabels = 0
        let labels = [totalAmountLabel, baseAmountLabel, exceedTimeAmountLabel, commissionLabel, pendingAmountLabel, discountCodeLabel, discountsLabel, paymentMethodLabel]
        labels.forEach {
            var empty = $0?.text == "" || $0?.text == "0.00"
            $0?.superview?.isHidden = empty
            self.visibleLabels += empty ? 0 : 1
        }
    }
}
