//
//  ChatInputView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/12/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ChatInputView: UIView {
    
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var inputTextViewBottom: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!

    override func awakeFromNib() {
        print("CHAT INPUT VIEW")
        autoresizingMask = [.flexibleHeight]
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        
    }

}
