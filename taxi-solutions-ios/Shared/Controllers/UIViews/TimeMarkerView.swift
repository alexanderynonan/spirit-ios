//
//  TimeMarkerView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 11/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class TimeMarkerView: UIView {

    @IBOutlet weak var timeLabel: UILabel!
    
    func setTime(_ time: String) {
        timeLabel.text = time.firstWord
    }

}
