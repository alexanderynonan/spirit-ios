//
//  CustomButton.swift
//  Michelle Belau
//
//  Created by PeruApps Dev on 9/4/19.
//  Copyright © 2019 PeruApps. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomButton: UIButton {

    @IBInspectable var borderRadius: CGFloat = 25
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var backgColor: UIColor = Colors.secondary
    
    @IBInspectable var topMargin: CGFloat = 0.0 {
        didSet {
            setMargins()
        }
    }
    
    @IBInspectable var bottomMargin: CGFloat = 0.0 {
        didSet {
            setMargins()
        }
    }
    
    @IBInspectable var leftMargin: CGFloat = 0.0 {
        didSet {
            setMargins()
        }
    }
    
    @IBInspectable var rightMargin: CGFloat = 0.0 {
        didSet {
            setMargins()
        }
    }
    
    @IBInspectable var shadow: Bool = false {
        didSet {
            setShadow()
        }
    }

    @IBInspectable var shadowOpacity: Double = 0.2
    @IBInspectable var shadowRadius: Double = 4
    
    @IBInspectable var coloredImage: Bool = false
    
    @IBInspectable var isUnderlined: Bool = false
    
    @IBInspectable var underlinedText: String = ""

    @IBInspectable var defaultTextSize: Bool = true
    
    override func awakeFromNib() {
        let image = self.imageView?.image?.withRenderingMode(coloredImage ? .alwaysTemplate : .alwaysOriginal)
        self.setImage(image, for: .normal)
        self.imageView?.tintColor = self.tintColor
        
        self.imageView?.contentMode = .scaleAspectFit
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        if isUnderlined, let titleText = self.titleLabel?.text {
            let underlineAttributes: [NSAttributedString.Key: Any] = [
                .font: self.titleLabel?.font,
                .foregroundColor: self.titleLabel?.textColor,
                .underlineStyle: NSUnderlineStyle.single.rawValue]
            
            let attributeString = NSMutableAttributedString(string: self.titleLabel?.text ?? "",
                                                            attributes: [:])
            if let range = titleText.range(of: underlinedText != "" ? underlinedText : titleText) {
                attributeString.addAttributes(underlineAttributes, range: titleText.nsRange(from: range))
                attributeString.addAttributes([.font: UIFont.boldSystemFont(ofSize: self.titleLabel?.font.pointSize ?? 14)], range: titleText.nsRange(from: range))
            }
            
            self.setAttributedTitle(attributeString, for: .normal)
        }
        self.titleLabel?.font = Fonts.medium(defaultTextSize ? 18 : self.titleLabel?.font.pointSize ?? 12)
        self.layer.cornerRadius = borderRadius == 0 ? self.frame.height/2 : borderRadius
        self.layer.backgroundColor = self.currentTitle?.isEmpty ?? true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)  : backgColor.cgColor
    }
    
    private func setMargins() {
        self.contentEdgeInsets = UIEdgeInsets(top: topMargin, left: leftMargin, bottom: bottomMargin, right: rightMargin)
    }
    
    private func setShadow() {
        if shadow {
            layer.shadowColor = Colors.secondary.cgColor
            layer.shadowOpacity = Float(shadowOpacity)
            layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
            layer.shadowRadius = CGFloat(shadowRadius)
        }
    }

}
