//
//  ImageSwitchButton.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/14/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

@IBDesignable
class ImageSwitchButton: SwitchButton {
    @IBInspectable var rightIconMargin: CGFloat = 0.0
    @IBInspectable var leftIconMargin: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageEdgeInsets = UIEdgeInsets(top: 0, left: leftIconMargin, bottom: 0, right: rightIconMargin)
    }
}
