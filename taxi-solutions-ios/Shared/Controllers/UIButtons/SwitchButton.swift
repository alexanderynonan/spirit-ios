//
//  SwitchButton.swift
//  Caja Arequipa
//
//  Created by Juan Carlos Guillén Castro on 7/2/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

protocol SwitchButtonDelegate {
    func switchButton(switchButton: SwitchButton, statusShouldChange: Bool) -> Bool
    func switchButton(switchButton: SwitchButton, statusDidChange: Bool)
}

@IBDesignable
class SwitchButton: CustomButton {

    @IBInspectable var isOn: Bool = false {
        didSet {
            DispatchQueue.main.async {
                UIView.transition(with: self, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                    if self.isColorSwitch {
                        //THE COLORS OF BUTTON CHANGES AND THE TEXT IF SPECIFIED
                        self.backgroundColor = self.isOn ? self.colorOn : self.colorOff
                        self.setTitleColor(self.isOn ? self.textColorOn : self.textColorOff, for: .normal)
                        self.setTitle(self.switchText && self.isOn ? self.textOn : self.textOff, for: .normal)
                        self.borderColor = self.isOn ? self.borderColorOn : self.borderColorOff
                    }
                    if self.isImageSwitch {
                        //ONLY THE IMAGE CHANGES
                        let imageOn = self.imageOn.withRenderingMode(self.coloredImage ? .alwaysTemplate : .alwaysOriginal)
                        let imageOff = self.imageOff.withRenderingMode(self.coloredImage ? .alwaysTemplate : .alwaysOriginal)
                        self.setImage(self.isOn ? imageOn : imageOff, for: .normal)
                    }
                }, completion: nil)
            }
        }
    }
    @IBInspectable var isColorSwitch: Bool = false
    @IBInspectable var isImageSwitch: Bool = false
    @IBInspectable var switchText: Bool = false
    @IBInspectable var imageOn: UIImage = UIImage(named: "checked-gray") ?? UIImage()
    @IBInspectable var imageOff: UIImage = UIImage(named: "unchecked-gray") ?? UIImage()
    @IBInspectable var colorOn: UIColor = #colorLiteral(red: 0.4039215686, green: 0.7137254902, blue: 0.7176470588, alpha: 1)
    @IBInspectable var colorOff: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    @IBInspectable var borderColorOn: UIColor = #colorLiteral(red: 0.4039215686, green: 0.7137254902, blue: 0.7176470588, alpha: 1)
    @IBInspectable var borderColorOff: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    @IBInspectable var textColorOn: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    @IBInspectable var textColorOff: UIColor = #colorLiteral(red: 0.08235294118, green: 0.1490196078, blue: 0.3294117647, alpha: 1)
    @IBInspectable var textOn: String = ""
    @IBInspectable var textOff: String = ""
    @IBInspectable var vibrate: Bool = false
    @IBInspectable var animate: Bool = false
    var delegate: SwitchButtonDelegate?
    var feedbackGenerator = UIImpactFeedbackGenerator()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView?.contentMode = .scaleAspectFit
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changeState))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func changeState() {
        if vibrate { feedbackGenerator.impactOccurred() }
        if delegate?.switchButton(switchButton: self, statusShouldChange: true) ?? true {
            isOn = !isOn
            delegate?.switchButton(switchButton: self, statusDidChange: true)
            
            if self.animate && self.isOn {
                UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                    self.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                }) { _ in
                    UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        self.transform = .identity
                    }, completion: nil)
                }
            }
        }
    }

}
