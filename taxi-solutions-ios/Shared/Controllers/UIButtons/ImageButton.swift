//
//  ImageButton.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ImageButton: CustomButton {
    @IBInspectable var rightIconMargin: CGFloat = 0.0
    @IBInspectable var leftIconMargin: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageEdgeInsets = UIEdgeInsets(top: 0, left: leftIconMargin, bottom: 0, right: rightIconMargin)
    }
}
