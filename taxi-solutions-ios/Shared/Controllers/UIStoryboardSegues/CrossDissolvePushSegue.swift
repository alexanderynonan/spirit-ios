//
//  CrossDissolvePushSegue.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class CrossDissolvePushSegue: UIStoryboardSegue {
    override func perform() {
        UIView.transition(with: UIApplication.shared.keyWindow ?? UIWindow(), duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.source.navigationController?.pushViewController(self.destination, animated: false)
            self.source.view.layoutIfNeeded()
        }, completion: nil)
    }
}
