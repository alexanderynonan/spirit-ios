//
//  PannableViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 10/21/19.
//  Copyright © 2019 PeruApps. All rights reserved.
//

import UIKit

protocol PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController)
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool)
}

class PannableViewController: UIViewController {
    
    var modalHeight: CGFloat = 0.0
    var viewToPan: UIView = UIView() {
        didSet {
            modalHeight = viewToPan.frame.size.height
        }
    }
    var panGestureRecognizer: UIPanGestureRecognizer?
    var originalPosition: CGPoint?
    var currentPositionTouched: CGPoint?
    var containerViewController: UIViewController?
    var panDelegate: PannableViewControllerDelegate?
    var isPanEnable = true {
        didSet {
            guard let gesture = panGestureRecognizer else { return }
            if isPanEnable {
                view.addGestureRecognizer(gesture)
            } else {
                view.removeGestureRecognizer(gesture)
            }
        }
    }
    var originY: CGFloat = 0.0
    var manualDismissEnabled = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        if isPanEnable {
            view.addGestureRecognizer(panGestureRecognizer!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        originY = getFrameInParentView().origin.y
    }
    
    @IBAction func close() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.transform = CGAffineTransform(translationX: 0, y: self.viewToPan.frame.size.height)
                self.panDelegate?.pannableViewController(gesture: UIPanGestureRecognizer(), willDismissViewController: self)
            }) { (_) in
                self.dismiss(animated: true) {
                    self.panDelegate?.pannableViewController(gesture: UIPanGestureRecognizer(), viewController: self, dismissed: true)
                }
            }
        }
    }
    
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: view)
        
        if panGesture.state == .began {
            originalPosition = view.center
            currentPositionTouched = panGesture.location(in: view)
        } else if panGesture.state == .changed {
            self.panDelegate?.pannableViewController(gesture: panGesture, viewController: self, dismissed: false)
            if translation.y > -20 {
                view.frame.origin = CGPoint(
                    x: 0,
                    y: translation.y
                )
            }
        } else if panGesture.state == .ended {
//            let velocity = panGesture.velocity(in: view)
            if /*velocity.y >= 2500 ||*/ translation.y > 0 && self.manualDismissEnabled {
                self.panDelegate?.pannableViewController(gesture: panGesture, viewController: self, dismissed: true)
                UIView.animate(withDuration: 0.2
                    , animations: {
                        self.view.frame.origin = CGPoint(
                            x: self.view.frame.origin.x,
                            y: self.view.frame.size.height
                        )
                }, completion: { (isCompleted) in
                    if isCompleted {
                        self.dismiss(animated: false, completion: nil)
                    }
                })
            } else {
                self.panDelegate?.pannableViewController(gesture: panGesture, viewController: self, dismissed: false)
                UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 4.0, options: .curveEaseInOut, animations: {
                    self.view.center = self.originalPosition!
                })
            }
        }
    }
    
    func getFrameInParentView() -> CGRect {
        guard let view = containerViewController?.view else { return .zero }
        return self.viewToPan.convert(self.viewToPan.frame, to: view)
    }
}
