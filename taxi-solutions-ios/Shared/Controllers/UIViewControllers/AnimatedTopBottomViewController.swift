//
//  AnimatedTopBottomViewController.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 17/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class AnimatedTopBottomViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: CustomView!
    
    override func viewDidLoad() {
        self.modalTransitionStyle = .crossDissolve
        self.view.backgroundColor = .clear
        self.topView.transform = CGAffineTransform(translationX: 0, y: -self.topView.frame.height)
        self.bottomView.transform = CGAffineTransform(translationX: 0, y: self.bottomView.frame.height)
//        self.topView.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.transition(with: self.view, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            self.bottomView.transform = .identity
            self.topView.transform = .identity
//            self.topView.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func dismiss() {
        UIView.transition(with: self.view, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            self.topView.transform = CGAffineTransform(translationX: 0, y: -self.topView.frame.height)
            self.bottomView.transform = CGAffineTransform(translationX: 0, y: self.bottomView.frame.height)
            self.view.layoutIfNeeded()
        }) { _ in
            super.dismiss()
        }
    }

}
