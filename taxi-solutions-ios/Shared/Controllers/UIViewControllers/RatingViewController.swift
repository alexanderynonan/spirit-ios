//
//  RatingViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 7/8/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
import IQKeyboardManager

protocol RatingViewControllerDelegate: class {
    func ratingViewController(didSentRating: Double, inViewController: RatingViewController)
}

class RatingViewController: AnimatedTopBottomViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var placeholderTextView: UITextView!
    @IBOutlet weak var commentaryTextView: UITextView!
    
    var delegate: RatingViewControllerDelegate?
    
    var userToRate: UserToRate?
    var descriptionText = ""
    
    #if DRIVER
    let roleToRate = "cliente"
    #endif
    
    #if USER
    let roleToRate = "conductor"
    #endif
    
    class func instantiate(in viewController: UIViewController, userToRate: UserToRate?, description: String, delegate: RatingViewControllerDelegate? = nil) -> RatingViewController? {
        guard let vc = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "RatingViewController") as? RatingViewController else { return nil }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.userToRate = userToRate
        vc.descriptionText = description
        vc.delegate = delegate
        viewController.present(vc, animated: true, completion: nil)
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func loadContent() {
        descriptionLabel.text = descriptionText
        guard let userToRate = userToRate else { return }
        imageView.sd_setImage(with: userToRate.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached]) { (image, error, _, _) in
            if image == nil {
                self.imageView.image = #imageLiteral(resourceName: "img_empty_user")
            }
        }
        userRatingLabel.text = userToRate.rating.double.oneDecimalString
        nameLabel.text = userToRate.clientName
        titleLabel.text = "Califica al \(roleToRate)"
    }
    
    @objc func setPlaceholder(_ notification: Notification) {
        let hidePlaceholder = notification.name == UIResponder.keyboardWillShowNotification ? true : commentaryTextView.text != "" ? true : false
        UIView.animate(withDuration: 0.25, animations: {
            self.placeholderTextView.isHidden = hidePlaceholder
        }, completion: nil)
    }
    
    @IBAction func rateUser() {
        guard let userToRate = userToRate else { return }
        self.startLoader()
        MainManager.sharedManager.rateUser(userToRate: userToRate, rating: ratingView.rating, commentary: commentaryTextView.text == "" ? nil : commentaryTextView.text, successResponse: { (message) in
            self.stopLoader()
            self.dismiss()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
}
