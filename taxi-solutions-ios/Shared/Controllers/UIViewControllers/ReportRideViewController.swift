//
//  ReportTripViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol ReportTripViewControllerDelegate: class {
}

class ReportRideViewController: AnimatedTopBottomViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var characterCountLabel: UILabel!
    @IBOutlet weak var placeholderTextView: UITextView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var evidenceImagesCollectionView: UICollectionView!
    @IBOutlet weak var confirmationButton: CustomButton!
    
    var delegate: ReportTripViewControllerDelegate?
    var parentVC = UIViewController()
    var maxCharacters = 250
    var placeholder = ""
    var confirmText = ""
    var rideId = 0
    
    var evidenceImages: [UIImage] = [] {
        didSet {
            evidenceImagesCollectionView.reloadData()
        }
    }
    
    
    class func instantiante(inViewController vc: UIViewController, withTitle title: String, maxCharacters: Int = 250, rideId: Int, placeholder: String, andConfirmText confirmText: String) {
        guard let reportTripVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "ReportTripViewController") as? ReportRideViewController else { return }
        reportTripVC.title = title
        reportTripVC.maxCharacters = maxCharacters
        reportTripVC.placeholder = placeholder
        reportTripVC.confirmText = confirmText
        reportTripVC.rideId = rideId
        reportTripVC.delegate = vc as? ReportTripViewControllerDelegate
        reportTripVC.modalPresentationStyle = .overFullScreen
        reportTripVC.parentVC = vc
        
        vc.present(reportTripVC, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UINavigationController.setCustomAppearance(false)
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        
        titleLabel.text = title
        placeholderTextView.text = placeholder
        inputTextView.delegate = self
        evidenceImagesCollectionView.delegate = self
        evidenceImagesCollectionView.dataSource = self
        let imageCollectionViewCellNib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
        evidenceImagesCollectionView.register(imageCollectionViewCellNib, forCellWithReuseIdentifier: "ImageCollectionViewCell")
        evidenceImagesCollectionView.contentInset = UIEdgeInsets(top: 0, left: self.view.frame.width * 0.065, bottom: 0, right: 0)
        confirmationButton.setTitle(confirmText, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        UINavigationController.setCustomAppearance(true)
    }
    
    func loadContent() {
        
    }
    
    @objc func setPlaceholder(_ notification: Notification) {
        let hidePlaceholder = notification.name == UIResponder.keyboardWillShowNotification ? true : inputTextView.text != "" ? true : false
        UIView.transition(with: placeholderTextView, duration: 0.1, options: [.transitionCrossDissolve], animations: {
            self.placeholderTextView.alpha = hidePlaceholder ? 0 : 1
            self.placeholderTextView.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func sendReport(_ sender: UIButton) {
        guard let claim = inputTextView.text, !claim.isEmptyOrWhitespace() else {
            self.showAlert(message: ValidationMessages.comment)
            return
        }
        startLoader()
        MainManager.sharedManager.reportTrip(rideId: rideId, claim: inputTextView.text, photos: evidenceImages, successResponse: {(response) in
            self.stopLoader()
            self.dismiss()
            _ = PopUpViewController.instantiate(viewController: self.parentVC, fullscreen: true, image: UIImage(named: "img_sent_mail") ?? UIImage(), title: "Reporte enviado", message: "Gracias por comunicarse con nosotros, le responderemos al correo electrónico en la brevedad posible")
        }, failureResponse: {(error) in
            self.stopLoader()
            print(error.localizedDescription)
        })
    }
    
    @IBAction func attachPhoto(_ sender: UIButton) {
        if evidenceImages.count < 2 {
            let alertVC = UIAlertController(title: "Adjuntar foto", message: "Seleccione de desde dónde quiere adjuntar la foto", preferredStyle: .actionSheet)
            alertVC.view.tintColor = Colors.primary
            let cameraOption = UIAlertAction(title: "Cámara", style: .default) { (_) in
                self.pickImage(sourceType: .camera)
            }
            let galleryOption = UIAlertAction(title: "Galería", style: .default) { (_) in
                self.pickImage(sourceType: .photoLibrary)
            }
            let cancelOption = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alertVC.addActions(actions: [cameraOption, galleryOption, cancelOption])
            self.present(alertVC, animated: true, completion: nil)
        } else {
            showAlert(message: "Puede subir 2 fotos como máximo")
        }
    }
    
    func pickImage(sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func dismiss() {
        super.dismiss()
        NotificationCenter.default.removeObserver(self)
    }
}

extension ReportRideViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return evidenceImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.delegate = self
        cell.image = evidenceImages[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension ReportRideViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text != "" {
            if textView.text.count + text.count <= 250 {
                return true
            }
            return false
        }
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        characterCountLabel.text = "\(textView.text.count)/\(maxCharacters)"
    }
}

extension ReportRideViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let photo = info[.originalImage] as? UIImage {
            print("PHOTO -> \(photo.description)")
            picker.dismiss()
            evidenceImages.append(photo)
        }
    }
}

extension ReportRideViewController: ImageCollectionViewCellDelegate {
    func imageCollectionViewCell(deleteImageFrom cell: ImageCollectionViewCell) {
        guard let index = evidenceImages.firstIndex(of: cell.image) else { return }
        evidenceImages.remove(at: index)
    }
}
