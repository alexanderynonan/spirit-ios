//
//  InfoCVVViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class InfoCVVViewController: PannableViewController {
    
    @IBOutlet weak var cardView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        viewToPan = cardView
    }

    func loadContent() {
        
    }
}
