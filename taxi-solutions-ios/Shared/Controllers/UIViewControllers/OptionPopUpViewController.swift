//
//  OptionPopUpViewController.swift
//  TaxiSolution
//
//  Created by Fernanda Alvarado Tarrillo on 12/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol OptionPopUpViewControllerDelegate {
    func didSelectedOption(optionType: OptionType, idSelect: Any, indexSelect: Int)
    func dismissView(optionType: OptionType, wasSelect: Bool)
}

class OptionPopUpViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var optionTableView: UITableView!
    //MARK: Variables
    var titleText = ""
    var strings : [String] = []
    var idOptions : [Any] = []
    var optionType : OptionType?
    var idSelect : Any?
    var indexSelect = -1
    var delegate : OptionPopUpViewControllerDelegate?

    //MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContent()
    }



    //MARK: Fuctions
    func searchIndexIdSelect() -> Int {
        var indexSearched = -1
        var idOptionsInt : [Any] = []
        var idOptionsString : [Any] = []
        var i = 0

        idOptionsString = idOptions as! [String]
        idOptionsString.forEach({
            if $0 as! String == idSelect as! String {
                indexSearched = i
                return
            }
            i = i + 1
        })

        return indexSearched
    }

    func loadContent() {

        optionTableView.delegate = self
        optionTableView.dataSource = self

        optionTableView.estimatedRowHeight = 70
        optionTableView.rowHeight = UITableView.automaticDimension
        optionTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom, right: 0)

    }

    @objc func dismissView(_ sender: UITapGestureRecognizer) {
        self.delegate?.dismissView(optionType: optionType!, wasSelect: false)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: TableDelegate
extension OptionPopUpViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / CGFloat (strings.count)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strings.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionTableViewCell
        cell.optionNameLabel.text = strings[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        idSelect = idOptions[indexPath.row]
        indexSelect = indexPath.row
        delegate?.didSelectedOption(optionType: optionType!, idSelect: idSelect ?? (Any).self, indexSelect: indexSelect)
        optionTableView.reloadData()

    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        self.viewWillLayoutSubviews()


    }

}

