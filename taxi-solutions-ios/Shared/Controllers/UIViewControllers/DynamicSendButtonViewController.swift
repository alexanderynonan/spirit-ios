//
//  DynamicSendButtonViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class DynamicBottomViewController: UIViewController {
    
    @IBOutlet weak var bottomBoxHeight: NSLayoutConstraint!
    @IBOutlet weak var viewToMove: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSetUp(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSetUp(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardSetUp(_ notification: Notification) {
        let notificationName = notification.name.rawValue
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            UIView.transition(with: viewToMove, duration: 0.5, options: [], animations: {
                self.bottomBoxHeight.constant = notificationName == "UIKeyboardWillShowNotification" ? keyboardRectangle.height : 20
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }

}
