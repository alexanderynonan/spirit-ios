//
//  LoaderViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol LoaderViewControllerDelegate: class {
    func loaderViewController(viewController: LoaderViewController, didConfirmAction: Bool)
    func loaderViewController(viewController: LoaderViewController, didDismiss: Bool)
}

class LoaderViewController: UIViewController {
    
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmationButton: UIButton!
    
    var delegate: LoaderViewControllerDelegate?
    
    var message = ""
    var confirmationTitle = ""
    var automaticDismiss = false
    
    class func instantiate(in viewController: UIViewController, title: String = "", message: String = "", confirmationTitle: String = "", automaticDismiss: Bool = false, delegate: LoaderViewControllerDelegate? = nil) -> LoaderViewController? {
        guard let vc = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "LoaderViewController") as? LoaderViewController else { return nil }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = delegate
        vc.title = title
        vc.message = message
        vc.automaticDismiss = automaticDismiss
        vc.confirmationTitle = confirmationTitle
        viewController.present(vc, animated: true, completion: nil)
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        imageButton.transform = CGAffineTransform(translationX: -400, y: 0)
        titleLabel.text = self.title
        messageLabel.text = self.message
        if confirmationTitle != "" {
            confirmationButton.setTitle(confirmationTitle, for: .normal)
        }
        NotificationCenter.default.addObserver(forName:UIApplication.willEnterForegroundNotification, object: nil, queue: nil) { (_) in
            self.titleLabel.transform = .identity
            self.messageLabel.transform = .identity
            self.imageButton.transform = .identity
            self.viewDidAppear(true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.imageButton.transform = .identity
            self.confirmationButton.transform = self.confirmationTitle != "" ? CGAffineTransform(translationX: 0, y: -220) : .identity
        }, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
 //       UIView.animate(withDuration: 0.35, delay: 0, options: [.repeat], animations: {
 //           self.imageButton.transform = CGAffineTransform(rotationAngle: -3.20)
  //      }, completion: nil)
        
        UIView.animate(withDuration: 1, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.titleLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.messageLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }, completion: nil)
        
        if self.automaticDismiss {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.dismiss()
            }
        }
    }
    
    override func dismissWithCompletion(completion: (() -> Void)? = nil) {
        guard viewIfLoaded != nil else { return }
        UIView.animate(withDuration: 0.25, delay: 0, options: [], animations: {
            self.imageButton.transform = CGAffineTransform(translationX: -40, y: 0)
        }, completion: { _ in
            super.dismiss()
            UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
                self.imageButton.transform = CGAffineTransform(translationX: 400, y: 0)
            }) { _ in
                self.delegate?.loaderViewController(viewController: self, didDismiss: true)
                completion?()
            }
        })
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        self.delegate?.loaderViewController(viewController: self, didConfirmAction: true)
    }
}
