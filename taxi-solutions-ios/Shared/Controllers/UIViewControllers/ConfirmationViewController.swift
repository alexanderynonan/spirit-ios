//
//  ConfirmationViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ConfirmationViewController: PannableViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmationButton: CustomButton!
    @IBOutlet weak var confirmButtonContainer: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    let coverLayer = CALayer()
    
    var parentVC = UIViewController()
    var message = ""
    var confirmText = ""
    var image: UIImage?
    var imageHeight: CGFloat = 0
    var animatedImage: Bool = false
    var confirmationTitle: ConfirmationTitles = .defaultTitle
    
    var confirmationHandler = { (_ vc: ConfirmationViewController, _ confirmed: Bool) -> () in }
    
    var confirmed = false

    class func instantiante(inViewController vc: UIViewController,
                            withTitle confirmationTitle: ConfirmationTitles,
                            message: String, image: UIImage? = nil,
                            withAnimation: Bool = false,
                            imageHeight: CGFloat = 0,
                            andConfirmText confirmText: String,
                            confirmationHandler: @escaping(_ vc: ConfirmationViewController, _ confirmed: Bool) -> ()) {
        guard let confirmationVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "ConfirmationViewController") as? ConfirmationViewController else { return }
        confirmationVC.confirmationTitle = confirmationTitle
        confirmationVC.message = message
        confirmationVC.confirmText = confirmText
        confirmationVC.confirmationHandler = confirmationHandler
        
        confirmationVC.modalPresentationStyle = .overFullScreen
        confirmationVC.parentVC = vc
        confirmationVC.image = image
        confirmationVC.animatedImage = withAnimation
        confirmationVC.imageHeight = imageHeight
        
        confirmationVC.coverLayer.frame = vc.view.bounds
        confirmationVC.coverLayer.backgroundColor = UIColor(named: "secondary_transparency")?.cgColor.copy(alpha: 0.5)
        confirmationVC.coverLayer.opacity = 1.0
        vc.view.layer.addSublayer(confirmationVC.coverLayer)
        
        vc.present(confirmationVC, animated: true, completion: nil)
        confirmationVC.changeLayerOpacity()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if animatedImage {
            UIView.transition(with: imageView, duration: 0.4, options: [.autoreverse, .repeat, .curveEaseInOut], animations: {
                self.imageView.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            }, completion: nil)
        }
    }
    
    func configureContent() {
        titleLabel.text = confirmationTitle.rawValue
        messageLabel.text = message
        imageView.image = image
        imageViewHeight.constant = imageHeight == -1 ? imageView.frame.width : imageHeight
        confirmationButton.setTitle(confirmText, for: .normal)
        viewToPan = cardView
        panDelegate = self
    }
    
    func loadContent() {
        
    }
    
    func changeLayerOpacity(toVisible: Bool = true) {
        UIView.transition(with: self.parentVC.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.coverLayer.opacity = toVisible ? 1.0 : 0.0
        }, completion: nil)
    }
    
    @IBAction func confirm() {
        confirmed = true
        self.close()
    }
}

extension ConfirmationViewController: PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController) {
        self.coverLayer.removeFromSuperlayer()
    }
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool) {
        let viewHeight = viewController.modalHeight
        if gesture.state == .changed {
            UIView.animate(withDuration: 5.0, animations: {
                self.coverLayer.opacity = Float((viewHeight - gesture.translation(in: self.view).y) / viewHeight)
                self.parentVC.view.layoutIfNeeded()
            })
            
        }
        if dismissed || (gesture.state == .ended && dismissed) {
            self.coverLayer.removeFromSuperlayer()
            confirmationHandler(self, confirmed)
        }
    }
}
