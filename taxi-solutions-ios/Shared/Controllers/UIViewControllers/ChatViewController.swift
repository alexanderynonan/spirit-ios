//
//  ChatViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 5/8/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ChatViewController: DynamicBottomViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var chatBubblesTableView: UITableView!
    @IBOutlet weak var inputTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    var username = ""
    var messages = [Message]()
    private var chatID = ""
    var ride: Ride? = nil {
        didSet {
            guard let ride = ride else { return }
            chatID = ride.chatId
        }
    }
    var ref = DatabaseReference()
    
    #if DRIVER
    private let type = 0
    #endif
    
    #if USER
    private let type = 1
    #endif
    
    
    class func instantiate(inViewController viewController: UIViewController, ride: Ride?, username: String) {
        guard let chatVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { return }
        chatVC.username = username
        chatVC.ride = ride
        viewController.navigationController?.pushViewController(chatVC, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        ref = Database.database().reference()
        hideKeyboardWhenTappedAround(chatBubblesTableView)
        chatBubblesTableView.delegate = self
        chatBubblesTableView.dataSource = self
        let ownBubbleTableViewCellNib = UINib(nibName: "ChatBubbleTableViewCell", bundle: nil)
        let otherBubbleTableViewCellNib = UINib(nibName: "ChatBubbleTableViewCell", bundle: nil)
        chatBubblesTableView.register(ownBubbleTableViewCellNib, forCellReuseIdentifier: "OwnBubbleTableViewCell")
        chatBubblesTableView.register(otherBubbleTableViewCellNib, forCellReuseIdentifier: "OtherBubbleTableViewCell")
        
        inputTextView.delegate = self
    }
    
    func loadContent() {
        usernameLabel.text = username
        loadChat()
    }
    
    func loadChat() {
        if !chatID.isEmpty {
            startLoader()
            messages.removeAll()
            ref.child("chats/\(self.chatID)").queryOrdered(byChild: "timestamp").observeSingleEvent(of: .value, with: { (snapshot) in
                for child in snapshot.children {
                    guard let data = child as? DataSnapshot else { return }
                    let message = Message(data: data)
                    self.messages.append(message)
                }
                self.chatBubblesTableView.reloadData()
                self.goToLastMessage()
                self.stopLoader()
                self.asyncChat()
            })
        }
    }
    
    private func asyncChat() {
        ref.child("chats/\(self.chatID)").queryOrdered(byChild: "timestamp").observe(.childAdded, with: {
            (snapshot) in
                let message = Message(data: snapshot)
                if message.type == self.type { return }
                if self.messages.contains(where: { $0.id == message.id }) { return }
                self.messages.append(message)
                self.chatBubblesTableView.reloadData()
                self.goToLastMessage()
            })
    }
    
    private func goToLastMessage() {
        if !messages.isEmpty {
            chatBubblesTableView.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    @IBAction func call(_ sender: UIButton) {
        guard let ride = ride else { return }
        #if DRIVER
        let phone = ride.clientOnRide?.phone
        #endif
        #if USER
        let phone = ride.driverOnRide?.phone
        #endif
        call(phoneNumber: phone)
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        guard let message = inputTextView.text, !message.isEmptyOrWhitespace() else {
            return
        }
        
        let newMessage = Message()
        newMessage.id = UUID().uuidString
        newMessage.message = message
        newMessage.timestamp = Date().currentTimeMillis()
        newMessage.type = type
        messages.append(newMessage)
        self.chatBubblesTableView.reloadData()
        goToLastMessage()
        if chatID.isEmpty {
            startLoader()
            asyncChat()
            stopLoader()
        } else {
            FirebaseManager.shared.sendMessage(message: newMessage, chatId: chatID)
        }
        inputTextView.text = ""
        inputTextViewHeight.constant = inputTextView.contentSize.height
    }
    
    override func keyboardWillMove(_ show: Bool) {
        if show {
            goToLastMessage()
        }
    }
    
    override func goBack() {
        self.tabBarController?.tabBar.isHidden = false
        super.goBack()
    }

}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ownBubble = messages[indexPath.row].type == self.type
        let cell = tableView.dequeueReusableCell(withIdentifier: ownBubble ? "OwnBubbleTableViewCell" : "OtherBubbleTableViewCell") as! ChatBubbleTableViewCell
        cell.message = messages[indexPath.row]
        cell.ownBubble = ownBubble
        cell.configure()
        return cell
    }
}

extension ChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        goToLastMessage()
        print("HEIGHT: \(textView.contentSize.height)")
        if textView.contentSize.height < 99 {
            inputTextViewHeight.constant = textView.contentSize.height
        }
    }
}
