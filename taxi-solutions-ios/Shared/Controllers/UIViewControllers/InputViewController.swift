//
//  InputViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol InputViewControllerDelegate: class {
    func inputViewController(_ viewController: InputViewController, didConfirm: Bool)
}

class InputViewController: PannableViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var characterCountLabel: UILabel!
    @IBOutlet weak var placeholderTextView: UITextView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var confirmationButton: CustomButton!
    @IBOutlet weak var bottomSafeArea: NSLayoutConstraint!
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var textViewContainer: UIView!
    
    let coverLayer = CALayer()
    
    var delegate: InputViewControllerDelegate?
    var parentVC = UIViewController()
    var maxCharacters = 250
    var placeholder = ""
    var confirmText = ""
    var validationMessage = ""
    var inputType: InputType = .textField
    var inputText = ""
    
    class func instantiante(inViewController vc: UIViewController, inputType: InputType, withTitle title: String, maxCharacters: Int = 250, placeholder: String, confirmText confirmText: String, validationMessage: String) {
        guard let inputVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "InputViewController") as? InputViewController else { return }
        inputVC.inputType = inputType
        inputVC.title = title
        inputVC.maxCharacters = maxCharacters
        inputVC.placeholder = placeholder
        inputVC.confirmText = confirmText
        inputVC.validationMessage = validationMessage
        inputVC.delegate = vc as? InputViewControllerDelegate
        inputVC.modalPresentationStyle = .overFullScreen
        inputVC.parentVC = vc
        
        inputVC.coverLayer.frame = vc.view.bounds
        inputVC.coverLayer.backgroundColor = UIColor(named: "secondary_transparency")?.cgColor.copy(alpha: 0.5)
        inputVC.coverLayer.opacity = 0.0
        vc.view.layer.addSublayer(inputVC.coverLayer)
        
        vc.present(inputVC, animated: true, completion: nil)
        inputVC.changeLayerOpacity()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func configureContent() {
        switch inputType {
        case .textField:
            self.textFieldContainer.isHidden = false
            self.textViewContainer.isHidden = true
            self.characterCountLabel.isHidden = true
        case .textView:
            self.textFieldContainer.isHidden = true
            self.textViewContainer.isHidden = false
        }
        
        titleLabel.text = title
        placeholderTextView.text = placeholder
        inputTextView.delegate = self
        inputTextField.placeholder = placeholder
        confirmationButton.setTitle(confirmText, for: .normal)
        characterCountLabel.text = "0/\(maxCharacters)"
        viewToPan = cardView
        panDelegate = self
        
        if let window = UIApplication.shared.windows.first {
            bottomSafeArea.constant = window.safeAreaInsets.bottom
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setPlaceholder(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        inputTextField.addTarget(self, action: #selector(changeInputText), for: .editingDidEnd)
    }
    
    func loadContent() {
        
    }
    
    @objc func changeInputText(_ textField: UITextField) {
        inputText = textField.text ?? ""
        inputTextField.text = String(format: "%.2f", inputText.double)
    }
    
    func changeLayerOpacity(toVisible: Bool = true) {
        UIView.transition(with: self.parentVC.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.coverLayer.opacity = toVisible ? 1.0 : 0.0
        }, completion: nil)
    }
    
    @objc func setPlaceholder(_ notification: Notification) {
        let hidePlaceholder = notification.name == UIResponder.keyboardWillShowNotification ? true : inputTextView.text != "" ? true : false
        UIView.animate(withDuration: 0.25, animations: {
            self.placeholderTextView.isHidden = hidePlaceholder
        }, completion: nil)
    }
    
    override func close() {
        if inputTextView.isFirstResponder || inputTextField.isFirstResponder {
            dismissKeyboard()
        } else {
            super.close()
        }
    }
    
    @IBAction func confirm() {
        if inputText.isEmptyOrWhitespace() {
            self.showAlert(message: validationMessage)
            return
        }
        self.close()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.delegate?.inputViewController(self, didConfirm: true)
        }
    }

}

extension InputViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text != "" {
            if textView.text.count + text.count <= 250 {
                return true
            }
            return false
        }
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        inputText = textView.text
        characterCountLabel.text = "\(textView.text.count)/\(maxCharacters)"
    }
}

extension InputViewController: PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController) {
        self.coverLayer.removeFromSuperlayer()
    }
    
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool) {
        let viewHeight = viewController.modalHeight
        if gesture.state == .changed {
            UIView.animate(withDuration: 5.0, animations: {
                self.coverLayer.opacity = Float((viewHeight - gesture.translation(in: self.view).y) / viewHeight)
                self.parentVC.view.layoutIfNeeded()
            })
            
        }
        if dismissed || (gesture.state == .ended && dismissed) {
            self.coverLayer.removeFromSuperlayer()
            delegate?.inputViewController(self, didConfirm: false)
        }
    }
}
