//
//  OptionSelectorViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol OptionSelectorViewControllerDelegate: class {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect)
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool)
}

class OptionSelectorViewController: PannableViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionsTableView: UITableView!
    @IBOutlet weak var confirmButtonContainer: UIView!
    @IBOutlet weak var confirmButtonContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmationButton: UIButton!
    
    let coverLayer = CALayer()
    
    var needsToConfirmWithButton = false
    var heightScale: CGFloat?
    var options: [OptionToSelect] = []
    var selectedOption: OptionToSelect? {
        didSet {
            guard viewIfLoaded != nil, let selectedOption = selectedOption else { return }
            confirmationButton.isEnabled = true
            confirmationButton.alpha = 1
            delegate?.optionSelectorViewController(self, didSelect: selectedOption)
        }
    }
    var delegate: OptionSelectorViewControllerDelegate?
    var parentVC = UIViewController()
    var confirmed = false
    
    class func instantiate(inViewController vc: UIViewController, delegate: OptionSelectorViewControllerDelegate?, heightScale: CGFloat? = nil, withTitle title: String, withOptions options: [OptionToSelect], selectedOption: OptionToSelect? = nil, withConfirmButton: Bool = false) {
        
        guard let optionsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "OptionSelectorViewController") as? OptionSelectorViewController else { return }
        optionsVC.heightScale = heightScale
        optionsVC.title = title
        optionsVC.options = options
        optionsVC.selectedOption = selectedOption
        optionsVC.needsToConfirmWithButton = withConfirmButton ? true : false
        optionsVC.delegate = delegate
        optionsVC.modalPresentationStyle = .overFullScreen
        optionsVC.parentVC = vc
        
        optionsVC.coverLayer.frame = vc.view.bounds
        optionsVC.coverLayer.backgroundColor = UIColor(named: "secondary_transparency")?.cgColor.copy(alpha: 0.5)
        optionsVC.coverLayer.opacity = 0.0
        vc.view.layer.addSublayer(optionsVC.coverLayer)
        
        vc.present(optionsVC, animated: true, completion: nil)
        optionsVC.changeLayerOpacity()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        titleLabel.text = title
        cardViewHeight.constant = heightScale != nil ? self.view.frame.height * heightScale! : self.cardView.frame.height
        
        viewToPan = cardView
        optionsTableView.delegate = self
        optionsTableView.dataSource = self
        
        confirmationButton.isEnabled = false
        confirmationButton.alpha = 0.5
        
        let optionTableViewCellNib = UINib(nibName: "OptionTableViewCell", bundle: nil)
        optionsTableView.register(optionTableViewCellNib, forCellReuseIdentifier: "OptionTableViewCell")
        
        confirmButtonContainerHeight.constant = needsToConfirmWithButton ? 90 : 0
        panDelegate = self
    }
    
    func loadContent() {
        
    }
    
    func changeLayerOpacity(toVisible: Bool = true) {
        UIView.transition(with: self.parentVC.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.coverLayer.opacity = toVisible ? 1.0 : 0.0
        }, completion: nil)
    }
    
    @IBAction func confirm() {
        guard let selectedOption = selectedOption, needsToConfirmWithButton else {
            return
        }
        self.close()
        self.confirmed = true
    }

}

extension OptionSelectorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / CGFloat (options.count)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionTableViewCell") as! OptionTableViewCell
        cell.option = options[indexPath.row]
        cell.awakeFromNib()
        cell.isSelectedOption = cell.option == selectedOption ? true : false
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.visibleCells.forEach {
            let optionTableViewCell = $0 as! OptionTableViewCell
            optionTableViewCell.isSelectedOption = false
        }
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        cell.fromDidSelect = true
        cell.isSelectedOption = true
        selectedOption = options[indexPath.row]
    }
}

extension OptionSelectorViewController: OptionTableViewCellDelegate {
    func didSelectOption(tableViewCell: OptionTableViewCell) {
        if !needsToConfirmWithButton {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.close()
            }
        }
    }
}

extension OptionSelectorViewController: PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController) {
        self.coverLayer.removeFromSuperlayer()
    }
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool) {
        let viewHeight = viewController.modalHeight
        if gesture.state == .changed {
            UIView.animate(withDuration: 5.0, animations: {
                self.coverLayer.opacity = Float((viewHeight - gesture.translation(in: self.view).y) / viewHeight)
                self.parentVC.view.layoutIfNeeded()
            })
            
        }
        if dismissed || (gesture.state == .ended && dismissed) {
            self.coverLayer.removeFromSuperlayer()
            self.delegate?.optionSelectorViewController(self, dismissed: needsToConfirmWithButton ? (confirmed ? true : false) : true)
        }
    }
}
