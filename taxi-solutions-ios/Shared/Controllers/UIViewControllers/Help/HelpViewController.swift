//
//  HelpViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var faqsTableView: UITableView!
    var themes: [Theme] = [] {
        didSet {
            faqsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
    }

    func configureContent() {
        faqsTableView.delegate = self
        faqsTableView.dataSource = self

        faqsTableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQTableViewCell")
    }
    
    func loadContent() {
        MainManager.sharedManager.getConfigurationParams(successResponse: { (message) in
            print(message)
        }) { (error) in
            print(error.localizedDescription)
        }
        startLoader(waitingForObjectsOfType: Theme.self)
        setData()
        MainManager.sharedManager.getHelpQuestions(successResponse: { (themes) in
            self.stopLoader()
            self.setData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    func setData() {
        let newThemes = Array(realm.objects(Theme.self))
        if self.themes != newThemes {
            self.themes = newThemes
        }
    }
    
    @IBAction func getHelpFrom(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.call(phoneNumber: App.contactPhone)
        case 1:
            self.openMail(email: App.contactEmail)
        case 2:
            self.openUrl(url: App.contactWeb)
        default:
            ()
        }
    }
}

extension HelpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: themes, emptyMessage: "Aún no hay temas registrados")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("CELL FOR ROW AT")
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        cell.name = themes[indexPath.row].name
        cell.awakeFromNib()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let innerFAQVC = self.storyboard?.instantiateViewController(withIdentifier: "FAQuestionsViewController") as! FAQuestionsViewController
        innerFAQVC.theme = themes[indexPath.row]
        self.navigationController?.pushViewController(innerFAQVC, animated: true)
    }
}

