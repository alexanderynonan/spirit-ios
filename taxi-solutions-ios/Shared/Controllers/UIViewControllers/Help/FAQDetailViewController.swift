//
//  FAQDetailViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class FAQDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    var question = Question()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        
    }
    
    func loadContent() {
        titleLabel.text = question.question
        answerLabel.text = question.answer
    }

}
