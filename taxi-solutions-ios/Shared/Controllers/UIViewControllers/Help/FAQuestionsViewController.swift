//
//  FAQuestionsViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class FAQuestionsViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var faqsTableView: UITableView!
    var themes: [Theme] = [] {
        didSet {
            faqsTableView.reloadData()
        }
    }
    var theme: Theme?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FAQDetailViewControllerSegue" {
            let vc = segue.destination as! FAQDetailViewController
            guard let question = sender as? Question else { return }
            vc.question = question
        }
    }
    
    func configureContent() {
        faqsTableView.delegate = self
        faqsTableView.dataSource = self
        
        faqsTableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQTableViewCell")
    }
    
    func loadContent() {
        titleLabel.text = theme?.name ?? "Preguntas Frecuentes"
        self.faqsTableView.reloadData()
    }
    
    func setData() {
        let newThemes = Array(realm.objects(Theme.self))
        if self.themes != newThemes {
            self.themes = newThemes
        }
    }
    
    @IBAction func askNewQuestion(_ sender: UIButton) {
        InputViewController.instantiante(inViewController: self, inputType: .textView, withTitle: "Haz una pregunta", placeholder: "Escribe tu pregunta aquí", confirmText: "ENVIAR", validationMessage: "Por favor, ingrese una pregunta válido")
    }
}

extension FAQuestionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: theme?.questionsArray ?? [Question](), emptyMessage: "No hay preguntas frecuentes")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("CELL FOR ROW AT")
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        cell.name = theme?.questions[indexPath.row].question ?? ""
        cell.awakeFromNib()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "FAQDetailViewControllerSegue", sender: theme?.questions[indexPath.row])
    }
}

extension FAQuestionsViewController: InputViewControllerDelegate {
    func inputViewController(_ viewController: InputViewController, didConfirm: Bool) {
        if didConfirm {
            self.startLoader()
            MainManager.sharedManager.sendHelpQuestion(themeId: theme?.id ?? 0, question: viewController.inputText, successResponse: { (message) in
                self.stopLoader()
                _ = PopUpViewController.instantiate(viewController: self, fullscreen: true, image: #imageLiteral(resourceName: "img_mail_help"), animated: true, title: "Pregunta enviada", message: "Gracias por comunicarse con nosotros, le responderemos al correo electrónico en la brevedad posible", needCloseButton: true, closeWithTap: false)
                print(message)
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
}
