//
//  PopUpViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

protocol PopUpViewControllerDelegate {
    //Delegate that responds when PopUpViewController is dismissed
    func popUpViewController(viewController: PopUpViewController, didDismiss: Bool)
}

class PopUpViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var infoTitleLabel: UILabel?
    @IBOutlet weak var infoMessageLabel: UILabel?
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView?
    
    var parentVC: UIViewController?
    var image = UIImage()
    var titleText = ""
    var message = ""
    var infoTitle = ""
    var infoMessage = ""
    var attributedInfoMessage = false
    var backgroundImage: UIImage? = nil
    var needCloseButton = true
    var closeWithTap = true
    var automaticDismiss = false
    var warning = false
    
    lazy var animator1 = {
        UIViewPropertyAnimator(duration: 0.15, curve: .linear) { [unowned self] in
            self.view.alpha = 1
        }
    }()
    lazy var animator2 = {
        UIViewPropertyAnimator(duration: 0.3, curve: .linear) { [unowned self] in
            self.titleLabel.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
        }
    }()
    lazy var animator3 = {
        UIViewPropertyAnimator(duration: 0.15, curve: .linear) { [unowned self] in
            self.titleLabel.transform = .identity
            self.messageLabel.alpha = 1
        }
    }()
    lazy var animator4 = {
        UIViewPropertyAnimator(duration: 0.6, curve: .linear) {
            self.imageView.transform = .identity
            self.imageView.alpha = 1
        }
    }()
    
    var delegate: PopUpViewControllerDelegate?
    
    class func instantiate(viewController: UIViewController, delegate: PopUpViewControllerDelegate? = nil, fullscreen: Bool = false, image: UIImage, animated: Bool = false, title: String, warning: Bool = false, message: String, infoTitle: String = "", infoMessage: String = "", attributedInfoMessage: Bool = false, backgroundImage: UIImage? = nil, needCloseButton: Bool = true, closeWithTap: Bool = false, automaticDismiss: Bool = false) -> PopUpViewController? {
        let vc = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: fullscreen ? "FullScreenPopUpViewController" : "PopUpViewController") as! PopUpViewController
        vc.parentVC = viewController
        vc.image = image
        vc.titleText = title
        vc.warning = warning
        vc.message = message
        vc.infoTitle = infoTitle
        vc.infoMessage = infoMessage
        vc.attributedInfoMessage = attributedInfoMessage
        vc.backgroundImage = backgroundImage
        vc.needCloseButton = needCloseButton
        vc.closeWithTap = closeWithTap
        vc.automaticDismiss = automaticDismiss
        vc.delegate = delegate
        
        //presents the PopUp
        viewController.definesPresentationContext = true
        viewController.modalPresentationStyle = .currentContext
        viewController.modalTransitionStyle = .crossDissolve
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.modalTransitionStyle = .crossDissolve
        navController.isNavigationBarHidden = true
        viewController.present(navController, animated: true, completion: nil)
        
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        imageViewHeight.constant = (image.size.height / image.size.width) * imageView.frame.size.width
        titleLabel.text = titleText
        titleLabel.textColor = warning ? Colors.error : Colors.primary
        messageLabel.text = message
        infoTitleLabel?.text = infoTitle
        
        if attributedInfoMessage {
            self.infoMessageLabel?.attributedText = infoMessage.getAttributedTextWithBoldWords()
        } else {
            self.infoMessageLabel?.text = self.infoMessage
        }
        
        closeButton.isHidden = needCloseButton ? false : true
        if automaticDismiss {
            Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (_) in
                self.close()
            }
        }
        backgroundImageView?.image = backgroundImage
        setInitialLayout()
    }
    
    func setInitialLayout() {
        self.view.alpha = 0
        self.imageView.alpha = 0
        self.imageView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.imageView.transform = CGAffineTransform(translationX: 0, y: 100)
        
        self.titleLabel.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.messageLabel.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.animator1.addCompletion { [unowned self] _ in
            self.animator2.startAnimation()
        }
        self.animator2.addCompletion { [unowned self] _ in
            self.animator3.startAnimation()
        }
        self.animator1.startAnimation()
        self.animator4.startAnimation()
    }
    
    @IBAction func closeWithTap(_ sender: Any) {
        if closeWithTap {
            close()
        }
    }
    
    @IBAction func close() {
        dismiss(animated: true) {
            self.delegate?.popUpViewController(viewController: self, didDismiss: true)
        }
    }
    
}
