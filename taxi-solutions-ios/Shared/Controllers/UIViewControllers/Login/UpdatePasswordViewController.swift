//
//  UpdatePasswordViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import IQKeyboardManager

class UpdatePasswordViewController: DynamicBottomViewController {
    
    @IBOutlet weak var backButton: CustomButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var currentPasswordTextFieldContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: ValidatingTextField!
    @IBOutlet weak var confirmPasswordTextField: ValidatingTextField!
    @IBOutlet weak var confirmButton: CustomButton!
    
    var updatePassword = false
    var accessToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if updatePassword {
            currentPasswordTextField.becomeFirstResponder()
        } else {
            passwordTextField.becomeFirstResponder()
        }
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        print("URL: \(self.accessToken)")
        //currentPasswordTextFieldContainerViewHeight.constant = updatePassword ? self.view.frame.height * 0.07 + 15 : 0
        backButton.setImage(updatePassword ? #imageLiteral(resourceName: "ic_back") : #imageLiteral(resourceName: "ic_close"), for: .normal)
        titleLabel.text = updatePassword ? "Cambiar contraseña" : "Actualizar contraseña"
        confirmButton.setTitle( updatePassword ? "CAMBIAR" : "RESTABLECER", for: .normal)
        
        passwordTextField.addTarget(self, action: #selector(checkIfEquals(_:)), for: .editingChanged)
        confirmPasswordTextField.addTarget(self, action: #selector(checkIfEquals(_:)), for: .editingChanged)
    }
    
    func loadContent() {
        
    }
    
    @objc func checkIfEquals(_ textField: UITextField) {
        if textField == passwordTextField && confirmPasswordTextField.text != "" {
            if confirmPasswordTextField.text == passwordTextField.text {
                passwordTextField.validated = true
                confirmPasswordTextField.validated = true
            } else {
                passwordTextField.validated = true
                confirmPasswordTextField.validated = false
            }
        }
        
        if textField == confirmPasswordTextField && passwordTextField.text != "" {
            if confirmPasswordTextField.text == passwordTextField.text {
                passwordTextField.validated = true
                confirmPasswordTextField.validated = true
            } else {
                passwordTextField.validated = true
                confirmPasswordTextField.validated = false
            }
        }
        
        if passwordTextField.text == "" || confirmPasswordTextField.text == "" {
            DispatchQueue.main.async {
                self.passwordTextField.validatingSwitchButton.setImage(nil, for: .normal)
                self.confirmPasswordTextField.validatingSwitchButton.setImage(nil, for: .normal)
            }
        }
    }
    
    @IBAction func resetPassword() {
        dismissKeyboard()
        
        guard let password = passwordTextField.text, !password.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.password)
            return
        }
        
        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.confirmPassword)
            return
        }
        
        guard password == confirmPassword else {
            showAlert(message: ValidationMessages.passwordsDifferent)
            return
        }
        
        if updatePassword {
            guard let currentPassword = currentPasswordTextField.text, !confirmPassword.isEmptyOrWhitespace() else {
                showAlert(message: ValidationMessages.currentPassword)
                return
            }
            self.startLoader()
            UserManager.sharedManager.updatePassword(oldPassword: currentPassword, newPassword: password, confirmPassword: confirmPassword, successResponse: {(message) in
                self.stopLoader()
                self.showAlert(message: message, okayHandler: { (_) in
                    self.navigationController?.popViewController(animated: true)
                })
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
            
        } else {
            self.startLoader()
            UserManager.sharedManager.resetPassword(password: password, accessToken: accessToken, successResponse: { (message) in
                self.stopLoader()
                self.showAlert(message: message, okayHandler: { (_) in
                    self.dismiss()
                })
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
        
        
    }
    
    override func dismiss() {
        if updatePassword {
            navigationController?.popViewController(animated: true)
        } else {
            super.dismiss()
        }
    }

}
