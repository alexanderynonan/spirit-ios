//
//  RegisterViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 2/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    var newUser = RegistrationUser()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

}
