//
//  VerifyPhoneViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import IQKeyboardManager

class VerifyPhoneViewController: DynamicBottomViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet var verificationCodeTextFields: [UITextField]!
    var verificationMode: PhoneVerificationMode = .sms
    var verificationCode = ""
    var phoneNumberVC: PhoneNumberViewController?
    var updateData = false
    var isEmailUpdate = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RegisterFirstStepViewControllerSegue" {
            let vc = segue.destination as! RegisterFirstStepViewController
            vc.newUser.phoneNumber = phoneNumberVC?.newUser.phoneNumber ?? ""
        }
    }
    
    func loadContent() {
//        for (index, string) in verificationCode.enumerated() {
//            verificationCodeTextFields[index].text = "\(string)"
//        }
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        messageLabel.text = isEmailUpdate ? "Te enviaremos un código de verificación por correo" : updateData ? "Te enviaremos un código de verificación por mensaje de texto o llamada" : "Te enviaremos un código de verificación por correo"
        verificationCodeTextFields.forEach { $0.delegate = self }
    }
    
    private func resendCodePhone() {
        if let phoneNumber = phoneNumberVC?.newUser.phoneNumber {
            startLoader()
            UserManager.sharedManager.verifyPhone(phoneNumber: phoneNumber, verificationMode: verificationMode, successResponse: { (code) in
                self.verificationCode = code
                self.clearTextFields()
                self.stopLoader()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            goBack()
        }
    }
    
    private func resendCodeEmail() {
        if let email = phoneNumberVC?.newUser.email {
            startLoader()
            UserManager.sharedManager.verifyEmail(email: email,successResponse: { (code) in
                self.verificationCode = code
                self.clearTextFields()
                self.stopLoader()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            goBack()
        }
    }
    
    private func executeUpdatePhone() {
        if let phoneNumber = phoneNumberVC?.newUser.phoneNumber {
            startLoader()
            UserManager.sharedManager.updatePhone(phone: phoneNumber, successResponse: {(message) in
                self.stopLoader()
                try! self.realm.write {
                    self.user.phone = phoneNumber
                }
                self.showAlert(message: message, okayHandler: { (_) in
                   NotificationCenter.default.post(name: .goToProfileView, object: nil)
                })
            }) {(error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            goBack()
        }
    }
    
    private func executeUpdateEmail() {
        if let email = phoneNumberVC?.newUser.email {
            startLoader()
            UserManager.sharedManager.updateEmail(email: email, successResponse: {(message) in
                self.stopLoader()
                try! self.realm.write {
                    self.user.email = email
                }
                self.showAlert(message: message, okayHandler: { (_) in
                   NotificationCenter.default.post(name: .goToProfileView, object: nil)
                })
            }) {(error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            goBack()
        }
    }
    
    private func clearTextFields() {
        verificationCodeTextFields.forEach {
            $0.text = ""
        }
    }
    
    
    @IBAction func verifyCode(_ sender: UIButton) {
        var verificationCode = ""
        verificationCodeTextFields.forEach { textField in
            verificationCode += textField.text ?? ""
        }

        print("CODE \(verificationCode)")

        if verificationCode == self.verificationCode {
            updateData ? (isEmailUpdate ? executeUpdateEmail() : executeUpdatePhone()) : performSegue(withIdentifier: "RegisterFirstStepViewControllerSegue", sender: nil)
        } else {
            showAlert(message: "El código de verificación ingresado no es válido, por favor vuelva a ingresar", okayHandler: { (_) in
                self.verificationCodeTextFields.first?.becomeFirstResponder()
            })
        }
    }
    
    @IBAction func resendCode(_ sender: UIButton) {
        isEmailUpdate ? resendCodeEmail() : updateData ? resendCodePhone() : resendCodeEmail()
    }

}

extension VerifyPhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            textField.text = ""
            verificationCodeTextFields.forEach {
                if $0.tag == textField.tag - 1 {
                    $0.becomeFirstResponder()
                }
            }
            return false
        }
        
        if string.count == 1 {
            if textField.text?.count == 1 {
                if verificationCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                    let nextTextField = verificationCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                    nextTextField?.text = string
                    nextTextField?.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.text = string
            }
        } else {
            if let character = string.first {
                textField.text = String(character)
            }
        }
        
        return false
    }
}
