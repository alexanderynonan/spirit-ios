//
//  RecoverPasswordViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RecoverPasswordViewController: DynamicBottomViewController {
    
    @IBOutlet weak var emailTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
    }
    
    func loadContent() {
        
    }
    
    @IBAction func sendRecoveryMail() {
        guard let email = emailTextField.text, !email.isEmptyOrWhitespace() && email.isValidEmail() else {
            showAlert(message: ValidationMessages.email)
            return
        }
        
        self.startLoader()
        UserManager.sharedManager.sendPasswordRecoveryMail(email: email, successResponse: { (successMessage) in
            self.stopLoader()
            self.showAlert(message: successMessage, okayHandler: { (_) in
                self.goBack()
            })
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

}
