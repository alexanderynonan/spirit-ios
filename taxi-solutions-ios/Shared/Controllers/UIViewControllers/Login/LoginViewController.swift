//
//  LoginViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class LoginViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        #if DRIVER
        logoImageView.sd_setImage(with: App.logoUrl.url, placeholderImage: UIImage(named:"img_client_logo"), options: .refreshCached)
        #endif
        #if USER
        logoImageView.sd_setImage(with: App.logoUrl.url, placeholderImage: UIImage(named: "img_client_logo"), options: .refreshCached)
        #endif
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func loadContent() {
        
    }
    
    @IBAction func login() {
        guard let email = emailTextField.text, !email.isEmptyOrWhitespace() && email.isValidEmail() else {
            showAlert(message: ValidationMessages.email)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.password)
            return
        }
        
        self.startLoader()
        UserManager.sharedManager.login(email: email, password: password, successResponse: { (user) in
            self.stopLoader()
            self.setRootViewController(fromStoryboard: "Main")
            SocketIOManager.sharedInstance.establishConnection()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    @IBAction func registerWithEmailPressed(_ sender: Any) {
        performSegue(withIdentifier: "PhoneNumberViewControllerSegue", sender: nil)

    }

    @IBAction func forgetPassword(_ sender: Any) {
        performSegue(withIdentifier: "recoverPasswordSegue", sender: nil)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            passwordTextField.becomeFirstResponder()
        }
        if textField.returnKeyType == .go {
            dismissKeyboard()
            login()
        }
        return true
    }
}
