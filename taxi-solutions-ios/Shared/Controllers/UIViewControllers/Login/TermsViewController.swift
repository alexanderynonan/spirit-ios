//
//  TermsViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/7/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {
    
    @IBOutlet weak var termsLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        termsLabel.text = App.terms.htmlToString
    }
}
