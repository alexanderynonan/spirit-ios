//
//  InitialViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    func configureContent() {
        Tags.show(viewController: self, withTextColor: Colors.text.withAlphaComponent(0.35), font: Fonts.semibold(13.0), position: .bottomRight)
        MainManager.sharedManager.getLogoUrl(successResponse: { (url) in
            UserDefaults.standard.set(url, forKey: Constants.logoUrlKey)
        }) { (error) in
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        backgroundImageView.image = UIImage.gifImageWithName(Date.isNight ? "gif_svico_night" : "gif_svico_day")
    }
    
    @IBAction func goLogin() {
        performSegue(withIdentifier: "LoginViewControllerSegue", sender: nil)
    }
    
    @IBAction func goRegister() {
        performSegue(withIdentifier: "PhoneNumberViewControllerSegue", sender: nil)
    }

}
