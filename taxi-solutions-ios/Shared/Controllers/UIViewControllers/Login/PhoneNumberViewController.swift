//
//  PhoneNumberViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import IQKeyboardManager

enum PhoneVerificationMode {
    case call
    case sms
}

class PhoneNumberViewController: DynamicBottomViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: CustomTextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var getCallButton: UIButton!
    
    private var isEmailUpdate = false
    var newUser = RegistrationUser()
    var verificationMode: PhoneVerificationMode = .sms
    var dataToUpdate: PersonalData? = nil
    var updateData = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerifyPhoneViewControllerSegue" {
            let vc = segue.destination as! VerifyPhoneViewController
            vc.phoneNumberVC = self
            vc.verificationMode = verificationMode
            vc.verificationCode = sender as? String ?? ""
            vc.updateData = updateData
            vc.isEmailUpdate = isEmailUpdate
        }
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        phoneNumberTextField.delegate = self
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        if let dataToUpdate = dataToUpdate {
            if dataToUpdate == .phone {
                titleLabel.text = "Actualizar número"
                sendMessageButton.setTitle("ENVIAR MENSAJE", for: .normal)
            }
            if dataToUpdate == .email {
                titleLabel.text = "Actualizar correo"
                messageLabel.text = "Te enviaremos un código al correo ingresado para su verificación"
                getCallButton.isHidden = true
                phoneNumberTextField.keyboardType = .emailAddress
                phoneNumberTextField.placeholder = "Correo electrónico"
                isEmailUpdate = true

                phoneNumberTextField.awakeFromNib()
                sendMessageButton.setTitle("ENVIAR CÓDIGO", for: .normal)
            }
        }
    }
    
    func loadContent() {
        
    }
    
    private func phoneVerification() {
        guard let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.phone, okayHandler: { (_) in
                self.phoneNumberTextField.becomeFirstResponder()
            })
            return
        }
        if phoneNumber.count < 9 {
            showAlert(message: ValidationMessages.phoneLength, okayHandler: { (_) in
                self.phoneNumberTextField.becomeFirstResponder()
            })
            return
        }
        startLoader()
        UserManager.sharedManager.verifyPhone(phoneNumber: phoneNumber, verificationMode: verificationMode, successResponse: { (code) in
            self.stopLoader()
            self.newUser.phoneNumber = phoneNumber
            self.performSegue(withIdentifier: "VerifyPhoneViewControllerSegue", sender: code)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    private func emailVerification() {
        guard let email = phoneNumberTextField.text, !email.isEmpty else {
            showAlert(message: ValidationMessages.email)
            return
        }
        
        guard !email.isEmptyOrWhitespace() && email.isValidEmail() else {
            showAlert(message: ValidationMessages.emailFormat)
            return
        }
        
        startLoader()
        UserManager.sharedManager.verifyEmail(email: email, successResponse: { (code) in
            self.stopLoader()
            self.newUser.email = email
            self.performSegue(withIdentifier: "VerifyPhoneViewControllerSegue", sender: code)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func verifyPhone(_ sender: UIButton) {
        dismissKeyboard()
        emailVerification()
    }
}

extension PhoneNumberViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
