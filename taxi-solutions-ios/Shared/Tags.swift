//
//  VersionLabel.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 27/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

enum VersionTagPosition {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
}

class Tags {
    
    static var appVersion: String {
        get {
            guard let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String else {
                return ""
            }
            guard let bundleVersion = Bundle.main.infoDictionary!["CFBundleVersion"] as? String else {
                return "v. \(appVersion)"
            }
            return "v. \(appVersion)(\(bundleVersion))"
        }
    }

    static func show(_ text: String = appVersion, viewController: UIViewController, withTextColor color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), font: UIFont = UIFont.systemFont(ofSize: 12.0), position: VersionTagPosition) {
        let label = UILabel()
        label.text = text
        label.font = font
        label.textAlignment = .right
        label.textColor = color
        
        let view = viewController.view ?? UIView()
        let windowSafeArea = (UIApplication.shared.keyWindow ?? UIView()).safeAreaInsets
        
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        
        switch position {
        case .topLeft:
            x = view.frame.width * 0.065
            y = windowSafeArea.top + 10
        case .topRight:
            x = view.frame.width * 0.935 - 80.0
            y = windowSafeArea.top + 10
        case .bottomLeft:
            x = view.frame.width * 0.065
            y = view.frame.height - windowSafeArea.bottom - 15.0
        case .bottomRight:
            x = view.frame.width * 0.935 - 80.0
            y = view.frame.height - ([windowSafeArea.bottom, viewController.tabBarController?.tabBar.frame.height ?? 0.0].max() ?? 0.0) - 10.0 - 15.0
        }
        
        label.frame = CGRect(x: x, y: y, width: 80, height: 15)
        
        viewController.view.addSubview(label)
        viewController.view.layoutIfNeeded()
    }
}
