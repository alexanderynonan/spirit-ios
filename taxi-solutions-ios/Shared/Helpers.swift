//
//  Helpers.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class Helpers {
    static func goToTaxiSvicoLocationSettings() {
        if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
    }
}
