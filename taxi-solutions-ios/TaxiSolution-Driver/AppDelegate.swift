//
//  AppDelegate.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 1/22/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManager
import RealmSwift
import FirebaseCrashlytics
import FirebaseCore
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var user: User? {
        return (try! Realm()).objects(User.self).first
    }
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().disabledDistanceHandlingClasses.add(ChatViewController.self)
        
        GMSServices.provideAPIKey(GoogleApiKeys.maps)
        GMSPlacesClient.provideAPIKey(GoogleApiKeys.places)
        performRealmMigration()
        
        MainManager.sharedManager.getConfigurationParams(successResponse: { (message) in
            print(message)
        }) { (error) in
            print(error.localizedDescription)
        }
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        if user != nil {
            Crashlytics.crashlytics().setUserID("\(user?.id ?? 0)")
            if let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
                window?.rootViewController = homeVC
            }
            checkRideRequest()
            SocketIOManager.sharedInstance.establishConnection()
        }
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if user != nil {
            SocketIOManager.sharedInstance.closeConnection()
        }
    }
    
    func performRealmMigration() {
        let versionNumber: UInt64 = 3
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: versionNumber,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < versionNumber) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        _ = try! Realm()
    }
    
    func checkRideRequest() {
        let realm = try! Realm()
        //Verificamos que el Ride guardado no sea solo una solicitud, validando que no tenga chatId asignado
        if let ride = realm.objects(Ride.self).first, ride.chatId == "" {
            try! realm.write {
                let rides = realm.objects(Ride.self)
                let clients = realm.objects(ClientOnRide.self)
                let paymentDetails = realm.objects(PaymentDetail.self)
                realm.delete(rides)
                realm.delete(clients)
                realm.delete(paymentDetails)
            }
            print("RIDE DELETED")
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                return false
        }
        
        if components.path.contains(Api.resetPassword) {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            guard var currentVC = UIApplication.shared.keyWindow?.rootViewController, let resetPasswordViewController = storyboard.instantiateViewController(withIdentifier: "UpdatePasswordViewController") as? UpdatePasswordViewController else {
                return false
            }
            
            resetPasswordViewController.accessToken = url.lastPathComponent.replacingOccurrences(of: "/", with: "")
            
            if  let presentedVC = currentVC.presentedViewController {
                currentVC = presentedVC
            }
            currentVC.present(resetPasswordViewController, animated: true, completion: nil)
            return true
        }
        
        return false
    }

}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("📨 FCM TOKEN -> \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: Constants.fcmTokenKey)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("🚫 Fail to register for remote Notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
//        parseNotification(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
//        parseNotification(userInfo: userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //ANTES DE MOSTRAR LA BURBUJA
        print("🔔 PRESENTARA UNA NOTIFICACION 🔔")
        print("\(notification.request.content.userInfo)")
        print("-🔔 PRESENTARA UNA NOTIFICACION 🔔-")
        completionHandler([.alert, .badge, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //TAP EN LA BURBUJA
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        getModule(from: userInfo)
    }
    
    func getModule(from userInfo: [AnyHashable : Any]) {
        guard let module = userInfo["module"] as? String else {
            print("🔔 NOTIFICATION WITH NO MODULE")
            return
        }
        print("🔔 NOTIFICATION MODULE -> \(module)")
        guard let mainVC = window?.rootViewController as? MainContainerViewController else {
            print("🔔 ROOTVIEWCONTROLLER IS NOT MAINCONTAINER")
            return
        }
        
        if module == "core" {
            if let rideData = userInfo as? [String: Any] {
                let ride = Ride(data: rideData)
                mainVC.showRideRequest(ride: ride)
            }
        } else {
            mainVC.openModule(module)
        }
        print("-------------------🔔----------------------")
    }
}
