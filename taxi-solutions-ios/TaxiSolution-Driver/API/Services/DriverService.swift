//
//  DriverService.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/26/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON

class DriverService: BaseServiceProtocol {
    
    static let sharedService = DriverService()
    
    func setConnection(_ connect: Bool,
                       successResponse success: @escaping(_ reponse: JSON) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "available_connection": connect
        ] as [String: AnyObject]
        POST(toEndpoint: "drivers/\(user.id)/connections", params: params, success: success, failure: failure)
    }
    
    func disableConnection(successResponse success: @escaping(_ response: JSON) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/connections/cancel", params: nil, success: success, failure: failure)
    }
    
    func getDocuments(successResponse success: @escaping(_ response: JSON) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/documents", params: nil, success: success, failure: failure)
    }
    
    func getConnections(timeFrame: String,
                        successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var filter = ""
        switch timeFrame {
        case RangeType.day.rawValue:
            filter = "day"
        case RangeType.week.rawValue:
            filter = "week"
        default:
            filter = "month"
        }
        GET(toEndpoint: "drivers/\(user.id)/connections?filterBy=\(filter)", params: nil, success: success, failure: failure)
    }
    
    func getEarnings(timeFrame: String,
                        successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        var filter = ""
        switch timeFrame {
        case RangeType.day.rawValue:
            filter = "day"
        case RangeType.week.rawValue:
            filter = "week"
        default:
            filter = "month"
        }
        GET(toEndpoint: "drivers/\(user.id)/profits?filterBy=\(filter)", params: nil, success: success, failure: failure)
    }
    
    func getGoals(successResponse success: @escaping(_ response: JSON) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/goals", params: nil, success: success, failure: failure)
    }
    
    func acceptRide(rideId: Int,
                    successResponse success: @escaping(_ response: JSON) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "driver_id": user.id
        ] as [String: AnyObject]
        POST(toEndpoint: "rides/\(rideId)/accept", params: params, success: success, failure: failure)
    }
    
    func setWaitingState(ride: Ride,
                         successResponse success: @escaping(_ response: JSON) -> Void,
                         failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        POST(toEndpoint: "rides/\(ride.rideId)/waiting-client", params: nil, success: success, failure: failure)
    }
    
    func startRide(ride: Ride,
                   successResponse success: @escaping(_ response: JSON) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let exceededTime = ride.timeWaiting - ride.maxTimeToWaitForClient
        var params = [
            "is_exceed_waiting_time": exceededTime > 0
            ] as [String : AnyObject]
        if exceededTime > 0 {
            params.updateValue(exceededTime as AnyObject, forKey: "seconds")
        }
        POST(toEndpoint: "rides/\(ride.rideId)/start-ride", params: params, success: success, failure: failure)
    }
    
    func getCancellationReasons(successResponse success: @escaping(_ response: JSON) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "rides/drivers/cancellation-reasons", params: nil, success: success, failure: failure)
    }
    
    func getPaymentDetails(ride: Ride,
                           successResponse success: @escaping(_ response: JSON) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "rides/\(ride.rideId)/get-payment-detail", params: nil, success: success, failure: failure)
    }
    
    func finishRide(ride: Ride,
                    paymentStatus: PaymentStatus,
                    pendingAmount: String,
                    successResponse success: @escaping(_ response: JSON) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "payment_status": paymentStatus.rawValue,
            "pending_amount": pendingAmount
            ] as [String: AnyObject]
        
        POST(toEndpoint: "rides/\(ride.rideId)/end-trip", params: params, success: success, failure: failure)
    }
    
    func getRecharges(successResponse success: @escaping(_ response: JSON) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/recharges", params: nil, success: success, failure: failure)
    }
    
    func getLastConsumptions(successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/last-consumptions", params: nil, success: success, failure: failure)
    }

    func getProfit(filterBy: String,
                   successResponse success: @escaping(_ response: JSON) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) {
        GET(toEndpoint: "drivers/\(user.id)/profits?filterBy=\(filterBy)", params: nil, success: success, failure: failure)
    }

    func getBalance(page: Int,
                    successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/balance?per_page=15&page=\(page)", params: nil, success: success, failure: failure)
    }

    func getLiquidation(page: Int,
                        successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "drivers/\(user.id)/liquidation-items?per_page=15&page=\(page)", params: nil, success: success, failure: failure)
    }
    
    func registerRecharges(type: String, amount: String, photo: [UIImage]?, cardToken: String? = nil,  successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        var params = [
            "type": type,
            "amount": amount
            ] as [String: AnyObject]

        if let cardToken = cardToken {
            params.updateValue(cardToken as AnyObject, forKey: "card_token")
        }
        
        if let photo = photo?.first {
            POSTImage(toEndpoint: "drivers/\(user.id)/recharges", key: "photo", images: [photo], params: params, success: success, failure: failure)
        } else {
            POST(toEndpoint: "drivers/\(user.id)/recharges", params: params, success: success, failure: failure)
        }
    }
    
}
