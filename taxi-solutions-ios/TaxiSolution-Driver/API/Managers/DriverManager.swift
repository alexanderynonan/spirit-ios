//
//  DriverManager.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/26/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class DriverManager {
    
    static let sharedManager = DriverManager()
    let realm = try! Realm()
    
    func setConnection(_ connect: Bool,
                       successResponse success: @escaping(_ driverId: Int) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.setConnection(connect, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let status = dictionary["status"]?.stringValue,
                status == "success",
                let data = dictionary["data"]?.dictionaryValue,
                let driverConnection = data["driver_connection"]?.dictionaryValue,
                let id = driverConnection["id"]?.intValue {
                success(id)
            } else {
                failure(NSError(message: "Error al habilitar conexión"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func disableConnection(successResponse success: @escaping() -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.disableConnection(successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let status = dictionary["status"]?.stringValue,
                status == "success",
                let data = dictionary["data"]?.dictionaryValue,
                let cancelled = data["cancel_driver_connection"]?.boolValue,
                cancelled {
                success()
            } else {
                failure(NSError(message: "Error al invalidar conexión"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getDocuments(successResponse success: @escaping(_ documents: [Document]) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getDocuments(successResponse: { (response) in
            let realm = try! Realm()
            var documents = [Document]()
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let documentsData = data["documents"]?.arrayValue {
                documentsData.forEach {
                    let document = Document(data: $0)
                    documents.append(document)
                    try! realm.write {
                        realm.add(document, update: .all)
                    }
                }
                success(documents)
            } else {
                failure(NSError(message: "Error al obtener documentos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getConnections(timeFrame: String,
                        successResponse success: @escaping(_ timeFrameDates: [ConnectionTimeFrameDate]) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getConnections(timeFrame: timeFrame, successResponse: { (response) in
            var timeFrameDates: [ConnectionTimeFrameDate] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let timeFrameDatesData = data["connections"]?.arrayValue {
                timeFrameDatesData.forEach {
                    let timeFrameDate = ConnectionTimeFrameDate(data: $0)
                    try! self.realm.write {
                        self.realm.add(timeFrameDate, update: .all)
                    }
                    timeFrameDates.append(timeFrameDate)
                }
                success(timeFrameDates)
            } else {
                failure(NSError(message: "Error al obtener registro de conexiones"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getEarnings(timeFrame: String,
                        successResponse success: @escaping(_ timeFrameDates: [EarningTimeFrameDate]) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getEarnings(timeFrame: timeFrame, successResponse: { (response) in
            var timeFrameDates: [EarningTimeFrameDate] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let timeFrameDatesData = data["profits"]?.arrayValue {
                timeFrameDatesData.forEach {
                    let timeFrameDate = EarningTimeFrameDate(data: $0)
                    timeFrameDates.append(timeFrameDate)
                }
                success(timeFrameDates)
            } else {
                failure(NSError(message: "Error al obtener registro de ganancias"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getGoals(successResponse success: @escaping(_ goals: [Goal]) -> Void,
                       failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getGoals(successResponse: { (response) in
            var goals: [Goal] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let goalsData = data["goals"]?.arrayValue {
                print("-----------------------*----------------------")
                goalsData.forEach {
                    let goal = Goal(data: $0)
                    try! self.realm.write {
                        self.realm.add(goal, update: .all)
                    }
                    goals.append(goal)
                }
                print("-----------------------x----------------------")
                success(goals)
            } else {
                failure(NSError(message: "Error al obtener objetivos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func acceptRide(rideId: Int,
                    successResponse success: @escaping(_ ride: Ride) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let realm = try! Realm()
        DriverService.sharedService.acceptRide(rideId: rideId, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryObject {
                let ride = Ride(data: data)
                defer {
                    success(ride)
                }
                guard let user = realm.objects(User.self).first else { return }
                try! realm.write {
                    user.rating = (data["driver_rating"] as? String ?? "").double
                    user.carBrand = data["car_brand"] as? String ?? ""
                    user.carColor = data["car_color"] as? String ?? ""
                }
            } else {
                failure(NSError(message: "Error al aceptar la solicitud del viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func setWaitingState(ride: Ride,
                         successResponse success: @escaping(_ waitingTime: Int) -> Void,
                         failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.setWaitingState(ride: ride, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let waitingTime = data["waiting_time"]?.stringValue {
                success(waitingTime.int ?? 0)
            } else {
                failure(NSError(message: "Error al notificar estado de espera"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func startRide(ride: Ride,
                   successResponse success: @escaping(_ ride: Ride) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.startRide(ride: ride, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryObject {
                let ride = Ride(data: data)
                success(ride)
            } else {
                failure(NSError(message: "Error al empezar el viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getCancellationReasons(successResponse success: @escaping(_ reasons: [OptionToSelect]) -> Void,
                                failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getCancellationReasons(successResponse: { (response) in
            let realm = try! Realm()
            var optionsToSelect: [OptionToSelect] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let reasonsData = data["reasons"]?.arrayValue {
                reasonsData.forEach {
                    let optionToSelect = OptionToSelect(data: $0, forType: "cancel_ride_reasons")
                    optionsToSelect.append(optionToSelect)
                    try! realm.write {
                        realm.add(optionToSelect, update: .all)
                    }
                }
                success(optionsToSelect)
            } else {
                failure(NSError(message: "Error al obtener razones de cancelación"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getPaymentDetails(ride: Ride,
                           successResponse success: @escaping(_ paymentDetail: PaymentDetail) -> Void,
                           failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getPaymentDetails(ride: ride, successResponse: { (response) in
            let realm = try! Realm()
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"] {
                if !ride.isInvalidated {
                    let paymentDetail = PaymentDetail(data: data, rideId: ride.rideId)
                    try! realm.write {
                        realm.add(paymentDetail, update: .all)
                    }
                    success(paymentDetail)
                } else {
                    failure(NSError(message: "El viaje fue elimnado de memoria"))
                }
            } else {
                failure(NSError(message: "Error al obtener detalle de pago"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func finishRide(ride: Ride,
                    paymentStatus: PaymentStatus,
                    pendingAmount: String,
                    successResponse success: @escaping(_ message: String) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.finishRide(ride: ride, paymentStatus: paymentStatus, pendingAmount: pendingAmount, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let status = dictionary["status"]?.stringValue, status == "success" {
                success("🟢 Viaje finalizado 🟢")
            } else {
                failure(NSError(message: "Error al finalizar viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getRecharges(successResponse success: @escaping(_ currentBalance: Double, _ recharges: [Recharge]) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getRecharges(successResponse: { (response) in
            var recharges = [Recharge]()
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionary, let currentBalance = data["current_balance"]?.stringValue, let rechargesData = data["recharges"]?.dictionary?["data"]?.arrayValue {
                rechargesData.forEach { rechargeData in
                    let recharge = Recharge(data: rechargeData)
                    try! self.realm.write {
                        self.realm.add(recharge, update: .all)
                    }
                    recharges.append(recharge)
                }
                success(currentBalance.double, recharges)
            } else {
                failure(NSError(message: "No se pudo obtener el listado de recargas"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getLastConsumptions(successResponse success: @escaping(_ lastConsumptions: [LastConsumption]) -> Void,
                             failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.getLastConsumptions(successResponse: { (response) in
            var consumptions = [LastConsumption]()
            let dictionary = response.dictionaryValue
            
            if let dataDictionary = dictionary["data"]?.dictionaryValue, let lastConsumption = dataDictionary["last_consumptions"]?.dictionaryValue, let data = lastConsumption["data"]?.arrayValue {
                
                for consumptionData in data {
                    let consumption = LastConsumption(consumptionData)
                    try! self.realm.write {
                        self.realm.add(consumption, update: .all)
                    }
                    consumptions.append(consumption)
                }
                
                success(consumptions)
            } else {
                failure(NSError(message: "No se pudo obtener el listado de últimos consumos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func registerRecharges(type: String, amount: String, photo: [UIImage]? = nil, cardToken: String? = nil, successResponse success: @escaping(_ response: JSON) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DriverService.sharedService.registerRecharges(type: type, amount: amount, photo: photo, cardToken: cardToken, successResponse: {(response) in
            let dictionary = response.dictionaryValue
            if let _ = dictionary["data"]?.dictionaryValue {
                success(response)
            }else {
                failure(NSError(message: "No se pudo recargar"))
            }
        }, failureResponse: {(error) in
            failure(error)
        })
    }

    func getProfit(filterBy: String,
                   successResponse success: @escaping(_ response: [EarningTimeFrameDate]) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) {
        DriverService.sharedService.getProfit(filterBy: filterBy, successResponse: {(response) in

            var earnings = [EarningTimeFrameDate]()
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue,
               let ridesArray = data["profits"]?.arrayValue  {
                for rideData in ridesArray {
                    let ride = EarningTimeFrameDate(data: rideData)
                    try! self.realm.write {
                        self.realm.add(ride, update: .all)
                    }
                    earnings.append(ride)
                }
                success(earnings)
            }
        }){(error) in
            failure(error)
        }
    }

    func getBalance(page: Int,
                   successResponse success: @escaping(_ response: Balance) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) {
        DriverService.sharedService.getBalance(page: page, successResponse: {(response) in
            
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue,
               let balanceJ = data["data"] {
                let balance = Balance(balanceJ)
                try! self.realm.write {
                    self.realm.add(balance, update: .all)
                }
                success(balance)
            }
        }){(error) in
            failure(error)
        }
    }

    func getLiquidation(page: Int,
                   successResponse success: @escaping(_ response: Liquidation) -> Void,
                   failureResponse failure: @escaping(_ error: NSError) -> Void) {
        DriverService.sharedService.getLiquidation(page: page, successResponse: {(response) in

            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue,
               let liquidationJ = data["data"] {
                let liquidation = Liquidation(liquidationJ)
                try! self.realm.write {
                    self.realm.add(liquidation, update: .all)
                }
                success(liquidation)
            }
        }){(error) in
            failure(error)
        }
    }
    
}
