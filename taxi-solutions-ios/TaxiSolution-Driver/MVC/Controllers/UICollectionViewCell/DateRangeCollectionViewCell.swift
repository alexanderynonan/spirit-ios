//
//  DateRangeCollectionViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class DateRangeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    var rangeType: RangeType?
    
    var connectionTimeFrameDate: ConnectionTimeFrameDate?
    var earningTimeFrameDate: EarningTimeFrameDate?
    
    override var isSelected: Bool {
        didSet {
            self.containerView.backgroundColor = isSelected ? Colors.primary : Colors.primary.withAlphaComponent(0.06)
            self.numberLabel.textColor = isSelected ? Colors.background : Colors.text
            self.monthLabel.textColor = isSelected ? Colors.background : Colors.text
        }
    }
    
    override func awakeFromNib() {
        let dateName = connectionTimeFrameDate?.name ?? earningTimeFrameDate?.earningDate ?? ""
        let titleComponents = dateName.components(separatedBy: "\n")
        numberLabel.isHidden = rangeType == .month ? true : false
        numberLabel.text = rangeType != .month ? titleComponents.first ?? "" : ""
        monthLabel.text = titleComponents.last ?? ""
    }
}
