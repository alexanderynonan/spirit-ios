//
//  ObjectivesTripTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ObjectivesTripTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var startingHourLabel: UILabel!
    @IBOutlet weak var endingHourLabel: UILabel!
    
    var goalRecord: GoalRecord?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        guard let goalRecord = goalRecord else { return }
        userNameLabel.text = goalRecord.name
        numberLabel.text = goalRecord.orderNumber
        dateLabel.text = goalRecord.date
        startingHourLabel.text = goalRecord.startingDate
        endingHourLabel.text = goalRecord.endingDate
    }
    
    

}
