//
//  ChargeTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/12/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ChargeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    
    var recharge: Recharge?
    
    private var isPending: Bool {
        guard let recharge = recharge else { return false }
        return recharge.status.lowercased() == "Rechazado".lowercased()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        loadContent()
    }
    
    private func loadContent() {
        guard let recharge = recharge else { return }
        amountLabel.text = String(format: "%.2f PEN", recharge.amount.double)
        dateLabel.text = recharge.dateString
        stateLabel.text = recharge.status
        stateLabel.textColor = isPending ? Colors.error : Colors.text
        paymentTypeLabel.text = recharge.paymentType
    }

}
