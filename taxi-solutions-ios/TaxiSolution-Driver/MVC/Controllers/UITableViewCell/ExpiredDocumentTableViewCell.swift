//
//  ExpiredDocumentTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/7/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ExpiredDocumentTableViewCell: UITableViewCell {
    @IBOutlet weak var documentNameLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    var document: Document?
    
    override func awakeFromNib() {
        self.selectionStyle = .none
        guard let document = self.document else { return }
        documentNameLabel.text = document.name
        expirationDateLabel.text = document.expiresAt
        expirationDateLabel.textColor = document.expired ? Colors.error : Colors.text
    }

}
