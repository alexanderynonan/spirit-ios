//
//  EarningRecordTableViewCell.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 8/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class EarningRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var metricsLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    
    var record: EarningRecord?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        guard let record = record else { return }
        timeLabel.text = record.duration.withDefault("--:-- - --:--")
        metricsLabel.text = record.totalMetrics.withDefault("'--min, --km")
        amountLabel.text = String(format: "PEN %.2f", record.amount.double)
        paymentMethodLabel.text = record.paymentMethod.withDefault("--")
    }

}
