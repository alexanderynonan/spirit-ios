//
//  ConnectionRecordTableViewCell.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ConnectionRecordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startedAtLabel: UILabel!
    @IBOutlet weak var finishedAtLabel: UILabel!
    
    var record: ConnectionRecord?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        guard let record = record else { return }
        startedAtLabel.text = record.startedAt.withDefault("--:--:--")
        finishedAtLabel.text = record.finishedAt.withDefault("--:--:--")
    }

}
