//
//  ConsumptionTableViewCell.swift
//  TaxiSvico-Driver
//
//  Created by MacBook Pro Fer on 8/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ConsumptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!

    var lastConsumption = LastConsumption()

    func loadData() {
        let type = lastConsumption.type ?? .service
        dateLabel.text = lastConsumption.createdAt
        addressLabel.text = (type == .service ? lastConsumption.address : lastConsumption.paymentMethod).withDefault("------")
        amountLabel.text = (type == .service ? "- " : "+ ") + lastConsumption.amount.double.string + " PEN"
        amountLabel.textColor = type == .service ? Colors.error : Colors.primary
        actionLabel.text = lastConsumption.action
    }
}
