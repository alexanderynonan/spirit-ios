//
//  TripRecordTableViewCell.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class TripRecordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timesLabel: UILabel!
    @IBOutlet weak var durationLengthLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
