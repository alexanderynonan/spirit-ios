//
//  ChargesViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/12/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ChargesViewController: UIViewController {
    
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var chargesTableView: UITableView!
    @IBOutlet weak var chargesTableViewHeight: NSLayoutConstraint!
    
    var recharges: [Recharge] = [] {
        didSet {
            chargesTableViewHeight.constant = chargesTableView.getMinimumHeight(forVC: self, items: recharges, tableViewCellHeight: tableViewCellHeight)
            chargesTableView.reloadData()
        }
    }
    let tableViewCellHeight: CGFloat = 90.0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
        chargesTableView.animate()
    }
    
    func configureContent() {
        chargesTableView.delegate = self
        chargesTableView.dataSource = self
        chargesTableView.register(UINib(nibName: "ChargeTableViewCell", bundle: nil), forCellReuseIdentifier: "ChargeTableViewCell")
    }
    
    func loadContent() {
        self.startLoader(waitingForObjectsOfType: Recharge.self)
        setData(Array(self.realm.objects(Recharge.self)))
        DriverManager.sharedManager.getRecharges(successResponse: { (currentBalance, recharges) in
            self.stopLoader()
            try! self.realm.write {
                self.user.currentBalance = currentBalance
            }
            self.setData(recharges)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData(_ recharges: [Recharge]) {
        self.creditLabel.text = String(format: "PEN %.2f", user.currentBalance)
        if self.recharges != recharges {
            self.recharges = recharges
        }
    }
    
    //MARK:- IBActions
    @IBAction func recharge(_ sender: Any) {
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.28, withTitle: "Tipo de Recarga", withOptions: OptionToSelect.driverRechargeType(), withConfirmButton: false)
    }

    @IBAction func consumptions(_ sender: UIButton) {
        performSegue(withIdentifier: "consumptionsSegue", sender: nil)
    }
}

extension ChargesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: recharges, emptyMessage: "Aún no has realizado recargas")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargeTableViewCell") as! ChargeTableViewCell
        cell.recharge = recharges[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension ChargesViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {
        print("DID SELECT")
    }
    
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        print("DISMISSED")
        if let optionKey = vc.selectedOption?.key, dismissed {
            switch optionKey {
            case "Depósito":
                self.performSegue(withIdentifier: "depositChargeSegue", sender: nil)
            case "Tarjeta":
                self.performSegue(withIdentifier: "creditCardChargeSegue", sender: nil)
            default:
                break
            }
        }
    }
}
