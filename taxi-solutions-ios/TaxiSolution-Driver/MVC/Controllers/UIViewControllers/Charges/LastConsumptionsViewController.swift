//
//  LastConsumptionsViewController.swift
//  TaxiSvico-Driver
//
//  Created by MacBook Pro Fer on 8/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class LastConsumptionsViewController: UIViewController {

    @IBOutlet weak var closeButton: CustomButton!
    @IBOutlet weak var lastConsumptionsTableView: UITableView!
    var loaded = false
        
    var lastConsumptions = [LastConsumption]() {
        didSet {
            lastConsumptionsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        configureContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !loaded {
            lastConsumptionsTableView.animate()
            loaded = true
        }
    }

    func configureContent() {
        lastConsumptionsTableView.delegate = self
        lastConsumptionsTableView.dataSource = self
        let nib = UINib.init(nibName: "ConsumptionsTableViewCell", bundle: nil)
        lastConsumptionsTableView.register(nib, forCellReuseIdentifier: "ConsumptionsTableViewCell")
        
        if #available(iOS 13.0, *) {
            closeButton.isHidden = true
        }
    }
    
    func loadData() {
        self.startLoader(waitingForObjectsOfType: LastConsumption.self)
        setData()
        DriverManager.sharedManager.getLastConsumptions(successResponse: {(lastConsumptions) in
            self.stopLoader()
            self.setData()
        }, failureResponse: {(error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        })
    }
    
    func setData() {
        let newConsumptions = Array(self.realm.objects(LastConsumption.self)).sorted { (c1, c2) -> Bool in
            guard let date1 = c1.date, let date2 = c2.date else { return c1.createdAt > c2.createdAt }
            return date1.compare(date2) == .orderedDescending
        }
        
        if self.lastConsumptions != newConsumptions {
            self.lastConsumptions = newConsumptions
        }
    }
}

extension LastConsumptionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lastConsumptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConsumptionsTableViewCell", for: indexPath) as! ConsumptionTableViewCell
        cell.lastConsumption = lastConsumptions[indexPath.row]
        cell.loadData()
        return cell
    }
}
