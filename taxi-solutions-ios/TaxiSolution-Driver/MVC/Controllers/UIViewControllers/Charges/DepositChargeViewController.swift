//
//  DepositChargeViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class DepositChargeViewController: UIViewController {
    
    @IBOutlet weak var voucherImageView: UIImageView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var attachImageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        amountTextField.delegate = self
    }
    
    @IBAction func recharge(_ sender: UIButton) {
        guard let voucherImage = voucherImageView.image else {
            self.showAlert(message: ValidationMessages.voucherImage)
            return
        }
        guard let amount = amountTextField.text, amount.isNumeric && amount.double != 0.0 else {
            self.showAlert(message: ValidationMessages.amountMoney)
            return
        }
        self.startLoader()
        DriverManager.sharedManager.registerRecharges(type: "cash", amount: amount, photo: [voucherImage], successResponse: { (response) in
            self.stopLoader()
            self.navigationController?.popViewController(animated: true)
            _ = PopUpViewController.instantiate(viewController: self.navigationController?.viewControllers.first ?? self, fullscreen: true, image: UIImage(named: "img_charge_in_process") ?? UIImage(), title: "Recarga en proceso", message: "SVICO debe validar el voucher enviado y el depósito, una vez aprobado se agregará el monto a su saldo")
        }, failureResponse: {(error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        })
    }
    
    @IBAction func attachPhoto(_ sender: UIButton) {
        let alertVC = UIAlertController(title: "Adjuntar foto", message: "Seleccione de desde dónde quiere adjuntar la foto", preferredStyle: .actionSheet)
        alertVC.view.tintColor = Colors.primary
        let cameraOption = UIAlertAction(title: "Cámara", style: .default) { (_) in
            print("CAMARA")
            self.pickImage(sourceType: .camera)
        }
        let galleryOption = UIAlertAction(title: "Galería", style: .default) { (_) in
            print("GALERIA")
            self.pickImage(sourceType: .photoLibrary)
        }
        let cancelOption = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertVC.addActions(actions: [cameraOption, galleryOption, cancelOption])
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func pickImage(sourceType: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension DepositChargeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let photo = info[.originalImage] as? UIImage {
            print("PHOTO -> \(photo.description)")
            picker.dismiss()
            attachImageLabel.text = ""
            voucherImageView.image = photo
        }
    }
}

extension DepositChargeViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = String(format: "%.2f", textField.text?.double ?? 0.0)
    }
}
