//
//  CreditCardChargeViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
#if targetEnvironment(simulator)
//import VisaNetSDK
#endif

class CreditCardChargeViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var creditCardsTableView: UITableView!
    @IBOutlet weak var creditCardsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var termsConditionsLabel: LinkClickableLabel!
    
    var savedCreditCards: [TokenizedCard] = [] {
        didSet {
            creditCardsTableViewHeight.constant = CGFloat(savedCreditCards.count + 1) * 65.0
            creditCardsTableView.animate {
                if self.savedCreditCards.count != 0 {
                    self.creditCardsTableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreditCardDetailViewControllerSegue",
            let vc = segue.destination as? CreditCardDetailViewController {
            vc.tokenizedCard = sender as? TokenizedCard
            vc.delegate = self
        }
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        scrollView.keyboardDismissMode = .onDrag
        
        creditCardsTableView.delegate = self
        creditCardsTableView.dataSource = self
        
        creditCardsTableView.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodTableViewCell")
    }
    
    func loadContent() {
        _ = termsConditionsLabel.setLinkedTextWithHandler(text: "Al agregar una tarjeta estarás aceptando los (Términos y Condiciones)", link: "Términos y Condiciones")
            {
                let termsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
                self.present(termsVC, animated: true, completion: nil)
            }
        self.startLoader()
        MainManager.sharedManager.getTokenizedCards(successResponse: { (tokenizedCards) in
            self.stopLoader()
            self.savedCreditCards = tokenizedCards
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
//    #if targetEnvironment(simulator)
//    func configureVisaNet(withToken token: String? = nil) {
//        print("💳 VISANET SECURITY TOKEN \(token)\n\n")
//        if let token = token {
//            Config.securityToken = token
//        } else {
//            Config.userCredential = NiubizInfo.user
//            Config.passwordCredential = NiubizInfo.password
//        }
//        Config.TK.Header.logoImage = #imageLiteral(resourceName: "img_logo_1")
//
//        //DATOS DE USUARIO
//        Config.TK.FirstNameField.defaultText = user.firstName
//        Config.TK.LastNameField.defaultText = user.lastName
//        Config.TK.email = user.email
//
//        //CUSTOMIZACION
//        Config.TK.textFieldFont = Fonts.regular()
//        Config.TK.dataChannel = .mobile
//        Config.TK.formBackground = Colors.background
//        Config.TK.AddCardButton.disableColor = Colors.error
//        Config.TK.AddCardButton.enableColor = Colors.primary
//        Config.TK.AddCardButton.titleTextColor = .white
//
//        //DATOS DE 'COMPRA'
//        Config.amount = 1.0
//        let currentDateInMillis = "\(Int(truncatingIfNeeded: Date().currentTimeMillis()))"
//        Config.TK.purchaseNumber = String(currentDateInMillis.suffix(12))
//        print("💰 PURCHASE NUMBER: \(Config.TK.purchaseNumber ?? "")")
//
//        //ANTIFRAUDE
//        var merchantDefineData = [String:Any]()
//        //COMENTADO PARA AMBIENTE DE TESTX
//        merchantDefineData["MDD4"] = "\(Config.TK.email)"
//        merchantDefineData["MDD32"] = "\(user.id)"
//        merchantDefineData["MDD75"] = "REGISTRADO"
//        merchantDefineData["MDD77"] = "\(user.daysRegistered)"
//        //CLIENTE FRECUENTE
//        merchantDefineData["MDD21"] = 0
//        Config.TK.Antifraud.merchantDefineData = merchantDefineData
//
//        Config.TK.endPointProdURL = "https:apiprod.vnforapps.com"
//        Config.TK.endPointDevURL = "https:://apisandbox.vnforappstest.com"
//        Config.TK.type = .dev
//        Config.merchantID = "456879852"
//    }
//    #endif
    
    func openVisaNetTokenizationForm() {
//        #if targetEnvironment(simulator)
//        configureVisaNet()
//        VisaNet.shared.delegate = self
//        _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
//        #endif
    }
    
    //MARK:- IBActions
    
    @IBAction func recharge(_ sender: Any) {
        guard let amount = amountTextField.text, amount.isNumeric else {
            self.showAlert(message: ValidationMessages.amountMoney)
            return
        }
        
        guard let selectedCellIndex = creditCardsTableView.indexPathForSelectedRow, let cell = creditCardsTableView.cellForRow(at: selectedCellIndex) as? PaymentMethodTableViewCell, let card = cell.card else {
            self.showAlert(message: "Seleccione con que tarjeta desea realizar la recarga por favor")
            return
        }
        
        startLoader()
        DriverManager.sharedManager.registerRecharges(type: "card", amount: amountTextField.text ?? "", cardToken: card.token, successResponse: {(response) in
            self.stopLoader()
            if let data = response.dictionaryValue["data"], let paymentData = data.dictionaryValue["payment_data"], let fullName = paymentData.dictionaryValue["full_name"]?.stringValue, let maskedCard = paymentData.dictionaryValue["masked_card_number"]?.stringValue, let createdAt = paymentData.dictionaryValue["created_at"]?.stringValue, let purchaseNumber = paymentData.dictionaryValue["purchase_number"]?.stringValue, let amount = paymentData.dictionaryValue["amount"]?.stringValue {
                            PopUpViewController.instantiate(viewController: self, delegate: self, fullscreen: true, image: UIImage(named: "img_no_credit") ?? UIImage(), title: "Recarga exitosa", message: "Se agregó el monto depositado al saldo \n\nNro. de pedido: \(purchaseNumber) \nCliente: \(fullName) \n\nTarjeta: \n\(card.brand) \n\(maskedCard) \nImporte pagado: S/ \(amount) \n\n\(createdAt)")

            }

        }, failureResponse: {(error) in
            self.stopLoader()
            if let purchaseNumber = error.userInfo["purchase_number"], let createdAt = error.userInfo["created_at"] {
                self.showAlert(withTitle: "Pago denegado", message: "\n\(error.localizedDescription)\n\nNro. de pedido \(purchaseNumber)\nImporte a pagar: S/ \(self.amountTextField.text ?? "")\n\n\(createdAt)")
            } else {
                self.showAlert(message: error.localizedDescription)
            }
        })
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CreditCardChargeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedCreditCards.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell") as! PaymentMethodTableViewCell
        let isCreditCard = savedCreditCards.indices.contains(indexPath.row)
        cell.paymentMethod = isCreditCard ? .creditCard : .none
        cell.card = isCreditCard ? savedCreditCards[indexPath.row] : nil
        cell.isSelected = isCreditCard ? savedCreditCards[indexPath.row].isDefault : false
        
        cell.delegate = self
        cell.awakeFromNib()
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! PaymentMethodTableViewCell
        switch cell.paymentMethod {
        case .none:
            openVisaNetTokenizationForm()
            return nil
        default:
            if cell.card?.isDefault == true {
                return nil
            }
            return indexPath
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PaymentMethodTableViewCell
        guard let card = cell.card else { return }
        self.startLoader()
        MainManager.sharedManager.setPredeterminedCard(card, successResponse: { (card) in
            self.stopLoader()
            cell.isSelected = true
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PaymentMethodTableViewCell
        cell.isSelected = false
    }
}

//MARK: - PaymentMethodTableViewCellDelegate
extension CreditCardChargeViewController: PaymentMethodTableViewCellDelegate {
    func paymentMethodTableViewCellDelegate(showDetailOfCell cell: PaymentMethodTableViewCell) {
        if let card = cell.card, savedCreditCards.contains(card) {
            performSegue(withIdentifier: "CreditCardDetailViewControllerSegue", sender: cell.card)
        } else {
            if cell.paymentMethod == .none {
                openVisaNetTokenizationForm()
            }
        }
    }
}

//MARK: - VisaNetDelegate
//#if targetEnvironment(simulator)
//extension CreditCardChargeViewController: VisaNetDelegate {
//    func registrationDidEnd(serverError: Any?, responseData: Any?) {
//        if let responseData = responseData as? [String: Any] {
//            if let tokenData = responseData["token"] as? [String: Any] {
//                let card = TokenizedCard(data: responseData)
//                self.startLoader()
//                MainManager.sharedManager.registerTokenizedCard(card, successResponse: { (card) in
//                    self.stopLoader()
//                    self.loadContent()
//                }) { (error) in
//                    self.stopLoader()
//                    self.showAlert(message: error.localizedDescription)
//                }
//            } else {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    if let orderData = responseData["order"] as? [String: Any], let actionCode = orderData["actionCode"] as? String, let actionDescription = orderData["actionDescription"] as? String {
//                        self.showAlert(withTitle: actionCode, message: actionDescription)
//                    } else {
//                        self.showAlert(message: "No se pudo registrar su tarjeta")
//                    }
//                }
//            }
//        } else if let error = responseData as? NSError {
//            self.view.isUserInteractionEnabled = false
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.view.isUserInteractionEnabled = true
//                self.showAlert(withTitle: "VisaNet \(error.code)", message: error.localizedDescription)
//            }
//        } else if let error = serverError as? NSError {
//            self.view.isUserInteractionEnabled = false
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                self.view.isUserInteractionEnabled = true
//                self.showAlert(withTitle: "VisaNet \(error.code)", message: error.localizedDescription)
//            }
//        }
//        print("DATA: \(responseData)")
//        print("ERROR: \(serverError)")
//    }
//}
//#endif

//MARK: - CreditCardDetailViewControllerDelegate
extension CreditCardChargeViewController: CreditCardDetailViewControllerDelegate {
    func creditCardDetailViewController(didRemoveCardInViewController vc: CreditCardDetailViewController) {
        self.savedCreditCards = []
        self.loadContent()
    }
}

//MARK: - PopUpViewControllerDelegate
extension CreditCardChargeViewController: PopUpViewControllerDelegate {
    func popUpViewController(viewController: PopUpViewController, didDismiss: Bool) {
        navigationController?.popViewController(animated: true)
    }
}

