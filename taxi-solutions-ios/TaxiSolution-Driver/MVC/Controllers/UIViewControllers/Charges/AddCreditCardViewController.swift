//
//  AddCreditCardViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/16/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class AddCreditCardViewController: UIViewController {
    
    let coverLayer = CALayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadContent()
        configureContent()
    }
    
    func configureContent() {
        hideKeyboardWhenTappedAround()
        coverLayer.frame = self.view.bounds
        coverLayer.backgroundColor = UIColor(named: "secondary_transparency")?.cgColor.copy(alpha: 0.5)
        coverLayer.opacity = 0.0
        view.layer.addSublayer(coverLayer)
    }
    
    func loadContent() {
        
    }
    
    @IBAction func showCVVInfo() {
        guard let infoVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "InfoCVVViewController") as? InfoCVVViewController else { return }
        infoVC.panDelegate = self
        infoVC.modalPresentationStyle = .overFullScreen
        self.present(infoVC, animated: true, completion: nil)
        changeLayerOpacity(toVisible: true)
    }
    
    func changeLayerOpacity(toVisible: Bool = true) {
        UIView.animate(withDuration: 0.2, animations: {
            self.coverLayer.opacity = toVisible ? 1.0 : 0.0
            self.view.layoutIfNeeded()
        })
    }

}

extension AddCreditCardViewController: PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController) {
        changeLayerOpacity(toVisible: false)
    }
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool) {
        let viewHeight = viewController.modalHeight
        if gesture.state == .changed {
            UIView.animate(withDuration: 5.0, animations: {
                self.coverLayer.opacity = Float((viewHeight - gesture.translation(in: self.view).y) / viewHeight)
                self.view.layoutIfNeeded()
            })
            
        }
        if gesture.state == .ended || dismissed {
            changeLayerOpacity(toVisible: false)
        }
    }
}
