//
//  EarningsViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class EarningsViewController: UIViewController {

    @IBOutlet weak var rangeTypeLabel: UILabel!
    @IBOutlet weak var totalTravelsLabel: UILabel!
    @IBOutlet weak var timeConnectedLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var totalEarningsLabel: UILabel!
    @IBOutlet weak var totalCashLabel: UILabel!
    @IBOutlet weak var contentScrollView: UIScrollView!

    var currentSheet = 0 {
        didSet {
            if !earningsDates.isEmpty {
                selectedTimeFrameDate = earningsDates[currentSheet]
            }
        }
    }

    var rangeType: RangeType {
        return RangeType.allCases.first(where: { $0.rawValue == selectedTimeFrame?.name }) ?? RangeType.day
    }
    
    //Ex.: Por día, por semana, por mes
    var timeFrameOptions = {
        return OptionToSelect.timeFrames()
    }()

    var selectedTimeFrame: OptionToSelect? {
        didSet {
            setDateRange()
        }
    }

    var earningsDates: [EarningTimeFrameDate] = [] {
        didSet {
            self.selectedTimeFrameDate = earningsDates.first
            setUpSlideScrollView(sheets: createSheets())
        }
    }

    var selectedTimeFrameDate: EarningTimeFrameDate? {
        didSet {
            guard let selectedTimeFrameDate = selectedTimeFrameDate else { return }
            var earnings = 0.0
            var cashPaid = 0.0
            selectedTimeFrameDate.earningRecords.forEach {
                earnings += $0.profit
                if $0.paymentMethod == "Efectivo" {
                    cashPaid += $0.amount.double
                }
            }
            totalTravelsLabel.text = String(selectedTimeFrameDate.earningRecords.count)
            timeConnectedLabel.text = "\(selectedTimeFrameDate.hoursConnected)h \(selectedTimeFrameDate.minutesConnected)min"
            totalAmountLabel.text = String(format: "S/ %.2f", selectedTimeFrameDate.totalAmount.double)
            totalEarningsLabel.text = String(format: "S/ %.2f", earnings)
            totalCashLabel.text = String(format: "S/ %.2f", cashPaid)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        contentScrollView.delegate = self
    }
    
    func loadContent() {
        selectedTimeFrame = timeFrameOptions.first
    }
    
    func setDateRange() {
        guard let rangeTypeName = selectedTimeFrame?.name else { return }
        guard let rangeTypeKey = selectedTimeFrame?.key else { return }
        rangeTypeLabel.text = "Mostrar por \(rangeTypeName.lowercased())"
        
        self.startLoader()
        DriverManager.sharedManager.getProfit(filterBy: rangeTypeKey, successResponse: { (earnings) in
            self.stopLoader()
            self.earningsDates = earnings
        }) { (error) in
            self.stopLoader()
        }
    }

    private func createSheets() -> [UIView] {
        var pages = [UIView]()
        earningsDates.forEach {
            let page = Bundle.main.loadNibNamed("EarningsView", owner: self, options: nil)?.first as! EarningsView
            page.earning = $0
            page.loadData()
            pages.append(page)
        }
        return pages
    }

    private func setUpSlideScrollView(sheets: [UIView]) {
        contentScrollView.frame = CGRect(x: 0, y: 0, width: contentScrollView.frame.width, height: contentScrollView.frame.height)
        contentScrollView.contentSize = CGSize(width: contentScrollView.frame.width, height: contentScrollView.frame.height)
        var i = 0
        DispatchQueue.main.async {
            sheets.forEach({
                $0.frame = CGRect(x: self.view.frame.width * CGFloat(i), y: 0, width: self.contentScrollView.frame.width, height: self.contentScrollView.frame.height)
                self.contentScrollView.addSubview($0)
                i += 1
            })
        }
    }
    
    @IBAction func changeFilter(_ sender: UIButton) {
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.35, withTitle: "Mostrar por", withOptions: timeFrameOptions, selectedOption: selectedTimeFrame ?? timeFrameOptions[0], withConfirmButton: false)
    }

    @IBAction func scrollButtonPressed(_ sender: UIButton) {
        let positionX = contentScrollView.contentOffset.x
        let viewWidth = contentScrollView.bounds.size.width
        if sender.tag == 1 {
            if currentSheet != earningsDates.count - 1 {
                let contentOffset = CGPoint(x: positionX + viewWidth, y: 0)
                contentScrollView.setContentOffset(contentOffset, animated: true)
            }
        } else {
            if currentSheet != 0 {
                let contentOffset = CGPoint(x: positionX - viewWidth, y: 0)
                contentScrollView.setContentOffset(contentOffset, animated: true)
            }
        }
    }

}

extension EarningsViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {}
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if selectedTimeFrame != vc.selectedOption {
            selectedTimeFrame = vc.selectedOption
        }
    }
}

extension EarningsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == contentScrollView {
            currentSheet = Int(round(contentScrollView.contentOffset.x/view.frame.width))
            if contentScrollView.contentOffset.y > 0 {
                contentScrollView.contentOffset = CGPoint(x: contentScrollView.contentOffset.x, y: 0)
            }
            if contentScrollView.contentOffset.y < 0 {
                contentScrollView.contentOffset = CGPoint(x: contentScrollView.contentOffset.x, y: 0)
            }
        }
    }
}
