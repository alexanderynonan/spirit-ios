//
//  ObjectivesViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class ObjectivesViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var progressContainerView: UIView!
    @IBOutlet weak var progressValueLabel: UILabel!
    @IBOutlet weak var progressUnitLabel: UILabel!
    @IBOutlet weak var circularProgressBarView: MBCircularProgressBarView!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var goalRecordsTableView: UITableView!
    @IBOutlet weak var goalRecordsTableViewHeight: NSLayoutConstraint!
    
    var goals: [Goal] = [] {
        didSet {
            if goals.first != nil {
                currentGoalIndex = 0
            }
        }
    }
    var currentGoalIndex = 0 {
        didSet {
            changeGoalDataToShow()
        }
    }
    var goalRecords: [GoalRecord] = [] {
        didSet {
            goalRecordsTableViewHeight.constant = goalRecordsTableView.getMinimumHeight(forVC: self, items: goalRecords, tableViewCellHeight: tableViewCellHeight)
            goalRecordsTableView.animate()
        }
    }
    let tableViewCellHeight: CGFloat = 130.0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        goalRecordsTableView.delegate = self
        goalRecordsTableView.dataSource = self
        goalRecordsTableView.register(UINib(nibName: "ObjectivesTripTableViewCell", bundle: nil), forCellReuseIdentifier: "ObjectivesTripTableViewCell")
        circularProgressBarView.fontColor = .clear
    }
    
    func loadContent() {
        self.startLoader(waitingForObjectsOfType: Goal.self)
        self.setData()
        DriverManager.sharedManager.getGoals(successResponse: { (goals) in
            self.stopLoader()
            self.setData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData() {
        let newGoals = Array(realm.objects(Goal.self))
        if self.goals != newGoals {
            self.goals = newGoals
        }
        self.checkButtonAvailability()
    }
    
    func changeGoalDataToShow() {
        let goal = goals[currentGoalIndex]
        goalRecords = Array(goal.goalRecords)
        UIView.transition(with: self.progressContainerView, duration: 0.35, options: [.transitionCrossDissolve], animations: {
            self.titleLabel.text = goal.name
            self.descriptionLabel.text = "Del \(goal.startingDate) al \(goal.endingDate)"
            self.circularProgressBarView.value = goal.current.float * 0.8
            self.circularProgressBarView.maxValue = goal.total.float
            self.progressValueLabel.text = "\(goal.current)/\(goal.total)"
            self.progressUnitLabel.text = goal.units
            self.rewardLabel.text = goal.reward
        }, completion: nil)
    }
    
    @IBAction func changeGoalToShow(_ sender: UIButton) {
        if sender.tag == 0 {
            if goals.indices.contains(currentGoalIndex - 1) {
                currentGoalIndex -= 1
            }
        }
        if sender.tag == 1 {
            if goals.indices.contains(currentGoalIndex + 1) {
                currentGoalIndex += 1
            }
        }
        checkButtonAvailability()
    }
    
    func checkButtonAvailability() {
        if !goals.indices.contains(currentGoalIndex + 1) {
            forwardButton.isEnabled = false
        } else {
            forwardButton.isEnabled = true
        }
        
        if !goals.indices.contains(currentGoalIndex - 1) {
            backwardButton.isEnabled = false
        } else {
            backwardButton.isEnabled = true
        }
    }
}

extension ObjectivesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewCellHeight
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: goalRecords, emptyMessage: "Aún no tienes registros para cumplir este objetivo")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectivesTripTableViewCell") as! ObjectivesTripTableViewCell
        cell.goalRecord = goalRecords[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 0.5, completionDelegate: AnyObject? = nil, backwards: Bool = false) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = backwards ? -CGFloat(.pi * 2.0) : CGFloat(.pi * 2.0)
        rotateAnimation.duration = duration

        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
