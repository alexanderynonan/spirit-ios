//
//  MyProfileViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage

class MyProfileViewController: UIViewController {

    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var bankTextField: UITextField!
    @IBOutlet weak var bankNumberTextField: UITextField!
    @IBOutlet weak var docNumberTextField: CustomTextField!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var lastNameTextField: CustomTextField!


    var viewHeight: CGFloat = 0
    var headerViewMaxHeight: CGFloat = 0
    var headerViewMinHeight: CGFloat = 0
    
    lazy var profileDataVC: ProfileDataViewController = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDataViewController") as? ProfileDataViewController ?? ProfileDataViewController()
        vc.delegate = self
        return vc
    }()
    
    lazy var profileDocumentsVC: ProfileDocumentsViewController = {
        return self.storyboard?.instantiateViewController(withIdentifier: "ProfileDocumentsViewController") as? ProfileDocumentsViewController ?? ProfileDocumentsViewController()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }

    func configureContent() {
        Tags.show(viewController: self, withTextColor: Colors.text.withAlphaComponent(0.35), font: Fonts.semibold(13.0), position: .bottomRight)
        Tags.show(self.user.id.string, viewController: self, withTextColor: Colors.text.withAlphaComponent(0.35), font: Fonts.semibold(13.0), position: .topRight)

        viewHeight = self.view.frame.height
        headerViewMaxHeight = (viewHeight * 0.075) + UIApplication.shared.statusBarFrame.height + 20
        + (viewHeight * 0.0615)
        headerViewMinHeight = (viewHeight * 0.075) + UIApplication.shared.statusBarFrame.height + 20
        self.view.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToProfileView), name: .goToProfileView, object: nil)
    }
    
    @objc func goToProfileView() {
        self.loadContent()
        self.profileDataVC.loadContent()
        self.navigationController?.popToViewController(self, animated: true)
    }
    
    func loadContent() {
        imageView.sd_setImage(with: user.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [], completed: nil)
        ratingLabel.text = user.rating.oneDecimalString
//        dniLabel.text = "\(user.documentType): \(user.documentNumber.hiddenCharacters(4))"
//        serviceTypeLabel.text = user.serviceName.withDefault("ESTÁNDAR")
        headerTitleLabel.text = "\(user.firstName.components(separatedBy: " ").first ?? "") \(user.lastName.components(separatedBy: " ").first ?? "")"
        phoneTextField.text = user.phone
        emailTextField.text = user.email
        bankTextField.text = user.bankName
        bankNumberTextField.text = user.bankAccountNumber
        docNumberTextField.text = user.documentNumber
        nameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        
        self.startLoader(waitingForObjectsOfType: Document.self)
        setData()
        DriverManager.sharedManager.getDocuments(successResponse: { (documents) in
            self.stopLoader()
            self.setData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData() {
        let newDocuments = Array(realm.objects(Document.self))
        if self.profileDocumentsVC.documents != newDocuments {
            self.profileDocumentsVC.documents = newDocuments
        }
    }
    
    @IBAction func updateInfoDriver(sender: UIButton) {
        guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "UpdatePasswordViewController") as? UpdatePasswordViewController else { return }
        vc.updatePassword = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MyProfileViewController: ProfileDataViewControllerDelegate {
    func profileDataViewController(updatePersonalData data: PersonalData) {
        switch data {
        case .phone, .email, .bank, .accountNumber:
            guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PhoneNumberViewController") as? PhoneNumberViewController else { return }
            vc.dataToUpdate = data
            vc.updateData = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .password:
            guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "UpdatePasswordViewController") as? UpdatePasswordViewController else { return }
            vc.updatePassword = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
