//
//  ProfileDataViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip

protocol ProfileDataViewControllerDelegate {
    func profileDataViewController(updatePersonalData data: PersonalData)
}

class ProfileDataViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var bankTextField: UITextField!
    @IBOutlet weak var bankNumberTextField: UITextField!
    
    var delegate: ProfileDataViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContent()
    }
    
    func loadContent() {
        phoneTextField.text = user.phone
        emailTextField.text = user.email
        bankTextField.text = user.bankName.withDefault("Agregar banco")
        bankNumberTextField.text = user.bankAccountNumber.withDefault("Agregar número de tarjeta")
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Datos personales")
    }
    
    @IBAction func updateInfo(sender: UIButton) {
        let personalData = PersonalData.allCases.first(where: { $0.rawValue == sender.tag })
        if let datatoUpdate = personalData {
            delegate?.profileDataViewController(updatePersonalData: datatoUpdate)
        }
    }

}
