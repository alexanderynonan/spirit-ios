//
//  ProfileDocumentsViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/6/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ProfileDocumentsViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var documentsTableView: UITableView!
    @IBOutlet weak var documentsTableViewHeight: NSLayoutConstraint!
    var documents: [Document] = [] {
        didSet {
            guard viewIfLoaded != nil else { return }
            documentsTableView.reloadData()
        }
    }
    var loaded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !loaded {
            loaded = true
            documentsTableView.animate()
        }
    }
    
    func configureContent() {
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
        let documentCellNib = UINib(nibName: "ExpiredDocumentTableViewCell", bundle: nil)
        documentsTableView.register(documentCellNib, forCellReuseIdentifier: "ExpiredDocumentTableViewCell")
    }
    
    func loadContent() {
        documentsTableView.animate()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Documentos")
    }
}

extension ProfileDocumentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = documents.count
        documentsTableViewHeight.constant = CGFloat(numberOfRows * 50)
        return numberOfRows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpiredDocumentTableViewCell") as! ExpiredDocumentTableViewCell
        cell.document = documents[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}
