//
//  RidesViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RidesViewController: UIViewController {
    
    @IBOutlet weak var rideRecordsTableView: UITableView!
    @IBOutlet weak var rideRecordsTableViewHeight: NSLayoutConstraint!
    var loaded = false
    
    var tableViewCellHeight = UIApplication.shared.keyWindow!.frame.height * 0.23
    var rideRecords: [RideRecord] = [] {
        didSet {
            guard viewIfLoaded != nil else { return }
            rideRecordsTableViewHeight.constant = rideRecordsTableView.getMinimumHeight(forVC: self, items: rideRecords, tableViewCellHeight: tableViewCellHeight)
            rideRecordsTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
        rideRecordsTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !loaded {
            rideRecordsTableView.animate()
            loaded = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if
            segue.identifier == "RideDetailViewControllerSegue",
            let vc = segue.destination as? RideDetailViewController,
            let rideId = sender as? Int {
            vc.rideId = rideId
        }
    }
    
    func configureContent() {
        rideRecordsTableView.delegate = self
        rideRecordsTableView.dataSource = self
        rideRecordsTableView.register(UINib(nibName: "RideTableViewCell", bundle: nil), forCellReuseIdentifier: "RideTableViewCell")
    }
    
    func loadContent() {
        self.startLoader(waitingForObjectsOfType: RideRecord.self)
        setData()
        MainManager.sharedManager.getRideRecords(successResponse: { (rideRecords) in
            self.stopLoader()
            self.setData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData() {
        let newRideRecords = Array(realm.objects(RideRecord.self)).sorted { (r1, r2) -> Bool in
            guard let date1 = r1.rideAtDate, let date2 = r2.rideAtDate else { return r1.rideAtString > r2.rideAtString }
            return date1.compare(date2) == .orderedDescending
        }
        if self.rideRecords != newRideRecords {
            self.rideRecords = newRideRecords
        }
    }
}

extension RidesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewCellHeight
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: rideRecords, emptyMessage: "Aún no has realizado viajes")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideTableViewCell") as! RideTableViewCell
        cell.rideRecord = rideRecords[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "RideDetailViewControllerSegue", sender: rideRecords[indexPath.row].id)
    }
}
