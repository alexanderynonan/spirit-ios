//
//  ConnectionsViewController.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 3/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ConnectionsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var rangeTypeLabel: UILabel!
    @IBOutlet weak var dateRangesCollectionView: UICollectionView!
    @IBOutlet weak var dateRangesCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var connectionsTableView: UITableView!
    @IBOutlet weak var connectionsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalDailyConnectionLabel: UILabel!
    
    //MARK: - Variables
    var rangeType: RangeType {
        return RangeType.allCases.first(where: { $0.rawValue == selectedTimeFrame?.key }) ?? RangeType.day
    }
    
    //Ex.: Por día, por semana, por mes
    var timeFrameOptions = {
        return OptionToSelect.timeFrames()
    }()
    var selectedTimeFrame: OptionToSelect? {
        didSet {
            setDateRange()
        }
    }
    
    //Ex.: 17 Mayo, 18 Mayo, 25 - 30 Marzo, Marzo
    var timeFrameDates: [ConnectionTimeFrameDate] = [] {
        didSet {
            self.dateRangesCollectionView.animate()
            self.selectedTimeFrameDate = timeFrameDates.first
        }
    }
    var selectedTimeFrameDate: ConnectionTimeFrameDate? {
        didSet {
            guard let selectedTimeFrameDate = selectedTimeFrameDate else { return }
            self.totalDailyConnectionLabel.text = selectedTimeFrameDate.totalTimeConnected
            
            var dateWithRecordsMatrix: [DateWithConnectionRecords] = []
            let dates = Array(Set(selectedTimeFrameDate.connectionRecords.map { $0.date })).sorted(by: >)
            dates.forEach { date in
                let records = Array(selectedTimeFrameDate.connectionRecords.filter({ $0.date == date }))
                let dateWithRecords = DateWithConnectionRecords(date: date.toText(withFormat: "dd/MM/yyyy") ?? "", records: records, open: false)
                dateWithRecordsMatrix.append(dateWithRecords)
            }
            self.dateWithRecordsMatrix = dateWithRecordsMatrix
        }
    }
    
    var dateWithRecordsMatrix: [DateWithConnectionRecords] = [] {
        didSet {
            guard let selectedTimeFrameDate = selectedTimeFrameDate else { return }
            var totalHeadersHeight = CGFloat(dateWithRecordsMatrix.count) * 50.0
            var totalRowsHeight = CGFloat(dateWithRecordsMatrix.filter({ $0.open }).reduce(0, { $0 + $1.records.count })) * 85.0
            if rangeType == .day {
                totalHeadersHeight = 0
                totalRowsHeight = CGFloat(dateWithRecordsMatrix.reduce(0, { $0 + $1.records.count })) * 85.0
            }
            self.connectionsTableViewHeight.constant = totalHeadersHeight + totalRowsHeight
            if selectedTimeFrame?.key == "día" {
                self.connectionsTableView.animate()
            } else {
                UIView.transition(with: self.connectionsTableView, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                    self.connectionsTableView.reloadData()
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        dateRangesCollectionView.delegate = self
        dateRangesCollectionView.dataSource = self
        dateRangesCollectionView.register(UINib(nibName: "DateRangeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateRangeCollectionViewCell")
        dateRangesCollectionView.contentInset = UIEdgeInsets(top: 0, left: self.view.frame.width * 0.065, bottom: 0, right: self.view.frame.width * 0.065)
        
        connectionsTableView.delegate = self
        connectionsTableView.dataSource = self
        connectionsTableView.register(UINib(nibName: "ConnectionRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionRecordTableViewCell")
    }
    
    func loadContent() {
        selectedTimeFrame = timeFrameOptions.first
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.inactivity, message: "Si se queda en la misma ubicación en un periodo de 30 min se dejará de contabilizar el tiempo de conexión", andConfirmText: "CONFIRMAR") { vc, confirmed in
//                vc.close()
//            }
//        }
    }
    
    func setDateRange() {
        guard let rangeTypeName = selectedTimeFrame?.key else { return }
        rangeTypeLabel.text = "Mostrar por \(rangeTypeName.lowercased())"
        
        self.startLoader()
        DriverManager.sharedManager.getConnections(timeFrame: rangeTypeName, successResponse: { (timeFrameDates) in
            self.stopLoader()
            self.timeFrameDates = timeFrameDates
        }) { (error) in
            self.stopLoader()
        }
    }
    
    func getSizeForDateRangeCollectionViewCell() -> CGSize {
        var width: CGFloat = 0.0
        var height: CGFloat = 0.0
        switch rangeType {
        case .day:
            width = view.frame.width * 0.2
            height = width
        case .week:
            width = view.frame.width * 0.25
            height = view.frame.width * 0.2
        case .month:
            width = view.frame.width * 0.25
            height = width * 0.46
        }
        
        UIView.transition(with: self.view, duration: 0.25, options: [], animations: {
            self.dateRangesCollectionViewHeight.constant = height + 10.0
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        return CGSize(width: width, height: height)
    }
    
    @IBAction func changeFilter(_ sender: UIButton) {
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.35, withTitle: "Mostrar por", withOptions: timeFrameOptions, selectedOption: selectedTimeFrame ?? timeFrameOptions[0], withConfirmButton: false)
    }

}

extension ConnectionsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getSizeForDateRangeCollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeFrameDates.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateRangeCollectionViewCell", for: indexPath) as! DateRangeCollectionViewCell
        cell.rangeType = rangeType
        cell.connectionTimeFrameDate = timeFrameDates[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        let cell = collectionView.cellForItem(at: indexPath) as! DateRangeCollectionViewCell
        selectedTimeFrameDate = cell.connectionTimeFrameDate
    }
}

extension ConnectionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        let numberOfSections = dateWithRecordsMatrix.count
        if numberOfSections == 0 {
            tableView.setEmptyMessage("No hay registros de conexión este día")
        } else {
            tableView.restore()
        }
        return numberOfSections
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if rangeType == .day {
            return .zero
        }
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if rangeType == .day {
            return nil
        }
        guard let dateHeaderView = UINib(nibName: "DateHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? DateHeaderView else { return nil }
        dateHeaderView.section = section
        dateHeaderView.delegate = self
        dateHeaderView.dateWithConnectionRecords = dateWithRecordsMatrix[section]
        return dateHeaderView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dateWithRecords = dateWithRecordsMatrix[section]
        if rangeType == .day {
            return dateWithRecords.records.count
        }
        return dateWithRecords.open ? dateWithRecords.records.count : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionRecordTableViewCell") as! ConnectionRecordTableViewCell
        cell.record = dateWithRecordsMatrix[indexPath.section].records[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension ConnectionsViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {}
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if selectedTimeFrame != vc.selectedOption {
            selectedTimeFrame = vc.selectedOption
        }
    }
}

extension ConnectionsViewController: DateHeaderViewDelegate {
    func dateHeaderView(_ view: DateHeaderView, tapped: Bool) {
        dateWithRecordsMatrix[view.section].open = !dateWithRecordsMatrix[view.section].open
    }
}
