//
//  RegisterDocumentsViewController.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 12/20/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RegisterDocumentsViewController: RegisterViewController {

    @IBOutlet weak var documentButton: CustomButton!
    @IBOutlet weak var soatButton: CustomButton!
    @IBOutlet weak var technicalReviewButton: CustomButton!
    @IBOutlet weak var technicalReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var plateButton: CustomButton!
    private var selectedButton = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        technicalReviewHeight.constant = newUser.requiresTechnicalReview ? 0 : 115
        // Do any additional setup after loading the view.
    }

    @IBAction func selectPhoto(_ sender: UIButton) {
        selectedButton = sender.tag
        let alertController = UIAlertController(title: "Foto de perfil", message: "Seleccione desde dónde quiere subir la imagen", preferredStyle: .actionSheet)
        let imagePickerController = UIImagePickerController()
        imagePickerController.view.tintColor = Colors.primary
        imagePickerController.delegate = self
        let camera = UIAlertAction(title: "Cámara", style: .default) { (_) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let gallery = UIAlertAction(title: "Galería", style: .default) { (_) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(cancel)
        alertController.view.tintColor = Colors.primary

        self.present(alertController, animated: true, completion: nil)
    }

    private func setImageInKey(_ image: UIImage) {
        switch selectedButton {
        case 1:
            newUser.documentPhoto = image.imageToBase64()
            documentButton.setImage(#imageLiteral(resourceName: "ic_accept"), for: .normal)

        case 2:
            newUser.soatPhoto = image.imageToBase64()
            soatButton.setImage(#imageLiteral(resourceName: "ic_accept"), for: .normal)
        case 3:
            newUser.technicalReviewPhoto = image.imageToBase64()
            technicalReviewButton.setImage(#imageLiteral(resourceName: "ic_accept"), for: .normal)
        case 4:
            newUser.platePhoto = image.imageToBase64()
            plateButton.setImage(#imageLiteral(resourceName: "ic_accept"), for: .normal)
        default:
            print("The image does not belong to any key")
        }
    }

    @IBAction func registerDriver(_ sender: Any) {
        if newUser.documentPhoto.isEmptyOrWhitespace() {
            showAlert(message: ValidationMessages.documentPhotoEmpty)
            return
        }

        if newUser.soatPhoto.isEmptyOrWhitespace() {
            showAlert(message: ValidationMessages.soatPhotoEmpty)
            return
        }

        if newUser.platePhoto.isEmptyOrWhitespace() {
            showAlert(message: ValidationMessages.platePhotoEmpty)
            return
        }
        
        if !newUser.requiresTechnicalReview && newUser.technicalReviewPhoto.isEmptyOrWhitespace() {
            showAlert(message: ValidationMessages.technicalReviewPhotoEmpty)
            return
        }

        startLoader()
        UserManager.sharedManager.register(user: newUser, successResponse: { (driver) in
            self.stopLoader()
            _ = self.setRootViewController(fromStoryboard: "Main")
            SocketIOManager.sharedInstance.establishConnection()
            print("DRIVER: \(driver)")
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

}


extension RegisterDocumentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image: UIImage?
        if let photo = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print("🌆 Image file size: \(photo.fileSizeInMB) MBs")
            if photo.fileSizeInMB <= App.maxFileSize {
                image = photo
            } else {
                picker.showAlert(message: "La imágen debe pesar menos de \(App.maxFileSize) Mb.\nPeso actual de imagen: \(photo.fileSizeInMB) Mb")
                return
            }
        } else {
            self.showAlert(message: "Ocurrió un error al obtener la foto. Por favor, vuelva a intentarlo")
        }

        picker.dismiss(animated: true) {
            guard let image = image else { return }
            let customAlert = CustomAlertController(title: "Foto seleccionada", message: "¿Seguro desea adjuntar esta imagen?", preferredStyle: .alert)
            customAlert.setTitleImage(image.sd_resizedImage(with: CGSize(width: 100, height: 100), scaleMode: .aspectFill), cornerRadius: 25.0)
            let okayAction = UIAlertAction(title: "Ok", style: .default) { (_) in

                self.setImageInKey(image)
            }
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            customAlert.addActions(actions: [okayAction, cancelAction])
            self.present(customAlert, animated: true, completion: nil)
        }
    }
}
