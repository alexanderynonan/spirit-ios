//
//  RegisterSecondStepViewController.swift
//  Taxi
//
//  Created by PeruApps Dev on 2/7/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RegisterThirdStepViewController: RegisterViewController {
    
    @IBOutlet weak var licenseNumberTextField: UITextField!
    @IBOutlet weak var drivingLicenseCategoryTextField: UITextField!
    @IBOutlet weak var licensePlateTextField: UITextField!
    @IBOutlet weak var tucExpiringDateTextField: UITextField!
    @IBOutlet weak var requiresTechnicalReviewSwitch: SwitchButton!
    @IBOutlet weak var termsAgreementSwitch: SwitchButton!
    let tucExpiringDatePicker = UIDatePicker()
    var licenseCategories: [OptionToSelect] = []
    var selectedLicenseCategory = OptionToSelect() {
        didSet {
            drivingLicenseCategoryTextField.text = selectedLicenseCategory.name
            newUser.category = selectedLicenseCategory.name
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RegisterDocumentViewControllerSegue" {
            let vc = segue.destination as! RegisterDocumentsViewController
            vc.newUser = newUser
        }
    }
    
    func configureContent() {
        tucExpiringDateTextField.inputView = tucExpiringDatePicker
        tucExpiringDatePicker.datePickerMode = .date
        tucExpiringDatePicker.timeZone = TimeZone(identifier: "es_PE")
        tucExpiringDatePicker.locale = Locale(identifier: "es_PE")
        tucExpiringDatePicker.addTarget(self, action: #selector(setDate(_:)), for: .valueChanged)
    }
    
    func loadContent() {
        startLoader()
        MainManager.sharedManager.getLicenseCategories(successResponse: { (licenseCategories) in
            self.licenseCategories = licenseCategories
            self.stopLoader()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @objc func setDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd / MM / yyyy"
        tucExpiringDateTextField.text = dateFormatter.string(from: sender.date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        newUser.TUCExpiringDate = dateFormatter.string(from: sender.date)
    }

    @IBAction func showTerms(_ sender: UIButton) {
        let termsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.present(termsVC, animated: true, completion: nil)
    } 
    
    @IBAction func selectDrivingLicenseCategory() {
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.55, withTitle: "Elegir categoría", withOptions: licenseCategories, selectedOption: selectedLicenseCategory, withConfirmButton: true)
    }
    
    @IBAction func finalizeRegister() {
        guard let licenseNumber = licenseNumberTextField.text, !licenseNumber.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.licenseNumber)
            return
        }
        
        guard licenseNumber.count > 8 else {
            showAlert(message: ValidationMessages.licenseNumberLength)
            return
        }

        guard let drivingLicenseCategory = drivingLicenseCategoryTextField.text, !drivingLicenseCategory.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.drivingLicenseCategory)
            return
        }

        guard let licensePlate = licensePlateTextField.text, !licensePlate.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.licensePlate)
            return
        }
        
    
        guard licensePlate.count == 6 else {
            showAlert(message: ValidationMessages.licensePlateLength)
            return
        }

        guard let tucExpiringDate = tucExpiringDateTextField.text, !tucExpiringDate.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.tucExpiringDate)
            return
        }

        guard termsAgreementSwitch.isOn else {
            showAlert(message: ValidationMessages.agreeTerms)
            return
        }

        newUser.licenseNumber = licenseNumber
        newUser.licensePlate = licensePlate
        newUser.requiresTechnicalReview = requiresTechnicalReviewSwitch.isOn

        performSegue(withIdentifier: "RegisterDocumentViewControllerSegue", sender: nil)
        
    }
    
    @IBAction override func goBack() {
        UIView.transition(with: UIApplication.shared.keyWindow ?? UIView(), duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.navigationController?.popViewController(animated: false)
        }, completion: nil)
    }

}

extension RegisterThirdStepViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {
        selectedLicenseCategory = option
    }
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {}
}


