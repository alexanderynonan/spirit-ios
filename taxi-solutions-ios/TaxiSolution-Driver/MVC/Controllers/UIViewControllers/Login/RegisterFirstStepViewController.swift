//
//  SignUpFirstStepViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/7/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RegisterFirstStepViewController: RegisterViewController {


    @IBOutlet weak var documentTypeTextField: CustomTextField!
    @IBOutlet weak var documentNumberTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var phoneTextField: CustomTextField!
    @IBOutlet weak var bankTextField: CustomTextField!
    @IBOutlet weak var accountNumberTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet var invitationCodeTextFields: [UITextField]!
    private var selectedGeneralDocument = OptionToSelect() {
        didSet {
            documentTypes.forEach { if $0.key == selectedGeneralDocument.key {
                selectedDocumentType = $0
                documentTypeTextField.text = $0.name
            }}
        }
    }

    private var transformedDocumentTypes : [OptionToSelect] = []

    var documentTypes: [DocumentType] = [] {
        didSet {
        }
    }
    var selectedDocumentType: DocumentType = DocumentType() {
        didSet {
            newUser.documentType = selectedDocumentType.key
            documentNumberTextField.keyboardType = selectedDocumentType.isNumeric ? .numberPad : .alphabet
            documentNumberTextField.text = ""
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RegisterSecondStepViewControllerSegue" {
            let vc = segue.destination as! RegisterThirdStepViewController
            vc.newUser = newUser
        }
    }
    
    func configureContent() {
        invitationCodeTextFields.forEach { $0.delegate = self }
        documentNumberTextField.delegate = self
    }
    
    func loadContent() {
        getDocumentTypes()
    }
    
    func getDocumentTypes() {
        startLoader()
        MainManager.sharedManager.getDocumentTypes(successResponse: { (documentTypes) in
            self.stopLoader()
            self.documentTypes = documentTypes
            self.transformedDocumentTypes.removeAll()
            documentTypes.forEach {
                let newOp = OptionToSelect()
                newOp.key = $0.key
                newOp.name = $0.name
                self.transformedDocumentTypes.append(newOp)
            }

        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func continueRegister(_ sender: UIButton) {
        guard let documentNumber = documentNumberTextField.text, !documentNumber.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.documentNumberEmpty)
            return
        }
        if self.newUser.documentType.lowercased() == Default.documentTypeToCheck {
            guard documentNumber.count == selectedDocumentType.maxValue else {
                showAlert(message: ValidationMessages.documentNumberLength)
                return
            }
        } else {
            guard documentNumber.count <= selectedDocumentType.maxValue else {
                showAlert(message: ValidationMessages.documentNumberLength)
                return
            }
        }
        guard let firstName = firstNameTextField.text?.trimmingCharacters(in: .whitespaces), let lastName = lastNameTextField.text?.trimmingCharacters(in: .whitespaces), !firstName.isEmptyOrWhitespace() && !lastName.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.namesEmpty)
            return
        }

        func saveData(firstname: String? = nil, lastname: String? = nil) {
            self.newUser.documentNumber = documentNumber
            self.newUser.firstName = firstname ?? firstName
            self.newUser.lastName = lastname ?? lastName

            continueRegister()
        }
        
        self.startLoader()
        UserManager.sharedManager.validateDocumentNumber(documentNumber: documentNumberTextField.text ?? "", documentType: selectedDocumentType.key, successResponse: { (response, _) in
            
            if self.newUser.documentType.lowercased() == Default.documentTypeToCheck {
                
                UserManager.sharedManager.verifyDocumentNumber(documentNumber: self.documentNumberTextField.text ?? "", successResponse: { (names) in
                    self.stopLoader()
                    if names.count == 2 {
                        let validFirstName = names[0]
                        let validLastName = names[1]
                        if validFirstName.uppercased().containsWord(firstName.uppercased()) && validLastName.uppercased().containsFirstWordOrAll(lastName.uppercased()) {
                            saveData(firstname: validFirstName.capitalizedName, lastname: validLastName.capitalizedName)
                        } else {
                            self.showAlert(message: ValidationMessages.reniecNames)
                        }
                    } else {
                        self.showAlert(message: DefaultApiMessages.formatResponse)
                    }
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription, okayHandler: { (_) in
                        self.documentNumberTextField.becomeFirstResponder()
                    })
                }
                
            } else {
                self.stopLoader()
                saveData()
            }
            
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    @IBAction func openDocumentSelectorPressed(_ sender: Any) {
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.55, withTitle: "Elegir documento", withOptions: transformedDocumentTypes, selectedOption: selectedGeneralDocument, withConfirmButton: true)
    }


    private func continueRegister() {
        guard let phone = phoneTextField.text, !phone.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.phone)
            return
        }

        guard let email = emailTextField.text, !email.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.email)
            return
        }

        guard email.isValidEmail() else {
            showAlert(message: ValidationMessages.emailFormat)
            return
        }

        if let bank = bankTextField.text, bank != "" {
            if bank.isEmptyOrWhitespace() {
                showAlert(message: ValidationMessages.bankName)
            }
        }

        if let accountNumber = accountNumberTextField.text, accountNumber != "" {
            if accountNumber.count < 10 && accountNumber.isEmptyOrWhitespace() {
                showAlert(message: ValidationMessages.accountNumberLength)
                return
            }
        }

        guard let password = passwordTextField.text, !password.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.password)
            return
        }

        guard password.count > 5 else {
            showAlert(message: ValidationMessages.passwordLength)
            return
        }

        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.confirmPassword)
            return
        }

        guard password == confirmPassword else {
            showAlert(message: ValidationMessages.passwordsDifferent)
            return
        }

        var invitationCode = ""
        invitationCodeTextFields.forEach { textField in
            invitationCode += textField.text ?? ""
        }
        print("INVITATION CODE: \(invitationCode)")

        guard [0, 6].contains(invitationCode.count) else {
            showAlert(message: ValidationMessages.invitationCode)
            return
        }
        newUser.email = email
        newUser.bank = bankTextField.text ?? ""
        newUser.accountNumber = accountNumberTextField.text ?? ""
        newUser.password = password
        newUser.invitationCode = invitationCode
        newUser.phoneNumber = phoneTextField.text ?? ""
        startLoader()
        UserManager.sharedManager.validateEmail(email: emailTextField.text ?? "", successResponse: { (response) in
            self.stopLoader()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "RegisterSecondStepViewControllerSegue", sender: nil)
            }
        }, failureResponse: { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        })
    }
    
    @IBAction override func goBack() {
        guard let phoneNumberVC = self.navigationController?.viewControllers[1] else {
            self.navigationController?.popToRootViewController(animated: true)
            return
        }
        self.navigationController?.popToViewController(phoneNumberVC, animated: true)
    }
}

extension RegisterFirstStepViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / CGFloat(documentTypes.count)), height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documentTypes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentTypeCollectionViewCell", for: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentType = documentTypes[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("SELECTED: \(documentTypes[indexPath.row])")
        let cell = collectionView.cellForItem(at: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentTypeSwitchButton.isOn = true
        selectedDocumentType = documentTypes[indexPath.row]
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("DESELECTED: \(documentTypes[indexPath.row])")
        let cell = collectionView.cellForItem(at: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentTypeSwitchButton.isOn = false
    }
}

extension RegisterFirstStepViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == documentNumberTextField {
            if let text = textField.text, (text.count < selectedDocumentType.maxValue || string == "") {
                return true
            } else {
                return false
            }
        } else {
            if string == "" {
                textField.text = ""
                invitationCodeTextFields.forEach {
                    if $0.tag == textField.tag - 1 {
                        $0.becomeFirstResponder()
                    }
                }
                return false
            }

            if string.count == 1 {
                if textField.text?.count == 1 {
                    if invitationCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                        let nextTextField = invitationCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                        nextTextField?.text = string
                        nextTextField?.becomeFirstResponder()
                    } else {
                        textField.resignFirstResponder()
                    }
                } else {
                    textField.text = string
                }
            } else {
                if let character = string.first {
                    textField.text = String(character)
                }
            }

            return false
        }
    }
}

extension RegisterFirstStepViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {
        selectedGeneralDocument = option
    }
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {}
}

