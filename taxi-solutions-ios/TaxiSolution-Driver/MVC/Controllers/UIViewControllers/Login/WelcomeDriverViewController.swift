//
//  WelcomeDriverViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol WelcomeDriverViewControllerDelegate: class {
    func welcomeDriverViewController(_ viewController: WelcomeDriverViewController)
}

class WelcomeDriverViewController: UIViewController {
    
    @IBOutlet weak var documentsToPresentLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    var delegate: WelcomeDriverViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        backgroundImageView.image = UIImage.gifImageWithName("gif_svico_background")
    }
    
    func loadContent() {
        documentsToPresentLabel.text = "- TUC\n- \(user.documentType.capitalized)\n- Brevete\n- Credencial\n- Cartilla\n- Educación Vial\n- Tarjeta de Propiedad\n- Soat\n- Revisión técnica"
    }
    
    @IBAction func goAddress() {
        goDirection(latitude: -12.0534395, longitude: -77.0708943)
    }
    
    @IBAction override func dismiss() {
        super.dismiss()
        delegate?.welcomeDriverViewController(self)
        if user.id != 0 {
            ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.logout, message: "¿Está seguro que desea salir de la apicación? Los datos se guardarán", andConfirmText: "Confirmar") { _, confirmed in
                _ = confirmed ? self.logout() : .none
            }
        }
    }
}
