//
//  RideRequestViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol RideRequestViewControllerDelegate: class {
    func tripRequestViewController(viewController: RideRequestViewController, didAcceptRide: Bool, ride: Ride?)
}

class RideRequestViewController: PannableViewController {
    
    @IBOutlet weak var cardView: CustomView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var tripPointsTableView: UITableView!
    
    let feedbackGenerator = UINotificationFeedbackGenerator()
    var mapVC: MapViewController?
    
    var delegate: RideRequestViewControllerDelegate?
    
    var ride: Ride?
    var tripPoints: [String] {
        return [ride?.originAddress ?? "", ride?.destinationAddress ?? ""]
    }
    
    var serviceDenialReasons = OptionToSelect.serviceDenialReasons()
    var selectedReason: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        viewToPan = cardView
        tripPointsTableView.delegate = self
        tripPointsTableView.dataSource = self
        
        let nib = UINib(nibName: "TripPointTableViewCell", bundle: nil)
        tripPointsTableView.register(nib, forCellReuseIdentifier: "TripPointTableViewCell")
    }
    
    func loadContent() {
        guard let clientOnRide = ride?.clientOnRide else { return }
        photoImageView.sd_setImage(with: clientOnRide.photoUrl.url, completed: nil)
        ratingLabel.text = clientOnRide.rating
        nameLabel.text = clientOnRide.name
        estimatedTimeLabel.text = "'\(ride?.estimatedTime ?? "")"
        amountLabel.text = String(format: "PEN %.2f", ride?.estimatedAmount.double ?? 0)
        setTimeOut()
    }
    
    @IBAction func answerTripRequest(_ sender: UIButton) {
        feedbackGenerator.notificationOccurred(sender.tag == 1 ? .success : .error)
        if sender.tag == 0 {
            OptionSelectorViewController.instantiate(inViewController: self, heightScale: 0.52, withTitle: "Rechazar servicio", withOptions: serviceDenialReasons, withConfirmButton: true)
        }
        if sender.tag == 1 {
            guard let ride = ride else { return }
            self.startLoader()
            DriverManager.sharedManager.acceptRide(ride: ride, successResponse: { (ride) in
                self.stopLoader()
                self.delegate?.tripRequestViewController(viewController: self, didAcceptRide: true, ride: ride)
                self.close()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription, withOkayButton: true, okayTitle: "Salir", okayHandler: { (_) in
                    self.delegate?.tripRequestViewController(viewController: self, didAcceptRide: false, ride: nil)
                    self.close()
                }, withCancelButton: true, cancelTitle: "Ok")
            }
        }
    }
    
    func setTimeOut() {
        guard let tripRequest = self.ride else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + tripRequest.maxWaitingTime.double) {
            guard self.ride != nil else { return }
            self.rejectTrip(ignored: true)
        }
    }
    
    func rejectTrip(reason: String = "", ignored: Bool = false) {
        guard let tripRequest = ride else { return }
        
        self.startLoader()
        MainManager.sharedManager.rejectTrip(ignored: false, reason: reason, tripRequest: tripRequest, successResponse: { (message) in
            print("MESSAGE FROM SUCCESSFULL REJECTION -> \(message)")
            self.stopLoader()
            self.close()
            self.ride = nil
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    override func close() {
        super.close()
        ride = nil
    }

}

extension RideRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.frame.height / CGFloat(tripPoints.count))
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripPoints.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripPointTableViewCell") as! TripPointTableViewCell
        cell.pointType = indexPath.row == 0 ? .initial : (indexPath.row == tripPoints.count - 1) ? .final : .middle
        cell.pointAddress = tripPoints[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension RideRequestViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {
        selectedReason = option.key
    }
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if dismissed {
            guard let selectedReason = selectedReason else {
                self.showAlert(message: "En caso de rechazo seleccione un motivo por favor")
                return
            }
            rejectTrip(reason: selectedReason)
        } else {
            selectedReason = nil
        }
    }
}
