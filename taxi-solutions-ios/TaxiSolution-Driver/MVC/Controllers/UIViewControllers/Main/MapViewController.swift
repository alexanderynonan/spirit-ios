//
//  MapsViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import GoogleMaps
import Toast_Swift
import FirebaseCrashlytics

class MapViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var hamburgerMenuButton: UIButton!
    @IBOutlet weak var destinationView: CustomView!
    @IBOutlet weak var destinationLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var rideMetricsContainer: CustomView!
    
    @IBOutlet weak var connectionButton: SwitchButton!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var myLocationButtonMaxY: NSLayoutConstraint!
    
    @IBOutlet weak var timerContainer: UIView!
    @IBOutlet weak var timerContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var userOnTripCardView: CustomView!
    @IBOutlet weak var userOnTripCardViewBottom: NSLayoutConstraint!
    @IBOutlet weak var userInfoViewContainer: UIView!
    @IBOutlet weak var userInfoViewContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonsStackView: UIStackView!
    
    @IBOutlet weak var cancelAloSvicoContainer: UIView!
    @IBOutlet weak var cancelAloSvicoContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var nearToOriginButtonsContainer: UIView!
    @IBOutlet weak var nearToOriginButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var waitingButtonsContainer: UIView!
    @IBOutlet weak var waitingButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var onRideButtonsContainer: UIView!
    @IBOutlet weak var onRideButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var nearToDestinationButtonsContainer: UIView!
    @IBOutlet weak var nearToDestinationButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentDetailContainer: UIView!
    @IBOutlet weak var paymentDetailContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentFeedbackButtonsContainer: UIView!
    @IBOutlet weak var paymentFeedbackButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentMainButton: CustomButton!
    
    //MARK: - Constants
    var driverCarMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    let defaultButtonHeight: CGFloat = 60.0
    let defaultUserInfoHeight: CGFloat = 70.0
    let defaultZoom: Float = 17.0
    var rideRequest = [Ride]()

    var fakeRide = Ride()
    
    //MARK: - Variables
    var isPendingUser: Bool {
        return user.approvalStatus == "pending"
    }
    var connected = false {
        didSet {
            try! realm.write {
                user.isConnected = connected
            }
            
            let connectionDisable = !user.hasEnoughBalance || user.hasExpiredDocuments
            
            connectionButton.isUserInteractionEnabled = connectionDisable ? false : true
            connectionButton.alpha = connectionDisable ? 0.25 : 1.0
            if connectionDisable {
                connectionButton.setTitle("NO DISPONIBLE", for: .normal)
            } else {
                connectionButton.isOn = connected
            }
        }
    }
    var loaderVC: LoaderViewController?
    lazy var userInfoView: UserInfoView = {
        guard let userInfoView = UINib(nibName: "UserInfoView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? UserInfoView else { return UserInfoView() }
        userInfoView.delegate = self
        self.userInfoView = userInfoView
        userInfoView.frame = userInfoViewContainer.bounds
        userInfoViewContainer.addSubview(userInfoView)
        return userInfoView
    }()
    lazy var paymentDetailView: PaymentDetailView = {
        guard let paymentDetailView = UINib(nibName: "PaymentDetailView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? PaymentDetailView else { return PaymentDetailView() }
        self.paymentDetailView = paymentDetailView
        paymentDetailView.frame = paymentDetailContainer.bounds
        paymentDetailContainer.addSubview(paymentDetailView)
        return paymentDetailView
    }()
    var tripCoordinates: [CLLocationCoordinate2D] = []
    var timer: Timer?
    
    var driverDataSentToClient = false
    
    var ride: Ride? {
        didSet {
            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }
            setTripClientInfo()
            rideState = ride?.rideState
            destinationLabel.text = rideState == .onRide ? ride?.destinationAddress.withDefault("") : ride?.originAddress.withDefault("")
        }
    }
    var anotherRide: AnotherRide?
    
    var rideState: RideState? {
        didSet {
            try! realm.write {
                self.ride?.state = rideState?.rawValue ?? -1
            }
            
            /*if rideState == nil && ride == nil {
                checkIfNeedsToRateUser()
            }*/
            
            DispatchQueue.main.async {
                UIView.transition(with: self.userOnTripCardView, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                    self.hamburgerMenuButton.transform = self.rideState != .none ? CGAffineTransform(translationX: self.view.frame.width * 0.8, y: 0) : .identity
                    self.hamburgerMenuButton.alpha = self.rideState != .none ? 0 : 1
                    self.destinationView.alpha = self.rideState != .none ? 1 : 0
                    
                    self.rideMetricsContainer.alpha = self.rideState == .onRide ? 1 : 0
                    self.rideMetricsContainer.transform = self.rideState == .onRide ? .identity : CGAffineTransform(scaleX: 0.5, y: 0.5)
                    
                    self.connectionButton.transform = self.rideState != .none ? CGAffineTransform(translationX: 0, y: self.connectionButton.frame.height + 50) : .identity
                    if self.rideState == .none {
                        self.userOnTripCardViewBottom.constant = -self.userOnTripCardView.frame.height
                        self.mapView.resetLeavingMarker(self.driverCarMarker)
                    }
                    self.configureUserOnTripCardViewButtons(show: true)
                }) { (_) in
                    self.userInfoView.setRideState()
                    DispatchQueue.main.async {
                        if self.rideState != .none && self.userOnTripCardView.transform == .identity {
                            UIView.animate(withDuration: 0.25, animations: {
                                self.userOnTripCardViewBottom.constant = 0
                            }) { (_) in
                                self.configureMyLocationButtonPosition()
                            }
                        } else {
                            self.configureMyLocationButtonPosition()
                        }
                    }
                }
            }
        }
    }
    
    var feedbackGenerator = UIImpactFeedbackGenerator()
    var locationManager = CLLocationManager()
    
    var currentLocationIndex = 0
    var currentLocation: CLLocationCoordinate2D? {
        didSet {
            guard let currentLocation = currentLocation else { return }
            
            switch rideState {
            case .onWay:
                print("ON WAY")
                guard !(self.ride?.isInvalidated ?? true), let ride = self.ride else {
                    Crashlytics.crashlytics().log("\(#file) > \(#function) > Line \(#line): Ride object invalidated")
                    return
                }
                MainManager.sharedManager.getPolyline(originCoordinate: currentLocation, destinationCoordinate: ride.originCoordinate ?? CLLocationCoordinate2D(), successResponse: { polyline, distance, duration  in
                    guard !(self.ride?.isInvalidated ?? true), let ride = self.ride else {
                        Crashlytics.crashlytics().log("\(#file) > \(#function) > Line \(#line): Ride object invalidated")
                        return
                    }
                    self.lastPolylineInfo = PolylineInfo(polyline: polyline, origin: currentLocation, destination: self.ride?.originCoordinate, duration: duration, driverMoving: true, rotation: self.driverCarMarker.rotation, animated: false)
                    let params = [
                        "driver_socket_id": self.socket.sid,
                        "origin_lat": self.ride?.originLat ?? "",
                        "origin_long": self.ride?.originLong ?? "",
                        "client_socket_id": self.ride?.clientSocketId ?? "",
                        "current_lat": currentLocation.latitude,
                        "current_long": currentLocation.longitude,
                        "poly_line": polyline,
                        //Datos del conductor
                        "driver_id": self.user.id,
                        "driver_full_name": self.user.fullName,
                        "driver_service_name": self.user.serviceName,
                        "driver_photo_url": self.user.photoUrl,
                        "driver_phone": self.user.phone,
                        "driver_rating": self.user.rating.string,
                        "car_plate_number": self.user.carPlateNumber,
                        "car_color": self.user.carColor,
                        "car_brand": self.user.carBrand,
                        "chat_id": self.ride?.chatId ?? "",
                        "duration": duration,
                        "rotation": self.driverCarMarker.rotation as Double
                        ] as [String : Any]
                    
                    self.socket.emit(SocketEventsEmit.arrivingToOriginPoint, with: [params as Any])
                }) { (error) in
                    Crashlytics.crashlytics().record(error: error)
                }
            case .onRide:
                print("ON TRIP")
                guard !(self.ride?.isInvalidated ?? true), let ride = self.ride else {
                    Crashlytics.crashlytics().log("\(#file) > \(#function) > Line \(#line): Ride object invalidated")
                    return
                }
                //OBTENEMOS POLYLINE PARA EL VIAJE EN PROGRESO
                MainManager.sharedManager.getPolyline(originCoordinate: currentLocation, destinationCoordinate: ride.destinationCoordinate, successResponse: { polyline, distance, duration  in
                    guard !(self.ride?.isInvalidated ?? true), let ride = self.ride else {
                        Crashlytics.crashlytics().log("\(#file) > \(#function) > Line \(#line): Ride object invalidated")
                        return
                    }
                    DispatchQueue.main.async {
                        self.durationLabel.text = duration
                        self.distanceLabel.text = distance
                        self.lastPolylineInfo = PolylineInfo(polyline: polyline, origin: currentLocation, destination: self.ride?.destinationCoordinate, driverMoving: true, rotation: self.driverCarMarker.rotation, animated: false)
                    }
                    
                    var params = [
                        "driver_socket_id": self.socket.sid,
                        "destination_lat": self.ride?.destinationLat ?? "",
                        "destination_long": self.ride?.destinationLong ?? "",
                        "destination_address": self.ride?.destinationAddress ?? "",
                        "client_socket_id": self.ride?.clientSocketId ?? "",
                        "ride_id": self.ride?.rideId ?? "",
                        "current_lat": currentLocation.latitude,
                        "current_long": currentLocation.longitude,
                        //AGREGAMOS EL ID DEL VIAJE AL POLYLINE PARA QUE EL CLIENTE PUEDA IDENTIFICAR QUE POLYLINE USAR
                        "poly_line": polyline,
                        "distance": distance,
                        "time_to_arrive": duration,
                        "rotation": self.driverCarMarker.rotation as Double,
                        ] as [String : Any]
                    
                    //VALIDAMOS SI EXISTE OTRO VIAJE
                    if let anotherRide = self.anotherRide {
                        //CONSULTAMOS OTRO POLYLINE PARA EL SEGUNDO VIAJE
                        MainManager.sharedManager.getPolyline(originCoordinate: currentLocation, middleCoordinate: self.ride?.destinationCoordinate ?? CLLocationCoordinate2D(), destinationCoordinate: anotherRide.originCoordinate, successResponse: { (polyline, distance, duration) in
                            guard !(self.anotherRide?.isInvalidated ?? true), let anotherRide = self.anotherRide else {
                                Crashlytics.crashlytics().log("\(#file) > \(#function) > Line \(#line): Ride object invalidated")
                                return
                            }
                            //AGREGAMOS EL ID DEL VIAJE AL POLYLINE PARA QUE EL CLIENTE PUEDA IDENTIFICAR QUE POLYLINE USAR
                            params.updateValue(polyline, forKey: "pending_poly_line")
                            params.updateValue(true, forKey: "has_new_ride")
                            params.updateValue(anotherRide.clientSocketId, forKey: "new_client_socket_id")
                            params.updateValue(anotherRide.originLat, forKey: "origin_lat")
                            params.updateValue(anotherRide.originLong, forKey: "origin_long")
                            //ENVIAMOS LA DATA DEL CONDUCTOR PARA QUE EL SEGUNDO CLIENTE PUEDA PINTARLO
                            params.updateValue(self.user.id, forKey: "driver_id")
                            params.updateValue(self.user.fullName, forKey: "driver_full_name")
                            params.updateValue(self.user.serviceName, forKey: "driver_service_name")
                            params.updateValue(self.user.photoUrl, forKey: "driver_photo_url")
                            params.updateValue(self.user.phone, forKey: "driver_phone")
                            params.updateValue(self.user.rating.string, forKey: "driver_rating")
                            params.updateValue(self.user.carPlateNumber, forKey: "car_plate_number")
                            params.updateValue(self.user.carColor, forKey: "car_color")
                            params.updateValue(self.user.carBrand, forKey: "car_brand")
                            params.updateValue(self.ride?.chatId ?? "", forKey: "chat_id")
                            params.updateValue(duration, forKey: "duration")
                            //EMITIMOS EL EVENTO
                            self.socket.emit(SocketEventsEmit.driverOnTrip, with: [params as Any])
                        }) { (error) in
                            print("🚨 ERROR second getPolyline - \(error.localizedDescription)")
                        }
                    } else {
                        //EMITIMOS EL EVENTO
                        self.socket.emit(SocketEventsEmit.driverOnTrip, with: [params as Any])
                    }
                }) { (error) in
                    print("🚨 ERROR getPolyline - \(error.localizedDescription)")
                }
            default:
                break
            }
            
            let params = [
                "lat": currentLocation.latitude,
                "long": currentLocation.longitude,
                "orientation": self.driverCarMarker.rotation as Double,
                "driverId": user.id
                ] as Any
            print("driverLocationParams", params)
            self.socket.emit(SocketEventsEmit.driverLocation, with: [params])
        }
    }
    var lastPolylineInfo: PolylineInfo? {
        didSet {
            guard let polylineInfo = lastPolylineInfo else { return }
            mapView.drawPolyline(polylineInfo)
        }
    }
    
    //To change mapView camera to default zoom
    var movingFromLocationButton = true
    
    var loaded = false

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        getUpdatedUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        #if targetEnvironment(simulator)
        Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (_) in
            let defaultCoordinates = TripCoordinatesDefault.shared.defaultTrip()
            if self.currentLocationIndex < defaultCoordinates.count {
                self.currentLocation = defaultCoordinates[self.currentLocationIndex]
                self.currentLocationIndex += 1
            } else {
                self.currentLocation = CLLocationCoordinate2D(latitude: -12.076633168213908, longitude: -77.00543459504843)
            }
        }
        #endif
        if !loaded {
            ride = self.realm.objects(Ride.self).last
            loaded = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !isPendingUser {
            checkIfEnoughBalance()
            configureMyLocationButtonPosition()
        }
    }
    
    //MARK: - Functions
    func configureContent() {
        setNeedsStatusBarAppearanceUpdate()
        connectionButton.delegate = self
        
        configureMapComponents()
        configureMyLocationButtonPosition()
        configureUserOnTripCard()
        self.view.layoutIfNeeded()
    }
    
    func setupDriver() {
        configureUserOnTripCard()
        self.view.layoutIfNeeded()
        
        SocketIOManager.sharedInstance.establishConnection()
        setSocketEvents()
    }
    
    func loadContent() {
        moveToCurrentLocation()
        self.checkExpiringDocuments()
        if OptionToSelect.dynamicRideCancelReasons().count == 1 {
            DriverManager.sharedManager.getCancellationReasons(successResponse: { (optionsToSelect) in
                print("REASONS GOT")
            }) { (error) in
                self.showAlert(message: error.localizedDescription)
            }
        }
//        ride = Ride()
//        if let ride = self.ride {
//            try! realm.write {
//                ride.shown = false
//            }
//            ride.state = 3
//            ride.destinationAddress = "Av. canto grande 937"
//            ride.originAddress = "Av. canto grande 937"
//            self.rideState = ride.rideState
//        }
        ride = self.realm.objects(Ride.self).first
        anotherRide = self.realm.objects(AnotherRide.self).first
        if ride == nil {
            if let anotherRide = anotherRide {
                let newRide = anotherRide.getRide()
                try! realm.write {
                    self.realm.add(newRide)
                }
                self.ride = newRide
                try! realm.write {
                    self.realm.delete(anotherRide)
                }
                self.anotherRide = nil
            }
        }
//        case onWay = 0
//        case waiting = 1
//        case onRide = 2
//        case finished = 3
//        case "arriving_to_origin_point":
//            return .onWay
//        case "at_origin_point":
//            return .waiting
//        case "in_progress", "ending_trip":
//            return .onRide
//        case "cancelled", "cancelled_in_service", "paying", "finished":


        if let ride = self.ride {
            try! realm.write {
                ride.shown = false
            }
            self.startLoader()
            MainManager.sharedManager.rideReconnection(successResponse: { (data, clientOnRide, pendingRide, clientOnPendingRide)  in
                self.stopLoader()
                try? self.realm.write {
                    ride.stateString = data["status"]?.stringValue ?? ""
                    ride.state = RideState.getStatusFromString(ride.stateString)?.rawValue ?? -1
                    ride.destinationAddress = data["destination_address"]?.stringValue ?? ""
                    ride.destinationLat = data["destination_lat"]?.stringValue.double ?? 0.0
                    ride.destinationLong = data["destination_long"]?.stringValue.double ?? 0.0
                    ride.originAddress = data["origin_address"]?.stringValue ?? ""
                    ride.originLat = data["origin_lat"]?.stringValue.double ?? 0.0
                    ride.originLong = data["origin_long"]?.stringValue.double ?? 0.0
                    ride.paymentStatus = data["payment_status"]?.stringValue ?? ""
                    if let client = clientOnRide as? ClientOnRide, ride.clientOnRide == nil {
                        ride.clientOnRide = client
                    }
                }

                if ride.rideState == .finished && ride.paymentStatus == "pending" && ride.stateString != "cancelled" {
                    _ = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Finalizando...", message: "Se procederá a realizar el cobro", automaticDismiss: true, delegate: nil)
                    self.getPaymentDetails()
                }
                self.rideState = ride.rideState

                if let anotherRide = self.anotherRide, let anotherRideData = pendingRide {
                    try? self.realm.write {
                        anotherRide.stateString = anotherRideData["status"]?.stringValue ?? ""
                        anotherRide.state = RideState.getStatusFromString(anotherRide.stateString)?.rawValue ?? -1
                        anotherRide.destinationAddress = anotherRideData["destination_address"]?.stringValue ?? ""
                        anotherRide.destinationLat = anotherRideData["destination_lat"]?.stringValue.double ?? 0.0
                        anotherRide.destinationLong = anotherRideData["destination_long"]?.stringValue.double ?? 0.0
                        anotherRide.originAddress = anotherRideData["origin_address"]?.stringValue ?? ""
                        anotherRide.originLat = anotherRideData["origin_lat"]?.stringValue.double ?? 0.0
                        anotherRide.originLong = anotherRideData["origin_long"]?.stringValue.double ?? 0.0
                        anotherRide.paymentStatus = anotherRideData["payment_status"]?.stringValue ?? ""
                        if let client = clientOnPendingRide as? ClientOnRide, anotherRide.clientOnRide == nil {
                            anotherRide.clientOnRide = client
                        }
                    }

                    if anotherRide.stateString == "cancelled" {
                        self.loaderVC = LoaderViewController.instantiate(in: self, title: "Próximo viaje cancelado", automaticDismiss: true, delegate: self)
                        self.clearRideData(isAnotherRide: true)
                    }
                }
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            self.startLoader()
            MainManager.sharedManager.rideReconnection(successResponse: { (data, clientOnRide, pendingRide, clientOnPendingRide)  in
                self.stopLoader()
                if data["payment_status"]?.stringValue ?? "" != "complete" && data["status"]?.stringValue ?? "" != "finished" {

                    let ride = Ride()
                    try? self.realm.write {
                        ride.maxTimeToWaitForClient = data["waiting_time"]?.intValue ?? 0
                        ride.startedToWaitAt = data["started_waiting_at"]?.stringValue.toDate(fromFormat: "yyyy-MM-dd HH:mm:ss") ?? Date()

                        ride.rideId = data["ride_id"]?.intValue ?? 0
                        ride.chatId = data["chat_id"]?.stringValue ?? ""
                        ride.paymentType = data["payment_type"]?.stringValue ?? ""
                        ride.stateString = data["status"]?.stringValue ?? ""
                        ride.state = RideState.getStatusFromString(ride.stateString)?.rawValue ?? -1
                        ride.destinationAddress = data["destination_address"]?.stringValue ?? ""
                        ride.destinationLat = data["destination_lat"]?.stringValue.double ?? 0.0
                        ride.destinationLong = data["destination_long"]?.stringValue.double ?? 0.0
                        ride.originAddress = data["origin_address"]?.stringValue ?? ""
                        ride.originLat = data["origin_lat"]?.stringValue.double ?? 0.0
                        ride.originLong = data["origin_long"]?.stringValue.double ?? 0.0
                        ride.paymentStatus = data["payment_status"]?.stringValue ?? ""
                        if let client = clientOnRide as? ClientOnRide, ride.clientOnRide == nil {
                            ride.clientOnRide = client
                        }
                    }

                    self.rideState = ride.rideState

                    self.ride = ride

                    try? self.realm.write {
                        self.realm.add(self.ride ?? Ride())
                    }

                    if ride.rideState == .finished && ride.paymentStatus == "pending" && ride.stateString != "cancelled" {
                        _ = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Finalizando...", message: "Se procederá a realizar el cobro", automaticDismiss: true, delegate: nil)
                        self.getPaymentDetails()
                    }

                    if let anotherRide = self.anotherRide, let anotherRideData = pendingRide {
                        try? self.realm.write {
                            anotherRide.stateString = anotherRideData["status"]?.stringValue ?? ""
                            anotherRide.state = RideState.getStatusFromString(anotherRide.stateString)?.rawValue ?? -1
                            anotherRide.destinationAddress = anotherRideData["destination_address"]?.stringValue ?? ""
                            anotherRide.destinationLat = anotherRideData["destination_lat"]?.stringValue.double ?? 0.0
                            anotherRide.destinationLong = anotherRideData["destination_long"]?.stringValue.double ?? 0.0
                            anotherRide.originAddress = anotherRideData["origin_address"]?.stringValue ?? ""
                            anotherRide.originLat = anotherRideData["origin_lat"]?.stringValue.double ?? 0.0
                            anotherRide.originLong = anotherRideData["origin_long"]?.stringValue.double ?? 0.0
                            anotherRide.paymentStatus = anotherRideData["payment_status"]?.stringValue ?? ""
                            if let client = clientOnPendingRide as? ClientOnRide, anotherRide.clientOnRide == nil {
                                anotherRide.clientOnRide = client
                            }
                        }

                        if anotherRide.stateString == "cancelled" {
                            self.loaderVC = LoaderViewController.instantiate(in: self, title: "Próximo viaje cancelado", automaticDismiss: true, delegate: self)
                            self.clearRideData(isAnotherRide: true)
                        }
                    }
                }


            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }

        }
    }
    
    func getUpdatedUser() {
        UserManager.sharedManager.getUser(user: user, successResponse: { (user) in
//            print("✅ APPROVAL STATUS -> \(user.approvalStatus)")
//            if self.isPendingUser {
//                print("pending")
//                guard let welcomeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "WelcomeDriverViewController") as? WelcomeDriverViewController else {
//                    print("NO SE PUDO INSTANCIAR EL CONTROLADOR DE BIENVENIDA")
//                    return
//                }
//                DispatchQueue.main.async {
//                    self.navigationController?.pushViewController(welcomeVC, animated: true)
//                }
//            } else {
                print("not pending")
                self.setupDriver()
                self.loadContent()
//            }
        }) { (error) in
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func checkExpiringDocuments() {
        var vc: UIViewController? = nil
        vc = checkIfEnoughBalance()
        
        DriverManager.sharedManager.getDocuments(successResponse: { (documents) in
            var expiredDocuments = [Document]()
            var aboutToExpireDocuments = [Document]()
            documents.forEach { document in
                if document.expired {
                    expiredDocuments.append(document)
                }
                if document.aboutToExpire {
                    aboutToExpireDocuments.append(document)
                }
            }
            
            if expiredDocuments.count != 0 {
                print("📑 DOCUMENTOS VENCIDOS")
                var textToShow = ""
                expiredDocuments.forEach {
                    textToShow += "*\($0.name)* - \($0.expiresAt)\n"
                }
                if self.user.isConnected {
                    self.setConnection(self.connectionButton)
                }
//                _ = PopUpViewController.instantiate(viewController: vc ?? self, image: #imageLiteral(resourceName: "ic_expiring_documents"), title: "Documentos vencidos", warning: true, message: "Los siguientes documentos se encuentran vencidos", infoMessage: textToShow, attributedInfoMessage: true)
                try! self.realm.write {
                    self.user.hasExpiredDocuments = true
                }
            } else {
                if aboutToExpireDocuments.count != 0 && !self.user.documentsAboutToExpireShownToday {
                    print("📑 DOCUMENTOS POR VENCER")
                    var textToShow = ""
                    aboutToExpireDocuments.forEach {
                        textToShow += "*\($0.name)* - \($0.expiresAt)\n"
                    }
//                    _ = PopUpViewController.instantiate(viewController: vc ?? self, image: #imageLiteral(resourceName: "ic_expiring_documents"), title: "Documentos a vencer", warning: true, message: "Renueva tus documentos antes de la fecha de vencimiento para seguir trabajando en Taxi Spirit", infoMessage: textToShow, attributedInfoMessage: true)
                    try! self.realm.write {
                        self.user.documentsCheckedDate = Date()
                    }
                } else {
                    try! self.realm.write {
                        self.user.hasExpiredDocuments = false
                    }
                    print("📑 DOCUMENTOS ACTUALIZADOS")
                    self.connected = self.user.isConnected
                }
            }
        }) { (error) in
            self.connected = self.user.isConnected
            print(error.localizedDescription)
        }
    }
    
    func checkIfEnoughBalance() -> UIViewController? {
        self.connected = user.isConnected
        
//        if !user.hasEnoughBalance {
//            if self.user.isConnected {
//                self.setConnection(self.connectionButton)
//            }
//            return PopUpViewController.instantiate(viewController: self, image: #imageLiteral(resourceName: "img_no_credit"), title: String(format: "Saldo: %.2f PEN", user.currentBalance), warning: user.currentBalance < 0, message: "Su saldo actual es de \(String(format: "%.2f PEN", user.currentBalance)). Para poder conectarse y recibir solicitudes de viajes debe realizar una recarga")
//        }
        return nil
    }
    
    //MARK: - MapFunctions
    func configureMapComponents() {
        
        let timerApp = Date().toText(withFormat: "hh:mm a")
        
        if timerApp ?? "" >= "07:00 PM"{
            do {
                if let styleURL = Bundle.main.url(forResource: "map_style_dark", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
        }else{
            do {
                if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
        }
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = false
        mapView.isBuildingsEnabled = false
        mapView.settings.rotateGestures = false
        mapView.settings.tiltGestures = false
        
        mapView.settings.myLocationButton = false
        myLocationButton.isHidden = true
        
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        locationManager.distanceFilter = App.distanceFilter
        
        driverCarMarker.icon = #imageLiteral(resourceName: "ic_driver_car")
        driverCarMarker.map = mapView
        driverCarMarker.rotation = locationManager.location?.course ?? 0
        driverCarMarker.isFlat = true
        connectionButton.gestureRecognizers?.removeAll()
    }
    
    func configureMyLocationButtonPosition() {
        self.view.layoutIfNeeded()
        
        let minY = [connectionButton.frame.origin.y, userOnTripCardView.frame.origin.y].min() ?? 0.0
        print("MIN Y -> \(minY)")
        let bottom = self.view.frame.height - minY - 15
        print(bottom)
        
        myLocationButtonMaxY.constant = minY - 10
        mapView.padding = UIEdgeInsets(top: 0, left: 30, bottom: bottom, right: 30)
        self.mapView.layoutIfNeeded()
    }
    
    //MARK: - ModalFunctions
    func showRideRequest() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RideRequestViewController") as? RideRequestViewController else { return }
        
        if let mainVC = self.navigationController?.parent {
            mainVC.presentedViewController?.dismiss()
        }
        
        vc.mapVC = self
        vc.delegate = self
        vc.isPanEnable = false
        if let anotherRide = self.anotherRide {
            vc.anotherRide = anotherRide
        } else {
            vc.ride = ride
        }
        vc.modalPresentationStyle = .overFullScreen
        UIDevice.vibrate()
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - IBActions
    @IBAction func toggleSideMenu() {
        guard let mainContainerVC = self.parent?.parent as? MainContainerViewController else {
            return
        }
        mainContainerVC.toggleSideMenu()
    }
    
    @IBAction func moveToCurrentLocation() {
        if locationPermissionsEnabled {
            locationManager.stopUpdatingLocation()
            movingFromLocationButton = true
            locationManager.startUpdatingLocation()
        }
    }
    
    @IBAction func setConnection(_ switchButton: SwitchButton) {
        //VALIDAR QUE EL SALDO SEA MAYOR A 0.10 CENTAVOS Y NO TENGA DOCUMENTOS VENCIDOS
        if (user.hasEnoughBalance && locationPermissionsEnabled) || user.isConnected {
            self.startLoader()
            DriverManager.sharedManager.setConnection(!connected, successResponse: { (connectionId) in
                self.connected = !self.connected
                self.stopLoader()
                print(connectionId)
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    @IBAction func goDirectionsApp() {
        guard let ride = ride else { return }
        goDirection(latitude: ride.destinationCoordinate.latitude, longitude: ride.destinationCoordinate.longitude, address: ride.destinationAddress)
    }
    
    func setSocketEvents() {
        self.socket.on(SocketEventsOn.rideRequest) { (dataArray, ack) in
            guard let rideRequestData = dataArray.first as? [String: Any], self.anotherRide == nil else { return }
            print(Date())
            //Validamos que si existe un viaje, creamos otro viaje

            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }

            if self.ride?.rideId == 0 {
                self.ride = nil
            }

            if self.ride != nil {
                if self.ride?.chatId != "" {

                    self.anotherRide = AnotherRide(data: rideRequestData)
                    if let anotherRide = self.anotherRide {
                        try! self.realm.write {
                            self.realm.add(anotherRide, update: .all)
                        }
                    }
                } else {
                    self.rideRequest.append(Ride(data: rideRequestData))
                    print("🧵ON_newClientRequest\n\(dataArray)\n_🧵")
                    return
                }
            } else {
                //Caso contrario creamos un viaje
                self.ride = Ride(data: rideRequestData)
                if let ride = self.ride {
                    self.rideRequest.append(ride)
                    try! self.realm.write {
                        self.realm.add(ride, update: .all)
                    }
                }
            }

            self.showRideRequest()
            print("🧵ON_newClientRequest\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.nearToOriginPoint) { (dataArray, ack) in
            print("⚠️⚠️ ESTAS CERCA AL PUNTO DE RECOJO ⚠️⚠️")
            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }

            if self.ride?.nearToOrigin == false {
                try! self.realm.write {
                    self.ride?.nearToOrigin = true
                }
                self.configureUserOnTripCardViewButtons(show: true)
            }
            print("🧵ON_nearToOriginPoint\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.clientCancelRideInProgress) { (dataArray, ack) in
            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }
            self.loaderVC = LoaderViewController.instantiate(in: self, title: "Viaje cancelado", message: "El cliente ha cancelado el viaje, se procederá a realizar el cobro hasta la ubicación actual", automaticDismiss: true, delegate: self)
            guard let data = dataArray.first as? [String: Any], let ride = self.ride else { return }
            let paymentDetail = PaymentDetail(dictionary: data, rideId: ride.rideId)
            self.showPaymentDetail(paymentDetail)
            print("🧵ON_notifyClientCancelRideInProgress\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.nearToDestinationPoint) { (dataArray, ack) in
            print("⚠️⚠️ ESTAS CERCA AL PUNTO DE DESTINO ⚠️⚠️")
            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }

            if self.ride?.nearToDestination == false {
                try! self.realm.write {
                    self.ride?.nearToDestination = true
                }
                UserManager.sharedManager.setDriverNearToDestination(successResponse: { (message) in
                    print("⚠️⚠️ SET NEAR TO DESTINATION SERVICE RESPONSE ⚠️⚠️")
                    print(message)
                    print("⚠️ SET NEAR TO DESTINATION SERVICE RESPONSE ⚠️")
                }) { (_) in () }
                self.configureUserOnTripCardViewButtons(show: true)
            }
            print("🧵ON_nearToDestinationPoint\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.rideCancelledByClientBeforeStart) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let rideId = data["ride_id"] as? Int, let message = data["message"] as? String {
                if self.ride?.isInvalidated ?? false {
                    self.ride = Ride()
                }
                var isAnotherRide = self.realm.objects(AnotherRide.self).filter("rideId == \(rideId)").first != nil
                defer {
                    self.clearRideData(isAnotherRide: isAnotherRide)
                }
                self.loaderVC = LoaderViewController.instantiate(in: self, title: "\(isAnotherRide ? "Próximo viaje" : "Viaje") cancelado", message: message, automaticDismiss: true, delegate: self)
            }
            print("🧵ON_rideCancelledByClientBeforeStart\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.routeChanged) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let destinationAddress = data["destination_address"] as? String, let destinationLat = data["destination_lat"] as? String, let destinationLong = data["destination_long"] as? String {
                self.loaderVC = LoaderViewController.instantiate(in: self, title: "Cambio de ruta", message: "El cliente ha cambiado la ruta", automaticDismiss: true, delegate: self)
                if self.ride?.isInvalidated ?? false {
                    self.ride = Ride()
                }
                try? self.realm.write {
                    self.ride?.destinationAddress = destinationAddress
                    self.ride?.destinationLat = destinationLat.double
                    self.ride?.destinationLong = destinationLong.double
                }
                self.destinationLabel.text = destinationAddress
            }
            print("🧵ON_notifyClientChangeRoute\n\(dataArray)\n_🧵")
            print("")
        }

        self.socket.on(SocketEventsOn.clientHasNewSocketId) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let clientSocketId = data["client_socket_id"] as? String {
                print("🧑‍🦱 CLIENT NEW SOCKET ID -> \(clientSocketId)")
                if self.ride?.isInvalidated ?? false {
                    self.ride = Ride()
                }
                try? self.realm.write {
                    self.ride?.clientSocketId = clientSocketId
                }
            }
            print("🧵ON_clientHasNewSocketId\n\(dataArray)\n_🧵")
            print("")
        }
    }
    
    func checkIfNeedsToRateUser() {
        self.startLoader()
        MainManager.sharedManager.getLastUserToRate(successResponse: { (userToRate) in
            self.stopLoader()
            if userToRate.rideId != 0 && userToRate.userId != 0 {
                RatingViewController.instantiate(in: self.parent?.parent ?? self, userToRate: userToRate, description: "Califica al cliente")
            }
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func clearRideData(isAnotherRide: Bool = false) {
        if isAnotherRide {
            guard let anotherRide = self.anotherRide else { return }
            try! realm.write {
                self.realm.delete(anotherRide)
            }
            self.anotherRide = nil
        } else {
            mapView.resetLeavingMarker(driverCarMarker)
            userInfoView.ride = nil
            resetTimer()
            
            guard let ride = self.ride else {
                return
            }
            
            try? realm.write {
                ride.clientOnRide != nil ? realm.delete(ride.clientOnRide!) : ()
                ride.paymentDetail != nil ? realm.delete(ride.paymentDetail!) : ()
                realm.delete(ride)
            }
            
            self.ride = nil

            if rideRequest.count != 0 {
                self.rideRequest.removeFirst()
                if let ride = rideRequest.first {
                    //Si existe solicitudes de viaje creamos un viaje
                    self.ride = ride
                    try! self.realm.write {
                        self.realm.add(ride, update: .all)
                    }
                    self.showRideRequest()
                }
            }
            
            //Validamos si hay otro viaje pendiente
            guard let anotherRide = self.anotherRide else { return }
            let newRide = anotherRide.getRide()
            try! realm.write {
                self.realm.add(newRide)
            }
            self.ride = newRide
            try! realm.write {
                self.realm.delete(anotherRide)
            }
            self.anotherRide = nil
            self.rideState = .onWay
            moveToCurrentLocation()
        }
    }
}

//MARK: - SwitchButtonDelegate
extension MapViewController: SwitchButtonDelegate {
    func switchButton(switchButton: SwitchButton, statusShouldChange: Bool) -> Bool {
        return true
    }
    func switchButton(switchButton: SwitchButton, statusDidChange: Bool) {}
}

//MARK: - MapDelegates

//MARK: GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        myLocationButton.isHidden = movingFromLocationButton ? true : false
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        movingFromLocationButton = false
    }
}

//MARK: CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
//            var defaultCoordinates = TripCoordinatesDefault.shared.defaultTrip()
//            if rideState != .none && currentLocationIndex < defaultCoordinates.count {
//                currentLocation = defaultCoordinates[currentLocationIndex]
//                currentLocationIndex += 1
//            } else {
            currentLocation = user.isConnected ? location.coordinate : nil
            mapView.isMyLocationEnabled = user.isConnected || rideState != nil ? false : true
//            }
            
            if movingFromLocationButton {
                let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: movingFromLocationButton ? defaultZoom : mapView.camera.zoom)
                mapView.animate(to: camera)
            }
            DispatchQueue.main.async {
                self.driverCarMarker.position = self.currentLocation ?? CLLocationCoordinate2D()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading:CLHeading) {
        driverCarMarker.rotation = newHeading.trueHeading
    }
}

//MARK: - RideRequestViewControllerDelegate
extension MapViewController: RideRequestViewControllerDelegate {
    func rideRequestViewController(viewController: RideRequestViewController, didAcceptRide: Bool, ride: Ride?) {
        guard let ride = ride, didAcceptRide else {
            self.anotherRide == nil ? clearRideData() : clearRideData(isAnotherRide: true)
            return
        }
        
        if self.anotherRide == nil {
            try! self.realm.write {
                self.ride?.chatId = ride.chatId
                self.ride?.clientSocketId = ride.clientSocketId
                self.ride?.driverSocketId = ride.driverSocketId
            }
            self.rideRequest.removeAll()
            self.rideState = .onWay
        } else {
            try! self.realm.write {
                self.anotherRide?.chatId = ride.chatId
                self.anotherRide?.clientSocketId = ride.clientSocketId
                self.anotherRide?.driverSocketId = ride.driverSocketId
            }
            self.rideRequest.removeAll()
        }
        
        //"ACTUALIZAMOS" EL CURRENT LOCATION PARA QUE EMITA EL DRIVER ARRIVING TO ORIGIN POINT
        let location = self.currentLocation
        self.currentLocation = location
        loadContent()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.moveToCurrentLocation()
        }
            // self.moveToCurrentLocation()
    }
}

//MARK: - LoaderViewControllerDelegate
extension MapViewController: LoaderViewControllerDelegate {
    func loaderViewController(viewController: LoaderViewController, didConfirmAction: Bool) {}
    func loaderViewController(viewController: LoaderViewController, didDismiss: Bool) {
        //LOGICA A REALIZAR CUANDO SE DISMISEE EL LOADERVIEWCONTROLLER
        self.loaderVC = nil
    }
}

//MARK: - UserInfoViewDelegate
extension MapViewController: UserInfoViewDelegate {
    func userInfoView(interactionVia: InteractionVia) {
        switch interactionVia {
        case .call:
            call(phoneNumber: self.ride?.clientOnRide?.phone)
        case .message:
            guard let _ = ride?.chatId else {
                self.showAlert(message: "Ocurrió un error, por favor vuelva a intentarlo más tarde")
                return
            }
            ChatViewController.instantiate(inViewController: self, ride: ride, username: ride?.clientOnRide?.name ?? "")
        case .sos:
            sendSOS()
        }
    }
}

//MARK: - OptionSelectorViewControllerDelegate
extension MapViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {}
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if dismissed {
            guard let selectedReason = vc.selectedOption else {
                self.showAlert(message: "En caso de rechazo seleccione un motivo por favor")
                return
            }
            if selectedReason.key != "others" && selectedReason.key != "Comentario" {
                cancelRide(reason: selectedReason.key, comment: nil)
            } else {
                InputViewController.instantiante(inViewController: self, inputType: .textView, withTitle: selectedReason.name, placeholder: "Ingrese su comentario", confirmText: "Enviar", validationMessage: "Por favor, ingrese un comentario válido")
            }
        }
    }
}
