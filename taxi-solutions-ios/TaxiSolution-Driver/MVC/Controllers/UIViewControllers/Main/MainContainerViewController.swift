//
//  MainContainerViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class MainContainerViewController: UIViewController {
    
    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var menuContainerViewLeadingConstraint: NSLayoutConstraint!
    var isTouchEnable = true
    var menuIsVisible = false {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    var menuVC: MenuViewController?
    var mapVC: MapViewController?
    
    let coverLayer = CALayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MenuViewControllerSegue" {
            menuVC = segue.destination as? MenuViewController
        }
        
        if segue.identifier == "MapViewControllerSegue" {
            mapVC = (segue.destination as? UINavigationController)?.viewControllers.first as? MapViewController
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !menuIsVisible, #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .lightContent
        }
    }
    
    func configureContent() {
        coverLayer.frame = self.view.bounds
        coverLayer.backgroundColor = UIColor.black.cgColor
        coverLayer.opacity = 0.0
        mainContainerView.layer.addSublayer(coverLayer)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleMenuPan))
        menuContainerView.addGestureRecognizer(panGestureRecognizer)
        
        menuContainerViewLeadingConstraint.constant = -menuContainerView.frame.size.width
    }
    
    func loadContent() {
        
    }
    
    @objc func handleMenuPan(_ recognizer: UIPanGestureRecognizer) {
        if isTouchEnable {
            let translation = recognizer.translation(in: self.view)
            let percentage = abs(translation.x) / self.menuContainerView.frame.size.width
            
            let state = recognizer.state
            if state == .ended || state == .failed || state == .cancelled {
                if menuIsVisible {
                    if translation.x < 0 {
                        toggleSideMenu()
                    }
                } else {
                    if translation.x > 10.0 {
                        toggleSideMenu()
                    } else {
                        self.view.layoutIfNeeded()
                        UIView.animate(withDuration: 0.2, animations: {
                            self.menuContainerViewLeadingConstraint.constant = -self.menuContainerView.frame.size.width
                            self.view.layoutIfNeeded()
                        })
                    }
                }
            } else {
                if !menuIsVisible && translation.x > 0.0 && translation.x <= menuContainerView.frame.size.width {
                    self.menuContainerViewLeadingConstraint.constant = -self.menuContainerView.frame.size.width + translation.x
                    changeOverlayOpacity(withPercentage: percentage, inTranslation: translation)
                }
                
                if menuIsVisible {
                    if abs(translation.x) <= self.menuContainerView.frame.size.width && translation.x < 0 {
                        menuContainerViewLeadingConstraint.constant = translation.x
                        changeOverlayOpacity(withPercentage: percentage, inTranslation: translation)
                    }
                }
            }
        }
    }
    
    @objc func toggleSideMenu() {
        if isTouchEnable {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: {
                // hide the side menu to the left
                self.menuContainerViewLeadingConstraint.constant = self.menuIsVisible ? -self.menuContainerView.frame.size.width : 0
                self.coverLayer.opacity = self.menuIsVisible ? 0.0 : 0.6
                self.view.layoutIfNeeded()
            })
            
            menuIsVisible = !menuIsVisible
            menuVC?.loadContent()
        }
    }
    
    func changeOverlayOpacity(withPercentage percentage: CGFloat, inTranslation translation: CGPoint) {
        if (translation.x > 0) {
            coverLayer.opacity = 0.6 * Float(percentage)
        } else {
            coverLayer.opacity = 0.6 * (1.0 - Float(percentage))
        }
    }
    
    func openModule(_ module: String) {
        var storyboardName = ""
        if [MenuDriverModuleNames.myProfile, MenuBackendModuleKeys.myProfile].contains(module)  {
            storyboardName = "MyProfile"
        }
//        if [MenuDriverModuleNames.objectives, MenuBackendModuleKeys.objectives].contains(module) {
//            storyboardName = "Objectives"
//        }
        if [MenuDriverModuleNames.rides, MenuBackendModuleKeys.rides].contains(module) {
            storyboardName = "Rides"
        }
        if [MenuDriverModuleNames.earnings, MenuBackendModuleKeys.earnings].contains(module) {
            storyboardName = "Earnings"
        }
//        if [MenuDriverModuleNames.charges, MenuBackendModuleKeys.charges].contains(module) {
//            storyboardName = "Charges"
//        }
        if [MenuDriverModuleNames.shareAndWin, MenuBackendModuleKeys.shareAndWin].contains(module) {
            storyboardName = "Invitations"
        }
//        if [MenuDriverModuleNames.connections, MenuBackendModuleKeys.connections].contains(module) {
//            storyboardName = "Connections"
//        }
        if [MenuDriverModuleNames.help, MenuBackendModuleKeys.help].contains(module) {
            storyboardName = "Help"
        }
        
        guard storyboardName != "" else { return }
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        
        guard let initialVC = storyboard.instantiateInitialViewController() else { return }
        self.present(initialVC, animated: true, completion: nil)
    }
    
    func showRideRequest(ride: Ride) {
        guard let mapVC = self.mapVC else { return }
        if ride.isValidRequest {
            DispatchQueue.main.asyncAfter(deadline: .now() + (self.mapVC?.viewIfLoaded != nil ? 0 : 2)) {
                if self.user.hasEnoughBalance && !self.user.hasExpiredDocuments && mapVC.ride == nil{
                    mapVC.ride = ride
                    try! self.realm.write {
                        self.realm.add(ride, update: .all)
                    }
                    mapVC.showRideRequest()
                } else {
                    print("🚕 ANOTHER RIDE ALREADY BEING REQUESTED 🚕")
                }
            }
        } else {
            print("⏱🚕 EXPIRED RIDE REQUEST 🚕⏱")
        }
    }

}
