//
//  RideRequestViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/2/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol RideRequestViewControllerDelegate: class {
    func rideRequestViewController(viewController: RideRequestViewController, didAcceptRide: Bool, ride: Ride?)
}

class RideRequestViewController: PannableViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: CustomView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var estimatedTimeLabel: UILabel!
    @IBOutlet weak var estimatedTimeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var tripPointsTableView: UITableView!
    
    let feedbackGenerator = UINotificationFeedbackGenerator()
    var mapVC: MapViewController?
    
    var delegate: RideRequestViewControllerDelegate?
    
    var ride: Ride? {
        didSet {
            guard let ride = ride else { return }
            isAloSvico = ride.maxTimeToAcceptTrip == 0
        }
    }
    var anotherRide: AnotherRide? {
        didSet {
            guard let anotherRide = anotherRide else { return }
            isAloSvico = anotherRide.maxTimeToAcceptTrip == 0
        }
    }
    private var isFirstRide: Bool {
        return ride != nil && anotherRide == nil
    }
    private var rideId: Int {
        return (isFirstRide ? ride?.rideId : anotherRide?.rideId) ?? 0
    }
    
    var tripPoints: [String] {
        if isFirstRide {
            guard let ride = ride else { return [] }
            return [ride.originAddress, ride.destinationAddress]
        } else {
            guard let anotherRide = anotherRide else { return [] }
            return [anotherRide.originAddress, anotherRide.destinationAddress]
        }
    }
    
    var serviceDenialReasons = OptionToSelect.rideRequestDenialReasons()
    var selectedReason: String?
    
    var time = 0 {
        didSet {
            estimatedTimeViewHeight.constant = 35
            estimatedTimeLabel.text = "'\(time)"
        }
    }
    
    var timer: Timer?
    var isAloSvico = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.transition(with: self.view, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            self.bottomView.transform = .identity
            self.topView.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func configureContent() {
        estimatedTimeViewHeight.constant = 0
        self.bottomView.transform = CGAffineTransform(translationX: 0, y: self.bottomView.frame.height)
        self.topView.alpha = 0
        
        tripPointsTableView.delegate = self
        tripPointsTableView.dataSource = self
        
        let nib = UINib(nibName: "TripPointTableViewCell", bundle: nil)
        tripPointsTableView.register(nib, forCellReuseIdentifier: "TripPointTableViewCell")
        
    }
    
    func loadContent() {
        if isFirstRide {
            guard let ride = ride, let clientOnRide = ride.clientOnRide else { return }
            setClientData(clientOnRide: clientOnRide, estimatedAmount: ride.estimatedAmount, maxTimeToAcceptTrip: ride.maxTimeToAcceptTrip)
        } else {
            guard let anotherRide = anotherRide, let clientOnRide = anotherRide.clientOnRide else { return }
            setClientData(clientOnRide: clientOnRide, estimatedAmount: anotherRide.estimatedAmount, maxTimeToAcceptTrip: anotherRide.maxTimeToAcceptTrip)
        }
    }
    
    func setClientData(clientOnRide: ClientOnRide, estimatedAmount: String, maxTimeToAcceptTrip: Double) {
        titleLabel.text = isAloSvico ? App.aloSvicoRequestTitle : App.rideRequestTitle
        photoImageView.sd_setImage(with: clientOnRide.photoUrl.url, completed: nil)
        ratingLabel.text = clientOnRide.rating.double.oneDecimalString
        nameLabel.text = clientOnRide.name
        amountLabel.text = "S/ \(estimatedAmount.double.string)"
        if !isAloSvico {
            time = Int(maxTimeToAcceptTrip) + 1
            setTimeOut()
        }
    }
    
    @IBAction func answerTripRequest(_ sender: UIButton) {
        feedbackGenerator.notificationOccurred(sender.tag == 1 ? .success : .error)
        if sender.tag == 0 {
            OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: 0.52, withTitle: "Rechazar servicio", withOptions: serviceDenialReasons, withConfirmButton: true)
        }
        if sender.tag == 1 {
            self.startLoader()
            DriverManager.sharedManager.acceptRide(rideId: rideId, successResponse: { (ride) in
                self.stopLoader()
                self.delegate?.rideRequestViewController(viewController: self, didAcceptRide: true, ride: ride)
                self.close()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription, withOkayButton: true, okayTitle: "Salir", okayHandler: { (_) in
                    self.delegate?.rideRequestViewController(viewController: self, didAcceptRide: false, ride: nil)
                    self.close()
                }, withCancelButton: true, cancelTitle: "Ok")
            }
        }
    }
    
    func setTimeOut() {
        if isFirstRide {
            guard self.ride != nil else { return }
        } else {
            guard self.anotherRide != nil else { return }
        }
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            self.time -= 1
            if self.time <= 0 {
                self.rejectTrip(reason: "", ignored: true, comment: "")
            }
        }
    }
    
    func rejectTrip(reason: String = "", ignored: Bool = false, comment: String? = nil) {
        self.startLoader()
        MainManager.sharedManager.cancelRide(ignored: ignored, reason: reason, rideId: rideId, comment: comment, successResponse: { (message) in
            print("MESSAGE FROM SUCCESSFULL REJECTION -> \(message)")
            self.stopLoader()
            self.close()
            self.delegate?.rideRequestViewController(viewController: self, didAcceptRide: false, ride: nil)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    override func close() {
        super.close()
        self.timer?.invalidate()
        ride = nil
        anotherRide = nil
    }

}

extension RideRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.frame.height / CGFloat(tripPoints.count))
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripPoints.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripPointTableViewCell") as! TripPointTableViewCell
        cell.pointType = indexPath.row == 0 ? .initial : (indexPath.row == tripPoints.count - 1) ? .final : .middle
        cell.pointAddress = tripPoints[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}

extension RideRequestViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {
        selectedReason = option.key
    }
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if dismissed {
            guard let selectedReason = selectedReason else {
                self.showAlert(message: "En caso de rechazo seleccione un motivo por favor")
                return
            }
            rejectTrip(reason: selectedReason)
        } else {
            selectedReason = nil
        }
    }
}
