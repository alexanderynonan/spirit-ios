//
//  MapsViewController + ModalFeatures.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

extension MapViewController {
    func configureUserOnTripCard() {
        configureUserOnTripCardViewButtons()
        
        let upSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(changeUserOnTripCardView(_:)))
        upSwipeGestureRecognizer.direction = .up
        
        let downSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(changeUserOnTripCardView(_:)))
        downSwipeGestureRecognizer.direction = .down
        
        userOnTripCardView.addGestureRecognizer(upSwipeGestureRecognizer)
        userOnTripCardView.addGestureRecognizer(downSwipeGestureRecognizer)
    }
    
    func configureUserOnTripCardViewButtons(show: Bool = true) {
        UIView.transition(with: self.buttonsStackView, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            self.userInfoViewContainerHeight.constant = self.defaultUserInfoHeight
            
            [self.cancelAloSvicoContainerHeight, self.nearToOriginButtonsContainerHeight, self.waitingButtonsContainerHeight, self.onRideButtonsContainerHeight, self.nearToDestinationButtonsContainerHeight, self.paymentDetailContainerHeight, self.paymentFeedbackButtonsContainerHeight].forEach {
                $0?.constant = 0
            }
            
            if show {
                guard let currentState = self.rideState else { return }
                switch currentState {
                case .onWay:
                    self.nearToOriginButtonsContainerHeight.constant = self.ride?.nearToOrigin == true ? self.defaultButtonHeight * 2 : 0
                    self.cancelAloSvicoContainerHeight.constant = self.ride?.requestType == "svico" && self.ride?.nearToOrigin == false ? self.defaultButtonHeight : 0
                case .waiting:
                    self.showWaitingTimer()
                    self.waitingButtonsContainerHeight.constant = self.defaultButtonHeight * 2
                case .onRide:
                    self.showWaitingTimer(false)
                    let nearToDestination = self.ride?.nearToDestination == true

                    self.onRideButtonsContainerHeight.constant = nearToDestination ? 0 : self.defaultButtonHeight
                    self.nearToDestinationButtonsContainerHeight.constant = nearToDestination ? self.defaultButtonHeight * 2 : 0
                case .finished:
                    if ["pending", "paying", ""].contains(self.ride?.paymentStatus) && self.ride?.stateString != "cancelled" {
                        self.paymentDetailContainerHeight.constant = self.paymentDetailView.preferredHeight
                        let paymentMethodIsCash = self.paymentDetailView.paymentDetail?.paymentMethod == .cash
                        self.paymentFeedbackButtonsContainerHeight.constant = self.defaultButtonHeight 
                        self.paymentMainButton.setTitle("Completar viaje", for: .normal)
                    } else {
                        defer {
                            self.clearRideData()
                        }
                        self.loaderVC = LoaderViewController.instantiate(in: self, title: "Viaje cancelado", automaticDismiss: true, delegate: self)
                    }
                }
            }
            
            self.userOnTripCardView.layoutIfNeeded()
        }, completion: { _ in
            self.configureMyLocationButtonPosition()
        })
    }
    
    func setTripClientInfo() {
        DispatchQueue.main.async {
            self.userInfoView.ride = self.ride
        }
    }
    
    func showWaitingTimer(_ show: Bool = true) {
        self.timerContainer.alpha = show ? 1 : 0
        self.timerContainer.transform = show ? .identity : CGAffineTransform(scaleX: 0.5, y: 0.5)
        show ? self.startWaitingForClientTimer() : ()
    }
    
    @objc func changeUserOnTripCardView(_ gesture: UISwipeGestureRecognizer) {
        if rideState != .finished {
            UIView.transition(with: userOnTripCardView, duration: 0.25, options: [.transitionCrossDissolve], animations: {

                let show = gesture.direction == .up
                print("MOSTRAR: \(show)")
                self.configureUserOnTripCardViewButtons(show: show)

                self.configureMyLocationButtonPosition()

                self.userOnTripCardView.layoutIfNeeded()
                self.mapView.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @IBAction func setWaitingState(_ sender: UIButton) {
        guard let ride = ride else { return }
        loaderVC = LoaderViewController.instantiate(in: self, title: "Esperando...", message: "Si el cliente demora llámelo")
        DriverManager.sharedManager.setWaitingState(ride: ride, successResponse: { (waitingTime) in
            try! self.realm.write {
                self.ride?.maxTimeToWaitForClient = waitingTime
                self.ride?.startedToWaitAt = Date()
            }
            self.rideState = .waiting
            self.loaderVC?.dismiss()
        }) { (error) in
            self.loaderVC?.dismiss()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func startWaitingForClientTimer() {
        var bgTask = UIBackgroundTaskIdentifier(rawValue: 0)
        bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(bgTask)
        })
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setWaitingForClientTimer), userInfo: nil, repeats: true)
        guard let timer = timer else { return }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.default)
    }
    
    @objc func setWaitingForClientTimer() {
        if self.ride?.isInvalidated ?? false {
            self.ride = Ride()
        }

        guard let ride = self.ride else { return }
        let time = ride.timeWaiting
        if ride.maxTimeToWaitForClient != 0  {
            if time > ride.maxTimeToWaitForClient {
                timerContainer.backgroundColor = Colors.error
            } else {
                timerContainer.backgroundColor = Colors.primary
            }
        }

        self.timerLabel.text = abs(time).secondsToTime()
    }
    
    func resetTimer() {
        self.timer?.invalidate()
        self.timerContainer.backgroundColor = Colors.primary
        self.timerLabel.text = "--:--"
        self.showWaitingTimer(false)
    }
    
    func showPaymentDetail(_ paymentDetail: PaymentDetail) {
        self.paymentDetailView.paymentDetail = paymentDetail
        
        if paymentDetail.pendingAmount.double > 0.0 {
            ConfirmationViewController.instantiante(inViewController: self.parent?.parent ?? self, withTitle: ConfirmationTitles.debtorUser, message: "Por favor, comuníquele al usuario que debe realizar el pago completo del presente viaje más el saldo pendiente, caso contrario no podrá realizar más viajes hasta cancelar su deuda", andConfirmText: "LISTO") { _, _ in () }
        }
        
        try! self.realm.write {
            ride?.paymentDetail = paymentDetail
        }
        
        self.rideState = .finished
    }
    
    @IBAction func startRide(_ sender: UIButton) {
        guard let ride = self.ride else { return }
        self.startLoader()
        DriverManager.sharedManager.startRide(ride: ride, successResponse: { (ride) in
            self.stopLoader()
            try! self.realm.write {
                self.ride?.clientSocketId = ride.clientSocketId
                self.ride?.destinationLat = ride.destinationLat
                self.ride?.destinationLong = ride.destinationLong
                self.ride?.paymentType = ride.paymentType
            }
            self.setTripClientInfo()
            self.rideState = .onRide
            self.destinationLabel.text = self.ride?.destinationAddress ?? ""
            self.resetTimer()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func sendSOS() {
        ConfirmationViewController.instantiante(inViewController: self.parent?.parent ?? self, withTitle: ConfirmationTitles.SOS, message: "", image: #imageLiteral(resourceName: "img_sos"), withAnimation: true, imageHeight: self.view.frame.height * 0.2, andConfirmText: "LLAMAR A LA CENTRAL") { _, confirmed in
            guard confirmed else { return }
            
            self.call(phoneNumber: App.sosPhone)
        }
        
        guard let coordinate = self.currentLocation, let ride = self.ride else { return }
        MainManager.sharedManager.sendSOS(currentCoordinate: coordinate, ride: ride,successResponse: { (message) in
            print("🆘 \(message) 🆘")
        }) { (error) in
            print("🆘 ERROR -> \(error.localizedDescription) 🆘")
        }
    }
    
    @IBAction func showCancelRideReasons(_ sender: UIButton) {
        guard let rideState = rideState else { return }
        var options: [OptionToSelect] = []
        var heightScale: CGFloat = 0.0
        switch rideState {
        case .onRide:
            options = OptionToSelect.driverOnRideCancelReasons()
            heightScale = 0.44
        default:
            options = OptionToSelect.dynamicRideCancelReasons()
            heightScale = 0.75
        }
        OptionSelectorViewController.instantiate(inViewController: self, delegate: self, heightScale: heightScale, withTitle: "Cancelar servicio", withOptions: options, withConfirmButton: true)
    }
    
    @IBAction func getPaymentDetails() {
        guard let ride = ride else { return }
        self.startLoader()
        DriverManager.sharedManager.getPaymentDetails(ride: ride, successResponse: { (paymentDetail) in
            self.stopLoader()
            self.setTripClientInfo()
            self.paymentDetailContainerHeight.constant = self.paymentDetailView.preferredHeight
            self.showPaymentDetail(paymentDetail)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func finishRide() {
        guard let ride = ride else { return }
        self.startLoader()
        DriverManager.sharedManager.finishRide(ride: ride, paymentStatus: .completed, pendingAmount: "", successResponse: { (message) in
            self.stopLoader()
            self.clearRideData()
            self.checkIfNeedsToRateUser()
            print(message)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func setUncompletedPayment() {
        InputViewController.instantiante(inViewController: self, inputType: .textField, withTitle: "Monto pagado", placeholder: "0.00", confirmText: "Confirmar", validationMessage: ValidationMessages.amountMoney)
    }
    
    @IBAction func setNoPayment() {
        guard let ride = ride, let paymentDetail = ride.paymentDetail else { return }
        
        DriverManager.sharedManager.finishRide(ride: ride, paymentStatus: .notPaid, pendingAmount: paymentDetail.totalAmount, successResponse: { (message) in
            self.clearRideData()
            self.checkIfNeedsToRateUser()
        }) { (error) in
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func cancelRide(reason: String, comment: String?, completion: (() -> Void)? = nil) {
        guard let ride = ride else { return }
        self.startLoader()
        MainManager.sharedManager.cancelRide(ignored: false, reason: reason, rideId: ride.rideId, comment: comment, successResponse: { (paymentDetail) in
            self.stopLoader()
            self.resetTimer()
            guard let paymentDetail = paymentDetail else {
                self.clearRideData()
                return
            }
            self.showPaymentDetail(paymentDetail)
            completion?()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
}

extension MapViewController: InputViewControllerDelegate {
    func inputViewController(_ viewController: InputViewController, didConfirm: Bool) {
        if didConfirm {
            if viewController.title == "Monto pagado" {
                guard let ride = ride else { return }
                self.startLoader()
                DriverManager.sharedManager.finishRide(ride: ride, paymentStatus: .incompleted, pendingAmount: viewController.inputText, successResponse: { (message) in
                    self.stopLoader()
                    self.clearRideData()
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            }
            if viewController.title == "Otros" || viewController.title == "Comentario" {
                cancelRide(reason: "others", comment: viewController.inputText)
            }
        }
    }
}
