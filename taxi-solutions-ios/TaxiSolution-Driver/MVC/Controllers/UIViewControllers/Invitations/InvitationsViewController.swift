//
//  InvitationsViewController.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 3/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import Toast_Swift

class InvitationsViewController: UIViewController {
    
    @IBOutlet weak var referralTableView: UITableView!
    @IBOutlet weak var referralTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var totalBonificationLabel: UILabel!
    @IBOutlet weak var referralsTitleLabel: UILabel!
    @IBOutlet weak var inviteCodeLabel: UILabel!
    var referrals: [ReferredFriend] = [] {
        didSet {
            referralTableViewHeight.constant = referralTableView.getMinimumHeight(forVC: self, items: referrals, tableViewCellHeight: tableViewCellHeight)
            referralTableView.animate()
        }
    }
    
    let tableViewCellHeight: CGFloat = 80

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        referralTableView.delegate = self
        referralTableView.dataSource = self
        referralTableView.register(UINib(nibName: "ReferralTableViewCell", bundle: nil), forCellReuseIdentifier: "ReferralTableViewCell")
        referrals = []
    }
    
    func loadContent() {
        self.startLoader(waitingForObjectsOfType: ReferredFriend.self)
        setData()
        MainManager.sharedManager.getReferredFriends(successResponse: { (referredFriends, totalBonus) in
            self.stopLoader()
            self.totalBonificationLabel.text = "PEN \(totalBonus)"
            self.setData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData() {
        self.inviteCodeLabel.text = user.invitationCode
        if referrals != Array(realm.objects(ReferredFriend.self)) {
            self.referrals = Array(realm.objects(ReferredFriend.self))
        }
        self.referralsTitleLabel.text = "Referidos (\(self.referrals.count))"
    }
    
    @IBAction func copyInviteCode() {
        UIPasteboard.general.string = user.invitationCode
        showToast(message: "Su código ha sido copiado")
    }
    
    @IBAction func shareCode() {
        let items = [user.invitationCode]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityController, animated: true)
    }

}

extension InvitationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: referrals, emptyMessage: "Aún no tienes referidos")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralTableViewCell") as! ReferralTableViewCell
        cell.referredFriend = referrals[indexPath.row]
        cell.loadData()
        return cell
    }
}

