
//
//  UserInfoView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 4/23/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

protocol UserInfoViewDelegate: class {
    func userInfoView(interactionVia: InteractionVia)
}

class UserInfoView: UIView {
    
    @IBOutlet weak var sosButton: CustomButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthSOSButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthMessageButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var stateLabel: UILabel!
    
    var phone = ""
    
    var ride: Ride? = nil {
        didSet {
            setData()
        }
    }
    
    var delegate: UserInfoViewDelegate?
    
    private func setData() {
        guard let ride = self.ride else { return }
        if let client = ride.clientOnRide {
            photoImageView.sd_setImage(with: client.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached], completed: nil)
            nameLabel.text = client.name
            phone = client.phone
        }
    }
    
    func   setRideState() {
        if let rideState = ride?.rideState {
            switch rideState {
            case .onWay:
                stateLabel.text = "Recoger a"
                hideButtons(false)
            case .waiting:
                stateLabel.text = "Esperando a"
                hideButtons(false)
            case .onRide:
                stateLabel.text = "Dejar a"
                hideButtons()
            case .finished:
                stateLabel.text = "Cobrar a"
                hideButtons()
            }
        }
    }
    
    func hideButtons(_ hide: Bool = true) {
        messageButton.isHidden = hide
        sosButton.isHidden = !hide
        widthMessageButtonConstraint.constant = hide ? 0 : 70
        widthSOSButtonConstraint.constant = hide ? 70 : 0
    }
    
    @IBAction func call(_ sender: UIButton) {
        delegate?.userInfoView(interactionVia: .call)
    }
    
    @IBAction func message(_ sender: UIButton) {
        delegate?.userInfoView(interactionVia: .message)
    }

    @IBAction func sos(_ sender: Any) {
        delegate?.userInfoView(interactionVia: .sos)
    }
}
