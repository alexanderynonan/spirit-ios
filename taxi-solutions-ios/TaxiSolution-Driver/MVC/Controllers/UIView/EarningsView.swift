//
//  EarningsView.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 23/02/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import Foundation
import UIKit

class EarningsView: UIView {

    @IBOutlet weak var earningDateLabel: UILabel!
    @IBOutlet weak var earningAmountLabel: UILabel!
    var earning = EarningTimeFrameDate()

    func loadData() {
        earningDateLabel.text = earning.earningDate
        earningAmountLabel.text = String(format: "S/ %.2f", earning.totalAmount.double)
    }
}
