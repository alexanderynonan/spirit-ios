//
//  RideStateView.swift
//  TaxiSvico
//
//  Created by Juan Carlos Guillén on 4/23/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RideStateView: UIView {
    
    @IBOutlet var stateButtons: [SwitchButton]!
    @IBOutlet weak var progressContainerView: CustomView!
    @IBOutlet weak var progressIndicatorView: CustomView!
    @IBOutlet weak var progressIndicatorViewWidth: NSLayoutConstraint!
    var currentState: RideState? = .onWay {
        didSet {
            updateState()
        }
    }
    
    override func awakeFromNib() {
        progressIndicatorViewWidth.constant = 0
    }
    
    private func updateState() {
        guard let currentState = currentState else {
            UIView.transition(with: progressIndicatorView, duration: 0.25, options: [], animations: {
                self.progressIndicatorViewWidth.constant = 0
                self.progressIndicatorView.layoutIfNeeded()
            }) { (_) in
                self.stateButtons.forEach { $0.isOn = false }
            }
            return
        }
        UIView.transition(with: progressIndicatorView, duration: 0.25, options: [], animations: {
            self.progressIndicatorViewWidth.constant = (self.progressContainerView.frame.width / 3.0) * CGFloat(currentState.rawValue)
            self.progressIndicatorView.layoutIfNeeded()
        }) { (_) in
            self.stateButtons.forEach {  stateButton in
                if stateButton.tag <= currentState.rawValue {
                    if !stateButton.isOn {
                        stateButton.isOn = true
                        UIView.transition(with: stateButton, duration: 0.25, options: [.autoreverse], animations: {
                            stateButton.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
                        }, completion: nil)
                    }
                } else {
                    stateButton.isOn = false
                }
            }
        }
    }

}
