//
//  DateHeaderView.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol DateHeaderViewDelegate {
    func dateHeaderView(_ view: DateHeaderView, tapped: Bool)
}

class DateHeaderView: UIView {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    
    var delegate: DateHeaderViewDelegate?
    var section = 0
    var dateWithConnectionRecords: DateWithConnectionRecords? {
        didSet {
            guard let dateWithRecords = dateWithConnectionRecords else { return }
            self.dateLabel.text = dateWithRecords.date
            self.separatorView.alpha = dateWithRecords.open ? 0 : 1
            self.arrowButton.setImage(dateWithRecords.open ? #imageLiteral(resourceName: "ic_up") : #imageLiteral(resourceName: "ic_down"), for: .normal)
        }
    }
    var dateWithEarningRecords: DateWithEarningRecords? {
        didSet {
            guard let dateWithRecords = dateWithEarningRecords else { return }
            self.dateLabel.text = dateWithRecords.date
            self.separatorView.alpha = dateWithRecords.open ? 0 : 1
            self.arrowButton.setImage(dateWithRecords.open ? #imageLiteral(resourceName: "ic_up") : #imageLiteral(resourceName: "ic_down"), for: .normal)
        }
    }
    
    override func awakeFromNib() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap)))
    }
    
    @objc func tap() {
        delegate?.dateHeaderView(self, tapped: true)
    }
}
