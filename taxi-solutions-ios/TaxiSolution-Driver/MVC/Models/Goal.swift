//
//  Goal.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/3/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Goal: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var current = 0
    @objc dynamic var total = 0
    @objc dynamic var startingDate = ""
    @objc dynamic var endingDate = ""
    @objc dynamic var reward = ""
    @objc dynamic var units = ""
    var goalRecords = List<GoalRecord>()
    
    override class func primaryKey() -> String? {
        return "id"
    }

    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["description"].stringValue
        self.current = data["current"].intValue
        self.total = data["total"].intValue
        self.startingDate = data["start_date"].stringValue
        self.endingDate = data["termination_date"].stringValue
        self.reward = data["reward"].stringValue
        self.units = data["txt_progress"].stringValue
        
        let goalRecordsData = data["trips"].arrayValue
        goalRecordsData.forEach {
            let goalRecord = GoalRecord(data: $0)
            goalRecords.append(goalRecord)
        }
    }
}
