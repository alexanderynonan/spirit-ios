//
//  LicenseCategory.swift
//  TaxiSvico
//
//  Created by PeruApps Dev on 2/17/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class LicenseCategory: Object {
    @objc dynamic var key = ""
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    func set(data: JSON) {
        self.key = data["key"].stringValue
        self.name = data["name"].stringValue
    }
}
