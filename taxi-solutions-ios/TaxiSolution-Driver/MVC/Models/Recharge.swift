//
//  Recharge.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/31/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Recharge: Object {
    @objc dynamic var id = 0
    @objc dynamic var amount = ""
    @objc dynamic var dateString = ""
    @objc dynamic var status = ""
    @objc dynamic var paymentType = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.amount = data["amount"].stringValue
        self.dateString = data["date"].stringValue
        self.status = data["status"].stringValue
        self.paymentType = data["payment_type"].stringValue
    }
}
