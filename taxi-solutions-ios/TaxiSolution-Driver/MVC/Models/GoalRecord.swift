//
//  GoalRecord.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 6/3/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class GoalRecord: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var orderNumber = ""
    @objc dynamic var date = ""
    @objc dynamic var startingDate = ""
    @objc dynamic var endingDate = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["name"].stringValue
        self.orderNumber = data["order_number"].stringValue
        self.date = data["trip_date"].stringValue
        self.startingDate = data["start_at"].stringValue
        self.endingDate = data["finished_at"].stringValue
    }
}
