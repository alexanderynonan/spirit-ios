//
//  LastConsumptions.swift
//  TaxiSvico-Driver
//
//  Created by MacBook Pro Fer on 8/11/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class LastConsumption: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var amount = ""
    @objc dynamic var createdAt = ""
    @objc dynamic var address = ""
    @objc dynamic var paymentMethod = ""
    @objc dynamic var action = ""
    
    var date: Date? {
        createdAt.date(fromFormat: "dd/MM/yy hh:mm a")
    }
    
    var type: ConsumptionType? {
        return ConsumptionType.allCases.first(where: { $0.rawValue == self.action })
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }

    convenience init(_ data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.amount = data["amount"].stringValue
        self.createdAt = data["created_at"].stringValue
        self.address = data["address"].stringValue
        self.paymentMethod = data["payment_method"].stringValue
        self.action = data["action"].stringValue

        
    }
}
