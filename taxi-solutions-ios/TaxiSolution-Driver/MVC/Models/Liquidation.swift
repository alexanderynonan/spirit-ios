//
//  Liquidation.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 24/02/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Liquidation: Object {

    @objc dynamic var lastPage = 0

    var payments = List<LiquidationRecord>()

    override class func primaryKey() -> String? {
        return "lastPage"
    }

    convenience init(_ data : JSON ) {
        self.init()
        self.lastPage = data["last_page"].intValue

        data["data"].arrayValue.forEach {
            let bRecord = LiquidationRecord($0)
            payments.append(bRecord)
        }
    }

}
