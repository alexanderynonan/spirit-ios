//
//  Balance.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 24/02/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Balance: Object {

    @objc dynamic var id = ""
    @objc dynamic var balance = 0.0
    @objc dynamic var amountMin = ""
    @objc dynamic var canTakeCashRides = false
    @objc dynamic var lastPage = 0

    var rides = List<BalanceRecord>()

    override class func primaryKey() -> String? {
        return "id"
    }

    convenience init(_ data : JSON ) {
        self.init()
        self.id = UUID().uuidString
        self.balance = data["balance"].doubleValue
        self.amountMin = data["amount_min"].stringValue
        self.canTakeCashRides = data["can_cash"].boolValue
        self.lastPage = data["last_page"].intValue

        data["data"].arrayValue.forEach {
            let bRecord = BalanceRecord($0)
            rides.append(bRecord)
        }
    }
    
}
