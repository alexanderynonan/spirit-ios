//
//  EarningTimeFrameDate.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 8/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class EarningTimeFrameDate: Object {
    
    @objc dynamic var earningDate = ""
    @objc dynamic var totalAmount = ""
    @objc dynamic var currentDate = ""
    @objc dynamic var hoursConnected = ""
    @objc dynamic var minutesConnected = ""

    var earningRecords = List<EarningRecord>()
    
    convenience init(data: JSON) {
        self.init()
        self.earningDate = data["profit_date"].stringValue
        self.totalAmount = data["total_amount"].stringValue
        self.currentDate = data["current_date"].stringValue
        self.hoursConnected = data["hours"].stringValue
        self.minutesConnected = data["minutes"].stringValue
        data["rides"].arrayValue.forEach {
            let record = EarningRecord(data: $0)
            earningRecords.append(record)
          //  print(record)
        }
    }
    
    override class func primaryKey() -> String? {
        return "earningDate"
    }
    
}
