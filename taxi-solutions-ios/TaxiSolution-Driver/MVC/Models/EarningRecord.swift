//
//  EarningRecord.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 8/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class EarningRecord: Object {
    @objc dynamic var id = 0
    @objc dynamic var duration = ""
    @objc dynamic var amount = ""
    @objc dynamic var totalMetrics = ""
    @objc dynamic var paymentMethod = ""
    @objc dynamic var dateString = ""
    @objc dynamic var comission = 0.0
    @objc dynamic var profit = 0.0

    var date: Date {
        return self.dateString.date(fromFormat: "dd/MM/yyyy") ?? Date()
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.dateString = data["formatted_date"].stringValue
        self.duration = data["duration"].stringValue
        self.amount = data["amount"].doubleValue.string
        self.totalMetrics = data["distance_time"].stringValue
        self.paymentMethod = data["payment_method"].stringValue
        self.comission = data["commission"].doubleValue
        self.profit = data["profit"].doubleValue
        print(data)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
