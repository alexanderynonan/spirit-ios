//
//  Document.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 7/3/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class Document: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var expiresAt = ""
    @objc dynamic var expirationDateFormatted = ""
    
    var expirationDate: Date {
        return expirationDateFormatted.date(fromFormat: "yyyy-MM-dd") ?? Date()
    }
    
    var expired: Bool {
        return self.expirationDate < Date()
    }
    
    var aboutToExpire: Bool {
        let days = Calendar.current.dateComponents([.day], from: Date(), to: expirationDate).day ?? 0
        print("📅 Dias \(days)")
        return days <= 7
    }
    
    override class func primaryKey() -> String? {
        return "name"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.id = data["id"].intValue
        self.name = data["name"].stringValue
        self.expiresAt = data["expiration_date"].stringValue
        self.expirationDateFormatted = data["expiration_date_format"].stringValue
    }
}
