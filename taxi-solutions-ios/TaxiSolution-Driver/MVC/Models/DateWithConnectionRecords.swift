//
//  DateWithRecords.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

struct DateWithConnectionRecords {
    var date = String()
    var records = [ConnectionRecord]()
    var open = Bool()
}
