//
//  TimeFrameDate.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ConnectionTimeFrameDate: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var totalTimeConnected = ""
    var connectionRecords = List<ConnectionRecord>()
    
    override class func primaryKey() -> String? {
        return "name"
    }
    
    convenience init(data: JSON) {
        self.init()
        self.name = data["formatted_connection"].stringValue
        self.totalTimeConnected = data["formatted_elapsed_seconds"].stringValue
        data["records"].arrayValue.forEach {
            let record = ConnectionRecord(data: $0)
            connectionRecords.append(record)
        }
    }
    
}
