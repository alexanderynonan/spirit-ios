//
//  DateWithEarningRecords.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 8/5/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation

struct DateWithEarningRecords {
    var date = String()
    var records = [EarningRecord]()
    var open = Bool()
}
