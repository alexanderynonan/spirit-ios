//
//  ConnectionRecord.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ConnectionRecord: Object {
    @objc dynamic var name = ""
    @objc dynamic var dateString = ""
    @objc dynamic var startedAt = ""
    @objc dynamic var finishedAt = ""
    @objc dynamic var totalTimeConnected = ""
    
    var date: Date {
        return self.dateString.date(fromFormat: "dd/MM/yyyy") ?? Date()
    }
    
    convenience init(data: JSON) {
        self.init()
        self.name = data["connection_day"].stringValue
        self.dateString = data["formatted_connection_day"].stringValue
        self.startedAt = data["started_at"].stringValue
        self.finishedAt = data["finished_at"].stringValue
        self.totalTimeConnected = data["elapsed_seconds"].stringValue
    }
    
    override class func primaryKey() -> String? {
        return "name"
    }
}
