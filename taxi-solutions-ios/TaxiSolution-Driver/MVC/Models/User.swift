//
//  User.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 2/16/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class User: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var accessToken = ""
    @objc dynamic var socketToken = ""
    @objc dynamic var serviceTypeId = ""
    @objc dynamic var serviceName = ""
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var fullName = ""
    @objc dynamic var deviceToken = ""
    @objc dynamic var email = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var phone = ""
    @objc dynamic var documentType = ""
    @objc dynamic var documentNumber = ""
    @objc dynamic var rating = 0.0
    @objc dynamic var bankName = ""
    @objc dynamic var bankAccountNumber = ""
    @objc dynamic var invitationCode = ""
    @objc dynamic var externalInvitationCode = ""
    @objc dynamic var approvalStatus = ""
    @objc dynamic var connectionStatus = ""
    @objc dynamic var formattedConnectionStatus = ""
    @objc dynamic var isConnected = false
    @objc dynamic var approvedAt = ""
    @objc dynamic var status = false
    @objc dynamic var profile = ""
    @objc dynamic var firebaseId = ""
    @objc dynamic var currentBalance = 0.0
    var hasEnoughBalance: Bool {
        return currentBalance >= 0.1
    }
    
    @objc dynamic var carPlateNumber = ""
    @objc dynamic var carColor = ""
    @objc dynamic var carBrand = ""
    @objc dynamic var createdAt = ""
    
    @objc dynamic var documentsCheckedDate = Date().yesterday
    @objc dynamic var hasExpiredDocuments = false
    var documentsAboutToExpireShownToday: Bool {
        let today = Date()
        return today.day == documentsCheckedDate.day && today.month == documentsCheckedDate.month
    }
    
    var createdAtDate: Date {
        return createdAt.date() ?? Date()
    }
    
    var daysRegistered: Int {
        return Calendar.current.dateComponents([.day], from: createdAtDate, to: Date()).day ?? 0
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(_ data: JSON, accessToken: String) {
        self.init()
        self.id = data["id"].intValue
        self.accessToken = accessToken
        self.socketToken = data["socket_auth_token"].stringValue
        self.serviceTypeId = data["service_type_id"].stringValue
        self.firstName = data["name"].stringValue
        self.lastName = data["last_name"].stringValue
        self.fullName = data["full_name"].stringValue
        self.deviceToken = data["device_token"].stringValue
        self.email = data["email"].stringValue
        self.photoUrl = data["photo_url"].stringValue
        self.phone = data["phone"].stringValue
        self.documentType = data["document_type"].stringValue
        self.documentNumber = data["document_number"].stringValue
        self.rating = data["rating"].doubleValue
        self.bankName = data["bank_name"].stringValue
        self.bankAccountNumber = data["bank_account_number"].stringValue
        self.invitationCode = data["invitation_code"].stringValue
        self.externalInvitationCode = data["external_invitation_code"].stringValue
        self.approvalStatus = data["approval_status"].stringValue
        self.connectionStatus = data["connection_status"].stringValue
        self.formattedConnectionStatus = data["formatted_connection_status"].stringValue
        self.isConnected = data["is_connected"].boolValue
        self.approvedAt = data["approved_at"].stringValue
        self.status = data["status"].boolValue
        self.profile = data["profile"].stringValue
        self.firebaseId = data["firebaseId"].stringValue
        self.currentBalance = data["current_balance"].stringValue.double
        
        self.serviceName = data["service_name"].stringValue
        self.carPlateNumber = data["car_plate_number"].stringValue
        self.carColor = data["car_color"].stringValue
        self.carBrand = data["car_brand"].stringValue
        self.createdAt = data["created_at"].stringValue
        
        print("🔑 ACCESS TOKEN ->\n\(self.accessToken)")
    }
}
