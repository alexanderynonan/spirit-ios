//
//  LiquidationRecord.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 24/02/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class LiquidationRecord: Object {

    @objc dynamic var id = 0
    @objc dynamic var driverId = 0
    @objc dynamic var dateFormatted = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var paidTo = ""
    @objc dynamic var isPayConfirmed = false

    override class func primaryKey() -> String? {
        return "id"
    }

    convenience init(_ data : JSON ) {
        self.init()
        self.id = data["id"].intValue
        self.driverId = data["driver_id"].intValue
        self.dateFormatted = data["created_formatted"].stringValue
        self.amount = data["amount"].doubleValue
        self.paidTo = data["paid_to"].stringValue
        self.isPayConfirmed = data["is_pay_confirmed"].boolValue
    }

}

