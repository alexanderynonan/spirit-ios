//
//  AnotherRide.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 10/09/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation
import SwiftyJSON

class AnotherRide: Object {
    @objc dynamic var id = ""
    @objc dynamic var rideId = 0
    @objc dynamic var chatId = ""
    @objc dynamic var clientSocketId = ""
    @objc dynamic var driverSocketId = ""
    @objc dynamic var serviceTypeId = ""
    @objc dynamic var estimatedTime = ""
    @objc dynamic var estimatedDistance = 0
    @objc dynamic var originAddress = ""
    @objc dynamic var originLat = 0.0
    @objc dynamic var originLong = 0.0
    @objc dynamic var destinationAddress = ""
    @objc dynamic var destinationLat = 0.0
    @objc dynamic var destinationLong = 0.0
    @objc dynamic var paymentType = ""
    @objc dynamic var estimatedAmount = ""
    @objc dynamic var maxTimeToAcceptTrip = 0.0
    @objc dynamic var maxTimeToWaitForClient = 0
    @objc dynamic var paymentStatus = ""
    @objc dynamic var requestType = ""
    
    @objc dynamic var shown = false
    
    @objc dynamic var stateString = ""
    @objc dynamic var state = -1
    
    @objc dynamic var clientOnRide: ClientOnRide?
    @objc dynamic var requestedAt: Date = Date()
    var isValidRequest: Bool {
        let elapsedSeconds = Calendar.current.dateComponents([.second], from: requestedAt, to: Date()).second ?? 0
        print("⏱ ELAPSED TIME: \(elapsedSeconds)")
        return maxTimeToAcceptTrip >= Double(elapsedSeconds)
    }
    
    var originCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: originLat, longitude: originLong)
    }
    
    var destinationCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
    }
    
    /// Estado del viaje cuando ya comenzó. Posibles valores: onWay, waiting, onRide, finished
    var rideState: RideState? {
        RideState.allCases.first(where: { $0.rawValue == state }) ?? .none
    }
    
    var paymentTypeTitle: String {
        self.paymentType == "cash" ? "Efectivo" : "Tarjeta"
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: [String: Any]) {
        self.init()
        self.id = UUID().uuidString
        self.rideId = (data["ride_id"] as? Int ?? (data["ride_id"] as? String)?.int) ?? 0
        self.chatId = data["chat_id"] as? String ?? ""
        self.clientSocketId = data["client_socket_id"] as? String ?? ""
        self.driverSocketId = data["driver_socket_id"] as? String ?? ""
        self.serviceTypeId = data["service_type_id"] as? String ?? ""
        self.estimatedTime = data["estimated_time"] as? String ?? ""
        
        self.originAddress = data["origin_address"] as? String ?? ""
        self.originLat = (data["origin_lat"] as? String ?? "").double
        self.originLong = (data["origin_long"] as? String ?? "").double
        
        self.destinationAddress = data["destination_address"] as? String ?? ""
        self.destinationLat = data["destination_lat"] as? Double ?? ((data["destination_lat"] as? String)?.double ?? 0.0)
        self.destinationLong = data["destination_long"] as? Double ?? ((data["destination_long"] as? String)?.double ?? 0.0)
        
        self.paymentType = data["payment_type"] as? String ?? ""
        
        self.estimatedAmount = data["total_amount"] as? String ?? ""
        self.maxTimeToAcceptTrip = (data["max_waiting_time_to_accept_ride"] as? String ?? "").double
        
        self.requestedAt = (data["started_at"] as? String)?.date() ?? Date()
        self.stateString = data["status"] as? String ?? ""
        self.state = RideState.getStatusFromString(stateString)?.rawValue ?? -1
        self.paymentStatus = data["payment_status"] as? String ?? ""
        self.requestType = data["type"] as? String ?? ""
        
        self.clientOnRide = ClientOnRide(data: data)
    }
    
    convenience init(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        self.init()
        self.originLat = origin.latitude
        self.originLong = origin.longitude
        self.destinationLat = destination.latitude
        self.destinationLong = destination.longitude
    }
    
    func setOriginCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let realm = try! Realm()
        try! realm.write {
            self.originLat = coordinate.latitude
            self.originLong = coordinate.longitude
        }
    }
    
    func setDestinationCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let realm = try! Realm()
        try! realm.write {
            self.destinationLat = coordinate.latitude
            self.destinationLong = coordinate.longitude
        }
    }
    
    func getRide() -> Ride {
        let ride = Ride()
        ride.id = self.id
        ride.rideId = self.rideId
        ride.chatId = self.chatId
        ride.clientSocketId = self.clientSocketId
        ride.driverSocketId = self.driverSocketId
        ride.serviceTypeId = self.serviceTypeId
        ride.estimatedTime = self.estimatedTime
        ride.estimatedDistance = self.estimatedDistance
        ride.originAddress = self.originAddress
        ride.originLat = self.originLat
        ride.originLong = self.originLong
        ride.destinationAddress = self.destinationAddress
        ride.destinationLat = self.destinationLat
        ride.destinationLong = self.destinationLong
        ride.paymentType = self.paymentType
        ride.estimatedAmount = self.estimatedAmount
        ride.maxTimeToAcceptTrip = self.maxTimeToAcceptTrip
        ride.maxTimeToWaitForClient = self.maxTimeToWaitForClient
        ride.paymentStatus = self.paymentStatus
        ride.requestType = self.requestType
        
        ride.shown = self.shown
        
        ride.stateString = self.stateString
        ride.state = self.state
        
        ride.clientOnRide = self.clientOnRide
        
        return ride
    }
}
