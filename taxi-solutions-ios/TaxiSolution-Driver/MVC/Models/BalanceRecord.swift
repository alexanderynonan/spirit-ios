//
//  BalanceRecord.swift
//  TaxiSolution-Driver
//
//  Created by Fernanda Alvarado Tarrillo on 24/02/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class BalanceRecord: Object {

    @objc dynamic var id = 0
    @objc dynamic var driverId = 0
    @objc dynamic var dateFormatted = ""
    @objc dynamic var originAddress = ""
    @objc dynamic var destinationAddress = ""
    @objc dynamic var paymentType = ""
    @objc dynamic var comission = 0.0
    @objc dynamic var paidAmount = 0.0
    @objc dynamic var amount = 0.0
    @objc dynamic var paidTo = ""

    override class func primaryKey() -> String? {
        return "id"
    }

    convenience init(_ data : JSON ) {
        self.init()
        self.id = data["id"].intValue
        self.driverId = data["driver_id"].intValue
        self.dateFormatted = data["created_formatted"].stringValue
        self.originAddress = data["origin_address"].stringValue
        self.destinationAddress = data["destination_address"].stringValue
        self.paymentType = data["payment_type_formatted"].stringValue
        self.comission = data["commission_amount"].doubleValue
        self.paidAmount = data["paid_amount"].doubleValue
        self.amount = data["amount"].doubleValue
        self.paidTo = data["paid_to"].stringValue
    }

}
