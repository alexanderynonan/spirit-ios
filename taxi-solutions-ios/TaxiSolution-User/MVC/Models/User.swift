//
//  ClientUser.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class User: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var accessToken = ""
    @objc dynamic var socketToken = ""
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var fullName = ""
    @objc dynamic var deviceToken = ""
    @objc dynamic var email = ""
    @objc dynamic var photoUrl = ""
    @objc dynamic var phone = ""
    @objc dynamic var documentType = ""
    @objc dynamic var documentNumber = ""
    @objc dynamic var qualification = 0.0
    @objc dynamic var invitationCode = ""
    @objc dynamic var externalInvitationCode = ""
    @objc dynamic var isActive = false
    @objc dynamic var approved = false
    @objc dynamic var profile = ""
    @objc dynamic var firebaseId = ""
    @objc dynamic var totalBonus = 0.0
    @objc dynamic var createdAt = ""
    
    var createdAtDate: Date {
        return createdAt.date() ?? Date()
    }
    
    var daysRegistered: Int {
        return Calendar.current.dateComponents([.day], from: createdAtDate, to: Date()).day ?? 0
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(_ data: JSON, accessToken: String) {
        self.init()
        self.id = data["id"].intValue
        self.accessToken = accessToken
        self.socketToken = data["socket_auth_token"].stringValue
        self.firstName = data["name"].stringValue
        self.lastName = data["last_name"].stringValue
        self.fullName = data["full_name"].stringValue
        self.deviceToken = data["device_token"].stringValue
        self.email = data["email"].stringValue
        self.photoUrl = data["photo_url"].stringValue
        self.phone = data["phone"].stringValue
        self.documentType = data["document_type"].stringValue
        self.documentNumber = data["document_number"].stringValue
        self.qualification = data["rating"].doubleValue
        self.invitationCode = data["invitation_code"].stringValue
        self.externalInvitationCode = data["external_invitation_code"].stringValue
        self.isActive = data["status"].boolValue
        self.approved = data["approval_status"].boolValue
        self.profile = data["profile"].stringValue
        self.firebaseId = data["firebaseId"].stringValue
        self.totalBonus = data["promotion_amount"].doubleValue
        self.createdAt = data["created_at"].stringValue
    }
}
