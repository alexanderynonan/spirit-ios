//
//  Car.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 7/09/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

struct CarArround {
    var id: Int
    var driverId: Int
    var latitude: Double
    var longitude: Double
    var orientation: Double
    
    init(data: [String: Any]) {
        id = data["id"] as? Int ?? 0
        driverId = data["driver_id"] as? Int ?? 0
        latitude = data.double(key: "latitude") ?? 0.0
        longitude = data.double(key: "longitude") ?? 0.0
        orientation = data.double(key: "orientation") ?? 0.0
    }
    
    func getCoordinate() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func setMarkerInMap(map: GMSMapView) {
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "ic_driver_car_arround")
        marker.position = self.getCoordinate()
        marker.rotation = self.orientation
        marker.map = map
    }
}
