//
//  FavoriteAddress.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 6/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import GoogleMaps

class Address: Object {
    @objc dynamic var id = 0
    @objc dynamic var clientId = 0
    @objc dynamic var name = ""
    @objc dynamic var address = ""
    @objc dynamic var placeId = ""
    @objc dynamic var lat = ""
    @objc dynamic var long = ""
    @objc dynamic var isFavorite = false
    
    var location: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat.double, longitude: long.double)
    }
    
    func setCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let realm = try! Realm()
        try! realm.write {
            self.lat = "\(coordinate.latitude)"
            self.long = "\(coordinate.longitude)"
        }
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: JSON, isFavorite: Bool = false) {
        self.init()
        self.id = data["id"].intValue
        self.clientId = data["client_id"].intValue
        self.name = data["name"].stringValue
        self.address = data["address"].stringValue
        self.lat = data["lat"].stringValue
        self.long = data["long"].stringValue
        self.isFavorite = isFavorite
    }
}
