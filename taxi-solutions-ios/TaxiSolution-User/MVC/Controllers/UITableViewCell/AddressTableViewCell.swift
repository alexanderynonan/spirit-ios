//
//  AddressTableViewCell.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

enum AddressToChooseType {
    case goMap
    case house
    case office
    case normal
}

protocol AddressTableViewCellDelegate {
    func addressTableViewCell(shouldSetFavorite: Bool, inCell cell: AddressTableViewCell) -> Bool
    func addressTableViewCell(didSetFavorite: Bool, inCell cell: AddressTableViewCell)
}

class AddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var actionIconButton: SwitchButton!
    
    var addressToChooseType: AddressToChooseType = .normal
    var delegate: AddressTableViewCellDelegate?
    
    var address: Address? {
        didSet {
            guard let address = address else { return }
            if address.name == FavoriteAddressNames.house { addressToChooseType = .house }
            if address.name == FavoriteAddressNames.work { addressToChooseType = .office }
        }
    }
    var likeable = true

    override func awakeFromNib() {
        super.awakeFromNib()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        actionIconButton.delegate = self
        
        selectionStyle = .none
        DispatchQueue.main.async {
            self.iconImageViewWidth.constant = 50
            self.actionIconButton.isHidden = self.likeable ? false : true
            switch self.addressToChooseType {
            case .goMap:
                self.iconImageView.image = #imageLiteral(resourceName: "ic_pin_address")
            case .house:
                self.iconImageView.image = #imageLiteral(resourceName: "ic_favorite_house_off")
            case .office:
                self.iconImageView.image = #imageLiteral(resourceName: "ic_favorite_office_off")
            case .normal:
                self.iconImageView.image = nil
                self.iconImageViewWidth.constant = 20
            }
        }
    }
    
    func loadContent() {
        guard let address = address else {
            print("GO MAP")
            self.addressLabel.text = "Elige en el mapa"
            return
        }
        
        self.addressLabel.text = address.name != "" ? address.name : address.address
        self.actionIconButton.isOn = address.isFavorite
    }
}

extension AddressTableViewCell: SwitchButtonDelegate {
    func switchButton(switchButton: SwitchButton, statusShouldChange: Bool) -> Bool {
        return delegate?.addressTableViewCell(shouldSetFavorite: switchButton.isOn, inCell: self) ?? true
    }
    
    func switchButton(switchButton: SwitchButton, statusDidChange: Bool) {}
}
