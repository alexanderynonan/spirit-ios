//
//  ServiceTypeTableViewCell.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceTypeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var conceptLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var arrivalTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    var serviceType: ServiceType?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        guard let serviceType = serviceType else { return }
        nameLabel.text = serviceType.name
        descriptionLabel.text = serviceType.information
        photoImageView.sd_setImage(with: serviceType.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_service_type"), options: [.refreshCached, .retryFailed], context: nil)
        priceLabel.text = String(format: "PEN %.2f", serviceType.amount)
        timeRemainingLabel.text = "'\(serviceType.estimatedArrivalTimeInMinutes) min"
        arrivalTimeLabel.text = "Llegada: \(serviceType.estimatedArrivalTime)"
        conceptLabel.text = serviceType.descriptionInfo
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        contentView.backgroundColor = selected ? Colors.primary.withAlphaComponent(0.15) : .clear
    }
}
