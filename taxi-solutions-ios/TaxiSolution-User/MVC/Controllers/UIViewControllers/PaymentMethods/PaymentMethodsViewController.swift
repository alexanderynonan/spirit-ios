//
//  PaymentMethodsViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/30/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
#if targetEnvironment(simulator)
//import VisaNetSDK
#endif

protocol PaymentMethodsViewControllerDelegate {
    func didSelectCreditCard(_ card: TokenizedCard)
}

class PaymentMethodsViewController: UIViewController {
    
    @IBOutlet weak var paymentMethodsTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var continueRideRequestButton: CustomButton!
    @IBOutlet weak var termsConditionsLabel: LinkClickableLabel!
    var savedCreditCards: [TokenizedCard] = [] {
        didSet {
            paymentMethodsTableView.reloadData()
        }
    }
    private var loaded = false
    var isTravel = false
    var delegate: PaymentMethodsViewControllerDelegate?
    private var isRidePayment: Bool {
        return delegate != nil
    }
    private var selectedPaymentCard: TokenizedCard? = nil {
        didSet {
            continueRideRequestButton.alpha = isRidePayment && selectedPaymentCard != nil ? 1 : 0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !loaded {
            paymentMethodsTableView.animate()
            loaded = true
        } else {
            paymentMethodsTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreditCardDetailViewControllerSegue",
            let vc = segue.destination as? CreditCardDetailViewController {
            vc.tokenizedCard = sender as? TokenizedCard
            vc.delegate = self
        }
    }
    
    func configureContent() {
        paymentMethodsTableView.delegate = self
        paymentMethodsTableView.dataSource = self
        
        paymentMethodsTableView.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodTableViewCell")

        _ = termsConditionsLabel.setLinkedTextWithHandler(text: "Al agregar una tarjeta estarás aceptando los (Términos y Condiciones)", link: "Términos y Condiciones") {
                let termsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
                self.present(termsVC, animated: true, completion: nil)
            }
        
        backButton.isHidden = isRidePayment ? false : true
    }
    
    func loadContent() {
        self.startLoader(waitingForObjectsOfType: TokenizedCard.self)
        setData()
        MainManager.sharedManager.getTokenizedCards(successResponse: { (tokenizedCards) in
            self.stopLoader()
            self.savedCreditCards = tokenizedCards
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func setData() {
        let newCards = Array(self.realm.objects(TokenizedCard.self))
        if self.savedCreditCards != newCards {
            self.savedCreditCards = newCards
        }
        
        if isRidePayment {
            selectedPaymentCard = savedCreditCards.first(where: { $0.isDefault })
        }
    }
    /*
    #if targetEnvironment(simulator)
    func configureVisaNet(withToken token: String? = nil) {
        print("💳 VISANET SECURITY TOKEN \(token)\n\n")
        if let token = token {
            Config.securityToken = token
        } else {
            Config.userCredential = NiubizInfo.user
            Config.passwordCredential = NiubizInfo.password
        }
        Config.TK.Header.logoImage = #imageLiteral(resourceName: "img_logo_1")

        //DATOS DE USUARIO
        Config.TK.FirstNameField.defaultText = user.firstName
        Config.TK.LastNameField.defaultText = user.lastName
        Config.TK.email = user.email

        //CUSTOMIZACION
        Config.TK.textFieldFont = Fonts.regular()
        Config.TK.dataChannel = .mobile
        Config.TK.formBackground = Colors.background
        Config.TK.AddCardButton.disableColor = Colors.error
        Config.TK.AddCardButton.enableColor = Colors.primary
        Config.TK.AddCardButton.titleTextColor = .white

        //DATOS DE 'COMPRA'
        Config.amount = 1.0
        let currentDateInMillis = "\(Int(truncatingIfNeeded: Date().currentTimeMillis()))"
        Config.TK.purchaseNumber = String(currentDateInMillis.suffix(12))
        print("💰 PURCHASE NUMBER: \(Config.TK.purchaseNumber ?? "")")

        //ANTIFRAUDE
        var merchantDefineData = [String:Any]()
        merchantDefineData["MDD4"] = "\(Config.TK.email)"
        merchantDefineData["MDD32"] = "\(user.id)"
        merchantDefineData["MDD75"] = "DRIVER"
        merchantDefineData["MDD77"] = "\(user.daysRegistered)"
        Config.TK.Antifraud.merchantDefineData = merchantDefineData

        Config.TK.endPointProdURL = "https://apiprod.vnforapps.com"
        Config.TK.endPointDevURL = "https://apisandbox.vnforappstest.com"
        Config.TK.type = .dev
        Config.merchantID = "456879852"
    }
    #endif
*/
    func openVisaNetTokenizationForm() {
     /*   #if targetEnvironment(simulator)
        configureVisaNet()
        VisaNet.shared.delegate = self
        _ = VisaNet.shared.presentVisaPaymentForm(viewController: self)
        #endif */
    }
    
    @IBAction func continueRideRequest() {
        guard let card = selectedPaymentCard else { return }
        self.startLoader()
        MainManager.sharedManager.setPredeterminedCard(card, successResponse: { (card) in
            self.stopLoader()
            self.delegate?.didSelectCreditCard(card)
            self.goBack()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func back() {
        if isTravel {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }

    }

}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension PaymentMethodsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedCreditCards.count + (isRidePayment ? 1 : 2)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodTableViewCell") as! PaymentMethodTableViewCell
        cell.delegate = self
        if indexPath.row == 0 && !isRidePayment {
            cell.paymentMethod = .cash
            cell.isSelected = savedCreditCards.contains(where: { $0.isDefault }) ? false : true
            cell.card = nil
        } else {
            let index = indexPath.row - (isRidePayment ? 0 : 1)
            if savedCreditCards.indices.contains(index) {
                cell.paymentMethod = .creditCard
                cell.card = savedCreditCards[index]
                cell.isSelected = savedCreditCards[index].isDefault
            } else {
                cell.paymentMethod = .none
                cell.card = nil
            }
        }
        cell.awakeFromNib()
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! PaymentMethodTableViewCell
        switch cell.paymentMethod {
        case .none:
            openVisaNetTokenizationForm()
            return nil
        default:
            if cell.card?.isDefault == true {
                return nil
            }
            return indexPath
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PaymentMethodTableViewCell
        if isRidePayment {
            selectedPaymentCard = cell.card
            cell.isSelected = true
        } else {
            //Tarjeta de la celda seleccionada
            var card = cell.card
            //Por defecto lo hacemos predeterminado
            var predetermined = true
            
            //Si la tarjeta de la celda es nula, la celda es de tipo efectivo, por lo cual buscaremos la tarjeta predeterminada y le ponemos false
            if card == nil {
                card = savedCreditCards.first(where: { $0.isDefault })
                predetermined = false
            }
            
            //Validamos que la tarjeta obtenida por la lógica anterior no sea nula
            guard let cardToUpdate = card else { return }
            
            self.startLoader()
            MainManager.sharedManager.setPredeterminedCard(cardToUpdate, predetermined: predetermined, successResponse: { (card) in
                self.stopLoader()
                self.paymentMethodsTableView.reloadData()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
}

//MARK: - PaymentMethodTableViewCellDelegate
extension PaymentMethodsViewController: PaymentMethodTableViewCellDelegate {
    func paymentMethodTableViewCellDelegate(showDetailOfCell cell: PaymentMethodTableViewCell) {
        if cell.paymentMethod == .creditCard {
            performSegue(withIdentifier: "CreditCardDetailViewControllerSegue", sender: cell.card)
        }
        if cell.paymentMethod == .none {
            openVisaNetTokenizationForm()
        }
    }
}

/*
//MARK: - VisaNetDelegate
#if targetEnvironment(simulator)
extension PaymentMethodsViewController: VisaNetDelegate {
    func registrationDidEnd(serverError: Any?, responseData: Any?) {
        if let responseData = responseData as? [String: Any], let token = responseData["token"] as? [String: Any] {
            let card = TokenizedCard(data: responseData)
            self.startLoader()
            MainManager.sharedManager.registerTokenizedCard(card, successResponse: { (card) in
                self.stopLoader()
                self.loadContent()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else if let error = responseData as? NSError {
            self.view.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.view.isUserInteractionEnabled = true
                self.showAlert(withTitle: "VisaNet\n\(error.code)", message: error.localizedDescription)
            }
        } else if let error = serverError as? NSError {
            self.view.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.view.isUserInteractionEnabled = true
                self.showAlert(withTitle: "VisaNet\n\(error.code)", message: error.localizedDescription)
            }
        }
        print("DATA: \(responseData)")
        print("ERROR: \(serverError)")
    }
}
#endif
*/
//MARK: - CreditCardDetailViewControllerDelegate
extension PaymentMethodsViewController: CreditCardDetailViewControllerDelegate {
    func creditCardDetailViewController(didRemoveCardInViewController vc: CreditCardDetailViewController) {
        self.loadContent()
    }
}
