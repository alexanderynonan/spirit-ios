//
//  CreditCardDetailViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/4/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol CreditCardDetailViewControllerDelegate {
    func creditCardDetailViewController(didRemoveCardInViewController vc: CreditCardDetailViewController)
}

class CreditCardDetailViewController: UIViewController {
    
    @IBOutlet weak var cardholderLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var expiresAtLabel: UILabel!
    
    var delegate: CreditCardDetailViewControllerDelegate?
    var tokenizedCard: TokenizedCard?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        
    }
    
    func loadContent() {
        guard let card = tokenizedCard else { return }
        cardholderLabel.text = "\(card.cardHolder)"
        cardNumberLabel.text = card.maskedCard.replacingOccurrences(of: " ", with: "     ")
        expiresAtLabel.text = "\(card.formattedExpirationDate.replacingOccurrences(of: "-", with: "/"))"
    }
    
    @IBAction func removeCreditCard() {
        ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.removeCreditCard, message: "¿Está seguro que desea eliminar esta tarjeta del método de pago?", andConfirmText: "CONFIRMAR") { _, confirmed in
            if confirmed {
                guard let card = self.tokenizedCard else {
                    self.showAlert(message: "No se pudo eliminar la tarjeta. Inténtelo más tarde")
                    return
                }
                self.startLoader()
                MainManager.sharedManager.removeTokenizedCard(cardId: card.id, successResponse: { (message) in
                    self.stopLoader()
                    self.showAlert(message: message, okayHandler: { (_) in
                        self.delegate?.creditCardDetailViewController(didRemoveCardInViewController: self)
                        self.goBack()
                    })
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            }
        }
    }

}
