//
//  MyProfileViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/30/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

class MyProfileViewController: UIViewController {
    
    @IBOutlet weak var documentTypeTextField: CustomTextField!
    @IBOutlet weak var documentNumberTextField: CustomTextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var dniLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: CustomTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UINavigationController.setCustomAppearance(false)
    }
    
    func configureContent() {
        Tags.show(viewController: self, withTextColor: Colors.text.withAlphaComponent(0.35), font: Fonts.semibold(13.0), position: .bottomRight)
        Tags.show(self.user.id.string, viewController: self, withTextColor: Colors.text.withAlphaComponent(0.35), font: Fonts.semibold(13.0), position: .topRight)
        NotificationCenter.default.addObserver(self, selector: #selector(goToProfileView), name: .goToProfileView, object: nil)
    }
    
    func loadContent() {
        imageView.sd_setImage(with: user.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached, .allowInvalidSSLCertificates, .retryFailed, .progressiveLoad, .continueInBackground], completed: nil)
        nameLabel.text = user.fullName
        nameTextField.text = user.fullName
        documentTypeTextField.text = user.documentType.uppercased()
        documentNumberTextField.text = user.documentNumber
        phoneTextField.text = user.phone
        emailTextField.text = user.email

    }
    
    @objc func goToProfileView() {
        self.loadContent()
        guard let tabBar = self.tabBarController else { return }
        tabBar.navigationController?.popToViewController(tabBar, animated: true)
    }
    
    @IBAction func selectPhoto(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Foto de perfil", message: "Seleccione desde dónde quiere subir la foto", preferredStyle: .actionSheet)
        let imagePickerController = UIImagePickerController()
        imagePickerController.view.tintColor = Colors.primary
        imagePickerController.delegate = self
        let camera = UIAlertAction(title: "Cámara", style: .default) { (_) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let gallery = UIAlertAction(title: "Galería", style: .default) { (_) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(camera)
        alertController.addAction(gallery)
        alertController.addAction(cancel)
        alertController.view.tintColor = Colors.primary
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func askLogout() {
        ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.logout, message: "¿Está seguro que desea salir de la aplicación? Los datos se guardarán", andConfirmText: "Confirmar") {
            _, confirmed in
            _ = confirmed ? self.logout() : .none
        }
    }
    
    
    @IBAction func updateInfo(_ sender: UIButton) {
        let personalData = PersonalData.allCases.first(where: { $0.rawValue == sender.tag })
        if let dataToUpdate = personalData, let navigationVC = self.tabBarController?.navigationController {
            switch dataToUpdate {
            case .phone, .email:
                guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "PhoneNumberViewController") as? PhoneNumberViewController else { return }
                vc.dataToUpdate = dataToUpdate
                vc.updateData = true
                navigationVC.pushViewController(vc, animated: true)
            case .password:
                guard let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "UpdatePasswordViewController") as? UpdatePasswordViewController else { return }
                vc.updatePassword = true
                navigationVC.pushViewController(vc, animated: true)
            default: ()
            }
        }
    }

}

extension MyProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image: UIImage?
        if let photo = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print("🌆 Image file size: \(photo.fileSizeInMB) MBs")
            if photo.fileSizeInMB <= App.maxFileSize {
                image = photo
            } else {
                picker.showAlert(message: "La imágen debe pesar menos de \(App.maxFileSize) Mb.\nPeso actual de imagen: \(photo.fileSizeInMB) Mb")
                return
            }
        } else {
            self.showAlert(message: "Ocurrió un error al obtener la foto. Por favor, vuelva a intentarlo")
        }
        
        picker.dismiss(animated: true) {
            guard let image = image else { return }
            let customAlert = CustomAlertController(title: "Foto seleccionada", message: "¿Seguro desea actualizar su foto de perfil?", preferredStyle: .alert)
            customAlert.setTitleImage(image.sd_resizedImage(with: CGSize(width: 100, height: 100), scaleMode: .aspectFill), cornerRadius: 25.0)
            let okayAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.startLoader()
                UserManager.sharedManager.updatePhoto(image, user: self.user, successResponse: { (_) in
                    self.stopLoader()
                    self.loadContent()
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            customAlert.addActions(actions: [okayAction, cancelAction])
            self.present(customAlert, animated: true, completion: nil)
        }
    }
}
