//
//  RegisterFirstStepViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

enum PhotoType {
    case profile
    case document
}

class RegisterFirstStepViewController: RegisterViewController {

    @IBOutlet weak var documentNumberTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet var invitationCodeTextFields: [UITextField]!
    @IBOutlet weak var phoneTextField: CustomTextField!
    @IBOutlet weak var termsConditionsLabel: LinkClickableLabel!
    
    var documentTypes: [DocumentType] = [] {
        didSet {

        }
    }
    var selectedDocumentType: DocumentType = DocumentType() {
        didSet {
            newUser.documentType = selectedDocumentType.key
            documentNumberTextField.keyboardType = selectedDocumentType.isNumeric ? .numberPad : .alphabet
            documentNumberTextField.text = ""
        }
    }
    var photoType: PhotoType = .profile
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UINavigationController.setCustomAppearance(false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RegisterSecondStepViewControllerSegue" {
            let vc = segue.destination as! RegisterSecondStepViewController
            vc.newUser = newUser
            vc.aloSvicoUser = sender as? User
        }
    }
    
    func configureContent() {
        documentNumberTextField.delegate = self
        invitationCodeTextFields.forEach { $0.delegate = self }
        UINavigationController.setCustomAppearance(true)
        _ = termsConditionsLabel.setLinkedTextWithHandler(text: "Al crear la cuenta usted reconoce que ha leído y acepta los (términos y condiciones).", link: "términos y condiciones") {
                let termsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
                self.present(termsVC, animated: true, completion: nil)
            }
        
    }
    
    func loadContent() {
        getDocumentTypes()
    }
    
    func getDocumentTypes() {
        startLoader()
        MainManager.sharedManager.getDocumentTypes(successResponse: { (documentTypes) in
            self.stopLoader()
            self.documentTypes = documentTypes
            self.selectedDocumentType = documentTypes[1]
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    @IBAction func selectDocumentType() {
    
    }
    
    @IBAction func continueRegister(_ sender: UIButton) {
        guard let documentNumber = documentNumberTextField.text, !documentNumber.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.documentNumberEmpty)
            return
        }
        
        if self.newUser.documentType.lowercased() == Default.documentTypeToCheck {
            guard documentNumber.count == selectedDocumentType.maxValue else {
                showAlert(message: ValidationMessages.documentNumberLength)
                return
            }
        } else {
            guard documentNumber.count <= selectedDocumentType.maxValue else {
                showAlert(message: ValidationMessages.documentNumberLength)
                return
            }
        }
        
        guard let firstName = firstNameTextField.text?.trimmingCharacters(in: .whitespaces), let lastName = lastNameTextField.text?.trimmingCharacters(in: .whitespaces), !firstName.isEmptyOrWhitespace() && !lastName.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.namesEmpty)
            return
        }

        
        func saveData(firstname: String? = nil, lastname: String? = nil) {
            self.newUser.documentNumber = documentNumber
            self.newUser.firstName = firstname ?? firstName
            self.newUser.lastName = lastname ?? lastName
            self.newUser.phoneNumber = phoneTextField.text ?? ""
        }
        
        self.startLoader()
        UserManager.sharedManager.validateDocumentNumber(documentNumber: documentNumberTextField.text ?? "", documentType: selectedDocumentType.key, successResponse: { (response, aloSvicoUser) in
            
            if self.newUser.documentType.lowercased() == Default.documentTypeToCheck {
                
                UserManager.sharedManager.verifyDocumentNumber(documentNumber: self.documentNumberTextField.text ?? "", successResponse: { (names) in
                    self.stopLoader()
                    if names.count == 2 {
                        let validFirstName = names[0]
                        let validLastName = names[1]
                        if validFirstName.uppercased().containsWord(firstName.uppercased()) && validLastName.uppercased().containsFirstWordOrAll(lastName.uppercased()) {
                            saveData(firstname: validFirstName.capitalizedName, lastname: validLastName.capitalizedName)
                            self.finishRegister()
                            //go to main
                            //self.performSegue(withIdentifier: "RegisterSecondStepViewControllerSegue", sender: aloSvicoUser)
                        } else {
                            self.showAlert(message: ValidationMessages.reniecNames)
                        }
                    } else {
                        self.showAlert(message: DefaultApiMessages.formatResponse)
                    }
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription, okayHandler: { (_) in
                        self.documentNumberTextField.becomeFirstResponder()
                    })
                }
                
            } else {
                self.stopLoader()
                saveData()
                self.finishRegister()
                //go to main
               // self.performSegue(withIdentifier: "RegisterSecondStepViewControllerSegue", sender: aloSvicoUser)
            }
            if let user = aloSvicoUser {
                self.emailTextField.text = user.email
                if self.invitationCodeTextFields.count == user.invitationCode.count {
                    for (index, codeTextField) in self.invitationCodeTextFields.enumerated() {
                        codeTextField.text = "\(Array(user.invitationCode)[index])"
                    }
                }
            }
            
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    private func finishRegister() {
        guard let email = emailTextField.text, !email.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.email)
            return
        }

        guard email.isValidEmail() else {
            showAlert(message: ValidationMessages.emailFormat)
            return
        }

        guard let password = passwordTextField.text, !password.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.password)
            return
        }

        guard password.count > 5 else {
            showAlert(message: ValidationMessages.passwordLength)
            return
        }

        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.confirmPassword)
            return
        }

        guard password == confirmPassword else {
            showAlert(message: ValidationMessages.passwordsDifferent)
            return
        }

//        guard termsAgreementSwitch.isOn else {
//            showAlert(message: ValidationMessages.agreeTerms)
//            return
//        }

        var invitationCode = ""
        invitationCodeTextFields.forEach { textField in
            invitationCode += textField.text ?? ""
        }
        print("INVITATION CODE: \(invitationCode)")

        guard [0, 6].contains(invitationCode.count) else {
            showAlert(message: ValidationMessages.invitationCode)
            return
        }

        newUser.email = email
        newUser.password = password
        newUser.invitationCode = invitationCode

        startLoader(withMessage: "Enviando datos")
        UserManager.sharedManager.register(user: newUser, successResponse: { (user) in
            print("🙋‍♂️ \(user)")
            self.stopLoader()
            _ = self.setRootViewController(fromStoryboard: "Main")
            SocketIOManager.sharedInstance.establishConnection()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction override func goBack() {
        guard let phoneNumberVC = self.navigationController?.viewControllers[1] else {
            self.navigationController?.popToRootViewController(animated: true)
            return
        }
        self.navigationController?.popToViewController(phoneNumberVC, animated: true)
    }

    @IBAction func showTerms(_ sender: UIButton) {
        let termsVC = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.present(termsVC, animated: true, completion: nil)
    }
}

extension RegisterFirstStepViewController: UINavigationControllerDelegate {

}

extension RegisterFirstStepViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / CGFloat(documentTypes.count)), height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documentTypes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentTypeCollectionViewCell", for: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentType = documentTypes[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentTypeSwitchButton.isOn = true
        selectedDocumentType = documentTypes[indexPath.row]
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! DocumentTypeCollectionViewCell
        cell.documentTypeSwitchButton.isOn = false
    }
}

extension RegisterFirstStepViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == documentNumberTextField {
            if let text = textField.text, (text.count < selectedDocumentType.maxValue || string == "") {
                return true
            } else {
                return false
            }
        } else {
            if string == "" {
                textField.text = ""
                invitationCodeTextFields.forEach {
                    if $0.tag == textField.tag - 1 {
                        $0.becomeFirstResponder()
                    }
                }
                return false
            }

            if string.count == 1 {
                if textField.text?.count == 1 {
                    if invitationCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                        let nextTextField = invitationCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                        nextTextField?.text = string
                        nextTextField?.becomeFirstResponder()
                    } else {
                        textField.resignFirstResponder()
                    }
                } else {
                    textField.text = string
                }
            } else {
                if let character = string.first {
                    textField.text = String(character)
                }
            }

            return false
        }
    }
}
