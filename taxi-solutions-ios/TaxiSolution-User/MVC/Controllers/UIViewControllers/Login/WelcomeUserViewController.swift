//
//  WelcomeUserViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 10/08/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class WelcomeUserViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = "\(user.firstName), ya cuentas con un bono para realizar viajes. Espere el proceso de verificación de datos para finalizar el registro"
        backgroundImageView.image = UIImage.gifImageWithName("gif_svico_background")
    }
    
    @IBAction override func dismiss() {
        super.dismiss()
        ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.logout, message: "¿Está seguro que desea salir de la apicación? Los datos se guardarán", andConfirmText: "Confirmar") { _, confirmed in
            _ = confirmed ? self.logout() : .none
        }
    }
}
