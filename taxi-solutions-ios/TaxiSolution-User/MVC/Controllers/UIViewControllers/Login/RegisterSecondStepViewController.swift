//
//  RegisterSecondStepViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/28/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class RegisterSecondStepViewController: RegisterViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet var invitationCodeTextFields: [UITextField]!
    @IBOutlet weak var termsAgreementSwitch: SwitchButton!
    
    var aloSvicoUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        invitationCodeTextFields.forEach { $0.delegate = self }
    }
    
    func loadContent() {
        if let user = aloSvicoUser {
            emailTextField.text = user.email
            if invitationCodeTextFields.count == user.invitationCode.count {
                for (index, codeTextField) in invitationCodeTextFields.enumerated() {
                    codeTextField.text = "\(Array(user.invitationCode)[index])"
                }
            }
        }
    }
    
    @IBAction func showTerms(_ sender: UIButton) {
        let termsVC = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.present(termsVC, animated: true, completion: nil)
    }
    
    @IBAction func register(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.email)
            return
        }
        
        guard email.isValidEmail() else {
            showAlert(message: ValidationMessages.emailFormat)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.password)
            return
        }
        
        guard password.count > 5 else {
            showAlert(message: ValidationMessages.passwordLength)
            return
        }
        
        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmptyOrWhitespace() else {
            showAlert(message: ValidationMessages.confirmPassword)
            return
        }
        
        guard password == confirmPassword else {
            showAlert(message: ValidationMessages.passwordsDifferent)
            return
        }
        
        guard termsAgreementSwitch.isOn else {
            showAlert(message: ValidationMessages.agreeTerms)
            return
        }
        
        var invitationCode = ""
        invitationCodeTextFields.forEach { textField in
            invitationCode += textField.text ?? ""
        }
        print("INVITATION CODE: \(invitationCode)")
        
        guard [0, 6].contains(invitationCode.count) else {
            showAlert(message: ValidationMessages.invitationCode)
            return
        }
        
        newUser.email = email
        newUser.password = password
        newUser.invitationCode = invitationCode
        
        startLoader(withMessage: "Enviando datos")
        UserManager.sharedManager.register(user: newUser, successResponse: { (user) in
            print("🙋‍♂️ \(user)")
            self.stopLoader()
            _ = self.setRootViewController(fromStoryboard: "Main")
            SocketIOManager.sharedInstance.establishConnection()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction override func goBack() {
        UIView.transition(with: UIApplication.shared.keyWindow ?? UIView(), duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.navigationController?.popViewController(animated: false)
        }, completion: nil)
    }
    
}

extension RegisterSecondStepViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            textField.text = ""
            invitationCodeTextFields.forEach {
                if $0.tag == textField.tag - 1 {
                    $0.becomeFirstResponder()
                }
            }
            return false
        }
        
        if string.count == 1 {
            if textField.text?.count == 1 {
                if invitationCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                    let nextTextField = invitationCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                    nextTextField?.text = string
                    nextTextField?.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.text = string
            }
        } else {
            if let character = string.first {
                textField.text = String(character)
            }
        }
        
        return false
    }
}
