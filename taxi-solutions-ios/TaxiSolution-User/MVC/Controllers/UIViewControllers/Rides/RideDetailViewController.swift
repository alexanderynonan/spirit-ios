//
//  TripsDetailViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/30/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import Cosmos

class RideDetailViewController: UIViewController {
    
//    @IBOutlet weak var dateLabel: UILabel!
//    @IBOutlet weak var statusLabel: UILabel!
//    @IBOutlet weak var totalAmountLabel: UILabel!
//    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var sharedRideLabel: UILabel!
    @IBOutlet weak var sharedRideLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var ridePointsTableView: UITableView!
    @IBOutlet weak var ridePointsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var carDetailsLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var paymentDetailContainer: UIView!
    @IBOutlet weak var paymentDetailContainerHeight: NSLayoutConstraint!
    
    var rideId = 0
    var rideRecord: RideRecord? {
        didSet {
            guard let rideRecord = rideRecord else { return }
            ridePoints.removeAll()
//            dateLabel.text = rideRecord.rideAtString
//            statusLabel.text = rideRecord.status.withDefault("-")
//            totalAmountLabel.text = String(format: "PEN %.2f", rideRecord.totalAmount.double)
//            paymentTypeLabel.text = rideRecord.paymentDetail?.paymentType.withDefault("No especificado")
            mapImageView.sd_setImage(with: rideRecord.photoUrl.url, placeholderImage: UIImage(named: "img_map"), completed: nil)
            sharedRideLabel.text = rideRecord.shared ? "El viaje fue compartido" : ""
            sharedRideLabelHeight.constant = rideRecord.shared ? 20 : 0
            ridePoints.append(rideRecord.originAddress)
            ridePoints.append(rideRecord.destinationAddress)
            userPhotoImageView.sd_setImage(with: rideRecord.userPhotoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached], completed: nil)
            userFullNameLabel.text = rideRecord.userFullName
            carDetailsLabel.text = rideRecord.carDetails
            ratingView.rating = rideRecord.userRating.double
        }
    }

    var ridePoints: [String] = [] {
        didSet {
            ridePointsTableViewHeight.constant = 25 * CGFloat(ridePoints.count)
            ridePointsTableView.animate()
        }
    }
    lazy var paymentDetailView: PaymentDetailView = {
        guard let paymentDetailView = UINib(nibName: "PaymentDetailView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? PaymentDetailView else { return PaymentDetailView() }
        self.paymentDetailView = paymentDetailView
        paymentDetailView.frame = paymentDetailContainer.bounds
        paymentDetailContainer.addSubview(paymentDetailView)
        return paymentDetailView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        Tags.show("Nº\(rideId)", viewController: self, withTextColor: Colors.text.withAlphaComponent(0.25), font: Fonts.semibold(10.0), position: .bottomRight)
        ridePointsTableView.delegate = self
        ridePointsTableView.dataSource = self
        ridePointsTableView.register(UINib(nibName: "TripPointAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "TripPointTableViewCell")
    }
    
    func loadContent() {
        if let rideRecord = self.realm.objects(RideRecord.self).filter("paymentDetail.id == \(self.rideId)").first {
            setData(rideRecord: rideRecord)
        } else {
            self.startLoader()
            MainManager.sharedManager.getRideRecordDetail(rideId: rideId, successResponse: { (rideRecord)  in
                self.stopLoader()
                self.setData(rideRecord: rideRecord)
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    func setData(rideRecord: RideRecord) {
        self.paymentDetailView.paymentDetail = rideRecord.paymentDetail
        self.paymentDetailView.showPaymentMethod = false
        self.paymentDetailContainerHeight.constant = CGFloat(self.paymentDetailView.visibleLabels * 45)
        self.rideRecord = rideRecord
    }

    @IBAction func reportTrip(_ sender: UIButton) {
        ReportRideViewController.instantiante(inViewController: self, withTitle: "Reportar viaje", rideId: rideId, placeholder: "Presenta tu reclamo...", andConfirmText: "ENVIAR")
    }
    
}

extension RideDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / CGFloat(ridePoints.count)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ridePoints.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripPointTableViewCell") as! TripPointTableViewCell
        cell.pointType = indexPath.row == ridePoints.count - 1 ? .final : .initial
        cell.pointAddress = ridePoints[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
    
}
