//
//  EnterCodeViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/15/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import PDFKit

protocol PromoCodeApplicationDelegate {
    func didApplyPromoCode(_ code: String, in viewController: UIViewController)
}

class EnterCodeCouponViewController: DynamicBottomViewController {
    
    @IBOutlet var promoCodeTextFields: [UITextField]!
    var bonusVC: BonusViewController?
    var delegate: PromoCodeApplicationDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        promoCodeTextFields.first?.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        promoCodeTextFields.forEach { $0.text = "" }
    }
    
    func configureContent() {
        promoCodeTextFields.forEach { $0.delegate = self }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func loadContent() {
        
    }
    
    @objc func handleTapGesture(_ tapGesture: UITapGestureRecognizer) {
        guard let bonusVC = bonusVC else { return }
//        if bonusVC.modalCloseButton.convert(bonusVC.modalCloseButton.bounds, to: bonusVC.view).contains(tapGesture.location(in: bonusVC.view)) {
//            self.dismiss()
//        }
    }
    
    override func dismiss() {
        guard let bonusVC = bonusVC else { return }
        dismissKeyboard()
       // bonusVC.showModalHeaderView(false)
        super.dismiss()
    }
    
    @IBAction func applyPromoCode() {
        var promoCode = ""
        promoCodeTextFields.forEach { textField in
            promoCode += textField.text ?? ""
        }
        
        if promoCode.isEmptyOrWhitespace() || promoCode.count != 5 {
            self.showAlert(message: "El código debe ser de 5 caracteres y sin espacios")
            return
        }
        
        self.startLoader()
        ClientManager.sharedManager.applyPromoCode(code: promoCode, successResponse: { (message, promotionAmount) in
            self.stopLoader()
            
            try! self.realm.write {
                self.user.totalBonus == 0 ? self.user.totalBonus = promotionAmount : ()
            }
            
            let customAlert = CustomAlertController(title: "", message: "Su código fue registrado con éxito. El descuento se aplicará en su próximo viaje", preferredStyle: .alert)
            customAlert.setTitleImage(#imageLiteral(resourceName: "ic_check").sd_resizedImage(with: CGSize(width: 20, height: 20), scaleMode: .aspectFill))
            let okayAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.dismiss()
                self.delegate?.didApplyPromoCode(promoCode, in: self)
            }
            customAlert.addAction(okayAction)
            self.present(customAlert, animated: true, completion: nil)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
}

extension EnterCodeCouponViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            textField.text = ""
            promoCodeTextFields.forEach {
                if $0.tag == textField.tag - 1 {
                    $0.becomeFirstResponder()
                }
            }
            return false
        }
        
        if string.count == 1 {
            if textField.text?.count == 1 {
                if promoCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                    let nextTextField = promoCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                    nextTextField?.text = string.uppercased()
                    nextTextField?.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.text = string.uppercased()
            }
        } else {
            if let character = string.first {
                textField.text = String(character).uppercased()
            }
        }
        
        return false
    }
}
