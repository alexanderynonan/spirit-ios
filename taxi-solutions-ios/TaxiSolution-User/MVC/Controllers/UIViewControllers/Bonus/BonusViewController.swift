//
//  BonusViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class BonusViewController: UIViewController {

    @IBOutlet var promoCodeTextFields: [UITextField]!
    var userInfo: [AnyHashable: Any]? = nil

    class func instantiate(in viewController: UIViewController, userInfo: [AnyHashable: Any]? = nil) {
        guard let bonusVC = UIStoryboard(name: "Bonus", bundle: nil).instantiateViewController(withIdentifier: "BonusViewController") as? BonusViewController else { return }
        viewController.navigationController?.pushViewController(bonusVC, animated: true)
        bonusVC.userInfo = userInfo
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        if let userInfo = userInfo {
            if let title = userInfo["title"] as? String, let body = userInfo["body"] as? String {
                _ = PopUpViewController.instantiate(viewController: self, fullscreen: true, image: #imageLiteral(resourceName: "img_new_referred"), animated: true, title: title, message: body, needCloseButton: true, closeWithTap: true, automaticDismiss: false)
            }
        }
        
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadContent()
    }

    func loadContent() {
        promoCodeTextFields.first?.becomeFirstResponder()
    }

    func configureContent() {
        promoCodeTextFields.forEach { $0.delegate = self }
    }
    @IBAction func applyPromoCode() {
        var promoCode = ""
        promoCodeTextFields.forEach { textField in
            promoCode += textField.text ?? ""
        }

        if promoCode.isEmptyOrWhitespace() || promoCode.count != 5 {
            self.showAlert(message: "El código debe ser de 5 caracteres y sin espacios")
            return
        }

        self.startLoader()
        ClientManager.sharedManager.applyPromoCode(code: promoCode, successResponse: { (message, promotionAmount) in
            self.stopLoader()

            try! self.realm.write {
                self.user.totalBonus == 0 ? self.user.totalBonus = promotionAmount : ()
            }

            let customAlert = CustomAlertController(title: "", message: "Su código fue registrado con éxito. El descuento se aplicará en su próximo viaje", preferredStyle: .alert)
            customAlert.setTitleImage(#imageLiteral(resourceName: "ic_accept").sd_resizedImage(with: CGSize(width: 20, height: 20), scaleMode: .aspectFill))
            let okayAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.navigationController?.popViewController(animated: true)

            }
            customAlert.addAction(okayAction)
            self.present(customAlert, animated: true, completion: nil)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
}

extension BonusViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            textField.text = ""
            promoCodeTextFields.forEach {
                if $0.tag == textField.tag - 1 {
                    $0.becomeFirstResponder()
                }
            }
            return false
        }

        if string.count == 1 {
            if textField.text?.count == 1 {
                if promoCodeTextFields.contains(where: { $0.tag == textField.tag + 1 }) {
                    let nextTextField = promoCodeTextFields.first(where: { $0.tag == textField.tag + 1 })
                    nextTextField?.text = string.uppercased()
                    nextTextField?.becomeFirstResponder()
                } else {
                    textField.resignFirstResponder()
                }
            } else {
                textField.text = string.uppercased()
            }
        } else {
            if let character = string.first {
                textField.text = String(character).uppercased()
            }
        }

        return false
    }
}
