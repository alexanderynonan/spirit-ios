//
//  ShareAndWinViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ShareAndWinViewController: UIViewController {

    @IBOutlet weak var inviteCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
    }
    
    func loadContent() {
        inviteCodeLabel.text = user.invitationCode
    }

    @IBAction func copyInviteCode() {
        UIPasteboard.general.string = user.invitationCode
        parent?.showToast(message: "Su código ha sido copiado")
    }
    
    @IBAction func shareCode() {
        let items = [user.invitationCode]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        parent?.present(activityController, animated: true)
    }

}
