//
//  EnterPromoCodeViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/13/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip

enum RedeemMode {
    case code
    case qr
}

protocol CouponsViewControllerDelegate {
    func couponsViewController(redeemBy: RedeemMode)
}
class CouponsViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var couponsTableView: UITableView!
    @IBOutlet weak var couponsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    
    var promotions: [Promotion] = [] {
        didSet {
            guard let tableView = couponsTableView else { return }
            tableView.reloadData()
        }
    }
    
    var delegate: CouponsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        couponsTableView.delegate = self
        couponsTableView.dataSource = self
        let referralTableViewCellNib = UINib(nibName: "ReferralTableViewCell", bundle: nil)
        couponsTableView.register(referralTableViewCellNib, forCellReuseIdentifier: "ReferralTableViewCell")
    }
    
    func loadContent() {
        couponsTableView.reloadData()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        let info = IndicatorInfo(title: "Ingresar código")
        return info
    }
    
    @IBAction func redeemCoupon(_ sender: UIButton) {
        if sender.tag == 1 {
            delegate?.couponsViewController(redeemBy: .code)
        }
        if sender.tag == 2 {
            delegate?.couponsViewController(redeemBy: .qr)
        }
    }
}

extension CouponsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        couponsTableViewHeight.constant = CGFloat(promotions.count) * 85.0
        return 85.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.numberOfRows(for: promotions, emptyMessage: "Aún no ha registrado códigos promocionales")
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralTableViewCell") as! ReferralTableViewCell
        cell.promotion = promotions[indexPath.row]
        cell.awakeFromNib()
        return cell
    }
}
