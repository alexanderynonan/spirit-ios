//
//  ScanQRCouponViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/15/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import AVKit

class ScanQRCouponViewController: UIViewController {

    @IBOutlet weak var scanView: CustomView!
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var bonusVC: BonusViewController?
    var code = ""
    
    var delegate: PromoCodeApplicationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  configureContent()
     //   loadContent()
    }
    
//    func configureContent() {
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
//        self.view.addGestureRecognizer(tapGesture)
//        startReadingQR()
//    }
//
//    func loadContent() {
//
//    }
//
//    func startReadingQR() {
//        let captureDevice = AVCaptureDevice.default(for: .video)
//        do {
//            guard let captureDevice = captureDevice else {
//                return
//            }
//            let input = try AVCaptureDeviceInput(device: captureDevice)
//            captureSession = AVCaptureSession()
//            captureSession?.addInput(input)
//        } catch let error as NSError {
//            print(error)
//        }
//
//        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
//        videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
//        videoPreviewLayer.frame = scanView.layer.bounds
//        scanView.layer.addSublayer(videoPreviewLayer)
//
//        /* Check for metadata */
//        let captureMetadataOutput = AVCaptureMetadataOutput()
//        captureMetadataOutput.rectOfInterest = .infinite
//        captureSession?.addOutput(captureMetadataOutput)
//        captureMetadataOutput.metadataObjectTypes = captureMetadataOutput.availableMetadataObjectTypes
//        print(captureMetadataOutput.availableMetadataObjectTypes)
//        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//        captureSession?.startRunning()
//    }
//
//    func stopReadingQR() {
//        captureSession?.stopRunning()
//        captureSession = nil
//        videoPreviewLayer.removeFromSuperlayer()
//    }
//
//    func applyPromoCode(_ shouldApply: Bool) {
//        if shouldApply {
//            startLoader()
//            ClientManager.sharedManager.applyPromoCode(code: code, successResponse: { (message, promotionAmount) in
//                self.stopLoader()
//
//                try! self.realm.write {
//                    self.user.totalBonus == 0 ? self.user.totalBonus = promotionAmount : ()
//                }
//
//                self.showAlert(message: "Su código fue procesado correctamente", okayHandler: { (_) in
//                    self.dismiss()
//                    self.bonusVC?.showModalHeaderView(false)
//                    self.delegate?.didApplyPromoCode(self.code, in: self)
//                })
//            }) { (error) in
//                self.stopLoader()
//                self.showAlert(message: error.localizedDescription, okayHandler: { (_) in
//                    self.captureSession?.startRunning()
//                })
//            }
//        } else {
//            captureSession?.startRunning()
//        }
//    }
//
//    @objc func handleTapGesture(_ tapGesture: UITapGestureRecognizer) {
//        guard let bonusVC = bonusVC else { return }
//        if bonusVC.modalCloseButton.convert(bonusVC.modalCloseButton.bounds, to: bonusVC.view).contains(tapGesture.location(in: bonusVC.view)) {
//            self.dismiss()
//            bonusVC.showModalHeaderView(false)
//        }
//    }
//}
//
//extension ScanQRCouponViewController: AVCaptureMetadataOutputObjectsDelegate {
//    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//        metadataObjects.forEach { data in
//            let metaData = data
//            let transformed = videoPreviewLayer?.transformedMetadataObject(for: metaData) as? AVMetadataMachineReadableCodeObject
//            if let unwraped = transformed {
//                guard let code = unwraped.stringValue else { return }
//                self.captureSession?.stopRunning()
//                self.code = code
//                ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.qr, message: "¿Deseas canjear este código: \(code)?", andConfirmText: "Confirmar") { _, confirmed in
//                    self.applyPromoCode(confirmed)
//                }
//            }
//        }
//    }
}
