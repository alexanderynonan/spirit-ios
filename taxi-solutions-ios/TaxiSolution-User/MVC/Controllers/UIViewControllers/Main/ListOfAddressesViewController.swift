//
//  ListOfAddressesViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SwiftyJSON
import CoreLocation
import GooglePlaces

enum ListOfAddressesType {
    case favorites
    case recents
}

protocol ListOfAddressesViewControllerDelegate {
    func listOfAddressesViewController(vc: ListOfAddressesViewController, didSelectAddress address: Address?)
}

class ListOfAddressesViewController: UIViewController {
    
    @IBOutlet weak var addressesTableView: UITableView!
    var delegate: ListOfAddressesViewControllerDelegate?
    var parentVC: UIViewController?
    var mapVC: MapViewController? {
        return (self.parent as? SearchAddressViewController)?.parentVC as? MapViewController
    }
    
    var typeOfAddresses: ListOfAddressesType = .favorites
    var addressResults: [Address] = [] {
        didSet {
            guard reloadData() else { return }
            addressesTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.isKeyboardPresented ? 200 : 0, right: 0)
            if addressResults.count != 0 {
                addressesTableView.scrollToRow(at: IndexPath(row: savedAddresses.count + 1, section: 0), at: .top, animated: true)
            }
        }
    }
    var savedAddresses: [Address] = [] {
        didSet {
            _ = reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        addressesTableView.delegate = self
        addressesTableView.dataSource = self
        let addressTableViewCellNib = UINib(nibName: "AddressTableViewCell", bundle: nil)
        addressesTableView.register(addressTableViewCellNib, forCellReuseIdentifier: "AddressTableViewCell")
    }
    
    func loadContent() {
        addressesTableView.keyboardDismissMode = .interactive
    }
    
    func reloadData() -> Bool {
        guard self.viewIfLoaded != nil else { return false }
        addressesTableView.reloadData()
        return true
    }
    
    func getCoordinateFromAddress(_ address: Address, successResponse: @escaping(_ place: CLLocationCoordinate2D) -> Void) {
        MainManager.sharedManager.getCoordinateFrom(address: address.address, successResponse: { (coordinate) in
            successResponse(coordinate)
        }) { _ in () }
    }
}

extension ListOfAddressesViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        switch typeOfAddresses {
        case .favorites:
            return IndicatorInfo(title: "Favoritos")
        case .recents:
            return IndicatorInfo(title: "Recientes")
        }
    }
}

extension ListOfAddressesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedAddresses.count + 1 + addressResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as! AddressTableViewCell
        cell.delegate = self
        cell.addressToChooseType = .normal
        if indexPath.row == 0 {
            cell.address = nil
            cell.addressToChooseType = .goMap
            cell.likeable = false
        } else {
            if indexPath.row <= savedAddresses.count {
                cell.address = savedAddresses[indexPath.row - 1]
                cell.actionIconButton.isHidden = false
                cell.likeable = true
            } else {
                let numberOfSavedAddresses = savedAddresses.count
                cell.address = addressResults[indexPath.row - 1 - numberOfSavedAddresses]
                cell.likeable = false
            }
        }
        cell.awakeFromNib()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.startLoader()
        
        guard let cell = tableView.cellForRow(at: indexPath) as? AddressTableViewCell else { return }
        guard let address = cell.address else {
            self.stopLoader()
            self.delegate?.listOfAddressesViewController(vc: self, didSelectAddress: cell.address)
            return
        }
        
        //SI TIENE PLACE ID PROVIENE DEL AUTOCOMPLETADO
        if address.placeId != "" {
            //OBTENEMOS LAS COORDENADAS
//            getCoordinateFromAddress(address) { (coordinate) in
//            }
            MainManager.sharedManager.getCoordinateFrom(address: address.address) { coordinate in
                
                address.setCoordinate(coordinate)
                self.stopLoader()
                self.delegate?.listOfAddressesViewController(vc: self, didSelectAddress: cell.address)
                
            } failureResponse: { error in
                self.stopLoader()
            }
            
        } else {
            //SI NO PROVIENE DEL AUTOCOMPLETADO
            self.stopLoader()
            self.delegate?.listOfAddressesViewController(vc: self, didSelectAddress: cell.address)
        }
    }
}

extension ListOfAddressesViewController: AddressTableViewCellDelegate {
    func addressTableViewCell(shouldSetFavorite: Bool, inCell cell: AddressTableViewCell) -> Bool {
        print("DID SET FAVORITE IN CELL")
        guard let isFavorite = cell.address?.isFavorite else { return false }
        if isFavorite {
            self.showAlert(message: "¿Desea eliminar \(cell.address?.name ?? "esta dirección") de sus favoritos?", withOkayButton: true, okayHandler: { (_) in
                guard let addressId = cell.address?.id else { return }
                
                self.startLoader()
                ClientManager.sharedManager.deleteFavoriteAddress(favoriteAddressId: addressId, successResponse: { (message) in
                    self.stopLoader()
                    try! self.realm.write {
                        cell.address?.isFavorite = false
                    }
                    self.addressesTableView.reloadData()
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            }, withCancelButton: true)
        } else {
            guard let address = cell.address else { return false }
            
            MainManager.sharedManager.getCoordinateFrom(address: address.address) { coordinate in
                
                address.setCoordinate(coordinate)
                AddFavoritesViewController.instantiate(in: self, address: address.address, name: address.name, coordinate: address.location)
                
            } failureResponse: { error in
                
            }
            
        }
        return false
    }
    
    func addressTableViewCell(didSetFavorite: Bool, inCell cell: AddressTableViewCell) {
    }
}

extension ListOfAddressesViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
            self.addressesTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.isKeyboardPresented ? 200 : 0, right: 0)
            self.view.layoutIfNeeded()
        }
    }
}
