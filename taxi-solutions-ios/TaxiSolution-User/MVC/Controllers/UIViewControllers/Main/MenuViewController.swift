//
//  MenuViewController.swift
//  TaxiSolution-User
//
//  Created by Fernanda Alvarado Tarrillo on 12/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

class MenuViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var modulesTableView: UITableView!

    var modules: [MenuModule] = MenuModule.getClientMenuOptions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }

    func configureContent() {
        modulesTableView.delegate = self
        modulesTableView.dataSource = self
        let moduleTableViewCellNib = UINib(nibName: "MenuModuleTableViewCell", bundle: nil)
        modulesTableView.register(moduleTableViewCellNib, forCellReuseIdentifier: "MenuModuleTableViewCell")
    }

    func loadContent() {
        profileImageView.sd_setImage(with: user.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [.refreshCached], completed: nil)
       // ratingLabel.text = user.rating.oneDecimalString
        nameLabel.text = user.fullName
    }

    @IBAction func toggleSideMenu() {
        guard let mainContainerVC = self.parent as? MainContainerViewController else {
            return
        }
        mainContainerVC.toggleSideMenu()
    }

    @IBAction func goMyProfile() {
        toggleSideMenu()
        guard let mainContainerVC = self.parent as? MainContainerViewController else { return }
        mainContainerVC.openModule(MenuDriverModuleNames.myProfile)
    }

    @IBAction func askLogout() {
        ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.logout, message: "¿Está seguro que desea salir de la aplicación? Los datos se guardarán", andConfirmText: "Confirmar") { _, confirmed in
            _ = confirmed ? self.logout() : .none
        }
    }

    @IBAction func goTerms() {
        let termsVC = UIStoryboard(name: StoryboardNames.shared, bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.present(termsVC, animated: true, completion: nil)
    }

}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
      //  return tableView.frame.height / CGFloat(modules.count)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modules.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuModuleTableViewCell") as! MenuModuleTableViewCell
       cell.module = modules[indexPath.row]
        cell.awakeFromNib()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toggleSideMenu()
        guard let mainContainerVC = self.parent as? MainContainerViewController else { return }
        mainContainerVC.openModule(modules[indexPath.row].title)
    }
}

