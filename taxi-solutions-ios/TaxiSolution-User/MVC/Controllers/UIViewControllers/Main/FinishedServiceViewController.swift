//
//  FinishedServiceViewController.swift
//  TaxiSolution-User
//
//  Created by Fernanda Alvarado Tarrillo on 12/01/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import UIKit
import Cosmos

protocol FinishedServiceViewControllerDelegate {
    func pressDismissPaymentDetail()
}

class FinishedServiceViewController: UIViewController {

    @IBOutlet weak var numberOrderLabel: UILabel!
    @IBOutlet weak var clientLabel: UILabel!
    @IBOutlet weak var typeCardLabel: UILabel!
    @IBOutlet weak var numberCardLabel: UILabel!
    @IBOutlet weak var amountPaidLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var operationDenegateLabel: UILabel!
    @IBOutlet var hiddenViews: [UIView]!
    @IBOutlet weak var detailPaymentLabel: UILabel!

    var paymentData = [String: Any]()
    var delegate: FinishedServiceViewControllerDelegate?

    class func instantiate(in viewController: UIViewController, delegate:  FinishedServiceViewControllerDelegate, paymentData: [String: Any]) {
        guard let finishedServiceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FinishedServiceViewControllerID") as? FinishedServiceViewController else { return }

        finishedServiceVC.delegate = delegate
        finishedServiceVC.paymentData = paymentData
        viewController.definesPresentationContext = true
        viewController.modalPresentationStyle = .currentContext
        viewController.modalTransitionStyle = .crossDissolve

        viewController.present(finishedServiceVC, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    func loadData() {
        let success = paymentData["success"] ?? ""

        if success as? Bool ?? false {
            if let amount = paymentData["amount"], let cardBrand = paymentData["card_brand"], let purchaseNumber = paymentData["purchase_number"], let  createdAt = paymentData["created_at"], let fullName = paymentData["full_name"], let description = paymentData["description"], let maskedCardNumber = paymentData["masked_card_number"]  {

                let date = createdAt as? String ?? ""
                operationDenegateLabel.text = "Detalle de pago"
                detailPaymentLabel.text = ""
                numberOrderLabel.text = "\(purchaseNumber)"
                clientLabel.text = "\(fullName)".capitalized
                typeCardLabel.text = "\(cardBrand)".firstWordCapitalized
                numberCardLabel.text = "\(maskedCardNumber)"
                amountPaidLabel.text = "S/ \(amount)"
                dateLabel.text = "\(date)".firstWord
                timeLabel.text = "\(date)".components(separatedBy: " ").last ?? ""
            }
        }else {
            if let purchaseNumber = paymentData["purchase_number"], let  createdAt = paymentData["created_at"], let descriptionPayment = paymentData["description"] {
                let date = createdAt as? String ?? ""
                hiddenViews.forEach { view in
                    view.isHidden = true
                }
                operationDenegateLabel.text = "Pago Denegada"
                detailPaymentLabel.text = "\(descriptionPayment)"
                numberOrderLabel.text = "\(purchaseNumber)"
                dateLabel.text = "\(date)".firstWord
                timeLabel.text = "\(date)".components(separatedBy: " ").last ?? ""
            }
        }
    }

}

