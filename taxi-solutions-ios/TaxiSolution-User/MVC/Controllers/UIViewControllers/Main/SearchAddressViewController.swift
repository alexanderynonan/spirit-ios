//
//  SearchAddressViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/18/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import CoreLocation
import GooglePlaces


protocol SearchAddressViewControllerDelegate {
    func searchAddressViewController(dismiss: Bool)
}

class SearchAddressViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchAddressViewHeight: NSLayoutConstraint!
    
    var parentVC: UIViewController?
    var searchAddressDelegate: SearchAddressViewControllerDelegate?
    var addressType: AddressType = .destination {
        didSet {
            switch addressType {
            case .origin:
                titleLabel.text = "Búscame en"
            case .destination:
                titleLabel.text = "¿A dónde vas?"
            }
        }
    }
    
    lazy var listOfFavoriteAddressesVC: ListOfAddressesViewController = {
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "ListOfAddressesViewController") as? ListOfAddressesViewController
        vc1?.typeOfAddresses = .favorites
        vc1?.parentVC = self
        return vc1 ?? ListOfAddressesViewController()
    }()
    
    lazy var listOfRecentAddressesVC: ListOfAddressesViewController = {
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "ListOfAddressesViewController") as? ListOfAddressesViewController
        vc2?.typeOfAddresses = .recents
        vc2?.parentVC = self
        return vc2 ?? ListOfAddressesViewController()
    }()
    
    var addressResults: [Address] = [] {
        didSet {
            listOfFavoriteAddressesVC.addressResults = addressResults
            listOfRecentAddressesVC.addressResults = addressResults
        }
    }
    
    var loaded = false

    override func viewDidLoad() {
        configureButtonBarPagerTabStrip()
        super.viewDidLoad()
        
        configureContent()
        loadContent()
        
       // self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loaded = true
    }
    
    func configureContent() {
        
    }
    
    func loadContent() {
        if !loaded {
            self.startLoader()
        }
        ClientManager.sharedManager.getFavoriteAddresses(successResponse: { (favoriteAddresses) in
            var addresses = favoriteAddresses
            if let houseAddressIndex = favoriteAddresses.firstIndex(where: { $0.name == FavoriteAddressNames.house }), favoriteAddresses.count > 1 {
                let houseAddress = addresses.remove(at: houseAddressIndex)
                if addresses.indices.contains(0) {
                    addresses.insert(houseAddress, at: 0)
                }
            }
            if let workAddressIndex = favoriteAddresses.firstIndex(where: { $0.name == FavoriteAddressNames.work }), favoriteAddresses.count > 1 {
                let index = addresses.indices.contains(1) ? 1 : 0
                let workAddress = addresses.remove(at: workAddressIndex)
                addresses.insert(workAddress, at: index)
            }
            
            self.listOfFavoriteAddressesVC.savedAddresses = addresses
            
            ClientManager.sharedManager.getRecentAddresses(successresponse: { (recentAddresses) in
                self.stopLoader()
                self.listOfRecentAddressesVC.savedAddresses = recentAddresses
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [listOfFavoriteAddressesVC, listOfRecentAddressesVC]
    }
    
    override func dismiss() {
        searchAddressDelegate?.searchAddressViewController(dismiss: true)
    }

}

/*
protocol SearchAddressViewControllerDelegate {
    func searchAddressViewController(dismiss: Bool)
    func listOfAddressesViewController(didSelectAddress address: Address?)
}

class SearchAddressViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchAddressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addressesTableView: UITableView!
    
    var parentVC: UIViewController?
    var searchAddressDelegate: SearchAddressViewControllerDelegate?
    var addressType: AddressType = .destination {
        didSet {
            switch addressType {
            case .origin:
                titleLabel.text = "Búscame en"
            case .destination:
                titleLabel.text = "¿A dónde vas?"
            }
        }
    }
    
    var addressResults: [Address] = [] {
        didSet {
            guard reloadData() else { return }
            addressesTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.isKeyboardPresented ? 200 : 0, right: 0)
            if addressResults.count != 0 {
                addressesTableView.scrollToRow(at: IndexPath(row: savedAddresses.count + 1, section: 0), at: .top, animated: true)
            }
        }
    }

    var savedAddresses: [Address] = [] {
        didSet {
            _ = reloadData()
        }
    }
    
    var loaded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureContent()
        loadContent()
        
        self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loaded = true
    }
    
    func configureContent() {
        addressesTableView.delegate = self
        addressesTableView.dataSource = self
        let addressTableViewCellNib = UINib(nibName: "AddressTableViewCell", bundle: nil)
        addressesTableView.register(addressTableViewCellNib, forCellReuseIdentifier: "AddressTableViewCell")
        addressesTableView.keyboardDismissMode = .interactive

    }
    
    func loadContent() {
        if !loaded {
            self.startLoader()
        }
        ClientManager.sharedManager.getFavoriteAddresses(successResponse: { (favoriteAddresses) in
            var addresses = favoriteAddresses
            if let houseAddressIndex = favoriteAddresses.firstIndex(where: { $0.name == FavoriteAddressNames.house }), favoriteAddresses.count > 1 {
                let houseAddress = addresses.remove(at: houseAddressIndex)
                if addresses.indices.contains(0) {
                    addresses.insert(houseAddress, at: 0)
                }
            }
            if let workAddressIndex = favoriteAddresses.firstIndex(where: { $0.name == FavoriteAddressNames.work }), favoriteAddresses.count > 1 {
                let index = addresses.indices.contains(1) ? 1 : 0
                let workAddress = addresses.remove(at: workAddressIndex)
                addresses.insert(workAddress, at: index)
            }
            
            self.savedAddresses = addresses
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }

    func reloadData() -> Bool {
        guard self.viewIfLoaded != nil else { return false }
        addressesTableView.reloadData()
        return true
    }

    override func dismiss() {
        searchAddressDelegate?.searchAddressViewController(dismiss: true)
    }

    func getCoordinateFromAddress(_ address: Address, successResponse: @escaping(_ place: CLLocationCoordinate2D) -> Void) {
        MainManager.sharedManager.getCoordinateFrom(address: address.address, successResponse: { (coordinate) in
            successResponse(coordinate)
        }) { _ in () }
    }

}

extension SearchAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedAddresses.count + 1 + addressResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as! AddressTableViewCell
        cell.delegate = self
        cell.addressToChooseType = .normal
        if indexPath.row == 0 {
            cell.address = nil
            cell.addressToChooseType = .goMap
            cell.likeable = false
        } else {
            if indexPath.row <= savedAddresses.count {
                cell.address = savedAddresses[indexPath.row - 1]
                cell.actionIconButton.isHidden = false
                cell.likeable = true
            } else {
                let numberOfSavedAddresses = savedAddresses.count
                cell.address = addressResults[indexPath.row - 1 - numberOfSavedAddresses]
                cell.likeable = false
            }
        }
        cell.awakeFromNib()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? AddressTableViewCell else { return }
        guard let address = cell.address else {
            searchAddressDelegate?.listOfAddressesViewController(didSelectAddress: cell.address)
                return
        }
        //SI TIENE PLACE ID PROVIENE DEL AUTOCOMPLETADO
        if address.placeId != "" {
            //OBTENEMOS LAS COORDENADAS
            getCoordinateFromAddress(address) { (coordinate) in
                address.setCoordinate(coordinate)
                self.searchAddressDelegate?.listOfAddressesViewController(didSelectAddress: cell.address)
            }
        } else {
            //SI NO PROVIENE DEL AUTOCOMPLETADO
            self.searchAddressDelegate?.listOfAddressesViewController( didSelectAddress: cell.address)
        }
    }
}

extension SearchAddressViewController: AddressTableViewCellDelegate {
    func addressTableViewCell(shouldSetFavorite: Bool, inCell cell: AddressTableViewCell) -> Bool {
        print("DID SET FAVORITE IN CELL")
        guard let isFavorite = cell.address?.isFavorite else { return false }
        if isFavorite {
            self.showAlert(message: "¿Desea eliminar \(cell.address?.name ?? "esta dirección") de sus favoritos?", withOkayButton: true, okayHandler: { (_) in
                guard let addressId = cell.address?.id else { return }

                self.startLoader()
                ClientManager.sharedManager.deleteFavoriteAddress(favoriteAddressId: addressId, successResponse: { (message) in
                    self.stopLoader()
                    try! self.realm.write {
                        cell.address?.isFavorite = false
                    }
                    self.addressesTableView.reloadData()
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            }, withCancelButton: true)
        } else {
            guard let address = cell.address else { return false }
            getCoordinateFromAddress(address) { (coordinate) in
                address.setCoordinate(coordinate)
                AddFavoritesViewController.instantiate(in: self, address: address.address, name: address.name, coordinate: address.location)
            }
        }
        return false
    }

    func addressTableViewCell(didSetFavorite: Bool, inCell cell: AddressTableViewCell) {
    }
}


extension SearchAddressViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
            self.addressesTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.isKeyboardPresented ? 200 : 0, right: 0)
            self.view.layoutIfNeeded()
        }
    }
}
*/

