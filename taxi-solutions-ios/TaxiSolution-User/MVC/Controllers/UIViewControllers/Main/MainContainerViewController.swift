//
//  MainContainerViewController.swift
//  TaxiSolution-User
//
//  Created by Fernanda Alvarado Tarrillo on 12/10/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class MainContainerViewController: UIViewController {

    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var menuContainerViewLeadingConstraint: NSLayoutConstraint!
    var isTouchEnable = true
    var menuIsVisible = false {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    var menuVC: MenuViewController?
    var mapVC: MapViewController?

    let coverLayer = CALayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !menuIsVisible, #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .lightContent
        }
    }

    func configureContent() {
        coverLayer.frame = self.view.bounds
        coverLayer.backgroundColor = UIColor.black.cgColor
        coverLayer.opacity = 0.0
        mainContainerView.layer.addSublayer(coverLayer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleMenuPan))
        menuContainerView.addGestureRecognizer(panGestureRecognizer)

        menuContainerViewLeadingConstraint.constant = -menuContainerView.frame.size.width
    }

    func loadContent() {

    }

    @objc func handleMenuPan(_ recognizer: UIPanGestureRecognizer) {
        if isTouchEnable {
            let translation = recognizer.translation(in: self.view)
            let percentage = abs(translation.x) / self.menuContainerView.frame.size.width

            let state = recognizer.state
            if state == .ended || state == .failed || state == .cancelled {
                if menuIsVisible {
                    if translation.x < 0 {
                        toggleSideMenu()
                    }
                } else {
                    if translation.x > 10.0 {
                        toggleSideMenu()
                    } else {
                        self.view.layoutIfNeeded()
                        UIView.animate(withDuration: 0.2, animations: {
                            self.menuContainerViewLeadingConstraint.constant = -self.menuContainerView.frame.size.width
                            self.view.layoutIfNeeded()
                        })
                    }
                }
            } else {
                if !menuIsVisible && translation.x > 0.0 && translation.x <= menuContainerView.frame.size.width {
                    self.menuContainerViewLeadingConstraint.constant = -self.menuContainerView.frame.size.width + translation.x
                    changeOverlayOpacity(withPercentage: percentage, inTranslation: translation)
                }

                if menuIsVisible {
                    if abs(translation.x) <= self.menuContainerView.frame.size.width && translation.x < 0 {
                        menuContainerViewLeadingConstraint.constant = translation.x
                        changeOverlayOpacity(withPercentage: percentage, inTranslation: translation)
                    }
                }
            }
        }
    }

    @objc func toggleSideMenu() {
        if isTouchEnable {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: {
                // hide the side menu to the left
                self.menuContainerViewLeadingConstraint.constant = self.menuIsVisible ? -self.menuContainerView.frame.size.width : 0
                self.coverLayer.opacity = self.menuIsVisible ? 0.0 : 0.6
                self.view.layoutIfNeeded()
            })

            menuIsVisible = !menuIsVisible
            menuVC?.loadContent()
        }
    }

    func changeOverlayOpacity(withPercentage percentage: CGFloat, inTranslation translation: CGPoint) {
        if (translation.x > 0) {
            coverLayer.opacity = 0.6 * Float(percentage)
        } else {
            coverLayer.opacity = 0.6 * (1.0 - Float(percentage))
        }
    }

    func openModule(_ module: String) {
        var storyboardName = ""
        if [MenuClientModuleNames.myProfile, MenuBackendModuleKeys.myProfile].contains(module)  {
            storyboardName = "MyProfile"
        }
        if [MenuClientModuleNames.rides, MenuBackendModuleKeys.rides].contains(module) {
            storyboardName = "Rides"
        }
        if [MenuClientModuleNames.payment, MenuBackendModuleKeys.payment].contains(module) {
            storyboardName = "PaymentMethods"
        }
        if [MenuClientModuleNames.promotions, MenuBackendModuleKeys.promotions].contains(module) {
            storyboardName = "Bonus"
        }
        if [MenuClientModuleNames.shareAndWin, MenuBackendModuleKeys.shareAndWin].contains(module) {
            storyboardName = "Share"
        }
        if [MenuClientModuleNames.trustContacts, MenuBackendModuleKeys.trustContacts].contains(module) {
            storyboardName = "TrustContacts"
        }
        if [MenuDriverModuleNames.help, MenuBackendModuleKeys.help].contains(module) {
            storyboardName = "Help"
        }

        guard storyboardName != "" else { return }

        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)

        guard let initialVC = storyboard.instantiateInitialViewController() else { return }
        self.present(initialVC, animated: true, completion: nil)
    }

   

}
