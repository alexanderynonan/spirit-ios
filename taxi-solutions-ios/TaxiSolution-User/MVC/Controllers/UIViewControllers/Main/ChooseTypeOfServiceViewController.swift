//
//  ChooseTypeOfServiceViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 5/25/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

protocol ChooseTypeOfServiceViewControllerDelegate: class {
    func chooseTypeOfServiceViewController(didSelectServiceType: ServiceType, in viewController: ChooseTypeOfServiceViewController)
    func chooseTypeOfServiceViewController(didChangePaymentMethod: PaymentMethod, in viewController: ChooseTypeOfServiceViewController)
}

class ChooseTypeOfServiceViewController: PannableViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var servicesCollectionView: UICollectionView!
    @IBOutlet weak var servicesTableView: UITableView!
    @IBOutlet weak var paymentMethodsCollectionView: UICollectionView!
    @IBOutlet weak var confirmButton: CustomButton!
    
    //MARK: - Variables
    var tableViewCellIdentifier = "ServiceTypeTableViewCell"
    var serviceCollectionViewCellIdentifier = "ServiceTypeCollectionViewCell"
    var collectionViewCellIdentifier = "ImageWithTextCollectionViewCell"
    
    var delegate: ChooseTypeOfServiceViewControllerDelegate?
    var mapVC = MapViewController()
    
    var serviceTypes: [ServiceType] = []
    var selectedServiceType: ServiceType? = .none
    
    var selectedPaymentMethod: PaymentMethod? = .none
    var cards: [TokenizedCard] {
        return Array(self.realm.objects(TokenizedCard.self))
    }
    
    //MARK: - Instantiation method
    class func instantiate(inViewController vc: MapViewController, serviceTypes: [ServiceType]) {
        
        guard let chooseTypeOfServiceVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseTypeOfServiceViewController") as? ChooseTypeOfServiceViewController else { return }
        chooseTypeOfServiceVC.serviceTypes = serviceTypes
        chooseTypeOfServiceVC.delegate = vc as? ChooseTypeOfServiceViewControllerDelegate
        chooseTypeOfServiceVC.panDelegate = vc as PannableViewControllerDelegate
        chooseTypeOfServiceVC.mapVC = vc
        
        //Parseamos la vista del presente controlador como un ControlView
        if let controlView = chooseTypeOfServiceVC.view as? ControlView {
            //Especificamos que la vista a pasar el touch será la vista del MapViewController, en otras palabras, el MapViewController reconocera los gestos que se realicen en la vista del presente controlador.
            controlView.touchDelegate = vc.view
        }
        
        chooseTypeOfServiceVC.enableMapAddressView(false)
        
        //Instanciamos el controlador en un navigation para poder hacerle push a la pantalla de métodos de pago en caso seleccione pago por tarjeta
        let navigationVC = UINavigationController(rootViewController: chooseTypeOfServiceVC)
        navigationVC.modalPresentationStyle = .overFullScreen
        navigationVC.setNavigationBarHidden(true, animated: true)
        vc.chooseTypeOfServiceVC = chooseTypeOfServiceVC
        vc.present(navigationVC, animated: true, completion: nil)
    }

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        paymentMethodsCollectionView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        enableMapAddressView()
    }
    
    func enableMapAddressView(_ enable: Bool = true) {
        mapVC.addressesView.isUserInteractionEnabled = enable
    }
    
    //MARK: Functions
    func configureContent() {
        //Configuramos tableview
//        servicesTableView.delegate = self
//        servicesTableView.dataSource = self
//        
        servicesCollectionView.delegate = self
        servicesCollectionView.dataSource = self
        
//        let serviceTypeTableViewCellNib = UINib(nibName: tableViewCellIdentifier, bundle: nil)
//        servicesTableView.register(serviceTypeTableViewCellNib, forCellReuseIdentifier: tableViewCellIdentifier)
//
        let serviceTypeCollectionViewCellNib = UINib(nibName: serviceCollectionViewCellIdentifier, bundle: nil)
        
        servicesCollectionView.register(serviceTypeCollectionViewCellNib, forCellWithReuseIdentifier: serviceCollectionViewCellIdentifier)
        
        
        //Configuramos collectionview
        paymentMethodsCollectionView.delegate = self
        paymentMethodsCollectionView.dataSource = self
        paymentMethodsCollectionView.allowsMultipleSelection = false
        
        let imageWithTextCollectionViewCellNib = UINib(nibName: collectionViewCellIdentifier, bundle: nil)
        paymentMethodsCollectionView.register(imageWithTextCollectionViewCellNib, forCellWithReuseIdentifier: collectionViewCellIdentifier)
        
        //Variable para evitar que se haga dismiss manual a la vista
        manualDismissEnabled = false
        
        //Seteamos el método de pago por defecto
        selectedPaymentMethod = cards.contains(where: { $0.isDefault }) ? .creditCard : .cash
    }
    
    func loadContent() {
        //Obtiene y guarda las tarjetas registradas que tiene el usuario
        self.mapVC.startLoader()
        MainManager.sharedManager.getTokenizedCards(successResponse: { (tokenizedCards) in
            self.mapVC.stopLoader()
        }) { (error) in
            self.mapVC.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func paymentMethodIsValid(_ paymentMethod: PaymentMethod, withValidationMessage message: String? = nil) -> Bool {
        //Revisa si el método de pago es válido
        switch paymentMethod {
        case .cash:
            return true
        case .creditCard:
            //Si el método de pago es tarjeta, entonces valida si tiene tarjetas guardadas
            let thereAreCards = self.realm.objects(TokenizedCard.self).count != 0
            //Si no hay tarjetas, mostramos el mensaje de validación en caso haya
            if !thereAreCards, let message = message {
                self.showAlert(message: message)
            }
            return thereAreCards
        }
    }
    
    //MARK: - IBActions
    @IBAction func requestTrip() {
        //Validamos que el parentVC sea el MapViewController para obtener acceso a la variable ride, validamos que tenga seleccionado el tipo de servicio y el método de pago
        guard let ride = mapVC.ride, let selectedServiceType = selectedServiceType, let selectedPaymentMethod = selectedPaymentMethod  else {
            self.showAlert(message: "Debe seleccionar el tipo de servicio y tipo de pago")
            return
        }
        
        //Validamos el método de pago seleccionado y especificamos un mensaje de validación en caso no lo sea
        guard paymentMethodIsValid(selectedPaymentMethod, withValidationMessage: "No tiene tarjetas registradas") else {
            paymentMethodsCollectionView.reloadData()
            return
        }
        
        //Validamos que el request es válido
        guard ride.isValidRequest else {
            self.mapVC.startLoader()
            ClientManager.sharedManager.getTripRoute(tripRequest: ride, successResponse: { (_, _, estimatedDistance, serviceTypes, originLong, originLat, destinationLong, destinationLat) in
                self.mapVC.stopLoader()
                try! self.realm.write {
                    ride.requested = true
                    ride.originLat = originLat
                    ride.originLong = originLong
                    ride.estimatedDistance = estimatedDistance
                    ride.requestedAt = Date()
                }
                self.serviceTypes = serviceTypes
                self.selectedServiceType = .none
//                self.servicesTableView.reloadData()
                self.servicesCollectionView.reloadData()
                self.showAlert(message: "Los precios han sido actualizados. Por favor vuelva a confirmar el tipo de servicio")
            }) { (error) in
                self.mapVC.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
            return
        }
        
        //Especificamos el tipo de servicio, el tiempo estimado, el tipo de pago, y el monto estimado al ride para poder solicitar el viaje
        try! realm.write {
            ride.serviceTypeId = selectedServiceType.id.string
            ride.estimatedTime = selectedServiceType.estimatedArrivalTimeInMinutes.string
            ride.paymentType = selectedPaymentMethod == .cash ? "cash" : "card"
            ride.estimatedAmount = selectedServiceType.amount.string
        }
        
        //Solicitamos el viaje
        self.mapVC.startLoader()
        ClientManager.sharedManager.findRide(ride: ride, successResponse: { (rideId) in
            self.mapVC.stopLoader()
            self.dismiss()
            try! self.realm.write {
                ride.rideId = rideId
                self.realm.add(ride)
            }
            self.mapVC.showLoaderScreen(title: "Buscando conductor", message: "Realizando búsqueda de conductor")
            UserDefaults.standard.set(true, forKey: "searching")
        }) { (error) in
            self.mapVC.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    override func dismiss() {
        mapVC.chooseTypeOfServiceVC = nil
        super.dismiss()
    }
}


//MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ChooseTypeOfServiceViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == servicesCollectionView {
            return CGSize(width: (collectionView.frame.width/3), height: collectionView.frame.height)
        } else {
            return CGSize(width: collectionView.frame.width / CGFloat(PaymentMethod.allCases.count), height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == servicesCollectionView ? serviceTypes.count : PaymentMethod.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == servicesCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: serviceCollectionViewCellIdentifier, for: indexPath) as! ServiceTypeCollectionViewCell
            // cell.separatorView.isHidden = indexPath.row == 0 ? true : false
            cell.serviceType = serviceTypes[indexPath.row]
            //cell.isSelect = indexPath.row == 0 ? true : false
            cell.awakeFromNib()
            return cell
            
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellIdentifier, for: indexPath) as! ImageWithTextCollectionViewCell
            let paymentMethod = PaymentMethod.allCases[indexPath.row]
            let imageOn = paymentMethod == .cash ? #imageLiteral(resourceName: "ic_cash_on") : #imageLiteral(resourceName: "ic_card_on")
            let imageOff = paymentMethod == .cash ? #imageLiteral(resourceName: "ic_cash_off") : #imageLiteral(resourceName: "ic_card_off")
            cell.imageWithTextSwitch = ImageWithTextSwitch(name: paymentMethod.rawValue, imageOn: imageOn, imageOff: imageOff, colorOn: Colors.primary, colorOff: Colors.background)
            cell.imageWithTextSwitchButton.layer.cornerRadius = 10
            cell.awakeFromNib()
            
            //Si es el método de pago seleccionado entonces se muestra resaltado
            cell.imageWithTextSwitchButton.isOn = paymentMethod == selectedPaymentMethod ? true : false
            
            //Valida si el método de pago no es válido para disminuir el alpha
            cell.contentView.alpha = paymentMethodIsValid(paymentMethod) ? 1.0 : 0.3
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == servicesCollectionView {
            selectedServiceType = serviceTypes[indexPath.row]
            //Seteamos el estado inicial de las celdas de tipo de servicio
            collectionView.visibleCells.forEach {
                let cell = $0 as! ServiceTypeCollectionViewCell
                cell.isSelect = false 
            }
            //Cambiamos la vista del tipo de servicio seleccionado
            let cell = collectionView.cellForItem(at: indexPath) as! ServiceTypeCollectionViewCell
            cell.isSelect = true
//            cell.detailsView.isHidden = false
//            cell.timeRemainingLabel.textColor = Colors.secondary
//
//            //Actualizamos el layout de la tabla para que la celda del tipo de servicio seleccionado tenga el alto correspondiente
//            collectionView.beginUpdates()
//            collectionView.endUpdates()
            
            //Cambiamos el titulo del botón de acuerdo al tipo de servicio seleccionado
            confirmButton.setTitle("SOLICITAR \(cell.serviceType?.name.uppercased() ?? "")", for: .normal)
            
        }else {
            let cell = collectionView.cellForItem(at: indexPath) as! ImageWithTextCollectionViewCell
            
            //Método de pago seleccionado
            let selectedPaymentMethodToSet = PaymentMethod.allCases.first(where: { $0.rawValue ==
                cell.imageWithTextSwitch.name }) ?? .none
            
            switch selectedPaymentMethodToSet {
            case .cash:
                //Si es cash obtenemos la tarjeta predeterminada para ponerla como false
                guard let predeterminedCard = self.cards.first(where: { $0.isDefault }) else { return }
                self.startLoader()
                MainManager.sharedManager.setPredeterminedCard(predeterminedCard, predetermined: false, successResponse: { (_) in
                    self.stopLoader()
                    //Seteamos que el método de pago seleccionado en la variable global del controlador
                    self.selectedPaymentMethod = selectedPaymentMethodToSet
                    collectionView.reloadData()
                }) { (error) in
                    self.stopLoader()
                    self.showAlert(message: error.localizedDescription)
                }
            case .creditCard:
                //Si el método seleccionado es tarjeta, se redirige a la pantalla de métodos de pago
                if let navVC = UIStoryboard(name: "PaymentMethods", bundle: nil).instantiateInitialViewController() as? UINavigationController  {
                    if let paymentVC = navVC.topViewController as? PaymentMethodsViewController {
                        //Se le especifica que al controlador que proviene de la opcion de tipo de pago del viaje
                        paymentVC.delegate = self
                        paymentVC.isTravel = true 
                        self.navigationController?.pushViewController(paymentVC, animated: true)
                        
                    }
                }
            default: ()
            }
        }
    }
}

extension ChooseTypeOfServiceViewController: PaymentMethodsViewControllerDelegate {
    func didSelectCreditCard(_ card: TokenizedCard) {
        selectedPaymentMethod = .creditCard
        //Recargamos el collectionview para que se pinte el método seleccionado
        paymentMethodsCollectionView.reloadData()
    }
}
