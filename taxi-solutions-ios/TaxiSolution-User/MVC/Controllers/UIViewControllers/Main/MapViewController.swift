//
//  MapViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/29/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Toast_Swift
import Cosmos

class MapViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var pinAddressImageView: UIImageView!
    @IBOutlet weak var pinAddressShadowImageView: UIImageView!
    @IBOutlet weak var addressesViewTop: NSLayoutConstraint!
    @IBOutlet weak var topOriginAddressTextField: UITextField!
    @IBOutlet weak var originAddressTextField: UITextField!
    @IBOutlet weak var dotsImageView: UIImageView!
    @IBOutlet weak var addressesView: UIView!
    @IBOutlet weak var topDestinationAddressTextField: UITextField!
    @IBOutlet weak var destinationAddressTextField: UITextField!
    @IBOutlet weak var originAddressContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var destinationAddressContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var searchAddressViewContainer: UIView!
    @IBOutlet weak var searchAddressViewContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var originActionButton: CustomButton!
    @IBOutlet weak var destinationActionButton: CustomButton!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var rideMetricsContainer: CustomView!
    
    @IBOutlet weak var userOnTripMyLocationButton: UIButton!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var myLocationButtonMaxY: NSLayoutConstraint!
    @IBOutlet weak var confirmCoordinateButton: CustomButton!
    @IBOutlet weak var userOnTripCardView: UIView!
    @IBOutlet weak var driverDetailsView: UIView!
    @IBOutlet weak var carDetailsView: UIView!
    @IBOutlet weak var carDetailsViewHeight: NSLayoutConstraint!

    @IBOutlet weak var cardViewContinueTripBottom: NSLayoutConstraint!
    
    @IBOutlet weak var cardViewRequestBottom: NSLayoutConstraint!
    
    @IBOutlet weak var timerContainer: UIView!
    @IBOutlet weak var timerContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var rideStateLabel: UILabel!
    @IBOutlet weak var rideStateLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var dismissCardViewButton: UIButton!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverServiceTypeLabel: UILabel!
    @IBOutlet weak var driverRatingLabel: UILabel!
    @IBOutlet weak var driverRatingView: CosmosView!
    @IBOutlet weak var driverCarPlateNumberAndColorLabel: UILabel!
    @IBOutlet weak var driverCarBrandLabel: UILabel!
    @IBOutlet weak var driverCarImageView: UIImageView!
    @IBOutlet weak var driverDetailsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sosButtonContainer: UIView!
    @IBOutlet weak var sosButtonContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var shareAndCancelButtonsContainer: UIView!
    @IBOutlet weak var shareAndCancelButtonsContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var contactAndCancelButtonsContainer: UIView!
    @IBOutlet weak var contactAndCancelButtonsContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var paymentDetailContainer: UIView!
    @IBOutlet weak var paymentDetailContainerHeight: NSLayoutConstraint!

    @IBOutlet weak var paymentMethodsCollectionView: UICollectionView!
    @IBOutlet weak var paymentMethodsView: UIView!
    
    @IBOutlet weak var sideMenuButton: UIButton!
    
    //MARK: - Constants
    let defaultZoom: Float = 17.0
    
    var serviceTypes: [ServiceType] = []
    
    //MARK: - Variables
    var collectionViewCellIdentifier = "ImageWithTextCollectionViewCell"
    var selectedPaymentMethod: PaymentMethod? = .none
    var cards: [TokenizedCard] {
        return Array(self.realm.objects(TokenizedCard.self))
    }
    var searchAddressViewController: SearchAddressViewController!
    var loaderVC: LoaderViewController?
    var timer: Timer?
    var isConfirmation = false
    var originAddress = ""
    var destinationAddress = ""
    var buttonDefaultHeight: CGFloat {
        return (UIApplication.shared.keyWindow?.frame.height ?? 0.0) * 0.075
    }
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D? {
        didSet {
            guard let currentLocation = currentLocation else { return }
            let params = [
                "lat": currentLocation.latitude,
                "long": currentLocation.longitude,
                ] as Any
            self.socket.emit(SocketEventsEmit.clientLocation, with: [params])
        }
    }
    var centerMapCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
    }
    
    
    
    //To change mapView camera to default zoom
    var movingFromLocationButton = true
    
    var coordinateToSearch: AddressType? = .none {
        didSet {
            switch coordinateToSearch {
            case .origin:
                if ride == nil { ride = Ride() }
                originActionButton.setImage(#imageLiteral(resourceName: "ic_favorite"), for: .normal)
            case .destination:
                if ride == nil { ride = Ride() }
                destinationActionButton.setImage(#imageLiteral(resourceName: "ic_favorite"), for: .normal)
            case .none:
                topOriginAddressTextField.text = ride?.rideState == .none ? "" : ride?.originAddress ?? ""
                originAddressTextField.text = ride?.rideState == .none ? "" : ride?.originAddress ?? ""
                topDestinationAddressTextField.text = ride?.rideState == .none ? "" : ride?.destinationAddress ?? ""
                destinationAddressTextField.text = ride?.rideState == .none ? "" : ride?.destinationAddress ?? ""
                
                originActionButton.setImage(#imageLiteral(resourceName: "ic_next"), for: .normal)
                destinationActionButton.setImage(#imageLiteral(resourceName: "ic_next"), for: .normal)
                
                let rideNearToOrigin = self.ride?.nearToOrigin ?? false
                
                UIView.transition(with: searchAddressViewContainer, duration: 0.5, options: [.transitionCrossDissolve], animations: {
                    self.originAddressContainerHeight.constant = rideNearToOrigin ? 0 : 55
                    self.destinationAddressContainerHeight.constant = 55
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
            
            UIView.transition(with: searchAddressViewContainer, duration: 0.5, options: [.transitionCrossDissolve], animations: {
                self.addressesViewTop.constant = self.coordinateToSearch != .none ? 70 : 15
                
                self.view.layoutIfNeeded()
            }, completion: nil)
            
            confirmCoordinateButton.setTitle("CONFIRMAR \(coordinateToSearch?.rawValue.uppercased() ?? "")", for: .normal)
        }
    }
    
    var rideState: RideState? = .none {
        didSet {
            try! realm.write {
                self.ride?.state = rideState?.rawValue ?? -1
            }
            self.topOriginAddressTextField.isEnabled = true
            self.originAddressTextField.isEnabled = true
            self.topDestinationAddressTextField.isEnabled = true
            self.destinationAddressTextField.isEnabled = true
            self.rideStateLabel.text = ""
            self.dismissCardViewButton.isHidden = true
            
            UIView.animate(withDuration: 0.35, delay: 0, animations: {
                self.rideMetricsContainer.alpha = self.rideState == .onRide ? 1 : 0
                self.rideMetricsContainer.transform = self.rideState == .onRide ? .identity : CGAffineTransform(scaleX: 0.5, y: 0.5)
                
                guard let rideState = self.rideState else {
                    self.userOnTripCardView.transform = .identity
                    self.topOriginAddressTextField.text = ""
                    self.originAddressTextField.text = ""
                    self.topDestinationAddressTextField.text = ""
                    self.destinationAddressTextField.text = ""
                    if self.ride == nil {
                        self.checkIfNeedsToRateUser()
                    }
                    return
                }
                
                self.showCoordinateSearchComponents(false)
                self.userOnTripCardView.transform = CGAffineTransform(translationX: 0, y: -416)

//                self.paymentMethodsView.isHidden = false
                self.selectedPaymentMethod = self.ride?.paymentType ?? "" == "cash" ? .cash : .creditCard
          //      self.paymentMethodsCollectionView.reloadData()

                switch rideState {
                case .onWay:
                    print("ON WAY")
                    self.loaderVC?.dismiss()
                    self.rideStateLabelHeight.constant = 0
                    self.carDetailsViewHeight.constant = 50
                    self.cardViewContinueTripBottom.constant = -600
                    self.topOriginAddressTextField.isEnabled = false
                    self.originAddressTextField.isEnabled = false
                    self.topDestinationAddressTextField.isEnabled = false
                    self.destinationAddressTextField.isEnabled = false
                    self.sosButtonContainerHeight.constant = 0
                    self.shareAndCancelButtonsContainerHeight.constant = 0
                    self.contactAndCancelButtonsContainerHeight.constant =
                    self.buttonDefaultHeight
                    self.paymentDetailContainerHeight.constant = 0
                case .waiting:
                    print("WAITING")
                    self.loaderVC?.dismiss()
                    self.topDestinationAddressTextField.isEnabled = false
                    self.destinationAddressTextField.isEnabled = false
                    self.rideStateLabelHeight.constant = 0
                    self.carDetailsViewHeight.constant = 50
                    self.cardViewContinueTripBottom.constant = -600
                    self.sosButtonContainerHeight.constant = 0
                    self.shareAndCancelButtonsContainerHeight.constant = 0
                    self.contactAndCancelButtonsContainerHeight.constant = self.buttonDefaultHeight
                    self.paymentDetailContainerHeight.constant = 0
                    self.showWaitingTimer()
                case .onRide:
                    print("ON TRIP")
//                    self.paymentMethodsView.isHidden = true
                    self.rideStateLabel.text = "Viaje en curso"
                    self.rideStateLabelHeight.constant = 20
                    self.carDetailsViewHeight.constant = 50
                    self.cardViewContinueTripBottom.constant = -600
                    self.sosButtonContainerHeight.constant = self.buttonDefaultHeight
                    self.shareAndCancelButtonsContainerHeight.constant = self.buttonDefaultHeight
                    self.contactAndCancelButtonsContainerHeight.constant = 0
                    self.paymentDetailContainerHeight.constant = 0
                    self.showWaitingTimer(false)
                case .finished:
                    print("FINISHED")
//                    self.paymentMethodsView.isHidden = true
                    self.rideStateLabel.text = "Servicio finalizado"
                    self.rideStateLabelHeight.constant = 20
                    self.dismissCardViewButton.isHidden = false
                    self.carDetailsViewHeight.constant = 0
                    self.cardViewContinueTripBottom.constant = -600
                    self.sosButtonContainerHeight.constant = 0
                    self.shareAndCancelButtonsContainerHeight.constant = 0
                    self.contactAndCancelButtonsContainerHeight.constant = 0
                    self.coordinateToSearch = nil
                    self.addressesView.isUserInteractionEnabled = false
                    self.paymentDetailContainerHeight.constant = self.paymentDetailView.preferredHeight
                    self.resetTimerClientTimer()
                    self.showWaitingTimer(false)
                }
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.configureMyLocationButtonPosition()
            })
        }
    }
    
    lazy var paymentDetailView: PaymentDetailView = {
        guard let paymentDetailView = UINib(nibName: "PaymentDetailView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? PaymentDetailView else { return PaymentDetailView() }
        self.paymentDetailView = paymentDetailView
        paymentDetailView.frame = paymentDetailContainer.bounds
        paymentDetailContainer.addSubview(paymentDetailView)
        return paymentDetailView
    }()
    
    var ride: Ride? {
        didSet {
            rideState = ride?.rideState
            self.setSocketEvents()
        }
    }
    var lastPolylineInfo: PolylineInfo? {
        didSet {
            guard let polylineInfo = lastPolylineInfo else { return }
            mapView.drawPolyline(polylineInfo)
        }
    }
    
    var chooseTypeOfServiceVC: ChooseTypeOfServiceViewController? = nil

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let color = #colorLiteral(red: 0.08566711098, green: 0.1312839091, blue: 0.3435831666, alpha: 1)
        self.topOriginAddressTextField.configureClearButtonColor(colorClear: color)
        self.topDestinationAddressTextField.configureClearButtonColor(colorClear: color)

        configureContent()
        getUpdatedUser()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUserData()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchAddressViewControllerSegue" {
            let vc = segue.destination as! SearchAddressViewController
            vc.searchAddressDelegate = self
            vc.parentVC = self
            vc.listOfFavoriteAddressesVC.delegate = self
            vc.listOfRecentAddressesVC.delegate = self
            searchAddressViewController = vc
        }
    }
    
    //MARK: - Functions
    /// Obtiene la data actualizada del usuario para validar si su cuenta ha sido o no aprobada
    func getUpdatedUser() {
        UserManager.sharedManager.getUser(user: user, successResponse: { (user) in
            //In this version we'll accept all drivers automatically


            
            //Valida el estado de aprobación de la cuenta del usuario
//            if user.approved {
//                //Si la cuenta está aprobada el controlador cargará las configuraciones respectivas
//                self.loadContent()
//            } else {
//                //Si la cuenta no está aprobada, mostrará la pantalla de Bienvenida
//                self.stopLoader()
//                guard let welcomeVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "WelcomeUserViewController") as? WelcomeUserViewController else {
//                    return
//                }
//                //Mostramos la pantalla de bienvenida desde el navigationcontroller base para que esconda todos los elementos de la navegación interna del aplicativo
//                self.navigationController?.parent?.navigationController?.pushViewController(welcomeVC, animated: true)
//            }

            self.loadContent()
        }) { (error) in
            self.showAlert(message: error.localizedDescription)
        }
    }

    func paymentMethodIsValid(_ paymentMethod: PaymentMethod, withValidationMessage message: String? = nil) -> Bool {
        //Revisa si el método de pago es válido
        switch paymentMethod {
        case .cash:
            return true
        case .creditCard:
        //Si el método de pago es tarjeta, entonces valida si tiene tarjetas guardadas
            let thereAreCards = self.realm.objects(TokenizedCard.self).count != 0
        //Si no hay tarjetas, mostramos el mensaje de validación en caso haya
            if !thereAreCards, let message = message {
                self.showAlert(message: message)
            }
            return thereAreCards
        }
    }

    func configureContent() {
        setNeedsStatusBarAppearanceUpdate()

        addressesView.isHidden = true
        
        //Configuración del tableview
      //  topOriginAddressTextField.isEnabled = false
        topOriginAddressTextField.delegate = self
        topOriginAddressTextField.addTarget(self, action: #selector(searchAddress(_:)), for: .editingChanged)
        originAddressTextField.delegate = self
        originAddressTextField.addTarget(self, action: #selector(searchAddress(_:)), for: .editingChanged)
        
       // topDestinationAddressTextField.isEnabled = false
        destinationAddressTextField.delegate = self
        destinationAddressTextField.addTarget(self, action: #selector(searchAddress(_:)), for: .editingChanged)
        topDestinationAddressTextField.delegate = self
        topDestinationAddressTextField.addTarget(self, action: #selector(searchAddress(_:)), for: .editingChanged)
        
            //Configuramos collectionview
//        paymentMethodsCollectionView.delegate = self
//        paymentMethodsCollectionView.dataSource = self
//        paymentMethodsCollectionView.allowsMultipleSelection = false
      //  let imageWithTextCollectionViewCellNib = UINib(nibName: collectionViewCellIdentifier, bundle: nil)
//        paymentMethodsCollectionView.register(imageWithTextCollectionViewCellNib, forCellWithReuseIdentifier: collectionViewCellIdentifier)
        
        //Configuraciones del mapa
        configureMapComponents()
        configureMyLocationButtonPosition()
        
        //Configuramos los gesturercognizers al cardview del viaje
        configureUserOnTripCardView()
        
        self.view.layoutIfNeeded()
    }
    
    func loadContent() {
        setUserData()
        
        moveToCurrentLocation()
        
        ride = self.realm.objects(Ride.self).first
        
        if let ride = self.ride {
            try! realm.write {
                ride.shown = false
            }
            
            self.setRideData()
            self.startLoader()
            MainManager.sharedManager.rideReconnection(successResponse: { (data, driverOnRide, _, _) in
                self.stopLoader()
                try? self.realm.write {
                    self.ride?.stateString = data["status"]?.stringValue ?? ""
                    self.ride?.state = RideState.getStatusFromString(self.ride?.stateString)?.rawValue ?? -1
                    self.ride?.destinationAddress = data["destination_address"]?.stringValue ?? ""
                    self.ride?.destinationLat = data["destination_lat"]?.stringValue.double ?? 0.0
                    self.ride?.destinationLong = data["destination_long"]?.stringValue.double ?? 0.0
                    self.ride?.originAddress = data["origin_address"]?.stringValue ?? ""
                    self.ride?.originLat = data["origin_lat"]?.stringValue.double ?? 0.0
                    self.ride?.originLong = data["origin_long"]?.stringValue.double ?? 0.0
                    if let driver = driverOnRide as? DriverOnRide, self.ride?.driverOnRide == nil {
                        self.ride?.driverOnRide = driver
                    }
                    self.ride?.nearToOrigin = self.ride?.rideState != .onWay
                }
                
                if self.ride?.rideState == .waiting {
                    let current = CLLocationCoordinate2D(latitude: data["current_driver_lat"]?.doubleValue ?? 0 , longitude: data["current_driver_long"]?.doubleValue ?? 0)
                    let origin = CLLocationCoordinate2D(latitude: self.ride?.originLat ?? 0, longitude: self.ride?.originLong ?? 0)
                    
                    let shown = self.ride?.shown ?? false

                    self.lastPolylineInfo = PolylineInfo(polyline: "", origin: current, destination: origin, duration: "1 min", driverMoving: true, rotation: 0, animated: shown)
                    
                    DispatchQueue.main.async {
                        if let polylineInfo = self.lastPolylineInfo {
                            self.mapView.drawPolyline(polylineInfo, nearToDestination: true)
                        }
                    }

                }
                print("🚗 RIDE STATUS -> \(self.ride?.stateString ?? "NONE")")
                self.setRideData()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {

            MainManager.sharedManager.rideReconnection(successResponse: { (data, driverOnRide, _, _) in

                self.ride = Ride()

                try? self.realm.write {
                    self.ride?.stateString = data["status"]?.stringValue ?? ""
                    self.ride?.maxTimeToWaitForClient = data["waiting_time"]?.intValue ?? 0
                    self.ride?.startedToWaitAt = data["started_waiting_at"]?.stringValue.toDate(fromFormat: "yyyy-MM-dd HH:mm:ss") ?? Date()

                    self.ride?.paymentType = data["payment_type"]?.stringValue ?? ""

//                    if data["payment_type"]?.stringValue ?? "" == "card" {
//                        self.selectedPaymentMethod = .creditCard
//                        self.paymentMethodsCollectionView.reloadData()
//                    }

                    self.ride?.rideId = data["ride_id"]?.intValue ?? 0
                    self.ride?.state = RideState.getStatusFromString(self.ride?.stateString)?.rawValue ?? -1
                    self.ride?.destinationAddress = data["destination_address"]?.stringValue ?? ""
                    self.ride?.destinationLat = data["destination_lat"]?.stringValue.double ?? 0.0
                    self.ride?.destinationLong = data["destination_long"]?.stringValue.double ?? 0.0
                    self.ride?.originAddress = data["origin_address"]?.stringValue ?? ""
                    // self.ride?.clientSocketId = data["client_socket_id"]?.stringValue ?? ""
                    //self.ride?.driverSocketId = data["driver_socket_id"]?.stringValue ?? ""
                    self.ride?.chatId = data["chat_id"]?.stringValue ?? ""
                    self.ride?.originLat = data["origin_lat"]?.stringValue.double ?? 0.0
                    self.ride?.originLong = data["origin_long"]?.stringValue.double ?? 0.0
                    if let driver = driverOnRide as? DriverOnRide, self.ride?.driverOnRide == nil {
                        self.ride?.driverOnRide = driver
                    }
                    self.ride?.shown = false
                    self.ride?.nearToOrigin = self.ride?.rideState != .onWay
                }

                DispatchQueue.main.async {
                    self.timerContainerBottom.constant = self.userOnTripCardView.frame.size.height < 270 ? self.userOnTripCardView.frame.size.height < 263  ? self.userOnTripCardView.frame.size.height * 1.95 + 10 : self.userOnTripCardView.frame.size.height * 1.75 : self.userOnTripCardView.frame.size.height * 1.65 + 10
                }

                try? self.realm.write {
                    self.realm.add(self.ride ?? Ride())
                }

                DispatchQueue.main.async {
                    SocketIOManager.sharedInstance.checkIfOnRide()
                }

                print("🚗 RIDE STATUS -> \(self.ride?.stateString ?? "NONE")")
                self.setRideData()
            }) { (error) in
                self.showAlert(message: error.localizedDescription)
            }

        }
    }
    
    func setUserData() {
    }
    
    func setRideData() {
        self.setDriverData()
        self.rideState = self.ride?.rideState
        
        if self.rideState == .finished {
            LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Viaje finalizado", message: "", automaticDismiss: true, delegate: nil)
            let gestureRecognizer = UISwipeGestureRecognizer()
            gestureRecognizer.direction = .down
            self.changeUserOnTripCardView(gestureRecognizer)
        }
    }
    
    func showSearchAddressViewContainer(_ show: Bool = true) {
        if show { searchAddressViewController.loadContent() }
        let finalHeight = -self.view.frame.height
        guard let coordinateToSearch = coordinateToSearch else { return }
        self.searchAddressViewController.addressType = coordinateToSearch
        self.searchAddressViewContainer.layoutIfNeeded()
        UIView.transition(with: searchAddressViewContainer, duration: 0.5, options: [.transitionCrossDissolve], animations: {
            if self.coordinateToSearch == .origin {
                self.originAddressContainerHeight.constant = 55
                self.destinationAddressContainerHeight.constant = 0
                self.originActionButton.setImage(show ? #imageLiteral(resourceName: "ic_reject") : #imageLiteral(resourceName: "ic_favorite"), for: .normal)
                self.originActionButton.tag = show ? 1 : 0
                self.originActionButton.isHidden = false
            } else {
                self.originAddressContainerHeight.constant = self.ride?.nearToOrigin == true ? 0 : 55
                self.destinationAddressContainerHeight.constant = 55
                self.destinationActionButton.setImage(show ? #imageLiteral(resourceName: "ic_reject") : #imageLiteral(resourceName: "ic_favorite"), for: .normal)
                self.destinationActionButton.tag = show ? 2 : 0
                self.destinationActionButton.isHidden = false
                self.searchAddress(self.destinationAddressTextField)
            }
            self.searchAddressViewController.searchAddressViewHeight.constant = self.coordinateToSearch == .origin || self.ride?.nearToOrigin == true ? 55 : 110
            self.dotsImageView.alpha = self.coordinateToSearch == .origin ? 0 : 1
            self.addressesViewTop.constant = show ? 85 : 70
            self.searchAddressViewContainer.alpha = show ? 1 : 0
            self.searchAddressViewContainerBottom.constant = show ? 0 : finalHeight
            self.searchAddressViewContainer.layoutIfNeeded()
            self.tabBarController?.tabBar.alpha = show ? 0 : 1
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        self.addressesView.isHidden = show ? false : true

       // originActionButton.isHidden = show ? true : false
       // destinationActionButton.isHidden = show ? true : false
    }
    
    func showCoordinateSearchComponents(_ show: Bool = true) {
        if !show { coordinateToSearch = .none }
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            UIView.transition(with: self.confirmCoordinateButton, duration: 0.5, options: [], animations: {
                if self.ride?.rideState != .none {
                    self.userOnTripCardView.transform = show ? .identity : CGAffineTransform(translationX: 0, y: -416)
                }
                self.pinAddressImageView.alpha = show ? 1 : 0
//                self.confirmCoordinateButton.transform = show ? CGAffineTransform(translationX: 0, y: -180) : .identity
                self.view.layoutIfNeeded()
            }) { _ in
                self.configureMyLocationButtonPosition()
            }
        }
    }
    
    func configureUserOnTripCardView() {
        let upSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(changeUserOnTripCardView(_:)))
        upSwipeGestureRecognizer.direction = .up

        let downSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(changeUserOnTripCardView(_:)))
        downSwipeGestureRecognizer.direction = .down

        userOnTripCardView.addGestureRecognizer(upSwipeGestureRecognizer)
        userOnTripCardView.addGestureRecognizer(downSwipeGestureRecognizer)
    }
    
    @objc func changeUserOnTripCardView(_ gesture: UISwipeGestureRecognizer) {
        if (rideState != .finished &&
            ((gesture.direction == .up && carDetailsViewHeight.constant == 0) ||
                (gesture.direction == .down && carDetailsViewHeight.constant == 50)))
            || (rideState == .finished || gesture.direction == .down) {
            UIView.transition(with: userOnTripCardView, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                let show = gesture.direction == .up
                guard let rideState = self.rideState else { return }
                switch rideState {
                case .onRide:
                    self.rideStateLabelHeight.constant = show ? 20 : 0
                    self.carDetailsViewHeight.constant = show ? 50 : 0
                    self.shareAndCancelButtonsContainerHeight.constant = show ? self.buttonDefaultHeight : 0
                    self.contactAndCancelButtonsContainerHeight.constant = 0
                    self.paymentDetailContainerHeight.constant = 0
                case .finished:
                    self.rideStateLabelHeight.constant = 0
                    self.carDetailsViewHeight.constant = 0
                    self.shareAndCancelButtonsContainerHeight.constant = 0
                    self.contactAndCancelButtonsContainerHeight.constant = 0
                default:
                    self.rideStateLabelHeight.constant = 0
                    self.carDetailsViewHeight.constant = show ? 50 : 0
                    self.shareAndCancelButtonsContainerHeight.constant = 0
                    self.contactAndCancelButtonsContainerHeight.constant = show ? self.buttonDefaultHeight : 0
                    self.paymentDetailContainerHeight.constant = 0
                }
                
                self.carDetailsView.layoutIfNeeded()
                self.configureMyLocationButtonPosition()
                self.userOnTripCardView.layoutIfNeeded()
                self.mapView.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        
        if rideState == .finished && gesture.direction == .down {
            self.clearRideData()
            rideState = .none
        }
    }
    
    func getTripRoute() {
        guard let ride = ride else { return }
        
        //Validamos que se haya seleccionado una dirección de destino
        if ride.destinationAddress == "" {
            self.showAlert(message: "Por favor seleccione la dirección de destino", okayHandler: { (alert) in
                self.destinationAddressTextField.becomeFirstResponder()
                self.topDestinationAddressTextField.becomeFirstResponder()
            })
            return
        }
        
        //Validamos que el viaje no haya iniciado (que no tenga ningún estado)
        if ride.rideState == .none {
            self.startLoader()
            ClientManager.sharedManager.getTripRoute(tripRequest: ride, successResponse: { (polyLine, estimatedTime, estimatedDistance, serviceTypes, originLong, originLat, destinationLong, destinationLat) in
                try! self.realm.write {
                    self.ride?.requested = true
                    self.ride?.originLat = originLat
                    self.ride?.originLong = originLong
                    self.ride?.destinationLat = destinationLat
                    self.ride?.destinationLong = destinationLong
                    self.ride?.estimatedDistance = estimatedDistance
                    self.ride?.requestedAt = Date()
                }
                
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.lastPolylineInfo = PolylineInfo(polyline: polyLine, origin: ride.originCoordinate, destination: ride.destinationCoordinate)
                    UIView.transition(with: self.confirmCoordinateButton, duration: 0.5, options: [], animations: {
                        self.pinAddressImageView.alpha = 0
                        self.confirmCoordinateButton.transform = .identity
                        self.configureMyLocationButtonPosition()
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
                
                self.view.layoutIfNeeded()
                UIView.animate(withDuration: 0.5, animations: {
                    self.cardViewContinueTripBottom.constant = -600
                    self.view.layoutIfNeeded()
                }, completion: {_ in
                    UIView.animate(withDuration: 0.5, animations: {
                        self.cardViewRequestBottom.constant = 20
                        self.addressesView.isHidden = false
                        self.sideMenuButton.tag = 1
                        self.topDestinationAddressTextField.text = self.destinationAddress
                        self.sideMenuButton.setImage(UIImage(named: "ic_back"), for: .normal)
                        self.topOriginAddressTextField.isEnabled = false
                        self.topDestinationAddressTextField.isEnabled = false
                        self.view.layoutIfNeeded()
                    })
                })
                self.serviceTypes = serviceTypes
                //ChooseTypeOfServiceViewController.instantiate(inViewController: self, serviceTypes: serviceTypes)
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        } else {
            self.startLoader()
            ClientManager.sharedManager.changeRoute(ride: ride, successResponse: { (message) in
                self.stopLoader()
                self.quitSearchingTripCoordinates()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    func quitSearchingTripCoordinates() {
        coordinateToSearch = .none
        showCoordinateSearchComponents(false)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            self.cardViewContinueTripBottom.constant = 0
            self.view.layoutIfNeeded()
        }, completion: {_ in
            UIView.animate(withDuration: 0.5, animations: {
                self.cardViewRequestBottom.constant = -100
                self.addressesView.isHidden = true
                self.sideMenuButton.tag = 0
                self.sideMenuButton.setImage(UIImage(named: "ic_menu"), for: .normal)
                self.moveToCurrentLocation()
                self.view.layoutIfNeeded()
            })
        })
    }
    
    //MARK: - MapFunctions
    func configureMapComponents() {
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.isBuildingsEnabled = false
        mapView.settings.rotateGestures = false
        mapView.settings.tiltGestures = false
        mapView.settings.myLocationButton = false
        myLocationButton.isHidden = true
        
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
        let timerApp = Date().toText(withFormat: "hh:mm a")
        
        if timerApp ?? "" >= "07:00 PM"{
            do {
                if let styleURL = Bundle.main.url(forResource: "map_style_dark", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
        }else{
            do {
                if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
        }
        
        
    }
    
    func configureMyLocationButtonPosition() {
        self.view.layoutIfNeeded()
        
        guard let tabBar = self.navigationController?.tabBarController?.tabBar else { return }
        let minY = [tabBar.frame.origin.y, userOnTripCardView.frame.origin.y, confirmCoordinateButton.frame.origin.y].min() ?? 0.0
        let bottom = self.view.frame.height - (minY + tabBar.frame.height) + 20
        
        DispatchQueue.main.async {
            UIView.transition(with: self.myLocationButton, duration: 0.25, options: [], animations: {
                print("CONFIGURE MY LOCATION BUTTON")
                self.mapView.padding = UIEdgeInsets(top: 0, left: 30, bottom: bottom, right: 30)
                self.myLocationButtonMaxY.constant = minY - 25
                self.mapView.layoutIfNeeded()
                self.view.layoutIfNeeded()
                self.topOriginAddressTextField.text = self.originAddress
                self.originAddressTextField.text = self.originAddress
            }, completion: nil)
        }
    }
    
    @objc func searchAddress(_ textField: UITextField) {
        addressesView.isHidden = false
        guard let addressInput = textField.text else { return }
        let placesClient = GMSPlacesClient()
        let filter = GMSAutocompleteFilter()
        filter.country = "pe"
        var addressResults: [Address] = []
      
        placesClient.autocompleteQuery(addressInput, bounds: nil, filter: filter) { (results, error: Error?) in
            guard let results = results else {
                print("ERROR: \(error?.localizedDescription ?? "")")
                self.showAlert(message: error?.localizedDescription ?? "ERROR!")
                return
            }
            
            results.forEach { result in
                let addressString = result.attributedFullText.string//.replacingOccurrences(of: ", Perú", with: "")
                let address = Address()
                address.placeId = result.placeID
                address.address = addressString
                addressResults.append(address)
            }
            self.searchAddressViewController.addressResults = addressResults
        }
    }
    
    func getLocation(for coordinate: CLLocationCoordinate2D) {
        print("COORDINATE IN MAP -> \(coordinate.latitude), \(coordinate.longitude)")
        MainManager.sharedManager.getAddressName(coordinate: coordinate, successResponse: { (addressName) in
            if let coordinateToSearch = self.coordinateToSearch {
                let topTextField = coordinateToSearch == .origin ? self.topOriginAddressTextField : self.topDestinationAddressTextField
                let textField = coordinateToSearch == .origin ? self.originAddressTextField : self.destinationAddressTextField
                let actionButton = coordinateToSearch == .origin ? self.originActionButton : self.destinationActionButton
                if coordinateToSearch == .origin {
                    self.originAddress = addressName
                }
                DispatchQueue.main.async {
                    topTextField?.text = addressName
                    textField?.text = addressName
                    
                    self.enableConfirmationButton()
                    
                    let favoriteAddresses = Array(self.realm.objects(Address.self).filter("isFavorite == true"))
                    let isFavorite = favoriteAddresses.contains(where: { $0.address == textField?.text })
                    actionButton?.setImage( isFavorite ? #imageLiteral(resourceName: "ic_favorites") : #imageLiteral(resourceName: "ic_favorite"), for: .normal)
                }
            } else {
                self.originAddress = addressName
            }
        }) { (_) in () }
    }
    
    func setSocketEvents() {
        if self.ride == nil {
            self.socket.off(SocketEventsOn.cancelledClientRide)
            self.socket.off(SocketEventsOn.notFoundClientRide)
            self.socket.off(SocketEventsOn.driverArrivingToOriginPoint)
            self.socket.off(SocketEventsOn.clientDriverAtOriginPoint)
            self.socket.off(SocketEventsOn.clientOnTrip)
            self.socket.off(SocketEventsOn.clientStartingRide)
            self.socket.off(SocketEventsOn.driverCancelRideInProgress)
            self.socket.off(SocketEventsOn.rideCancelledByDriverForClientReasons)
            self.socket.off(SocketEventsOn.clientEndRideDetail)
            self.socket.on(SocketEventsOn.closeDrivers) { (dataArray, ack) in
                let text = "arrived"
                self.mapView.clear()
                let dictionary = (dataArray.first as? String)?.arrayDictionary
                if let carsDataArray = dictionary {
                    DispatchQueue.main.async {
                        carsDataArray.forEach {
                            let car = CarArround(data: $0)
                            car.setMarkerInMap(map: self.mapView)
                        }
                    }
                } else {
                    self.view.makeToast(text)
                    print("🧵ON_responseDriversClosestToClient\n\(dataArray)\n_🧵")
                }
            }
            return
        }
        self.socket.off(SocketEventsOn.closeDrivers)
        self.socket.on(SocketEventsOn.cancelledClientRide) { (dataArray, ack) in
            let message = dataArray.first as? String ?? "Realizando búsqueda de conductor"
            self.showLoaderScreen(title: "Buscando conductor", message: message)
            self.rideState = .none
            print("🧵ON_notifyCancelledClientRide\n\(dataArray)\n_🧵")
        }
        self.socket.on(SocketEventsOn.notFoundClientRide) { (dataArray, ack) in
            let message = dataArray.first as? String ?? "Realizando búsqueda de conductor"
            self.loaderVC?.dismissWithCompletion(completion: {
                self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "VIAJE CANCELADO", message: message, automaticDismiss: true, delegate: self)
                self.clearRideData()
            })
            print("🧵ON_notifyNotFoundClientRide\n\(dataArray)\n_🧵")
        }
        
        //DRIVER EN CAMINO
        self.socket.on(SocketEventsOn.driverArrivingToOriginPoint) { (dataArray, ack) in
            if self.rideState != .onWay {
                self.rideState = .onWay
            }
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let polyline = data["poly_line"] as? String, let currentLat = data.double(key: "current_lat"), let currentLong = data.double(key: "current_long"), let originLat = data.double(key: "origin_lat"), let originLong = data.double(key: "origin_long"), let ride = self.ride {
                print(polyline)
                let current = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
                let origin = CLLocationCoordinate2D(latitude: originLat, longitude: originLong)
                let newDriverOnRide = DriverOnRide(data: data)
                try! self.realm.write {
                    ride.chatId = data["chat_id"] as? String ?? ""
                    if ride.driverOnRide?.name != newDriverOnRide.name {
                        if ride.driverOnRide != nil {
                            let drivers = self.realm.objects(DriverOnRide.self)
                            self.realm.delete(drivers)
                            ride.driverOnRide = newDriverOnRide
                        }else {
                            ride.driverOnRide = newDriverOnRide
                        }
                        //ride.driverOnRide = newDriverOnRide
                        self.setDriverData()
                    }
                }
                let shown = self.ride?.shown ?? false
                self.lastPolylineInfo = PolylineInfo(polyline: polyline, origin: current, destination: origin, duration: data["duration"] as? String, driverMoving: true, rotation: data.double(key: "rotation"), animated: !shown)
                if !shown {
                    try! self.realm.write {
                        self.ride?.shown = true
                    }
                }
            }
            print("🧵ON_driverArrivingToOriginPoint\n\(dataArray)\n_🧵")
        }
        
        //DRIVER EN EL PUNTO DE ORIGEN (EN ESPERA)
        self.socket.on(SocketEventsOn.clientDriverAtOriginPoint) { (dataArray, ack) in
            DispatchQueue.main.async {
                let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary

                var maxTimeToWaitForClient = 0

                if let data = dictionary, let waitingTime = data["waiting_time"] as? String {
                    maxTimeToWaitForClient = Int(waitingTime) ?? 0
                }
                self.originAddressContainerHeight.constant = 0
                self.destinationAddressContainerHeight.constant = 55
                try! self.realm.write {
                    self.ride?.nearToOrigin = true
                    self.ride?.startedToWaitAt = Date()
                    self.ride?.maxTimeToWaitForClient = maxTimeToWaitForClient
                }
                self.showWaitingTimer()
                self.view.layoutIfNeeded()
                self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "El conductor te está esperando", automaticDismiss: true, delegate: self)
                if let polylineInfo = self.lastPolylineInfo {
                    self.mapView.drawPolyline(polylineInfo, nearToDestination: true)
                }
            }
            print("🧵ON_notifyClientDriverAtOriginPoint\n\(dataArray)\n_🧵")
        }
        
        self.socket.on(SocketEventsOn.clientOnTrip) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let polyline = data["poly_line"] as? String, let currentLat = data.double(key: "current_lat"), let currentLong = data.double(key: "current_long"), let destinationLat = data.double(key: "destination_lat"), let destinationLong = data.double(key: "destination_long"), let duration = data["time_to_arrive"] as? String, let distance = data["distance"] as? String {
                print(polyline)
                self.resetTimerClientTimer()
                self.durationLabel.text = duration
                self.distanceLabel.text = distance
                let current = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
                let destination = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong)
                let shown = self.ride?.shown ?? false
                self.lastPolylineInfo = PolylineInfo(polyline: polyline, origin: current, destination: destination, driverMoving: true, rotation: data.double(key: "rotation"), animated: !shown)
                if !shown {
                    try! self.realm.write {
                        self.ride?.shown = true
                    }
                }
            }
            print("🧵ON_clientOnTrip\n\(dataArray)\n_🧵")
        }
        //SEE THIS
        self.socket.on(SocketEventsOn.clientStartingRide) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let message = data["message"] as? String {
                defer { self.rideState = .onRide }
                self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: message, automaticDismiss: true, delegate: self)
            }
            print("🧵ON_clientStartingRide\n\(dataArray)\n_🧵")
        }
        
        self.socket.on(SocketEventsOn.driverCancelRideInProgress) { (dataArray, ack) in
            if let data = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary, let ride = self.ride {
                defer {
                    let paymentDetail = PaymentDetail(dictionary: data, rideId: ride.rideId)
                    self.paymentDetailView.paymentDetail = paymentDetail
                    self.rideState = .finished
                }
                self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Viaje cancelado", automaticDismiss: true, delegate: self)
            }
            print("🧵ON_notifyDriverCancelRideInProgress\n\(dataArray)\n_🧵")
        }
        
        self.socket.on(SocketEventsOn.rideCancelledByDriverForClientReasons) { (dataArray, ack) in
            let dictionary = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary
            if let data = dictionary, let message = data["message"] as? String {
                self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Viaje cancelado", message: message, automaticDismiss: true, delegate: self)
                self.rideState = .finished
                self.clearRideData()
            }
            print("🧵ON_rideCancelledByDriverForClientReasons\n\(dataArray)\n_🧵")
        }
        
        self.socket.on(SocketEventsOn.clientEndRideDetail) { (dataArray, ack) in
            self.loaderVC = LoaderViewController.instantiate(in: self.parent?.parent ?? self, title: "Finalizando...", message: "En unos segundos llegarás a tu destino", automaticDismiss: true, delegate: self)
            if let data = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary, let ride = self.ride {
                let paymentDetail = PaymentDetail(dictionary: data, rideId: ride.rideId)
                self.paymentDetailView.paymentDetail = paymentDetail
                self.rideState = .finished
                print("Payment Detail: \(paymentDetail)")
            }
            print("🧵ON_notifyClientEndRideDetail\n\(dataArray)\n_🧵")
        }

        self.socket.on(SocketEventsOn.clientEndRidePaymentDetail) { (dataArray, ack) in
                if let data = dataArray.first as? [String: Any] ?? (dataArray.first as? String)?.dictionary, let paymentData = data["payment_data"] as? [String: Any] {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
                            FinishedServiceViewController.instantiate(in: self, delegate: self, paymentData: paymentData)
                        })

                    }
                }
        
    }

    func showWaitingTimer(_ show: Bool = true) {
        self.timerContainer.alpha = show ? 1 : 0
        self.timerContainer.transform = show ? .identity : CGAffineTransform(scaleX: 0.5, y: 0.5)

        //self.timerContainerBottom.constant = self.userOnTripCardView.frame.size.height * 1.65

        self.timerContainerBottom.constant = userOnTripCardView.frame.size.height < 270 ? userOnTripCardView.frame.size.height < 263  ? userOnTripCardView.frame.size.height * 1.95 + 10 : userOnTripCardView.frame.size.height * 1.75 : userOnTripCardView.frame.size.height * 1.65 + 10
        show ? self.startWaitingForClientTimer() : ()
    }

        func startWaitingForClientTimer() {
            var bgTask = UIBackgroundTaskIdentifier(rawValue: 0)
            bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                UIApplication.shared.endBackgroundTask(bgTask)
            })
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setWaitingForClientTimer), userInfo: nil, repeats: true)
            guard let timer = timer else { return }
            RunLoop.current.add(timer, forMode: RunLoop.Mode.default)
        }

        @objc func setWaitingForClientTimer() {
            if self.ride?.isInvalidated ?? false {
                self.ride = Ride()
            }
            guard let ride = self.ride else { return }

            let time = Calendar.current.dateComponents([.second], from: ride.startedToWaitAt, to: Date()).second ?? 0
            if time > ride.maxTimeToWaitForClient { timerContainer.backgroundColor = Colors.error }
            self.timerLabel.text = abs(time).secondsToTime()

        }

    func resetTimerClientTimer() {
        self.timer?.invalidate()
        self.timerContainer.backgroundColor = Colors.primary
        self.timerLabel.text = "--:--"
        self.showWaitingTimer(false)
    }
    
    func setDriverData() {
        guard let driver = ride?.driverOnRide else { return }
        self.driverImageView.sd_setImage(with: driver.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_empty_user"), options: [], completed: nil)
        self.driverNameLabel.text = driver.name.withDefault("-----")
        self.driverServiceTypeLabel.text = driver.serviceType.withDefault("---")
        self.driverRatingLabel.text = "Rating: \(driver.rating.double.oneDecimalString)"
        self.driverRatingView.rating = driver.rating.double
        self.driverCarPlateNumberAndColorLabel.text = "\(driver.carPlateNumber), \(driver.carColor.withDefault("---"))"
        self.driverCarBrandLabel.text = driver.carBrand.withDefault("-----")
    }
    
    func showLoaderScreen(title: String, message: String) {
        guard let baseVC = self.parent?.parent else { return }
        loaderVC = LoaderViewController.instantiate(in: baseVC, title: title, message: message, confirmationTitle: "CANCELAR SERVICIO", delegate: self)
    }
    
    func enableConfirmationButton(_ enable: Bool = true) {
        self.confirmCoordinateButton.alpha = enable ? 1 : 0.5
        self.confirmCoordinateButton.isEnabled = enable
    }
    
    func cancelRide(reason: String, comment: String?, completion: (() -> Void)? = nil) {
        guard let ride = ride else { return }
        self.startLoader()
        MainManager.sharedManager.cancelRide(ignored: false, reason: reason, rideId: ride.rideId, comment: comment, successResponse: { (paymentDetail) in
            self.stopLoader()
            guard let paymentDetail = paymentDetail else {
                self.rideState = .finished
                self.clearRideData()
                return
            }
            self.paymentDetailView.paymentDetail = paymentDetail
            self.rideState = .finished
            completion?()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func checkIfNeedsToRateUser() {
        self.startLoader()
        MainManager.sharedManager.getLastUserToRate(successResponse: { (userToRate) in
            self.stopLoader()
            if userToRate.rideId != 0 && userToRate.userId != 0 {
                RatingViewController.instantiate(in: self.parent?.parent ?? self, userToRate: userToRate, description: "Califica al cliente")
            }
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    //MARK: -IBActions
    @IBAction func goBonuses() {
        //Muestra la pantalla de promociones
        guard let mainNavVC = self.navigationController?.parent else { return }
        BonusViewController.instantiate(in: mainNavVC)
    }
    
    ///Solicitar viaje
    @IBAction func requestTrip() {
        sideMenuButton.tag = 2
        ChooseTypeOfServiceViewController.instantiate(inViewController: self, serviceTypes: serviceTypes)
    }
    
    ///Agregar una dirección a favoritos
    @IBAction func setFavoriteAddress(_ sender: CustomButton) {
        if sender.tag == 0 {
            //Valida si se está buscando una coordenada (ya sea para origen o destino de viaje)
            guard let coordinateToSearch = coordinateToSearch, let originAddress = originAddressTextField.text, let destinationAddress = destinationAddressTextField.text else { return }
            let address = coordinateToSearch == .origin ? originAddress : destinationAddress
            guard address != "" else {
                self.showAlert(message: "No se pudo obtener la dirección para agregar a favoritos")
                return
            }
            //Muestra la pantalla para agregar a favoritos
            AddFavoritesViewController.instantiate(in: self, address: address, name: "", coordinate: self.mapView.camera.target)
        } else if sender.tag == 1 {
            self.topOriginAddressTextField.text = ""
            self.originAddressTextField.text = ""
            self.originActionButton.isHidden = true
            self.searchAddress(originAddressTextField)
        } else if sender.tag == 2 {
            self.topDestinationAddressTextField.text = ""
            self.destinationAddressTextField.text = ""
            self.destinationActionButton.isHidden = true
            self.showSearchAddressViewContainer()
            self.searchAddress(destinationAddressTextField)

        }
    }
    
    ///Confirmar coordenada en búsqueda (ya sea coordenada de origen o destino)
    @IBAction func confirmCoordinate() {
        
        //Validamos que existe una coordenada de búsqueda
        guard let coordinateToSearch = coordinateToSearch, let ride = ride, let originAddress = originAddressTextField.text, let destinationAddress = destinationAddressTextField.text else { return }
        self.originAddress = originAddress
        self.destinationAddress = destinationAddress

        switch coordinateToSearch {
        case .origin:
            //Seteamos la dirección y coordenadas de origen al objeto ride
            try! realm.write {
                ride.originAddress = originAddress
            }
            ride.setOriginCoordinate(mapView.projection.coordinate(for: mapView.center))
            //Hacemos que el textfield de destino se active para proceder a buscar la coordenada de destino
            topDestinationAddressTextField.becomeFirstResponder()
            destinationAddressTextField.becomeFirstResponder()
        case .destination:
            //Seteamos la dirección y coordenadas de destino al objeto ride
            try! realm.write {
                ride.destinationAddress = destinationAddress
            }
            ride.setDestinationCoordinate(mapView.projection.coordinate(for: mapView.center))

            if ride.originAddress == "" && ride.rideState == .none, let currentLocation = currentLocation {
                //En caso no se haya especificado la dirección de origen y se sabe la ubicación actual, consultamos a google para que nos retorne el nombre de la dirección de la ubicación actual
                MainManager.sharedManager.getAddressName(coordinate: currentLocation, successResponse: { (addressName) in
                    try! self.realm.write {
                        self.ride?.originAddress = addressName
                        self.ride?.originLat = currentLocation.latitude
                        self.ride?.originLong = currentLocation.longitude
                    }
                    self.isConfirmation = true
                    //Se procede a consultar la ruta y tipos de servicio
                    self.getTripRoute()
                }) { (_) in () }
            } else {
                //En caso si se haya especificado la dirección de origen se procede a consultar la ruta y tipos de servicio
                self.isConfirmation = true
                getTripRoute()
            }
        }
    }
    
    @IBAction func moveToCurrentLocation() {
        if locationPermissionsEnabled {
            locationManager.stopUpdatingLocation()
            movingFromLocationButton = true
            locationManager.startUpdatingLocation()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:  {
                self.topOriginAddressTextField.text = self.originAddress
                self.originAddressTextField.text = self.originAddress
            })
        }
    }
    
    @IBAction func callDriver() {
        call(phoneNumber: self.ride?.driverOnRide?.phone)
    }

    @IBAction func toggleSideMenu(_ sender: UIButton) {
        if sender.tag == 0 {
            guard let mainContainerVC = self.parent?.parent as? MainContainerViewController else {
                return
            }
            mainContainerVC.toggleSideMenu()
        }else if sender.tag == 1{
            self.clearRideData()
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                self.cardViewContinueTripBottom.constant = 0
                self.view.layoutIfNeeded()
            }, completion: {_ in
                UIView.animate(withDuration: 0.5, animations: {
                    self.cardViewRequestBottom.constant = -100
                    self.addressesView.isHidden = true
                    self.sideMenuButton.tag = 0
                    self.sideMenuButton.setImage(UIImage(named: "ic_menu"), for: .normal)
                    self.view.layoutIfNeeded()
                })
            })
        }else if sender.tag == 2 {
            if let vc = chooseTypeOfServiceVC {
                vc.dismiss()
                sender.tag = 1
               //self.isConfirmation = false
               // showCoordinateSearchComponents()
            }
        }
    }

    @IBAction func messageDriver() {
        guard let mainVC = self.navigationController?.parent, let chatId = ride?.chatId else { return }
        ChatViewController.instantiate(inViewController: mainVC, ride: ride, username: ride?.driverOnRide?.name ?? "")
    }
    
    @IBAction func sendSOS() {
        ConfirmationViewController.instantiante(inViewController: self, withTitle: ConfirmationTitles.SOS, message: "", image: #imageLiteral(resourceName: "img_sos"), withAnimation: true, imageHeight: -1, andConfirmText: "LLAMAR A LA CENTRAL") { _, confirmed in
            guard confirmed else { return }
            self.call(phoneNumber: App.sosPhone)
        }
        
        guard let coordinate = self.currentLocation, let ride = self.ride else { return }
        MainManager.sharedManager.sendSOS(currentCoordinate: coordinate, ride: ride,successResponse: { (message) in
            print("🆘 \(message) 🆘")
        }) { (error) in
            print("🆘 ERROR -> \(error.localizedDescription) 🆘")
        }
    }
    
    @IBAction func dismissCardView() {
        let gestureRecognizer = UISwipeGestureRecognizer()
        gestureRecognizer.direction = .down
        self.changeUserOnTripCardView(gestureRecognizer)
        //CONSULTAMOS EL USUARIO ACTUALIZADO PARA TENER EL MONTO PROMOCIONAL ACTUAL
        getUpdatedUser()
    }
    
    @IBAction func showCancelRideReasons(_ sender: UIButton) {
        var cancelReasons: [OptionToSelect] = []
        guard let rideState = rideState else { return }
        switch rideState {
        case .onWay:
            cancelReasons = OptionToSelect.clientWaitingCancelReasons()
        case .waiting:
            cancelReasons = OptionToSelect.clientWaitingCancelReasons()
        case .onRide:
            cancelReasons = OptionToSelect.clientOnRideCancelReasons()
        case .finished:
            return
        }
        let heightScale = 0.2 + (0.09 * CGFloat(cancelReasons.count))
        OptionSelectorViewController.instantiate(inViewController: self.parent?.parent ?? self, delegate: self, heightScale: heightScale, withTitle: "Cancelar servicio", withOptions: cancelReasons, withConfirmButton: true)
    }
    
    @IBAction func shareRide(_ sender: UIButton) {
        self.startLoader()
        MainManager.sharedManager.getLinkToShare(rideId: ride?.rideId ?? 0, successResponse: { (url) in
            self.stopLoader()
            let items = ["Hola!\nTe comparto la ubicación actual de mi \(App.name):\n\(url)"]
            let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @IBAction func clearRideData() {
        self.addressesView.isUserInteractionEnabled = true
        if let vc = chooseTypeOfServiceVC {
            vc.dismiss()
            self.isConfirmation = false
            showCoordinateSearchComponents()
        } else {
            defer { quitSearchingTripCoordinates() }
            
            //Validamos que el viaje haya finalizado para proceder borrar la data
            if rideState == .finished || rideState == .none {
                mapView.clear()
                socket.off(SocketEventsOn.cancelledClientRide)
                socket.off(SocketEventsOn.notFoundClientRide)
                socket.off(SocketEventsOn.driverArrivingToOriginPoint)
                socket.off(SocketEventsOn.clientDriverAtOriginPoint)
                socket.off(SocketEventsOn.rideCancelledByDriverForClientReasons)
                socket.off(SocketEventsOn.clientStartingRide)
                socket.off(SocketEventsOn.clientOnTrip)
                try! realm.write {
                    let rides = realm.objects(Ride.self)
                    let drivers = realm.objects(DriverOnRide.self)
                    realm.delete(rides)
                    realm.delete(drivers)
                }
                if ride != nil {
                    ride = nil
                }
            }
        }
    }
}

//MARK: - MapDelegates

//MARK: GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        myLocationButton.isHidden = movingFromLocationButton ? true : false
        if coordinateToSearch != .none && self.chooseTypeOfServiceVC == nil {
            self.enableConfirmationButton(false)
            //self.isConfirmation = false
            self.view.layoutIfNeeded()
            DispatchQueue.main.async {
                UIView.transition(with: self.pinAddressImageView, duration: 0.5, options: .curveEaseInOut, animations: {
                    self.pinAddressImageView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    self.pinAddressImageView.transform = CGAffineTransform(translationX: 0, y: -15)
                    self.pinAddressShadowImageView.alpha = 1
                }, completion: nil)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("IDLE AT")
        movingFromLocationButton = false

        if let myLocation = mapView.myLocation?.coordinate {
            if !self.isConfirmation {
                self.getLocation(for: myLocation)
            }
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.pinAddressImageView.transform = .identity
            self.pinAddressShadowImageView.alpha = 0
        }) { _ in
            if self.coordinateToSearch != .none && self.chooseTypeOfServiceVC == nil && self.sideMenuButton.tag != 1 {
                
                let coordinate = self.mapView.projection.coordinate(for: self.mapView.center)
                self.getLocation(for: coordinate)
            }
        }
    }
}

//MARK: CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            currentLocation = location.coordinate
            if movingFromLocationButton {
                let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: movingFromLocationButton ? defaultZoom : mapView.camera.zoom)
                mapView.animate(to: camera)
            }
        }
    }
}

//MARK: - SearchAddressViewControllerDelegate
extension MapViewController: SearchAddressViewControllerDelegate {
    func searchAddressViewController(dismiss: Bool) {
        dismissKeyboard()
        showSearchAddressViewContainer(false)
    }
    /*
    func listOfAddressesViewController(didSelectAddress address: Address?) {
        addressesView.isHidden = true
        searchAddressViewController.dismiss()
        guard let location = address?.location else { return }

        DispatchQueue.main.async {
            let cameraPosition = GMSCameraPosition(target: location, zoom: 18, bearing: 0, viewingAngle: 0)
            self.mapView.animate(to: cameraPosition)
            self.mapView.layoutIfNeeded()
        }
    }
     */
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MapViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / CGFloat(PaymentMethod.allCases.count), height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PaymentMethod.allCases.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellIdentifier, for: indexPath) as! ImageWithTextCollectionViewCell
        let paymentMethod = PaymentMethod.allCases[indexPath.row]
        let imageOn = paymentMethod == .cash ? #imageLiteral(resourceName: "ic_cash_on") : #imageLiteral(resourceName: "ic_card_on")
        let imageOff = paymentMethod == .cash ? #imageLiteral(resourceName: "ic_cash_off") : #imageLiteral(resourceName: "ic_card_off")
        cell.imageWithTextSwitch = ImageWithTextSwitch(name: paymentMethod.rawValue, imageOn: imageOn, imageOff: imageOff, colorOn: Colors.primary, colorOff: Colors.background)
        cell.imageWithTextSwitchButton.layer.cornerRadius = 10
        cell.awakeFromNib()

        //Si es el método de pago seleccionado entonces se muestra resaltado
        cell.imageWithTextSwitchButton.isOn = paymentMethod == selectedPaymentMethod ? true : false

        //Valida si el método de pago no es válido para disminuir el alpha
        //cell.contentView.alpha = paymentMethodIsValid(paymentMethod) ? 1.0 : 0.3
        cell.contentView.alpha = 1.0

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageWithTextCollectionViewCell

        //Método de pago seleccionado
        let selectedPaymentMethodToSet = PaymentMethod.allCases.first(where: { $0.rawValue ==
            cell.imageWithTextSwitch.name }) ?? .none

        switch selectedPaymentMethodToSet {
        case .cash:
            //Si es cash obtenemos la tarjeta predeterminada para ponerla como false
            guard let predeterminedCard = self.cards.first(where: { $0.isDefault }) else { return }
            self.startLoader()
            MainManager.sharedManager.setPredeterminedCard(predeterminedCard, predetermined: false, successResponse: { (_) in
                self.stopLoader()
                //Seteamos que el método de pago seleccionado en la variable global del controlador
                try! self.realm.write {
                    self.ride?.paymentType = "cash"
                }

                self.selectedPaymentMethod = selectedPaymentMethodToSet


                collectionView.reloadData()
            }) { (error) in
                self.stopLoader()
                self.showAlert(message: error.localizedDescription)
            }
        case .creditCard:
            //Si el método seleccionado es tarjeta, se redirige a la pantalla de métodos de pago
            if let paymentVC = UIStoryboard(name: "PaymentMethods", bundle: nil).instantiateInitialViewController() as? PaymentMethodsViewController {
                //Se le especifica que al controlador que proviene de la opcion de tipo de pago del viaje
                paymentVC.delegate = self
                self.navigationController?.pushViewController(paymentVC, animated: true)
            }
        default: ()
        }
    }
}

//MARK: - UITextFieldDelegate
extension MapViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        func handleTextFieldResponder() {
            if textField == topOriginAddressTextField || textField == topDestinationAddressTextField {
                coordinateToSearch = textField == topOriginAddressTextField ? .origin : .destination
                if coordinateToSearch == .destination && topOriginAddressTextField.text == "" {
                    topOriginAddressTextField.text = originAddress
                    topOriginAddressTextField.textColor = Colors.primary
                    topOriginAddressTextField.font = Fonts.semibold(18.0)
                    
                    originAddressTextField.text = originAddress
                    originAddressTextField.textColor = Colors.primary
                    originAddressTextField.font = Fonts.semibold(18.0)
                } else {
                    topOriginAddressTextField.text = originAddress
                    topOriginAddressTextField.textColor = Colors.secondary
                    topOriginAddressTextField.font = Fonts.regular(17.0)
                    
                    originAddressTextField.text = originAddress
                    originAddressTextField.textColor = Colors.secondary
                    originAddressTextField.font = Fonts.regular(17.0)
                }
            }else {
                textField.inputView = UIView()
                coordinateToSearch = textField == originAddressTextField ? .origin : .destination
                if coordinateToSearch == .destination && originAddressTextField.text == "" {
                    topOriginAddressTextField.text = originAddress
                    topOriginAddressTextField.textColor = Colors.primary
                    topOriginAddressTextField.font = Fonts.semibold(18.0)
                    
                    originAddressTextField.text = originAddress
                    originAddressTextField.textColor = Colors.primary
                    originAddressTextField.font = Fonts.semibold(18.0)
                } else {
                    topOriginAddressTextField.text = originAddress
                    topOriginAddressTextField.textColor = Colors.secondary
                    topOriginAddressTextField.font = Fonts.regular(17.0)
                    
                    originAddressTextField.text = originAddress
                    originAddressTextField.textColor = Colors.secondary
                    originAddressTextField.font = Fonts.regular(17.0)
                }
            }
            showSearchAddressViewContainer()
            showCoordinateSearchComponents()
        }
        
        if rideState != .none && coordinateToSearch == .none {
            ConfirmationViewController.instantiante(inViewController: self, withTitle: .changeRoute, message: "¿Está seguro de cambiar el lugar de destino? La tarifa será recalculada", andConfirmText: "CONFIRMAR") { _, confirmed in
                guard confirmed else {
                    textField.resignFirstResponder()
                    return
                }
                handleTextFieldResponder()
                textField.becomeFirstResponder()
            }
            return false
        } else {
            handleTextFieldResponder()
            return true
        }
    }
}

extension MapViewController: PaymentMethodsViewControllerDelegate {
    func didSelectCreditCard(_ card: TokenizedCard) {
        selectedPaymentMethod = .creditCard
        //Recargamos el collectionview para que se pinte el método seleccionado
        try! self.realm.write {
            self.ride?.paymentType = "card"
        }
        paymentMethodsCollectionView.reloadData()
    }
}

//MARK: - PannableViewControllerDelegate
extension MapViewController: PannableViewControllerDelegate {
    func pannableViewController(gesture: UIPanGestureRecognizer, willDismissViewController viewController: PannableViewController) {}
    
    func pannableViewController(gesture: UIPanGestureRecognizer, viewController: PannableViewController, dismissed: Bool) {
        if dismissed {
            print("🚨 pannableViewController dismissed")
        }
    }
}

extension MapViewController: ListOfAddressesViewControllerDelegate {
    func listOfAddressesViewController(vc: ListOfAddressesViewController, didSelectAddress address: Address?) {
        searchAddressViewController.dismiss()
        guard let location = address?.location else { return }
        
        DispatchQueue.main.async {
            let cameraPosition = GMSCameraPosition(target: location, zoom: 18, bearing: 0, viewingAngle: 0)
            self.mapView.animate(to: cameraPosition)
            self.mapView.layoutIfNeeded()
        }
    }
}

extension MapViewController: LoaderViewControllerDelegate {
    func loaderViewController(viewController: LoaderViewController, didConfirmAction: Bool) {
        guard let ride = ride else {
            viewController.dismiss()
            return
        }
        self.startLoader()
        MainManager.sharedManager.cancelRide(ignored: false, reason: "", rideId: ride.rideId, comment: nil, successResponse: { (response) in
            self.stopLoader()
            viewController.dismiss()
            self.clearRideData()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    func loaderViewController(viewController: LoaderViewController, didDismiss: Bool) {
        loaderVC = nil
    }
}

extension MapViewController: OptionSelectorViewControllerDelegate {
    func optionSelectorViewController(_ vc: OptionSelectorViewController, didSelect option: OptionToSelect) {}
    
    func optionSelectorViewController(_ vc: OptionSelectorViewController, dismissed: Bool) {
        if dismissed {
            guard let selectedReason = vc.selectedOption else {
                self.showAlert(message: "En caso de rechazo seleccione un motivo por favor")
                return
            }
            if selectedReason.key != "Comentario" {
                cancelRide(reason: selectedReason.key, comment: nil)
            } else {
                InputViewController.instantiante(inViewController: self, inputType: .textView, withTitle: selectedReason.name, placeholder: "Ingrese su comentario", confirmText: "Enviar", validationMessage: "Por favor, ingrese un comentario válido")
            }
        }
    }
}

extension MapViewController: InputViewControllerDelegate {
    func inputViewController(_ viewController: InputViewController, didConfirm: Bool) {
        if didConfirm {
            if viewController.title == "Otros" || viewController.title == "Comentario" {
                cancelRide(reason: "others", comment: viewController.inputText)
            }
        }
    }
}

extension MapViewController: FinishedServiceViewControllerDelegate {
    func pressDismissPaymentDetail() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.clearRideData()
            self.rideState = .none
            self.getUpdatedUser()

        })
    }
}

extension UITextField {
    
    func configureClearButtonColor(colorClear : UIColor ) {

        guard let clearButton = self.value(forKey: "_clearButton") as? UIButton else {
            return
        }
        
        let templateImage = clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        clearButton.setImage(templateImage, for: .normal)
        clearButton.setImage(templateImage, for: .highlighted)
        
        clearButton.tintColor = colorClear
    }

}

extension String {
    
    func toString(_ date : Date) -> String {
        let formato = DateFormatter()
        return formato.string(from: date)
    }
}
