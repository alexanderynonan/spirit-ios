//
//  ClientMenuTabBarViewController.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/29/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit

class ClientMenuTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.items?.forEach { item in
            item.image?.withRenderingMode(.alwaysOriginal)
            item.selectedImage?.withRenderingMode(.alwaysOriginal)
        }
        
        self.selectedIndex = 2
    }
    
    func openModule(_ module: String, userInfo: [AnyHashable: Any]) {
        switch module {
        case MenuBackendModuleKeys.myProfile:
            self.selectedIndex = 0
        case MenuBackendModuleKeys.rides:
            self.selectedIndex = 1
        case MenuBackendModuleKeys.charges:
            self.selectedIndex = 3
        case MenuBackendModuleKeys.help:
            self.selectedIndex = 4
        case MenuBackendModuleKeys.invitations:
            BonusViewController.instantiate(in: self, userInfo: userInfo)
        default:
            self.selectedIndex = 2
        }
    }

}
