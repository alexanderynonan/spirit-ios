//
//  AddFavoritesViewController.swift
//  TaxiSvico-Driver
//
//  Created by Juan Carlos Guillén on 5/14/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import UIKit
import CoreLocation

class AddFavoritesViewController: UIViewController {
    
    @IBOutlet weak var favoritePlacesCollectionView: UICollectionView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameTextFieldContainer: UIView!
    @IBOutlet weak var nameTextFieldContainerHeight: NSLayoutConstraint!
    
    var favoritePlaces = ImageWithTextSwitch.getFavoritePlaces()
    var favoritePlaceCellIdentifier = "ImageFavoriteCollectionViewCell"
    var favoritePlaceName = "" {
        didSet {
            print("NAME: \(favoritePlaceName)")
        }
    }
    var name = ""
    var address = ""
    var coordinate = CLLocationCoordinate2D()
    
    var mapVC: MapViewController?
    
    class func instantiate(in viewController: UIViewController, address: String, name: String, coordinate: CLLocationCoordinate2D) {
        guard let addFavoritesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddFavoritesViewController") as? AddFavoritesViewController else { return }
        addFavoritesVC.name = name
        addFavoritesVC.address = address
        addFavoritesVC.coordinate = coordinate
        addFavoritesVC.mapVC = viewController as? MapViewController
        viewController.navigationController?.pushViewController(addFavoritesVC, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureContent()
        loadContent()
    }
    
    func configureContent() {
        favoritePlacesCollectionView.delegate = self
        favoritePlacesCollectionView.dataSource = self
    }
    
    func loadContent() {
        addressLabel.text = address
        nameTextField.text = ""
    }
    
//    func checkIfDefaultNameIsMarked() {
//        let defaultIsMarked = favoritePlacesCollectionView.visibleCells.contains(where: { ($0 as? ImageWithTextCollectionViewCell)?.imageWithTextSwitchButton.isOn ?? false })
//        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
//            self.nameTextField.text = ""
//            self.nameTextFieldContainerHeight.constant = defaultIsMarked ? 0 : self.view.frame.height * 0.09
//            self.view.layoutIfNeeded()
//        }, completion: nil)
//    }
    
    @IBAction func btnKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func saveFavoriteAddress() {
        
        if self.nameTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            self.showAlert(message: "Debe ingresar un nombre para guardar su dirección favorita")
            return
        }        
        
        self.startLoader()
        ClientManager.sharedManager.saveFavoriteAddress(address: address, name: self.nameTextField.text ?? "", coordinate: coordinate, successResponse: { (address) in
            self.stopLoader()
            self.mapVC?.searchAddressViewController.loadContent()
            self.goBack()
        }) { (error) in
            self.stopLoader()
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    @objc func setFavoritePlaceName(_ textField: UITextField) {
        favoritePlaceName = textField.text ?? ""
    }

}
extension AddFavoritesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favoritePlaces.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "ImageFavoriteCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! ImageFavoriteCollectionViewCell
        cell.imageWithTextSwitch = self.favoritePlaces[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ImageFavoriteCollectionViewCell else { return }
        self.favoritePlaces.forEach({ $0.state = false })
        cell.imageWithTextSwitch.state = true
        self.favoritePlacesCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfElements: CGFloat = CGFloat(self.favoritePlaces.count)
        let ancho = (UIScreen.main.bounds.width - (numberOfElements + 1) * 50) / numberOfElements
        return CGSize(width: ancho, height: 110)
    }
    
}
