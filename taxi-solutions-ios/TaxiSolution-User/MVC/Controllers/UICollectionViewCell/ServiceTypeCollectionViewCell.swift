//
//  ServiceTypeCollectionViewCell.swift
//  Spirit User
//
//  Created by Yonny on 15/12/21.
//  Copyright © 2021 Peru Apps. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var customView: UIView!
    
    var serviceType: ServiceType?
    var isSelect: Bool = false {
        didSet {
            loadContent()
        }
    }
    
    func loadContent() {
        customView.layer.shadowOpacity = isSelect ? 0.5 : 0.1
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        selectionStyle = .none
        guard let serviceType = serviceType else { return }
        nameLabel.text = serviceType.name
        
        photoImageView.sd_setImage(with: serviceType.photoUrl.url, placeholderImage: #imageLiteral(resourceName: "img_service_type"), options: [.refreshCached, .retryFailed], context: nil)
        priceLabel.text = String(format: "PEN %.2f", serviceType.amount)
    }
    
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        contentView.backgroundColor = selected ? Colors.primary.withAlphaComponent(0.15) : .clear
//    }
        
}

