//
//  ClientService.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class ClientService: BaseServiceProtocol {
    static let sharedService = ClientService()
    
    func saveFavoriteAddress(address: String,
                             name: String,
                             coordinate: CLLocationCoordinate2D,
                             successResponse success: @escaping(_ response: JSON) -> Void,
                             failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "address": address,
            "name": name,
            "lat": coordinate.latitude,
            "long": coordinate.longitude
        ] as [String: AnyObject]
        
        POST(toEndpoint: "clients/\(user.id)/favorite-address", params: params, success: success, failure: failure)
    }
    
    func deleteFavoriteAddress(favoriteAddressId: Int,
                               successResponse success: @escaping(_ response: JSON) -> Void,
                               failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        DELETE(toEndpoint: "favorite-address/\(favoriteAddressId)", params: nil, success: success, failure: failure)
    }
    
    func getFavoriteAddresses(successResponse success: @escaping(_ response: JSON) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "clients/\(user.id)/favorite-address", params: nil, success: success, failure: failure)
    }
    
    func getRecentAddresses(successResponse success: @escaping(_ response: JSON) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "clients/\(user.id)/frequent-rides", params: nil, success: success, failure: failure)
    }
    
    func applyPromoCode(code: String,
                        successResponse success: @escaping(_ response: JSON) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "code": code
        ] as [String: AnyObject]
        POST(toEndpoint: "promotions/apply", params: params, success: success, failure: failure)
    }
    
    func getCoupons(successResponse success: @escaping(_ response: JSON) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        GET(toEndpoint: "clients/\(user.id)/promotions", params: nil, success: success, failure: failure)
    }
    
    func getTripRoute(tripRequest: Ride,
                      successResponse success: @escaping(_ response: JSON) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "origin_lat": tripRequest.originLat,
            "origin_long": tripRequest.originLong,
            "destination_lat": tripRequest.destinationLat,
            "destination_long": tripRequest.destinationLong
        ] as [String: AnyObject]
        POST(toEndpoint: "ride/calculate", params: params, success: success, failure: failure)
    }
    
    func findRide(ride: Ride,
                  successResponse success: @escaping(_ response: JSON) -> Void,
                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "service_type_id": ride.serviceTypeId,
            "estimated_time": ride.estimatedTime,
            "estimated_distance": ride.estimatedDistance,
            "origin_address": ride.originAddress,
            "origin_lat": ride.originLat,
            "origin_long": ride.originLong,
            "destination_address": ride.destinationAddress,
            "destination_lat": ride.destinationLat,
            "destination_long": ride.destinationLong,
            "payment_type": ride.paymentType,
            "estimated_amount": ride.estimatedAmount
        ] as [String: AnyObject]
        POST(toEndpoint: "ride/find", params: params, success: success, failure: failure)
    }
    
    func changeRoute(ride: Ride,
                     successResponse success: @escaping(_ response: JSON) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        let params = [
            "destination_address": ride.destinationAddress,
            "destination_lat": ride.destinationLat,
            "destination_long": ride.destinationLong,
        ] as [String: AnyObject]
        POST(toEndpoint: "rides/\(ride.rideId)/change-route", params: params, success: success, failure: failure)
    }
}
//api/v1/rides/idRide
