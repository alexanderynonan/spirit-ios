//
//  ClientManager.swift
//  TaxiSvico-User
//
//  Created by Juan Carlos Guillén on 4/27/20.
//  Copyright © 2020 Peru Apps. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift

class ClientManager {
    
    static let sharedManager = ClientManager()
    let realm = try! Realm()
    
    func saveFavoriteAddress(address: String, name: String,coordinate: CLLocationCoordinate2D, successResponse success: @escaping(_ favoriteAddress: Address) -> Void, failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        
        ClientService.sharedService.saveFavoriteAddress(address: address, name: name, coordinate: coordinate, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let favoriteAddressData = data["favorite"] {
                let favoriteAddress = Address(data: favoriteAddressData, isFavorite: true)
                success(favoriteAddress)
            } else {
                failure(NSError(message: "Error al guardar dirección en favoritos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func deleteFavoriteAddress(favoriteAddressId: Int,
                               successResponse success: @escaping(_ message: String) -> Void,
                               failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.deleteFavoriteAddress(favoriteAddressId: favoriteAddressId, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let status = dictionary["status"]?.stringValue, status == "success" {
                success("La dirección se eliminó satisfactoriamente")
            } else {
                failure(NSError(message: "Error al eliminar dirección de favoritos"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getFavoriteAddresses(successResponse success: @escaping(_ favoriteAddresses: [Address]) -> Void,
                              failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.getFavoriteAddresses(successResponse: { (response) in
            var favoriteAddresses: [Address] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let addressesData = data["favorites"]?.arrayValue {
                addressesData.forEach {
                    let favoriteAddress = Address(data: $0, isFavorite: true)
                    favoriteAddresses.append(favoriteAddress)
                    try! self.realm.write {
                        self.realm.add(favoriteAddress, update: .all)
                    }
                }
                success(favoriteAddresses)
            } else {
                failure(NSError(message: "Error al obtener sus direcciones favoritas"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getRecentAddresses(successresponse success: @escaping(_ recentAddresses: [Address]) -> Void,
                            failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.getRecentAddresses(successResponse: { (response) in
            var recentAddresses: [Address] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let addressesData = data["rides"]?.arrayValue {
                addressesData.forEach {
                    let recentAddress = Address(data: $0)
                    recentAddresses.append(recentAddress)
                }
                success(recentAddresses)
            } else {
                failure(NSError(message: "Error al obtener direcciones recientes"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func applyPromoCode(code: String,
                        successResponse success: @escaping(_ message: String, _ promotionAmount: Double) -> Void,
                        failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.applyPromoCode(code: code, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let message = dictionary["message"]?.stringValue, let promotionAmmount = data["promotion_amount"]?.double {
                success(message, promotionAmmount)
            } else {
                failure(NSError(message: "Error al aplicar el código promocional"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getCoupons(successResponse success: @escaping(_ coupons: [Promotion]) -> Void,
                    failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.getCoupons(successResponse: { (response) in
            //TODO:- Parsear el resultado del servicio
            var coupons: [Promotion] = []
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let couponsData = data["promotions"]?.arrayValue {
                couponsData.forEach {
                    let coupon = Promotion(data: $0)
                    try! self.realm.write {
                        self.realm.add(coupon, update: .all)
                    }
                    coupons.append(coupon)
                }
                success(coupons)
            } else {
                failure(NSError(message: "Error al obtener el listado de cupones"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getTripRoute(tripRequest: Ride,
                      successResponse success: @escaping(_ polyString: String, _ estimatedTime: Int, _ estimatedDistance: Int, _ services: [ServiceType], _ originLong : Double, _ originLat: Double, _ destinationLong : Double, _ destinationLat: Double) -> Void,
                      failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.getTripRoute(tripRequest: tripRequest, successResponse: { (response) in
            var serviceTypes: [ServiceType] = []
            let dictionary = response.dictionaryValue
            
            if
                let data = dictionary["data"]?.dictionaryValue,
                let poliLyne = data["poly_line"]?.stringValue,
                let estimatedTime = data["estimated_time"]?.intValue,
                let estimatedDistance = data["estimated_distance"]?.intValue,
                let servicesData = data["services"]?.arrayValue,
                let originLong = data["origin_long"]?.doubleValue,
                let originLat = data["origin_lat"]?.doubleValue,
                let destinationLong = data["destination_long"]?.doubleValue,
                let destinationLat = data["destination_lat"]?.doubleValue{
                servicesData.forEach {
                    let serviceType = ServiceType($0)
                    serviceTypes.append(serviceType)
                }
                success(poliLyne, estimatedTime, estimatedDistance, serviceTypes, originLong, originLat, destinationLong, destinationLat)
            } else {
                failure(NSError(message: "No se pudo obtener la ruta para su viaje"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func findRide(ride: Ride,
                  successResponse success: @escaping(_ rideId: Int) -> Void,
                  failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.findRide(ride: ride, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"]?.dictionaryValue, let rideId = data["ride_id"]?.intValue {
                success(rideId)
            } else {
                failure(NSError(message: "No se pudo enviar su petición conductor"))
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func changeRoute(ride: Ride,
                     successResponse success: @escaping(_ message: String) -> Void,
                     failureResponse failure: @escaping(_ error: NSError) -> Void) -> Void {
        ClientService.sharedService.changeRoute(ride: ride, successResponse: { (response) in
            let dictionary = response.dictionaryValue
            if let data = dictionary["data"] {
                let destinationAddress = data["destination_address"].stringValue
                success("Cambio de ruta satisfactoria: \(destinationAddress)")
            }
        }) { (error) in
            failure(error)
        }
    }
}
